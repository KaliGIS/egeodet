package sk.gku.egeodet;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.util.Log;

import sk.gku.egeodet.BuildConfig;

import sk.gku.egeodet.configs.Constants;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class eGeodetApplication extends Application {
    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("eGeodetApplication", "onCreate: Setting default uncaught exception handler");
        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                Log.d("eGeodetApplication", "In uncaughtException method");
                handleUncaughtException(thread, e);
            }
        });

        if (!BuildConfig.IS_PRODUCTION) { // Create logfile and write all logcat content to it
            File logPath = new File(getExternalFilesDir(null) + "/" + "log");
            File logFile = new File( logPath, "logcat" + System.currentTimeMillis() + ".log" );
            Log.d("eGeodetApplication", "onCreate: Creating logfile " + logFile.getAbsolutePath());
            // create app folder
            if ( !logPath.exists() ) {
                logPath.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec( "logcat -c");
                // log all
                process = Runtime.getRuntime().exec( "logcat -f " + logFile);

                // log only particular activities
                // process = Runtime.getRuntime().exec( "logcat -f " + logFile + " *:S MyActivity:D MyActivity2:D");
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
}

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void handleUncaughtException (Thread thread, Throwable e)
    {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        Writer writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        String stackTrace = writer.toString();

        Intent intent = new Intent();
        intent.putExtra("stackTrace", stackTrace);
        intent.setAction(Constants.SEND_LOG_INTENT);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity(intent);
    }

    public static String getModel() {
        String model = Build.MODEL;
        if (!model.startsWith(Build.MANUFACTURER))
            model = Build.MANUFACTURER + " " + model;

        return model;
    }

    public static PackageInfo getPackageInfo(Context context) {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
        }

        return info;
    }
}
