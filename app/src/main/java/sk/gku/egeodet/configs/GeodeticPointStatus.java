package sk.gku.egeodet.configs;

public class GeodeticPointStatus {
    private int mId;
    private String mName;
    private Boolean mSelected;

    GeodeticPointStatus(int id, String name) {
        mId = id;
        mName = name;
        mSelected = false;
    }

    public int getId() { return this.mId; };
    public void setId(int id) { mId = id; };

    public String getName() { return this.mName; };
    public void setName(String name) {mName = name; };

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(Boolean selected) {
        this.mSelected = selected;
    }
}
