package sk.gku.egeodet.configs;

public abstract class Constants {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SLOVAK_DATE_TIME_FORMAT = "dd.MM.yyyy HH:mm:ss";
    public static final String MARIA_DB_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    public static final String FILE_SQL_IMPORT = "egeodet_import.sql";
    public static final int BUFFER_SIZE = 2048;
    public static final String GEODETIC_POINT_TYPE_SPS_A = "ŠPS-A";
    public static final String GEODETIC_POINT_TYPE_SPS_B = "ŠPS-B";
    public static final String GEODETIC_POINT_TYPE_SPS_C = "ŠPS-C";
    public static final String GEODETIC_POINT_TYPE_SNS = "ŠNS";
    public static final String GEODETIC_POINT_TYPE_STS = "ŠTS";
    public static final String GEODETIC_POINT_TYPE_SGS = "ŠGS";
    public static final String GEODETIC_POINT_TYPE_BORDER = "HZ";
    public static final String TASK_TYPE_BORDER_STONE = "hraničný znak";
    public static final String TASK_TYPE_PROTECTIVE_ROD = "ochranná tyč";
    public static final String TASK_TYPE_GEODETIC_POINT = "geodetický bod";
    public static final String TASK_TYPE_GP_PROTECTION = "ochrana GB";
    public static final String TASK_TYPE_OTHER = "iné";
    public static final String MANDATORY_TASK_NEW_POINT = "Nový bod";
    public static final String MANDATORY_TASK_GEODETIC_EXAMINATION = "Geodetické meranie";
    public static final int IMAGE_MAX_SIDE_GEODETIC_POINT_TYPE_BORDER = 1024;
    public static final int IMAGE_MAX_SIDE_GEODETIC_POINT_TYPE_OTHER = 800;

    public static final int REQUEST_CODE_NEW_GEODETIC_POINT_TASK_ACTIVITY = 1;
    public static final int RESULT_CODE_CREATE = 2;
    public static final int RESULT_CODE_ADD = 3;
    public static final int REQUEST_CODE_IMPLICIT_FILE_PICKER = 4;
    public static final int REQUEST_CODE_TAKE_PHOTO = 5;
    public static final int REQUEST_CODE_NEW_GEODETIC_POINT_ACTIVITY = 6;
    public static final int REQUEST_CODE_NEW_NOTE_EDIT_ACTIVITY = 7;
    public static final int REQUEST_CODE_EDIT_NOTE_EDIT_ACTIVITY = 8;
    public static final int REQUEST_CODE_RECOVERABLE_SECURITY_EXCEPTION = 8;
    public static final int REQUEST_CODE_SYNCHRONIZATION_ACTIVITY = 9;

    public static final GeodeticPointStatus[] GEODETIC_POINT_STATUSES = {
            new GeodeticPointStatus(-1, "Filter stavu"), // Used as a spinner name
            new GeodeticPointStatus(0, "Nezačatý"),
            new GeodeticPointStatus(1, "Rozpracovaný"),
            new GeodeticPointStatus(2, "Ukončený")
    };

    public static final String[] GEODETIC_POINT_TYPES = {
            "Filter typu", // Used as a spinner name
            GEODETIC_POINT_TYPE_SPS_A,
            GEODETIC_POINT_TYPE_SPS_B,
            GEODETIC_POINT_TYPE_SPS_C,
            GEODETIC_POINT_TYPE_SNS,
            GEODETIC_POINT_TYPE_STS,
            GEODETIC_POINT_TYPE_SGS,
            GEODETIC_POINT_TYPE_BORDER
    };

    public static final GeodeticPointTypeTaskType[] GEODETIC_POINT_TYPE_TASK_TYPES = {
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_A, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_A, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_A, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_B, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_B, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_B, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_C, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_C, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SPS_C, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SNS, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SNS, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SNS, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_STS, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_STS, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_STS, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SGS, TASK_TYPE_GEODETIC_POINT),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SGS, TASK_TYPE_GP_PROTECTION),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_SGS, TASK_TYPE_OTHER),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_BORDER, TASK_TYPE_BORDER_STONE),
            new GeodeticPointTypeTaskType(GEODETIC_POINT_TYPE_BORDER, TASK_TYPE_PROTECTIVE_ROD),
    };

    public static final String[] MANDATORY_TASKS = {
            MANDATORY_TASK_GEODETIC_EXAMINATION,
            MANDATORY_TASK_NEW_POINT
    };

    public static final String RESTART_APPLICATION_INTENT = "restart_application_intent";
    public static final String GEODETIC_POINT_STATUS_INTENT = "geodetic_point_status_intent";
    public static final String GEODETIC_POINT_TYPE_INTENT = "geodetic_point_type_intent";
    public static final String SELECTED_TASK_INTENT = "selected_task_intent";
    public static final String GEODETIC_POINT_TASK_INTENT = "geodetic_point_task_intent";
    public static final String INSERT_PHOTOS_TO_DB_INTENT = "insert_photos_to_db_intent";
    public static final String SEND_LOG_INTENT = "send_log_intent";
    public static final String SYNCHRONIZATION_INTENT = "synchronization_intent";

    public static final int PERMISSION_REQUEST_READ_STORAGE = 1000;
    public static final int PERMISSION_REQUEST_WRITE_STORAGE = 2000;
    public static final int PERMISSION_REQUEST_SYSTEM_ALERT_WINDOW = 3000;

    public static final String GEODETIC_EXAMINATION_TASK_NAME = "geodetické meranie";

    public static final int INSERT_PHOTOS_TO_DB_NOTIFICATION_ID = 100;
    public static final int RESTART_APPLICATION_ACTIVITY_ID = 101;
    public static final int SYNCHRONIZATION_NOTIFICATION_ID = 102;

    public static final String PRIMARY_NOTIFICATION_CHANNEL_ID = "primary_notification_channel";

    public static final String GEODETIC_POINT_MODEL = "GeodeticPoint";
    public static final String GEODETIC_POINT_MISSION_MODEL = "GeodeticPointMission";
    public static final String GEODETIC_POINT_TASK_MODEL = "GeodeticPointTask";
    public static final String NOTE_MODEL = "Note";
    public static final String IMAGE_MODEL = "Image";
    public static final String SYNCHRONIZATION_MODEL = "Synchronization";
    public static final String USER_MODEL = "User";
    public static final String USER_GROUP_MODEL = "UserGroup";
    public static final String TASK_MODEL = "Task";

    public static final Integer SYNC_STATUS_UPLOADING = 1;
    public static final Integer SYNC_STATUS_DOWNLOADING = 2;
    public static final Integer SYNC_STATUS_DONE = 0;
    public static final Integer SYNC_STATUS_DONE_WITH_ERROR = 3;

    public static final String USER_ROLE_ADMIN = "admin";
    public static final String EPOCH_START_DATE_TIME = "1970-01-01 00:00:00";

    public static final String SHARED_PREF_CONFLICTS_KEY = "conflictMixes";

    public enum SynchronizationDirection {
        UPLOAD,
        DOWNLOAD
    }

    public static final String[][] TASK_NAMES_GROUPS = {
            {"Nový bod", "Zachovalý", "Zničený", "Nenájdený"},
            {"Narovnanie mierne nakloneného HZ", "Odkopanie hraničného znaku", "Dosypanie hraničného znaku"}
    };
}
