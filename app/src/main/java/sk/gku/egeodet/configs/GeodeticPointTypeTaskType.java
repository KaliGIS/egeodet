package sk.gku.egeodet.configs;

public class GeodeticPointTypeTaskType {
    private String mGeodeticPointType;
    private String mTaskType;

    public GeodeticPointTypeTaskType(String geodeticPointType, String taskType) {
        mGeodeticPointType = geodeticPointType;
        mTaskType = taskType;
    }

    public String getGeodeticPointType() { return mGeodeticPointType; };
    public void setGeodeticPointType(String geodeticPointType) { mGeodeticPointType = geodeticPointType; };

    public String getTaskType() { return mTaskType; };
    public void setTaskType(String taskType) { mTaskType = taskType; };
}
