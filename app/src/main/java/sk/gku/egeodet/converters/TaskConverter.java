package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaskConverter {
    public Task fromJSONObject(JSONObject taskObj) throws JSONException {
        JSONObject createdObj = new JSONObject();
        if (taskObj.has("created") && !taskObj.getString("created").equals("null")) {
            createdObj = taskObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (taskObj.has("modified") && !taskObj.getString("modified").equals("null")) {
            modifiedObj = taskObj.getJSONObject("modified");
        }

        // int id, String name, String type, Date created, Date modified
        Task task = new Task(
                taskObj.getInt("id"),
                taskObj.has("name") ? taskObj.getString("name") : null,
                taskObj.has("type") ? taskObj.getString("type") : null,
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return task;
    }

    public List<Task> fromJSONArray(JSONArray tasksArr) throws JSONException {
        List<Task> tasks = new ArrayList<>();

        for (int i = 0; i < tasksArr.length(); i++) {
            JSONObject taskObj = (JSONObject) tasksArr.get(i);

            Task task = fromJSONObject(taskObj);
            tasks.add(task);
        }

        return tasks;
    }
}
