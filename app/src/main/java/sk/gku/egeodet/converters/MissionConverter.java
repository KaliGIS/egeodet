package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MissionConverter {
    public List<Mission> fromJSONArray(JSONArray missionsArr) throws JSONException {
        List<Mission> missions = new ArrayList<>();

        for (int i = 0; i < missionsArr.length(); i++) {
            JSONObject missionObj = (JSONObject) missionsArr.get(i);
            Mission mission = fromJSONObject(missionObj);

            missions.add(mission);
        }

        return missions;
    }

    public Mission fromJSONObject(JSONObject missionObj) throws JSONException {
        JSONObject createdObj = missionObj.getJSONObject("created");
        JSONObject modifiedObj = missionObj.getJSONObject("modified");
        Integer groupId = null;
        if (missionObj.has("groupId") && !missionObj.getString("groupId").equals("null")) {
            groupId = missionObj.getInt("groupId");
        }

        // int id, String name, int year, Integer groupId, Date created, java.util.Date modified
        Mission mission = new Mission(
                missionObj.getInt("id"),
                missionObj.has("name") ? missionObj.getString("name") : null,
                missionObj.has("year") ? missionObj.getInt("year") : 0,
                groupId,
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return mission;
    }
}
