package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.User;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserConverter {
    public List<User> fromJSONArray(JSONArray usersArr) throws JSONException {
        List<User> users = new ArrayList<>();

        for (int i = 0; i < usersArr.length(); i++) {
            JSONObject userObj = (JSONObject) usersArr.get(i);
            User user = fromJSONObject(userObj);
            users.add(user);
        }

        return users;
    }

    public User fromJSONObject(JSONObject userObj) throws JSONException {
        JSONObject createdObj = userObj.getJSONObject("created");
        JSONObject modifiedObj = userObj.getJSONObject("modified");

        User user = new User(
                userObj.getInt("id"),
                userObj.getString("name"),
                userObj.getString("role"),
                userObj.has("email") ? userObj.getString("email") : "",
                userObj.has("password") ? userObj.getString("password") : "",
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );
        return user;
    }
}
