package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GroupConverter {
    public List<Group> fromJSONArray(JSONArray groupsArr) throws JSONException {
        List<Group> groups = new ArrayList<>();

        for (int i = 0; i <groupsArr.length(); i++) {
            JSONObject groupObj = (JSONObject) groupsArr.get(i);
            Group group = fromJSONObject(groupObj);
            groups.add(group);
        }

        return groups;
    }

    public Group fromJSONObject(JSONObject groupObj) throws JSONException {
        JSONObject createdObj = groupObj.getJSONObject("created");
        JSONObject modifiedObj = groupObj.getJSONObject("modified");

        // int id, String name, Date created, Date modified
        Group group = new Group(
                groupObj.getInt("id"),
                groupObj.getString("name"),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );
        return group;
    }
}
