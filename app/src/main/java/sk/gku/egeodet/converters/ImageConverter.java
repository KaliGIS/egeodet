package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ImageConverter {
    public List<Image> fromJSONArray(JSONArray imagesArr) {
        List<Image> images = new ArrayList<>();

        for (int i = 0; i <imagesArr.length(); i++) {
            JSONObject imageObj = null;
            try {
                imageObj = (JSONObject) imagesArr.get(i);
                Image image = fromJSONObject(imageObj);
                images.add(image);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return images;
    }

    public Image fromJSONObject(JSONObject imageObj) throws JSONException {
        JSONObject createdObj = new JSONObject();
        if (imageObj.has("created") && !imageObj.getString("created").equals("null")) {
            createdObj = imageObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (imageObj.has("modified") && !imageObj.getString("modified").equals("null")) {
            modifiedObj = imageObj.getJSONObject("modified");
        }

        // int id, String name, String path, int geodeticPointId, int missionId, Date created, Date modified
        Image image = new Image(
                imageObj.getInt("id"),
                imageObj.has("name") ? imageObj.getString("name") : null,
                imageObj.has("path") ? imageObj.getString("path") : null,
                imageObj.getInt("geodeticPointId"),
                imageObj.getInt("missionId"),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return image;
    }
}
