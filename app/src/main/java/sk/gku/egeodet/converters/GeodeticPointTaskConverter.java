package sk.gku.egeodet.converters;

import android.util.ArrayMap;

import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GeodeticPointTaskConverter {
    private MissionConverter mMissionConverter;
    private GeodeticPointConverter mGeodeticPointConverter;
    private TaskConverter mTaskConverter;

    public GeodeticPointTaskConverter() {
        mMissionConverter = new MissionConverter();
        mGeodeticPointConverter = new GeodeticPointConverter();
        mTaskConverter = new TaskConverter();
    }

    public Map<String, List> fromJSONArray(JSONArray geodeticPointTasksArr) throws JSONException {
        List<GeodeticPointTask> geodeticPointTasks = new ArrayList<>();
        List<GeodeticPoint> geodeticPoints = new ArrayList<>();
        List<Mission> missions = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        for (int i = 0; i < geodeticPointTasksArr.length(); i++) {
            JSONObject geodeticPointTaskObj = (JSONObject) geodeticPointTasksArr.get(i);
            GeodeticPointTask geodeticPointTask = fromJSONObject(geodeticPointTaskObj);
            geodeticPointTasks.add(geodeticPointTask);

            JSONObject geodeticPointObj = geodeticPointTaskObj.getJSONObject("geodeticPoint");
            GeodeticPoint geodeticPoint = mGeodeticPointConverter.fromJSONObject(geodeticPointObj);
            geodeticPoints.add(geodeticPoint);

            JSONObject missionObj = geodeticPointTaskObj.getJSONObject("mission");
            Mission mission = mMissionConverter.fromJSONObject(missionObj);
            missions.add(mission);

            JSONObject taskObj = geodeticPointTaskObj.getJSONObject("task");
            Task task = mTaskConverter.fromJSONObject(taskObj);
            tasks.add(task);
        }

        Map<String, List> geodeticPointTasksMap = new ArrayMap<>();
        geodeticPointTasksMap.put("geodeticPointTasks", geodeticPointTasks);
        geodeticPointTasksMap.put("geodeticPoints", geodeticPoints);
        geodeticPointTasksMap.put("missions", missions);
        geodeticPointTasksMap.put("tasks", tasks);
        return geodeticPointTasksMap;
    }

    public JoinGeodeticPointTaskTask fromJSONObject(JSONObject geodeticPointTaskObj) throws JSONException {
        JSONObject doneDateObj = new JSONObject();
        if (geodeticPointTaskObj.has("doneDate") && !geodeticPointTaskObj.getString("doneDate").equals("null")) {
            doneDateObj = geodeticPointTaskObj.getJSONObject("doneDate");
        }

        JSONObject createdObj = new JSONObject();
        if (geodeticPointTaskObj.has("created") && !geodeticPointTaskObj.getString("created").equals("null")) {
            createdObj = geodeticPointTaskObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (geodeticPointTaskObj.has("modified") && !geodeticPointTaskObj.getString("modified").equals("null")) {
            modifiedObj = geodeticPointTaskObj.getJSONObject("modified");
        }

        boolean isMandatory = false;
        if (geodeticPointTaskObj.has("isMandatory")) {
            isMandatory = geodeticPointTaskObj.getInt("isMandatory") > 0;
        }

        boolean isDone = false;
        if (geodeticPointTaskObj.has("isDone")) {
            isDone = geodeticPointTaskObj.getInt("isDone") > 0;
        }

        Integer userId = null;
        if (geodeticPointTaskObj.has("userId") && !geodeticPointTaskObj.getString("userId").equals("null")) {
            userId = geodeticPointTaskObj.getInt("userId");
        }

        Integer groupId = null;
        if (geodeticPointTaskObj.has("groupId") && !geodeticPointTaskObj.getString("groupId").equals("null")) {
            groupId = geodeticPointTaskObj.getInt("groupId");
        }

        String taskName = null;
        String taskType = null;
        if (geodeticPointTaskObj.has("task") && !geodeticPointTaskObj.getString("task").equals("null")) {
            JSONObject taskObj = geodeticPointTaskObj.getJSONObject("task");
            if (taskObj.has("name")) {
                taskName = taskObj.getString("name");
            }
            if (taskObj.has("type")) {
                taskName = taskObj.getString("name");
            }
        }

        if (geodeticPointTaskObj.has("task") && !geodeticPointTaskObj.getString("task").equals("null")) {
            JSONObject taskObj = geodeticPointTaskObj.getJSONObject("task");
            if (taskObj.has("name")) {
                taskName = taskObj.getString("name");
            }
        }

        String geodeticPointLabel = null;
        if (geodeticPointTaskObj.has("geodeticPoint") && !geodeticPointTaskObj.getString("geodeticPoint").equals("null")) {
            JSONObject geodeticPointObj = geodeticPointTaskObj.getJSONObject("geodeticPoint");
            if (geodeticPointObj.has("label")) {
                geodeticPointLabel = geodeticPointObj.getString("label");
            }
        }


        // int id, int missionId, int geodeticPointId, int taskId, Integer userId, Integer groupId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified, String taskName, String taskType, String userName, String geodeticPointLabel
        JoinGeodeticPointTaskTask geodeticPointTask = new JoinGeodeticPointTaskTask(
                geodeticPointTaskObj.getInt("id"),
                geodeticPointTaskObj.getInt("missionId"),
                geodeticPointTaskObj.getInt("geodeticPointId"),
                geodeticPointTaskObj.getInt("taskId"),
                userId,
                groupId,
                isMandatory,
                isDone,
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(doneDateObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj),
                taskName,
                taskType,
                null,
                geodeticPointLabel
        );

        return geodeticPointTask;
    }
}
