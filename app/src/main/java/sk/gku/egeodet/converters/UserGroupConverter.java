package sk.gku.egeodet.converters;

import android.util.ArrayMap;

import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserGroupConverter {
    private UserConverter mUserConverter;
    private GroupConverter mGroupConverter;

    public UserGroupConverter() {
        mUserConverter = new UserConverter();
        mGroupConverter = new GroupConverter();
    }

    public Map<String, List> fromJSONArray(JSONArray userGroupsArr) throws JSONException {
        List<UserGroup> userGroups = new ArrayList<>();
        List<User> users = new ArrayList<>();
        List<Group> groups = new ArrayList<>();

        for (int i = 0; i < userGroupsArr.length(); i++) {
            JSONObject userGroupObj = (JSONObject) userGroupsArr.get(i);
            UserGroup userGroup = fromJSONObject(userGroupObj);
            userGroups.add(userGroup);

            JSONObject userObj = userGroupObj.getJSONObject("user");
            User user = mUserConverter.fromJSONObject(userObj);
            users.add(user);

            JSONObject groupObj = userGroupObj.getJSONObject("group");
            Group group = mGroupConverter.fromJSONObject(groupObj);
            groups.add(group);
        }

        Map<String, List> userGroupsMap = new ArrayMap<>();
        userGroupsMap.put("userGroups", userGroups);
        userGroupsMap.put("users", users);
        userGroupsMap.put("groups", groups);
        return userGroupsMap;
    }

    public UserGroup fromJSONObject(JSONObject userGroupObj) throws JSONException {
        JSONObject createdObj = new JSONObject();
        if (userGroupObj.has("created") && !userGroupObj.getString("created").equals("null")) {
            createdObj = userGroupObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (userGroupObj.has("modified") && !userGroupObj.getString("modified").equals("null")) {
            modifiedObj = userGroupObj.getJSONObject("modified");
        }

        // int id, int userId, int groupId, Date created, Date modified
        UserGroup userGroup = new UserGroup(
                userGroupObj.getInt("id"),
                userGroupObj.getInt("userId"),
                userGroupObj.getInt("groupId"),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return userGroup;
    }
}
