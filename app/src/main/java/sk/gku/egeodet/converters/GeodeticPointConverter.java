package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONException;
import org.json.JSONObject;

public class GeodeticPointConverter {
    public GeodeticPoint fromJSONObject(JSONObject geodeticPointObj) throws JSONException {
        JSONObject lastMaintenanceObj = new JSONObject();
        if (geodeticPointObj.has("lastMaintenance") && !geodeticPointObj.getString("lastMaintenance").equals("null")) {
            lastMaintenanceObj = geodeticPointObj.getJSONObject("lastMaintenance");
        }

        JSONObject createdObj = new JSONObject();
        if (geodeticPointObj.has("created") && !geodeticPointObj.getString("created").equals("null")) {
            createdObj = geodeticPointObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (geodeticPointObj.has("modified") && !geodeticPointObj.getString("modified").equals("null")) {
            modifiedObj = geodeticPointObj.getJSONObject("modified");
        }

        // int id, String label, String type, Date lastMaintenance, Date created, Date modified
        GeodeticPoint geodeticPoint = new GeodeticPoint(
                geodeticPointObj.getInt("id"),
                geodeticPointObj.has("label") ? geodeticPointObj.getString("label") : null,
                geodeticPointObj.has("type") ? geodeticPointObj.getString("type") : null,
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(lastMaintenanceObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return geodeticPoint;
    }
}
