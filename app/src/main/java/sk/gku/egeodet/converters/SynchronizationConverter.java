package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SynchronizationConverter {
    public List<Synchronization> fromJSONArray(JSONArray synchronizationsArr) throws JSONException {
        List<Synchronization> synchronizations = new ArrayList<>();

        for (int i = 0; i < synchronizationsArr.length(); i++) {
            JSONObject synchronizationObj = (JSONObject) synchronizationsArr.get(i);
            JSONObject startDateObj = synchronizationObj.getJSONObject("start_date");
            JSONObject endDateObj = synchronizationObj.getJSONObject("end_date");

            Synchronization synchronization = new Synchronization(
                    synchronizationObj.getInt("id"),
                    synchronizationObj.getString("sync_key"),
                    synchronizationObj.getInt("user_id"),
                    DatetimeTypeConverters.getDateTimeFromMariaDbJson(startDateObj),
                    DatetimeTypeConverters.getDateTimeFromMariaDbJson(endDateObj),
                    synchronizationObj.getInt("status")
            );

            synchronizations.add(synchronization);
        }

        return synchronizations;
    }

    public Synchronization fromJSONObject(JSONObject synchronizationObj) throws JSONException {
        JSONObject startDateObj = new JSONObject();
        JSONObject endDateObj = new JSONObject();
        if (synchronizationObj.has("start_date") && !synchronizationObj.isNull("start_date")) {
            startDateObj = synchronizationObj.getJSONObject("start_date");
        }
        if (synchronizationObj.has("end_date") && !synchronizationObj.isNull("end_date")) {
            endDateObj = synchronizationObj.getJSONObject("end_date");
        }

        Synchronization synchronization = new Synchronization(
                synchronizationObj.getInt("id"),
                synchronizationObj.getString("sync_key"),
                synchronizationObj.getInt("user_id"),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(startDateObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(endDateObj),
                synchronizationObj.getInt("status")
        );

        return synchronization;
    }
}
