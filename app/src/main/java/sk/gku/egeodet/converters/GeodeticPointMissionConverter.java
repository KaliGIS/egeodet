package sk.gku.egeodet.converters;

import android.util.ArrayMap;
import android.util.Log;

import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GeodeticPointMissionConverter {
    private MissionConverter mMissionConverter;
    private GeodeticPointConverter mGeodeticPointConverter;

    public GeodeticPointMissionConverter() {
        mMissionConverter = new MissionConverter();
        mGeodeticPointConverter = new GeodeticPointConverter();
    }

    public Map<String, List> fromJSONArray(JSONArray geodeticPointMissionsArr) throws JSONException {
        List<GeodeticPointMission> geodeticPointMissions = new ArrayList<>();
        List<GeodeticPoint> geodeticPoints = new ArrayList<>();
        List<Mission> missions = new ArrayList<>();

        for (int i = 0; i < geodeticPointMissionsArr.length(); i++) {
            JSONObject geodeticPointMissionObj = (JSONObject) geodeticPointMissionsArr.get(i);
            GeodeticPointMission geodeticPointMission = fromJSONObject(geodeticPointMissionObj);
            geodeticPointMissions.add(geodeticPointMission);

            JSONObject geodeticPointObj = geodeticPointMissionObj.getJSONObject("geodeticPoint");
            GeodeticPoint geodeticPoint = mGeodeticPointConverter.fromJSONObject(geodeticPointObj);
            geodeticPoints.add(geodeticPoint);

            JSONObject missionObj = geodeticPointMissionObj.getJSONObject("mission");
            Mission mission = mMissionConverter.fromJSONObject(missionObj);
            missions.add(mission);
        }

        Map<String, List> geodeticPointMissionsMap = new ArrayMap<>();
        geodeticPointMissionsMap.put("geodeticPointMissions", geodeticPointMissions);
        geodeticPointMissionsMap.put("geodeticPoints", geodeticPoints);
        geodeticPointMissionsMap.put("missions", missions);
        return geodeticPointMissionsMap;
    }

    public GeodeticPointMission fromJSONObject(JSONObject geodeticPointMissionObj) throws JSONException {
        JSONObject createdObj = new JSONObject();
        if (geodeticPointMissionObj.has("created") && !geodeticPointMissionObj.getString("created").equals("null")) {
            createdObj = geodeticPointMissionObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (geodeticPointMissionObj.has("modified") && !geodeticPointMissionObj.getString("modified").equals("null")) {
            modifiedObj = geodeticPointMissionObj.getJSONObject("modified");
        }

        Integer userId = null;
        if (geodeticPointMissionObj.has("userId") && !geodeticPointMissionObj.getString("userId").equals("null")) {
            userId = geodeticPointMissionObj.getInt("userId");
        }

        Integer groupId = null;
        if (geodeticPointMissionObj.has("groupId") && !geodeticPointMissionObj.getString("groupId").equals("null")) {
            groupId = geodeticPointMissionObj.getInt("groupId");
        }

        // int id, int missionId, int geodeticPointId, Integer userId, Integer status, Date created, Date modified
        GeodeticPointMission geodeticPointMission = new GeodeticPointMission(
                geodeticPointMissionObj.getInt("id"),
                geodeticPointMissionObj.getInt("missionId"),
                geodeticPointMissionObj.getInt("geodeticPointId"),
                userId,
                groupId,
                geodeticPointMissionObj.has("status") ? geodeticPointMissionObj.getInt("status") : null,
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );
        Log.d("GPMisionConverter", "fromJSONObject: incoming geodetic point mission id: " + geodeticPointMissionObj.getInt("id"));
        Log.d("GPMisionConverter", "fromJSONObject: new geodetic point mission object id: " + geodeticPointMission.getId());

        return geodeticPointMission;
    }
}
