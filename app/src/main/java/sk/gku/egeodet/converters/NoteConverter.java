package sk.gku.egeodet.converters;

import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NoteConverter {
    public Note fromJSONObject(JSONObject noteObj) throws JSONException {
        JSONObject createdObj = new JSONObject();
        if (noteObj.has("created") && !noteObj.getString("created").equals("null")) {
            createdObj = noteObj.getJSONObject("created");
        }

        JSONObject modifiedObj = new JSONObject();
        if (noteObj.has("modified") && !noteObj.getString("modified").equals("null")) {
            modifiedObj = noteObj.getJSONObject("modified");
        }

        // int id, String note, int userId,  int geodeticPointId, int missionId, Date created, Date modified
        Note note = new Note(
                noteObj.getInt("id"),
                noteObj.has("note") ? noteObj.getString("note") : null,
                noteObj.getInt("userId"),
                noteObj.getInt("geodeticPointId"),
                noteObj.getInt("missionId"),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(createdObj),
                DatetimeTypeConverters.getDateTimeFromMariaDbJson(modifiedObj)
        );

        return note;
    }

    public List<Note> fromJSONArray(JSONArray notesArr) throws JSONException {
        List<Note> notes = new ArrayList<>();

        for (int i = 0; i < notesArr.length(); i++) {
            JSONObject noteObj = (JSONObject) notesArr.get(i);

            Note note = fromJSONObject(noteObj);
            notes.add(note);
        }

        return notes;
    }
}
