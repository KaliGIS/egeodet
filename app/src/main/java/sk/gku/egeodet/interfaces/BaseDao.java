package sk.gku.egeodet.interfaces;

import androidx.room.Dao;
import androidx.room.RawQuery;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.lang.reflect.ParameterizedType;

@Dao
public abstract class BaseDao<T> {
    public void updateId(Integer oldId, Integer newId) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                "UPDATE " + getTableName() + " SET id = " + newId + ", modified = datetime() WHERE id = " + oldId
        );
        doUpdateId(query);
    }

    @RawQuery
    protected abstract Integer doUpdateId(SupportSQLiteQuery query);

    public void absIds() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                "UPDATE " + getTableName() + " SET id = ABS(id) WHERE id < 0"
        );
        doAbsIds(query);
    }

    @RawQuery
    protected abstract Integer doAbsIds(SupportSQLiteQuery query);

    public String getTableName() {
        Class theClass = (Class)
                ((ParameterizedType) getClass().getSuperclass().getGenericSuperclass())
                        .getActualTypeArguments()[0];
        String className = theClass.getSimpleName();
        String tableName = camelCaseToSnakeCase(className) + "s"; // make plural form
        return tableName;
    }

    private String camelCaseToSnakeCase(String str)
    {
        String result = "";

        // Append first character(in lower case)
        // to result string
        char c = str.charAt(0);
        result += Character.toLowerCase(c);

        // Tarverse the string from
        // ist index to last index
        for (int i = 1; i < str.length(); i++) {

            char ch = str.charAt(i);

            // Check if the character is upper case
            // then append '_' and such character
            // (in lower case) to result string
            if (Character.isUpperCase(ch)) {
                result +=  '_';
                result += Character.toLowerCase(ch);
            }

            // If the character is lower case then
            // add such character into result string
            else {
                result = result + ch;
            }
        }

        // return the result
        return result;
    }
}
