package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.Synchronization;

import java.util.List;

@Dao
public interface SynchronizationDao {
    @Query("SELECT * FROM synchronizations")
    LiveData<List<Synchronization>> getSynchronizations();

    @Query("SELECT * FROM synchronizations WHERE sync_key = :syncKey")
    Synchronization[] getSynchronizationBySyncKey(String syncKey);

    @Update
    Integer update(Synchronization synchronization);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Synchronization synchronization);

    @Query("DELETE FROM synchronizations WHERE id IN (:ids)")
    void deleteByIds(Integer... ids);

    @Delete
    Integer delete(Synchronization synchronization);
}
