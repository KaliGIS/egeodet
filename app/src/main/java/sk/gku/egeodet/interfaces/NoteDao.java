package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.JoinNoteUser;
import sk.gku.egeodet.models.Note;

import java.util.List;

@Dao
public abstract class NoteDao extends BaseDao<Note> {
    @Query("SELECT * FROM notes")
    public abstract LiveData<List<Note>> getNotes();

    @Query("SELECT * FROM notes WHERE id IN (:noteIds)")
    public abstract List<Note> getNotesByIds(Integer... noteIds);

    @Query("SELECT * FROM notes WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract List<Note> getNotesByGeodeticPointIdAndMissionId(Integer geodeticPointId, Integer missionId);

    @Query("SELECT * FROM notes WHERE geodetic_point_id = :geodeticPointId")
    public abstract List<Note> getNotesByGeoPntId(int geodeticPointId);

    @Query("SELECT notes.id as mNoteId, notes.note AS mNote, users.id AS mUserId, users.name AS mUserName, users.role AS mUserRole, notes.created AS mCreated, notes.modified AS mModified FROM notes LEFT JOIN users ON users.id = notes.user_id " +
            "WHERE notes.geodetic_point_id = :geodeticPointId AND notes.mission_id = :missionId")
    public abstract LiveData<List<JoinNoteUser>> getNotesWithUsersByMissionIdAndGeodeticPointId(Integer missionId, Integer geodeticPointId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Note note);

    @Query("UPDATE notes SET note = :noteText, modified = datetime() WHERE id = :id")
    public abstract void update(int id, String noteText);

    @Update
    public abstract Integer update(Note note);

    @Delete
    public abstract void delete(Note note);

    @Query("DELETE FROM notes WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

    @Query("DELETE FROM notes")
    public abstract void deleteAll();
}
