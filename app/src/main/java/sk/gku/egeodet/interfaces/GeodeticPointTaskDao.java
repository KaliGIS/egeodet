package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.TaskType;

import java.util.Date;
import java.util.List;

@Dao
public abstract class GeodeticPointTaskDao extends BaseDao<GeodeticPointTask> {
    @Query("SELECT * FROM geodetic_point_tasks")
    public abstract LiveData<List<GeodeticPointTask>> getGeodeticPointTasks();

    @Query("SELECT * FROM geodetic_point_tasks WHERE id IN (:geodeticPointTaskIds)")
    public abstract List<GeodeticPointTask> getGeodeticPointTasksByIds(Integer... geodeticPointTaskIds);

    @Query("SELECT COUNT(*) FROM geodetic_point_tasks WHERE mission_id = :missionId AND geodetic_point_id = :geodeticPointId AND task_id = :taskId")
    public abstract Integer getCountOfRecords(Integer missionId, Integer geodeticPointId, Integer taskId);

    @Query("SELECT gpt.*,  t.name AS mTaskName, t.type AS mTaskType, u.name as mUserName " +
            "FROM geodetic_point_tasks AS gpt LEFT JOIN tasks AS t ON gpt.task_id = t.id LEFT JOIN users AS u ON gpt.user_id = u.id " +
            "WHERE gpt.geodetic_point_id = :geodeticPointId AND gpt.mission_id = :missionId AND lower(t.type) IN (:taskTypes) ORDER BY gpt.id ASC")
    public abstract LiveData<List<JoinGeodeticPointTaskTask>> getGPTasksWithTasksByMissionIdAndGPIdAndTaskType(Integer missionId, Integer geodeticPointId, String[] taskTypes);

//    @Query("SELECT gpt.id AS mId, gpt.mission_id AS mMissionId, gpt.geodetic_point_id AS mGeodeticPointId, gpt.task_id AS mTaskId, gpt.user_id AS mUserId, gpt.is_mandatory AS mIsMandatory, gpt.is_done AS mIsDone, gpt.done_date AS mDoneDate, gpt.created AS mCreated, gpt.modified AS mModified, t.name AS mTaskName, t.type AS mTaskType, u.name as mUserName " +
//            "FROM geodetic_point_tasks AS gpt LEFT JOIN tasks AS t ON gpt.task_id = t.id LEFT JOIN users AS u ON gpt.user_id = u.id " +
//            "WHERE gpt.geodetic_point_id = :geodeticPointId AND gpt.mission_id = :missionId AND lower(t.type) IN (:taskTypes) ORDER BY gpt.id ASC")
//    LiveData<List<JoinGeodeticPointTaskTask>> getGPTasksWithTasksByMissionIdAndGPIdAndTaskType(Integer missionId, Integer geodeticPointId, String[] taskTypes);

    @Query("SELECT gpt.*, t.name AS mTaskName, t.type AS mTaskType " +
            "FROM geodetic_point_tasks AS gpt LEFT JOIN tasks AS t ON gpt.task_id = t.id LEFT JOIN users AS u ON gpt.user_id = u.id " +
            "WHERE gpt.geodetic_point_id = :geodeticPointId AND gpt.mission_id = :missionId ORDER BY gpt.id ASC")
    public abstract List<JoinGeodeticPointTaskTask> getGPTasksWithTasksByMissionIdAndGPId(Integer missionId, Integer geodeticPointId);
//    @Query("SELECT gpt.id AS mId, gpt.mission_id AS mMissionId, gpt.geodetic_point_id AS mGeodeticPointId, gpt.task_id AS mTaskId, gpt.user_id AS mUserId, gpt.is_mandatory AS mIsMandatory, gpt.is_done AS mIsDone, gpt.done_date AS mDoneDate, gpt.created AS mCreated, gpt.modified AS mModified, t.name AS mTaskName, t.type AS mTaskType " +
//            "FROM geodetic_point_tasks AS gpt LEFT JOIN tasks AS t ON gpt.task_id = t.id LEFT JOIN users AS u ON gpt.user_id = u.id " +
//            "WHERE gpt.geodetic_point_id = :geodeticPointId AND gpt.mission_id = :missionId ORDER BY gpt.id ASC")
//    List<JoinGeodeticPointTaskTask> getGPTasksWithTasksByMissionIdAndGPId(Integer missionId, Integer geodeticPointId);

    @Query("SELECT DISTINCT t.type AS mTaskType " +
            "FROM geodetic_point_tasks AS gpt LEFT JOIN tasks AS t ON gpt.task_id = t.id " +
            "WHERE gpt.geodetic_point_id = :geodeticPointId AND gpt.mission_id = :missionId")
    public abstract LiveData<List<TaskType>> getDistinctTaskTypesByMissionIdAndGeoPntId(Integer missionId, Integer geodeticPointId);

    @Query("SELECT * FROM geodetic_point_tasks WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId " +
            "AND task_id IN (:taskIds)")
    public abstract List<GeodeticPointTask> getGeodeticPointTasksByGeoPntIdAndMissionIdAndTaskIds(int geodeticPointId, int missionId, List<Integer> taskIds);

    @Query("SELECT * FROM geodetic_point_tasks WHERE geodetic_point_id = :geodeticPointId")
    public abstract List<GeodeticPointTask> getGeodeticPointTasksByGeoPntId(int geodeticPointId);

    @Query("UPDATE geodetic_point_tasks SET is_done = :isChecked, modified = datetime(), done_date = :doneDate WHERE id = :geodeticPointTaskId")
    public abstract void updateIsDone(int geodeticPointTaskId, boolean isChecked, Date doneDate);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(GeodeticPointTask geodeticPointTask);

    @Update
    public abstract Integer update(GeodeticPointTask geodeticPointTask);

    @Query("DELETE FROM geodetic_point_tasks WHERE id = :geodeticPointTaskId")
    public abstract void delete(int geodeticPointTaskId);

    @Query("DELETE FROM geodetic_point_tasks WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

    @Query("DELETE FROM geodetic_point_tasks")
    public abstract void deleteAll();
}
