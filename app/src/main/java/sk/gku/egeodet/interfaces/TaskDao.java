package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.Task;

import java.util.List;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM tasks")
    LiveData<List<Task>> getTasks();

    @Query("SELECT * FROM tasks WHERE type IN (:types)")
    List<Task> getTasksByTypes(List<String> types);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Task task);

    @Update
    Integer update(Task task);

    @Query("DELETE FROM tasks WHERE id IN (:ids)")
    void deleteByIds(Integer... ids);

    @Query("DELETE FROM tasks")
    void deleteAll();
}
