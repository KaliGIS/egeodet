package sk.gku.egeodet.interfaces;

import androidx.room.Dao;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import sk.gku.egeodet.models.User;

@Dao
public interface RawDao {
    @RawQuery(observedEntities = User.class)
    int execSQL(SupportSQLiteQuery query);
}
