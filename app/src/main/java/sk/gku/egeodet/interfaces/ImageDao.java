package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.Image;

import java.util.List;

@Dao
public abstract class ImageDao extends BaseDao<Image> {
    @Query("SELECT * FROM images")
    public abstract LiveData<List<Image>> getImages();

    @Query("SELECT * FROM images WHERE id IN (:imageIds)")
    public abstract List<Image> getImagesByIds(Integer... imageIds);

    @Query("SELECT EXISTS (SELECT id FROM geodetic_points WHERE id = :geodeticPointId)")
    public abstract boolean geodeticPointExists(Integer geodeticPointId);

    @Query("SELECT * FROM images WHERE mission_id = :missionId AND geodetic_point_id = :geodeticPointId")
    public abstract LiveData<List<Image>> getImagesByMissionIdAndGeodeticPointId(int missionId, int geodeticPointId);

    @Query("SELECT * FROM images WHERE geodetic_point_id = :geodeticPointId")
    public abstract List<Image> getImagesByGeoPntId(int geodeticPointId);

    @Query("SELECT Count(*) FROM images " +
            "WHERE name = :name AND path = :path AND geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract int getCountByAttributes(String name, String path, int geodeticPointId, int missionId);

    @Query("SELECT Count(*) FROM images WHERE path = :path AND name = :name")
    public abstract Integer getCountByPathAndName(String path, String name);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(Image image);

    @Update
    public abstract Integer update(Image image);

    @Delete
    public abstract void delete(Image image);

    @Query("DELETE FROM images WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

    @Query("DELETE FROM images")
    public abstract void deleteAll();
}
