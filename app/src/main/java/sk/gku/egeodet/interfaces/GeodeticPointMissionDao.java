package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.JoinGeodeticPointMission;
import sk.gku.egeodet.models.JoinGeodeticPointMissionGeoPnt;

import java.util.List;

@Dao
public abstract class GeodeticPointMissionDao extends BaseDao<GeodeticPointMission> {
    @Query("SELECT * FROM geodetic_point_missions")
    public abstract LiveData<List<GeodeticPointMission>> getGeodeticPointMissions();

    @Query("SELECT * FROM geodetic_point_missions WHERE id IN (:geodeticPointMissionIds)")
    public abstract List<GeodeticPointMission> getGeodeticPointMissionsByIds(Integer... geodeticPointMissionIds);

    @Query("SELECT gpm.id AS mId, gpm.mission_id AS mMissionId, gpm.geodetic_point_id AS mGeodeticPointId, gpm.user_id AS mUserId, gpm.status AS mStatus, gpm.created AS mCreated, gpm.modified AS mModified, gp.label AS mGeodeticPointLabel, gp.type AS mGeodeticPointType " +
            "FROM geodetic_point_missions AS gpm LEFT JOIN geodetic_points AS gp ON gpm.geodetic_point_id = gp.id " +
            "WHERE gpm.mission_id = :missionId AND gpm.group_id IN (:groupIds)")
    public abstract LiveData<List<JoinGeodeticPointMissionGeoPnt>> getGeodeticPointMissionsWithGeoPntsByMissionIdAndGroupIds(Integer missionId, Integer... groupIds);

    @Query("SELECT gpm.id AS mId, gpm.mission_id AS mMissionId, gpm.geodetic_point_id AS mGeodeticPointId, gpm.user_id AS mUserId, gpm.status AS mStatus, gpm.created AS mCreated, gpm.modified AS mModified, gp.label AS mGeodeticPointLabel, gp.type AS mGeodeticPointType " +
            "FROM geodetic_point_missions AS gpm LEFT JOIN geodetic_points AS gp ON gpm.geodetic_point_id = gp.id " +
            "WHERE gpm.mission_id = :missionId")
    public abstract LiveData<List<JoinGeodeticPointMissionGeoPnt>> getGeodeticPointMissionsWithGeoPntsByMissionId(Integer missionId);

    @Query("SELECT * FROM geodetic_point_missions WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract GeodeticPointMission[] getGeodeticPointMissionsByGeoPntIdAndMissionId(int geodeticPointId, int missionId);

    @Query("SELECT * FROM geodetic_point_missions WHERE geodetic_point_id = :geodeticPointId")
    public abstract List<GeodeticPointMission> getGeodeticPointMissionsByGeoPntId(int geodeticPointId);

    @Query("SELECT gp.label AS mGeodeticPointLabel, m.name AS mMissionName, m.year AS mMissionYear " +
            "FROM geodetic_point_missions AS gpm " +
            "INNER JOIN geodetic_points AS gp ON gpm.geodetic_point_id = gp.id " +
            "INNER JOIN missions AS m ON gpm.mission_id = m.id")
    public abstract List<JoinGeodeticPointMission> getGeodeticPointMissionsJoin();

    @Query("SELECT * FROM geodetic_point_missions WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract List<GeodeticPointMission> getGeodeticPointMissionsByGeodeticPointIdAndMissionId(int geodeticPointId, int missionId);

    @Query("SELECT id FROM geodetic_point_missions WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract Integer[] getGeodeticPointMissionIdsByGeodeticPointIdAndMissionId(Integer geodeticPointId, Integer missionId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(GeodeticPointMission geodeticPointMission);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long[] insertAll(GeodeticPointMission... geodeticPointMissions);

    @Update
    public abstract Integer update(GeodeticPointMission geodeticPointMission);

    @Query("UPDATE geodetic_point_missions SET status = :geodeticPointStatus, modified = datetime() " +
            "WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract void updateStatus(int geodeticPointId, int missionId, int geodeticPointStatus);

    @Query("UPDATE geodetic_point_missions SET user_id = :userId, modified = datetime() " +
            "WHERE geodetic_point_id = :geodeticPointId AND mission_id = :missionId")
    public abstract void updateUserId(Integer geodeticPointId, Integer missionId, Integer userId);

    @Query("DELETE FROM geodetic_point_missions WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

    @Query("DELETE FROM geodetic_point_missions")
    public abstract void deleteAll();

}