package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SupportSQLiteQuery;

import sk.gku.egeodet.models.User;

import java.util.List;

@Dao
public abstract class UserDao extends BaseDao<User> {
    @Query("SELECT * FROM users")
    public abstract LiveData<List<User>> getUsers();

    @Query("SELECT * FROM users")
    public abstract List<User> getUserList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(User user);

    @Update
    public abstract Integer update(User user);

    @Query("SELECT * FROM users LIMIT 1")
    public abstract User[] getAnyUser();

    @Query("SELECT * FROM users WHERE name LIKE :userName")
    public abstract User[] getUserByName(String userName);

    @Query("SELECT * FROM users WHERE id IN (:userIds)")
    public abstract List<User> getUsersByIds(Integer... userIds);

    @Query("DELETE FROM users")
    public abstract void deleteAll();

    @RawQuery(observedEntities = User.class)
    public abstract int execSQL(SupportSQLiteQuery query);

    @Query("DELETE FROM users WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

}
