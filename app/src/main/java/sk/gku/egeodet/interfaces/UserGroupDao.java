package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.UserGroup;

import java.util.List;

@Dao
public interface UserGroupDao {
    @Query("SELECT * FROM user_groups")
    LiveData<List<UserGroup>> getUserGroups();

    @Query("SELECT * FROM user_groups WHERE user_id = :userId")
    List<UserGroup> getUserGroupsByUserId(Integer userId);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(UserGroup userGroup);

    @Update
    Integer update(UserGroup userGroup);

    @Query("DELETE FROM user_groups")
    void deleteAll();
}
