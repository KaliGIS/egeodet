package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.Group;

import java.util.List;

@Dao
public interface GroupDao {
    @Query("SELECT * FROM groups")
    LiveData<List<Group>> getGroups();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Group group);

    @Update
    Integer update(Group group);

    @Query("DELETE FROM groups")
    void deleteAll();
}
