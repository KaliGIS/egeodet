package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.ToSynchronizeEntity;

import java.util.List;

@Dao
public interface ToSynchronizeEntityDao {
    @Query("SELECT * FROM to_synchronize_entities WHERE model_name = :modelName AND entity_id = :entityId LIMIT 1")
    ToSynchronizeEntity getToSynchronizeEntityByModelNameAndEntityId(String modelName, Integer entityId);

    @Query("SELECT * FROM to_synchronize_entities WHERE synchronization_error > 0 OR master_id = 0")
    LiveData<List<ToSynchronizeEntity>> getNotSynchronizedToSynchronizeEntities();

    @Query("SELECT Count(*) FROM to_synchronize_entities WHERE synchronization_error > 0 OR master_id = 0")
    Integer getCountOfNotSynchronizedToSynchronizeEntities();

    @Query("SELECT * FROM to_synchronize_entities WHERE synchronization_error = 0 OR master_id > 0")
    List<ToSynchronizeEntity> getSynchronizedToSynchronizeEntities();

    @Query("SELECT entity_id FROM to_synchronize_entities WHERE model_name = :modelName AND (synchronization_error > 0 OR master_id = 0)")
    List<Integer> getEntityIdsByModelName(String modelName);

    @Query("SELECT * FROM to_synchronize_entities WHERE model_name = :modelName AND (synchronization_error > 0 OR master_id = 0)")
    List<ToSynchronizeEntity> getToSynchronizeEntitiesByModelName(String modelName);

    @Query("SELECT * FROM to_synchronize_entities WHERE model_name = :modelName AND synchronization_error = 0")
    List<ToSynchronizeEntity> getRemainingToSynchronizeEntitiesByModelName(String modelName);

    @Query("SELECT * FROM to_synchronize_entities WHERE synchronization_error = 0")
    List<ToSynchronizeEntity> getRemainingToSynchronizeEntities();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(ToSynchronizeEntity toSynchronizeEntity);

    @Update
    Integer update(ToSynchronizeEntity toSynchronizeEntity);

    @Query("UPDATE to_synchronize_entities SET synchronization_error = :value")
    void updateSynchronizationErrors(Integer value);

    @Delete()
    void delete(ToSynchronizeEntity toSynchronizeEntity);

    @Query("DELETE FROM to_synchronize_entities WHERE id IN (:ids)")
    void deleteByIds(Integer... ids);

    @Query("SELECT * FROM to_synchronize_entities WHERE synchronization_error = 0 AND master_id != 0 ORDER BY model_name")
    List<ToSynchronizeEntity> getToChangeIdToSynchronizeEntities();

    @Query("DELETE FROM to_synchronize_entities")
    void deleteAll();
}
