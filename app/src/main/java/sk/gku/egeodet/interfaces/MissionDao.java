package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.Mission;

import java.util.List;

@Dao
public interface MissionDao {
    @Query("SELECT * FROM missions ORDER BY datetime(modified) DESC")
    LiveData<List<Mission>> getMissions();

    @Query("SELECT * FROM missions")
    List<Mission> getMissionList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Mission mission);

    @Update
    Integer update(Mission mission);

    @Query("UPDATE missions SET modified = datetime() WHERE id = :id")
    int updateModified(Integer id);

    @Query("SELECT * FROM missions LIMIT 1")
    Mission[] getAnyMission();

    @Query("DELETE FROM missions")
    void deleteAll();

    @Query("DELETE FROM missions WHERE id IN (:ids)")
    void deleteByIds(Integer... ids);

}
