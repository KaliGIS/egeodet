package sk.gku.egeodet.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import sk.gku.egeodet.models.GeodeticPoint;

import java.util.List;

@Dao
public abstract class GeodeticPointDao extends BaseDao<GeodeticPoint> {
    @Query("SELECT * FROM geodetic_points")
    public abstract LiveData<List<GeodeticPoint>> getGeodeticPoints();

    @Query("SELECT * FROM geodetic_points")
    public abstract List<GeodeticPoint> getGeodeticPointList();

    @Query("SELECT * FROM geodetic_points WHERE id IN (:geodeticPointIds)")
    public abstract List<GeodeticPoint> getGeodeticPointsByIds(Integer... geodeticPointIds);

    @Query("SELECT * FROM geodetic_points WHERE label LIKE :geodeticPointLabel")
    public abstract GeodeticPoint[] getGeodeticPointsByLabel(String geodeticPointLabel);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    public abstract long insert(GeodeticPoint geodeticPoint);

    @Update
    public abstract Integer update(GeodeticPoint geodeticPoint);

    @Query("UPDATE geodetic_points SET last_maintenance = datetime(), modified = datetime() WHERE id = :geodeticPointId")
    public abstract void updateLastMaintenance(int geodeticPointId);

    @Query("UPDATE geodetic_points SET id = -1 * id WHERE id IN (:ids)")
    public abstract void setNegativeIds(Integer... ids);

    @Query("DELETE FROM geodetic_points WHERE id IN (:ids)")
    public abstract void deleteByIds(Integer... ids);

    @Query("DELETE FROM geodetic_points")
    public abstract void deleteAll();

}
