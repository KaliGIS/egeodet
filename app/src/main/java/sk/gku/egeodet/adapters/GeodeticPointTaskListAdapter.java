package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.GeodeticExaminationActivity;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;

import java.util.ArrayList;
import java.util.List;

public class GeodeticPointTaskListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "GPTaskListAdapter";
    private final LayoutInflater mInflater;
    private List<JoinGeodeticPointTaskTask> mGeodeticPointTasks;
    private List<JoinGeodeticPointTaskTask> mGeodeticExaminationTasks;
    private Context mContext;
    private int mGeodeticPointStatus;
    private int mCurrUserId;

    public GeodeticPointTaskListAdapter(Context context, List<JoinGeodeticPointTaskTask> geodeticPointTasks, int geodeticPointStatus, int currUserId) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mGeodeticPointTasks = new ArrayList<>();
        this.mGeodeticExaminationTasks = new ArrayList<>();
        this.mGeodeticPointStatus = geodeticPointStatus;
        this.mCurrUserId = currUserId;

        // Filter geodetic examination tasks from general geodetic point tasks
        separateTasks(geodeticPointTasks);
    }

    public void separateTasks(List<JoinGeodeticPointTaskTask> geodeticPointTasks) {
        for (JoinGeodeticPointTaskTask geodeticPointTask : geodeticPointTasks) {
            Log.d(TAG, "separateTasks: current taskType: " + geodeticPointTask.getTaskType());
            if (geodeticPointTask.getTaskType().toLowerCase().equals(Constants.GEODETIC_EXAMINATION_TASK_NAME)) {
                Log.d(TAG, "separateTasks: adding to mGeodeticExaminationTasks: " + geodeticPointTask.getTaskName());
                mGeodeticExaminationTasks.add(geodeticPointTask);
            } else {
                Log.d(TAG, "separateTasks: adding to mGeodeticPointTasks: " + geodeticPointTask.getTaskName());
                mGeodeticPointTasks.add(geodeticPointTask);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: Creating view holder, parent: " + parent);
        View itemView = mInflater.inflate(R.layout.recyclerview_item_geodetic_point_task, parent, false);
        return new GPTaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        GPTaskViewHolder holder = (GPTaskViewHolder) viewHolder;

        // Get current geodetic point task.
        final JoinGeodeticPointTaskTask currentGeodeticPointTask = mGeodeticPointTasks.get(position);
        Log.d(TAG, "onBindViewHolder: position: " + position + ", currentGeodeticPointTask: " + currentGeodeticPointTask.getTaskName());
        Log.d(TAG, "onBindViewHolder: getItemCount: " + getItemCount());

        // Populate the views with data.
        holder.mTaskCheckBox.setOnCheckedChangeListener(null);
        holder.mTaskCheckBox.setChecked(currentGeodeticPointTask.getIsDone());

        Log.d(TAG, "bindTo: currentGeodeticPointTask: " + currentGeodeticPointTask.getTaskName() + ", isMandatory: " + currentGeodeticPointTask.getIsMandatory());

        boolean shouldBeActive = getShouldBeActive(currentGeodeticPointTask);
        if (shouldBeActive) {
            holder.mTaskCheckBox.setEnabled(true);
            holder.mTaskCheckBox.setTag(holder.getAdapterPosition());
            holder.mTaskCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d(TAG, "onCheckedChanged: geodetic point task");
                    Integer position = (Integer) buttonView.getTag();
                    int geodeticPointTaskId = mGeodeticPointTasks.get(position).getId();

                    Intent checkIntent = new Intent(Constants.GEODETIC_POINT_TASK_INTENT);
                    checkIntent.putExtra("geodeticPointTaskId", geodeticPointTaskId);
                    checkIntent.putExtra("isChecked", isChecked);
                    LocalBroadcastManager.getInstance(mContext).sendBroadcast(checkIntent);
                }
            });
        } else {
            holder.mTaskCheckBox.setEnabled(false);
        }

        holder.mTaskLabel.setText(currentGeodeticPointTask.getTaskName());
        Log.d(TAG, "bindTo later: currentGeodeticPointTask: " + currentGeodeticPointTask.getTaskName() + ", isMandatory: " + currentGeodeticPointTask.getIsMandatory());

        if (currentGeodeticPointTask.getIsMandatory()) {
            Log.d(TAG, "onBindViewHolder: isMandatory: " + currentGeodeticPointTask.getIsMandatory());
            holder.mTaskLabel.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }

        Log.d(TAG, "onBindViewHolder: current task name: " + currentGeodeticPointTask.getTaskName());

        if (currentGeodeticPointTask.getUserId() != null) {
            holder.mTaskOwner.setVisibility(View.VISIBLE);
            holder.mTaskOwner.setText(currentGeodeticPointTask.getUserName());
        } else {
            holder.mTaskOwner.setVisibility(View.GONE);
            holder.mTaskOwner.setText("");
        }

        if (currentGeodeticPointTask.getTaskName().toLowerCase().equals(Constants.GEODETIC_EXAMINATION_TASK_NAME)) {
            Log.d(TAG, "onBindViewHolder: is geodetic examination task");
            Log.d(TAG, "onBindViewHolder: mGeodeticExaminationTasks size: " + mGeodeticExaminationTasks.size());
            StringBuilder examinationTasks = new StringBuilder();
            for (JoinGeodeticPointTaskTask examinationTask : mGeodeticExaminationTasks) {
                Log.d(TAG, "onBindViewHolder: adding geodetic examination: " + examinationTask.getTaskName());
                examinationTasks.append(examinationTask.getTaskName()).append(" ");
            }
            holder.mGeodeticExaminations.setText(examinationTasks.toString());
            holder.mTaskCheckBox.setVisibility(View.INVISIBLE);
        } else {
            Log.d(TAG, "onBindViewHolder: is not geodetic examination task");
            holder.mGeodeticExaminations.setVisibility(View.GONE);
        }
    }

    private boolean getShouldBeActive(JoinGeodeticPointTaskTask currentGPTask) {
        boolean shouldBeActive = true;
        if (mGeodeticPointStatus == 2) {
            shouldBeActive = false;
        } else if (currentGPTask.getUserId() != null && !currentGPTask.getUserId().equals(mCurrUserId)) {
            shouldBeActive = false;
        } else if (currentGPTask.getUserId() != null && !currentGPTask.getIsDone()) {
            shouldBeActive = false;
        }
        return shouldBeActive;
    }

    @Override
    public int getItemCount() {
        if (mGeodeticPointTasks != null) {
            return mGeodeticPointTasks.size();
        } else {
            return 0;
        }
    }

    class GPTaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the views
        private final CheckBox mTaskCheckBox;
        private final TextView mTaskLabel;
        private final TextView mTaskOwner;
        private final TextView mGeodeticExaminations;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_geodetic_point_task.xml layout file.
         */
        GPTaskViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mTaskCheckBox = itemView.findViewById(R.id.checkbox_geodetic_point_task);
            mTaskLabel = itemView.findViewById(R.id.textview_geodetic_point_task_label);

            mGeodeticExaminations = itemView.findViewById(R.id.textview_geodetic_examinations);
            mTaskOwner = itemView.findViewById(R.id.textview_geodetic_point_task_owner);

            if (mGeodeticPointStatus == 0 || mGeodeticPointStatus == 1) {
                itemView.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            Integer position = getAdapterPosition();
            JoinGeodeticPointTaskTask currGeodeticPointTask = mGeodeticPointTasks.get(position);

            if (currGeodeticPointTask.getTaskName().toLowerCase().equals(Constants.GEODETIC_EXAMINATION_TASK_NAME)) {
                Intent geodeticExaminationIntent = new Intent(mContext, GeodeticExaminationActivity.class);
                Integer examinationGroupId = currGeodeticPointTask.getGroupId();
                geodeticExaminationIntent.putExtra("examinationGroupId", examinationGroupId);
                mContext.startActivity(geodeticExaminationIntent);
            }
        }
    }
}
