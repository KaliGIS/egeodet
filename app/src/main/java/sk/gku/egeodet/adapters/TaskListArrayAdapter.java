package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import sk.gku.egeodet.R;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.Task;

import java.util.List;

public class TaskListArrayAdapter extends ArrayAdapter<Task> {
    private static final String TAG = "TaskListArrayAdapter";
    private List<Task> mTasks;
    private Context mContext;
    private Boolean mIsFromView;
    private int itemLayout;

    public TaskListArrayAdapter(Context context, int resource, List<Task> tasks) {
        super(context, resource, tasks);
        mTasks = tasks;
        mContext = context;
        itemLayout = resource;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: Count of tasks: " + mTasks.size());
        return mTasks.size();
    }

    @Override
    public Task getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return mTasks.get(position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        return getCustomView(position, view, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {
        final TaskListArrayAdapter.ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item_task_selection, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.textview_task);
            holder.mButton = (Button) convertView
                    .findViewById(R.id.button_task);
            convertView.setTag(holder);
        } else {
            holder = (TaskListArrayAdapter.ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(mTasks.get(position).getName());

        if ((position == 0)) {
            holder.mButton.setVisibility(View.INVISIBLE);
        } else {
            holder.mButton.setVisibility(View.VISIBLE);
        }
        holder.mButton.setTag(position);
        holder.mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currPosition = (Integer) v.getTag();
                Task task = mTasks.get(currPosition);
                Log.d(TAG, "onClick: Sending broadcast with task: " + task.getName() + " and position: " + currPosition);

                Intent checkIntent = new Intent(Constants.SELECTED_TASK_INTENT);
                checkIntent.putExtra("taskId", task.getId());
                checkIntent.putExtra("taskName", task.getName());
                checkIntent.putExtra("taskType", task.getType());
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(checkIntent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private Button mButton;
    }

    public void setTasks(List<Task> tasks) {
        Log.d(TAG, "onChanged: Setting tasks");
        for (int i = 0; i < tasks.size(); i++) {
            Log.d(TAG, tasks.get(i).getName());
        }
        mTasks = tasks;
        notifyDataSetChanged();
    }
}
