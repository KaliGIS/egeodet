package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import sk.gku.egeodet.R;
import sk.gku.egeodet.configs.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeodeticPointTypeListAdapter extends ArrayAdapter<String> {
    private static final String TAG = "GeoPntTypeListAdapter";
    private List<String> mTypes;
    private List<String> mSelectedTypes;
    private Context mContext;

    public GeodeticPointTypeListAdapter(Context context, int resource) {
        super(context, resource, Arrays.asList(Constants.GEODETIC_POINT_TYPES));
        mContext = context;
        mTypes = Arrays.asList(Constants.GEODETIC_POINT_TYPES);
        mSelectedTypes = new ArrayList<>();
    }

    public void setSelectedTypes(List<String> selectedTypes) {
        mSelectedTypes = selectedTypes;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: Count of types: " + mTypes.size());
        return mTypes.size();
    }

    @Override
    public String getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return mTypes.get(position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        return getCustomView(position, view, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {
        final GeodeticPointTypeListAdapter.ViewHolder holder;
        if (convertView == null) {
            Log.d(TAG, "getCustomView: Convert view is null");
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item_geodetic_point_attribute, null);
            holder = new GeodeticPointTypeListAdapter.ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.textview_geodetic_point_attribute);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox_geodetic_point_attribute);
            convertView.setTag(holder);
        } else {
            Log.d(TAG, "getCustomView: Convert view exists");
            holder = (GeodeticPointTypeListAdapter.ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(mTypes.get(position));

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }

        holder.mCheckBox.setOnCheckedChangeListener(null);
        if (mSelectedTypes.contains(mTypes.get(position))) {
            Log.d(TAG, "getCustomView: setting checked to the type: " + mTypes.get(position));
            holder.mCheckBox.setChecked(true);
        } else {
            Log.d(TAG, "getCustomView: setting unchecked to the type: " + mTypes.get(position));
            holder.mCheckBox.setChecked(false);
        }

        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "onCheckedChanged: geodetic point type");
                int currPosition = (Integer) buttonView.getTag();
                String type = mTypes.get(currPosition);

                Intent checkIntent = new Intent(Constants.GEODETIC_POINT_TYPE_INTENT);
                checkIntent.putExtra("type", type);
                checkIntent.putExtra("isChecked", isChecked);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(checkIntent);
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
