package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.GeodeticPointTaskListActivity;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.TaskType;

import java.util.ArrayList;
import java.util.List;

public class TaskTypeListAdapter extends RecyclerView.Adapter<TaskTypeListAdapter.ViewHolder> {
    private static final String TAG = "TaskTypeListAdapter";
    private List<TaskType> mTaskTypes;
    private Context mContext;

    public TaskTypeListAdapter(Context context, List<TaskType> taskTypes) {
        List<TaskType> taskTypesFiltered = filterOutGeodeticExamination(taskTypes);
        this.mTaskTypes = taskTypesFiltered;
        this.mContext = context;
    }

    public List<TaskType> filterOutGeodeticExamination(List<TaskType> taskTypes) {
        List<TaskType> filteredTaskTypes = new ArrayList<>();

        for (TaskType taskType : taskTypes) {
            if (!taskType.getTaskType().toLowerCase().equals(Constants.GEODETIC_EXAMINATION_TASK_NAME)) {
                filteredTaskTypes.add(taskType);
            }
        }

        return filteredTaskTypes;
    }

    public void setTaskTypes(List<TaskType> taskTypes) {
        Log.d(TAG, "onChanged: Setting taskTypes");
        for (int i = 0; i < taskTypes.size(); i++) {
            Log.d(TAG, taskTypes.get(i).getTaskType());
        }

        List<TaskType> taskTypesFiltered = filterOutGeodeticExamination(taskTypes);
        mTaskTypes = taskTypesFiltered;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TaskTypeListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TaskTypeListAdapter.ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_task_type, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TaskTypeListAdapter.ViewHolder holder, int position) {
        // Get current taskType.
        TaskType currentTaskType = mTaskTypes.get(position);

        // Populate the textviews with data.
        holder.bindTo(currentTaskType);
    }

    @Override
    public int getItemCount() {
        return mTaskTypes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the TextViews
        private TextView mNameText;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_task_type.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mNameText = itemView.findViewById(R.id.taskType);

            itemView.setOnClickListener(this);
        }

        void bindTo(TaskType currentTaskType) {
            // Populate the textview with data.
            mNameText.setText(currentTaskType.getTaskType());
        }

        @Override
        public void onClick(View v) {
            TaskType currentTaskType = mTaskTypes.get(getAdapterPosition());

            Intent geodeticPointTaskIntent = new Intent(mContext, GeodeticPointTaskListActivity.class);
            geodeticPointTaskIntent.putExtra("taskType", currentTaskType.getTaskType());
            mContext.startActivity(geodeticPointTaskIntent);
        }
    }
}
