package sk.gku.egeodet.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.NoteEditActivity;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.JoinNoteUser;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.ViewHolder> {
    private static final String TAG = "NoteListAdapter";
    private List<JoinNoteUser> mNotes;
    private Context mContext;
    private Integer mUserId;

    public NoteListAdapter(Context context, List<JoinNoteUser> notes, Integer userId) {
        this.mNotes = notes;
        this.mContext = context;
        mUserId = userId;
    }

    public void setNotes(List<JoinNoteUser> notes) {
        Log.d(TAG, "onChanged: Setting notes");
        mNotes = notes;
        notifyDataSetChanged();
    }

    public JoinNoteUser getNoteAtPosition (int position) {
        return mNotes.get(position);
    }

    @NonNull
    @Override
    public NoteListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteListAdapter.ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_note, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NoteListAdapter.ViewHolder holder, int position) {
        // Get current note.
        JoinNoteUser currentNote = mNotes.get(position);

        // Populate the views with data.
        holder.bindTo(currentNote, mUserId);
    }

    @Override
    public int getItemCount() {
        return mNotes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the TextViews
        private TextView mHeaderText;
        private TextView mBodyText;
        private ImageButton mEditButton;
        private int mUserId;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_note.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mHeaderText = itemView.findViewById(R.id.note_header);
            mBodyText = itemView.findViewById(R.id.note_body);
            mEditButton = itemView.findViewById(R.id.button_edit_note);

            mEditButton.setOnClickListener(this);
        }

        void bindTo(JoinNoteUser currentNote, int userId) {
            mUserId = userId;
            String datetime;

            SimpleDateFormat sdf = new SimpleDateFormat(Constants.SLOVAK_DATE_TIME_FORMAT, Locale.getDefault());;
            sdf.setTimeZone(TimeZone.getDefault());
            datetime = sdf.format(currentNote.getModified());

            Log.d(TAG, "bindTo: orig modified: " + currentNote.getModified());
            Log.d(TAG, "bindTo: default timezone: " + TimeZone.getDefault());
            Log.d(TAG, "bindTo: after transformation: " + datetime);

            // Populate the textviews with data.
            mHeaderText.setText(currentNote.getUserName() + ", " + datetime);
            mBodyText.setText(currentNote.getNote());

            if (currentNote.getUserId() != userId) {
                mEditButton.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onClick(View v) {
            JoinNoteUser currentNote = mNotes.get(getAdapterPosition());

            Intent noteIntent = new Intent(mContext, NoteEditActivity.class);
            noteIntent.putExtra("noteText", currentNote.getNote());
            noteIntent.putExtra("noteId", currentNote.getNoteId());
            ((Activity) mContext).startActivityForResult(noteIntent, Constants.REQUEST_CODE_EDIT_NOTE_EDIT_ACTIVITY);
        }
    }
}

