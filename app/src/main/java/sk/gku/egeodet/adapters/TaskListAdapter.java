package sk.gku.egeodet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import sk.gku.egeodet.R;
import sk.gku.egeodet.models.Task;

import java.util.List;

public class TaskListAdapter extends BaseAdapter {
    private static final String TAG = "TaskListAdapter";
    private List<Task> mTasks;
    private Context mContext;
    private int itemLayout;

    public TaskListAdapter(Context context, int resource, List<Task> tasks) {
        mContext = context;
        itemLayout = resource;
        mTasks = tasks;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: Count of tasks: " + mTasks.size());
        return mTasks.size();
    }

    @Override
    public Task getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return mTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        Log.d(TAG, "getView: getItem(position): " + getItem(position).getName());
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        TextView taskNameTV = (TextView) view.findViewById(R.id.task_name);
        taskNameTV.setText(getItem(position).getName());

        return view;
    }

    public void setTasks(List<Task> tasks) {
        mTasks = tasks;
        notifyDataSetChanged();
    }
}
