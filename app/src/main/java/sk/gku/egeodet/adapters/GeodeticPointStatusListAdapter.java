package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import sk.gku.egeodet.R;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.configs.GeodeticPointStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeodeticPointStatusListAdapter extends ArrayAdapter<GeodeticPointStatus> {
    private static final String TAG = "StatusListAdapter";
    private List<GeodeticPointStatus> mStatuses;
    private List<Integer> mSelectedStatuses;
    private Context mContext;

    public GeodeticPointStatusListAdapter(Context context, int resource) {
        super(context, resource, Arrays.asList(Constants.GEODETIC_POINT_STATUSES));
        mContext = context;
        mStatuses = Arrays.asList(Constants.GEODETIC_POINT_STATUSES);
        mSelectedStatuses = new ArrayList<>();
    }

    public void setSelectedStatuses(List<Integer> selectedStatuses) {
        mSelectedStatuses = selectedStatuses;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: Count of statuses: " + mStatuses.size());
        return mStatuses.size();
    }

    @Override
    public GeodeticPointStatus getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return mStatuses.get(position);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        return getCustomView(position, view, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item_geodetic_point_attribute, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.textview_geodetic_point_attribute);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox_geodetic_point_attribute);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(mStatuses.get(position).getName());

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }

        holder.mCheckBox.setOnCheckedChangeListener(null);
        if (mSelectedStatuses.contains(mStatuses.get(position).getId())) {
            Log.d(TAG, "getCustomView: setting checked to the status: " + mStatuses.get(position).getName());
            holder.mCheckBox.setChecked(true);
        } else {
            Log.d(TAG, "getCustomView: setting unchecked to the status: " + mStatuses.get(position).getName());
            holder.mCheckBox.setChecked(false);
        }

        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "onCheckedChanged: geodetic point status");
                int currPosition = (Integer) buttonView.getTag();
                Integer statusId = mStatuses.get(currPosition).getId();

                Intent checkIntent = new Intent(Constants.GEODETIC_POINT_STATUS_INTENT);
                checkIntent.putExtra("statusId", statusId);
                checkIntent.putExtra("isChecked", isChecked);
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(checkIntent);
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
