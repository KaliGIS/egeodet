package sk.gku.egeodet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import sk.gku.egeodet.R;

import sk.gku.egeodet.models.User;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends ArrayAdapter<User> {
    private static final String TAG = "UserListAdapter";
    private List<User> mUsers;
    private List<User> mOriginalUsers = new ArrayList<>();
    private Context mContext;
    private int itemLayout;

    private UserListAdapter.ListFilter listFilter = new UserListAdapter.ListFilter();

    public UserListAdapter(Context context, int resource, List<User> users) {
        super(context, resource, users);
        mContext = context;
        itemLayout = resource;
        mUsers = users;
        Log.d(TAG, "UserListAdapter: mUsers size at initialization is: " + mUsers.size());
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount: Count of users: " + mUsers.size());
        return mUsers.size();
    }

    @Override
    public User getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return mUsers.get(position);
    }

    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent) {
        Log.d(TAG, "getView: getItem(position): " + getItem(position).getName());
        if (view == null) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(itemLayout, parent, false);
        }

        TextView userNameTV = (TextView) view.findViewById(R.id.username);
        userNameTV.setText(getItem(position).getName());

        return view;
    }

    public void setUsers(List<User> users) {
//        Log.d(TAG, "onChanged: I am here");
//        for (int i = 0; i < users.size(); i++) {
//            Log.d(TAG, users.get(i).getName());
//        }
        mUsers = users;
        mOriginalUsers = users;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    public class ListFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            List<User> filteredList = new ArrayList<>();
            if (prefix == null || prefix.length() == 0) {
                for (User user : mOriginalUsers) {
                    filteredList.add(user);
                }
            } else {
                for (User user : mOriginalUsers) {
                    String[] nameParts = getNameParts(user.getName());

                    if (user.getName().toLowerCase().contains(prefix.toString().toLowerCase())) {
                        if (nameParts[0].toLowerCase().startsWith(prefix.toString().toLowerCase()) ||
                            nameParts[1].toLowerCase().startsWith(prefix.toString().toLowerCase()) ) {
                            filteredList.add(0, user);
                        } else {
                            filteredList.add(user);
                        }
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            filterResults.count = filteredList.size();
            Log.d(TAG, "performFiltering: filteredList size: " + filteredList.size());
            return filterResults;
        }

        private String[] getNameParts(String userName) {
            String[] finalNameParts = new String[2];
            String[] nameParts = userName.split(" ");
            String lastName = nameParts[nameParts.length - 1];
            String firstNames = "";
            for (int i = 0; i < nameParts.length; i++) {
                if (!nameParts[i].equals(lastName)) {
                    firstNames += nameParts[i] + " ";
                }
            }
            finalNameParts[0] = firstNames;
            finalNameParts[1] = lastName;
            return finalNameParts;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            Log.d(TAG, "publishResults: results count: " + results.count);
            if (results.count > 0) {
                mUsers = (ArrayList<User>) results.values;
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
