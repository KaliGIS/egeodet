package sk.gku.egeodet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.models.Task;

import java.util.List;

public class TaskSelectedAdapter extends RecyclerView.Adapter<TaskSelectedAdapter.ViewHolder> {
    public static final String TAG = "TaskSelectedAdapter";
    private List<Task> mTasks;
    private Context mContext;

    public TaskSelectedAdapter(Context context, List<Task> tasks) {
        this.mTasks = tasks;
        this.mContext = context;
    }

    public void setTasks(List<Task> tasks) {
        Log.d(TAG, "setTasks: setting selected tasks: " + tasks.size());
        mTasks = tasks;
        notifyDataSetChanged();
    }

    /**
     * Required method for creating the viewholder objects.
     *
     * @param parent The ViewGroup into which the new View will be added
     *               after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return The newly created ViewHolder.
     */
    @Override
    public TaskSelectedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclereview_item_selected_task, parent, false));
    }

    /**
     * Required method that binds the data to the viewholder.
     *
     * @param holder The viewholder into which the data should be put.
     * @param position The adapter position.
     */
    @Override
    public void onBindViewHolder(TaskSelectedAdapter.ViewHolder holder, int position) {
        // Get current task.
        Task currentTask = mTasks.get(position);

        // Populate the textviews with data.
        holder.bindTo(currentTask);
    }

    /**
     * Required method for determining the size of the data set.
     *
     * @return Size of the data set.
     */
    @Override
    public int getItemCount() {
        return mTasks.size();
    }

    /**
     * ViewHolder class that represents each row of data in the RecyclerView.
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Member Variables for the TextViews
        private TextView mTextViewTaskName;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_selected_task.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mTextViewTaskName = itemView.findViewById(R.id.textview_task_name);

            itemView.setOnClickListener(this);
        }

        void bindTo(Task currentTask){
            // Populate the textviews with data.
            Log.d(TAG, "bindTo: setting text to task textview: " + currentTask.getName());
            mTextViewTaskName.setText(currentTask.getName());
        }

        @Override
        public void onClick(View view) {
//            Sport currentSport = mSportsData.get(getAdapterPosition());
//
//            Intent detailIntent = new Intent(mContext, DetailActivity.class);
//            detailIntent.putExtra("title", currentSport.getTitle());
//            detailIntent.putExtra("image_resource", currentSport.getImageResource());
//            mContext.startActivity(detailIntent);
        }
    }
}
