package sk.gku.egeodet.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class GeodeticExaminationListAdapter extends RecyclerView.Adapter<GeodeticExaminationListAdapter.ViewHolder> {
    private static final String TAG = "GeoExamiListAdapter";
    private List<JoinGeodeticPointTaskTask> mGeodeticPointTasks;
    private Context mContext;

    public GeodeticExaminationListAdapter(Context context, List<JoinGeodeticPointTaskTask> geodeticPointTaskTasks) {
        this.mGeodeticPointTasks = geodeticPointTaskTasks;
        this.mContext = context;
    }

    public void setGeodeticPointTasks(List<JoinGeodeticPointTaskTask> geodeticPointTasks) {
        mGeodeticPointTasks = geodeticPointTasks;
        notifyDataSetChanged();
    }

    public JoinGeodeticPointTaskTask getGeodeticPointTaskAtPosition (int position) {
        return mGeodeticPointTasks.get(position);
    }

    @NonNull
    @Override
    public GeodeticExaminationListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GeodeticExaminationListAdapter.ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_geodetic_examination, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GeodeticExaminationListAdapter.ViewHolder holder, int position) {
        // Get current geodetic point task.
        final JoinGeodeticPointTaskTask currentGeodeticPointTask = mGeodeticPointTasks.get(position);
        Log.d(TAG, "onBindViewHolder: position: " + position);
        Log.d(TAG, "onBindViewHolder: currentGeodeticPointTask: " + currentGeodeticPointTask.getTaskName());

        // Populate the views with data.
        holder.bindTo(currentGeodeticPointTask);
    }

    @Override
    public int getItemCount() {
        return mGeodeticPointTasks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        // Member Variables for the views
        private TextView mGeodeticExaminationNameTV;
        private TextView mDoneDateTV;
        private TextView mOwnerTV;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_geodetic_point_task.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mGeodeticExaminationNameTV = itemView.findViewById(R.id.textview_geodetic_examination);
            mDoneDateTV = itemView.findViewById(R.id.textview_done_date);
            mOwnerTV = itemView.findViewById(R.id.textview_geodetic_point_examination_owner);
        }

        void bindTo(JoinGeodeticPointTaskTask currentGeodeticPointTask) {
            mGeodeticExaminationNameTV.setText(currentGeodeticPointTask.getTaskName());
            mOwnerTV.setText(currentGeodeticPointTask.getUserName());

            SimpleDateFormat sdf = new SimpleDateFormat(Constants.SLOVAK_DATE_TIME_FORMAT, Locale.getDefault());;
            sdf.setTimeZone(TimeZone.getDefault());
            String datetime = sdf.format(currentGeodeticPointTask.getCreated());
            mDoneDateTV.setText(datetime);
        }
    }
}
