package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.GeodeticPointTaskMainActivity;
import sk.gku.egeodet.models.JoinGeodeticPointMissionGeoPnt;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class GeodeticPointListAdapter extends RecyclerView.Adapter<GeodeticPointListAdapter.ViewHolder> implements Filterable {
    private static final String TAG = "GeodeticPntListAdapter";
    private List<JoinGeodeticPointMissionGeoPnt> mGeodeticPointMissionsGeoPnt;
    private List<JoinGeodeticPointMissionGeoPnt> mOriginalGeodeticPointMissionsGeoPnt;
    private Context mContext;

    private GeodeticPointListAdapter.ListFilter listFilter = new GeodeticPointListAdapter.ListFilter();
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    private AdapterCallBack mAdapterCallback;
    public interface AdapterCallBack
    {
        void onChangePointsCount(int pointsCount);
    }

    public GeodeticPointListAdapter(Context context, List<JoinGeodeticPointMissionGeoPnt> geodeticPointMissionsGeoPnt, AdapterCallBack adapterCallback) {
        this.mGeodeticPointMissionsGeoPnt = geodeticPointMissionsGeoPnt;
        this.mOriginalGeodeticPointMissionsGeoPnt = geodeticPointMissionsGeoPnt;
        this.mContext = context;
        this.mAdapterCallback = adapterCallback;
    }

    public void setGeodeticPointMissionsGeoPnt(List<JoinGeodeticPointMissionGeoPnt> geodeticPointMissionsGeoPnt, CharSequence prefix) {
        Log.d(TAG, "onChanged: Setting geodetic point missions with geo pnts");
        mGeodeticPointMissionsGeoPnt = geodeticPointMissionsGeoPnt;
        mOriginalGeodeticPointMissionsGeoPnt = geodeticPointMissionsGeoPnt;
        // notifyDataSetChanged();
        listFilter.publishResults(prefix, listFilter.performFiltering(prefix));
    }

    @NonNull
    @Override
    public GeodeticPointListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_geodetic_point, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GeodeticPointListAdapter.ViewHolder holder, int position) {
        // Get current geodetic point.
        JoinGeodeticPointMissionGeoPnt currentGeodeticPointMissionGeoPnt = mGeodeticPointMissionsGeoPnt.get(position);

        // Populate the textviews with data.
        holder.bindTo(currentGeodeticPointMissionGeoPnt);
    }

    @Override
    public int getItemCount() {
        return mGeodeticPointMissionsGeoPnt.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the TextViews
        private TextView mLabelText;
        private TextView mTypeText;
        private CardView mCard;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_geodetic_point.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mLabelText = itemView.findViewById(R.id.geodeticPointLabel);
            mTypeText = itemView.findViewById(R.id.geodeticPointType);
            mCard = (CardView) itemView;

            itemView.setOnClickListener(this);
        }

        void bindTo(JoinGeodeticPointMissionGeoPnt currentGeodeticPointMissionGeoPnt) {
            // Populate the textview with data.
            mLabelText.setText(currentGeodeticPointMissionGeoPnt.getGeodeticPointLabel());
            mTypeText.setText(currentGeodeticPointMissionGeoPnt.getGeodeticPointType());

            switch (currentGeodeticPointMissionGeoPnt.getStatus()) {
                case 0:
                    mCard.setBackgroundColor(mContext.getResources().getColor(R.color.GeodeticPointStatusNotStarted));
                    break;
                case 1:
                    mCard.setBackgroundColor(mContext.getResources().getColor(R.color.GeodeticPointStatusInProgress));
                    break;
                case 2:
                    mCard.setBackgroundColor(mContext.getResources().getColor(R.color.GeodeticPointStatusDone));
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            JoinGeodeticPointMissionGeoPnt currentGeodeticPointMission = mGeodeticPointMissionsGeoPnt.get(getAdapterPosition());
            SharedPreferences preferences = mContext.getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
            SharedPreferences.Editor preferencesEditor = preferences.edit();
            preferencesEditor.putInt("geodeticPointStatus", currentGeodeticPointMission.getStatus());
            preferencesEditor.apply();

            Intent geodeticPointIntent = new Intent(mContext, GeodeticPointTaskMainActivity.class);
            geodeticPointIntent.putExtra("geodeticPointId", currentGeodeticPointMission.getGeodeticPointId());
            geodeticPointIntent.putExtra("geodeticPointLabel", currentGeodeticPointMission.getGeodeticPointLabel());
            geodeticPointIntent.putExtra("geodeticPointType", currentGeodeticPointMission.getGeodeticPointType());
            geodeticPointIntent.putExtra("geodeticPointMissionId", currentGeodeticPointMission.getId());
            mContext.startActivity(geodeticPointIntent);
        }
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }

    public class ListFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            Log.d(TAG, "performFiltering: mOriginalGeodeticPointMissionsGeoPnt count: " + mOriginalGeodeticPointMissionsGeoPnt.size());
            List<JoinGeodeticPointMissionGeoPnt> filteredList = new ArrayList<>();
            if (prefix == null || prefix.length() == 0) {
                for (JoinGeodeticPointMissionGeoPnt row : mOriginalGeodeticPointMissionsGeoPnt) {
                    filteredList.add(row);
                }
            } else {
                for (JoinGeodeticPointMissionGeoPnt row : mOriginalGeodeticPointMissionsGeoPnt) {
                    if (row.getGeodeticPointLabel().toLowerCase().contains(prefix.toString().toLowerCase())) {
                        filteredList.add(row);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            filterResults.count = filteredList.size();
            Log.d(TAG, "performFiltering: filteredList size: " + filteredList.size());
            return filterResults;
        }


        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            Log.d(TAG, "publishResults: results count: " + results.count);
            mGeodeticPointMissionsGeoPnt = (ArrayList<JoinGeodeticPointMissionGeoPnt>) results.values;
            notifyDataSetChanged();

            mAdapterCallback.onChangePointsCount(results.count);
//
//            if (results.count > 0) {
//                mGeodeticPointMissionsGeoPnt = (ArrayList<JoinGeodeticPointMissionGeoPnt>) results.values;
//                notifyDataSetChanged();
//            }
//            else {
//                notifyDataSetInvalidated();
//            }

            for (JoinGeodeticPointMissionGeoPnt item : mGeodeticPointMissionsGeoPnt) {
                Log.d(TAG, "publishResults: filtered geodetic point: " + item.getGeodeticPointLabel());
            }
        }
    }
}
