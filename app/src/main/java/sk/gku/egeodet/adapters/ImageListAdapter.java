package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.FullScreenImageActivity;
import sk.gku.egeodet.models.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ViewHolder> {
    private static final String TAG = "ImageListAdapter";
    private List<Image> mImages;
    private Context mContext;

    public ImageListAdapter(Context context, List<Image> images) {
        this.mImages = images;
        this.mContext = context;
    }

    public void setImages(List<Image> images) {
        Log.d(TAG, "onChanged: Setting images");
        for (int i = 0; i < images.size(); i++) {
            Log.d(TAG, images.get(i).getName());
        }
        mImages = images;
        notifyDataSetChanged();
    }

    public Image getImageAtPosition(int position) {
        if (mImages.size() > 0) {
            return mImages.get(position);
        } else {
            return null;
        }
    }

    public void getImageByName(String name) {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        Cursor cursor = mContext.getContentResolver().query(uri, null, null, null, null);

        ArrayList<String> ids = new ArrayList<String>();
        // mAlbumsList.clear();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                String bucketDisplayName = cursor.getString(columnIndex);

                columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
                String displayName = cursor.getString(columnIndex);

                Log.d(TAG, "getImageByName: butcket display name: " + bucketDisplayName);
                Log.d(TAG, "getImageByName: display name: " + displayName);
            }
            cursor.close();
        }
    }

    @NonNull
    @Override
    public ImageListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageListAdapter.ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageListAdapter.ViewHolder holder, int position) {
        // Get current image.
        Image currentImage = mImages.get(position);

        // Populate the imageView with data.
        holder.bindTo(currentImage);
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the ImageViews
        private ImageView mImageView;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_image.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mImageView = itemView.findViewById(R.id.imageViewGeodeticPointPhoto);

            itemView.setOnClickListener(this);
        }

        void bindTo(Image currentImage) {
            // Populate the imageView with data.
            Uri contentUri = null;

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // TODO implement for Android 10 and more
//                String[] projection = new String[] {
//                        MediaStore.Images.Media._ID,
//                        MediaStore.Images.Media.DISPLAY_NAME,
//                };
//                String selection = MediaStore.Images.Media.RELATIVE_PATH + " LIKE ? AND " +
//                        MediaStore.Images.Media.DISPLAY_NAME + " LIKE ?";
//                String[] selectionArgs = new String[] {
//                        "%" + "Pictures/eGeodetImages/" + currentImage.getPath() + "%",
//                        "%" + currentImage.getName() + "%"
//                };
//
//                Log.d(TAG, "mediaStore before query");
//
//                try (Cursor cursor = mContext.getApplicationContext().getContentResolver().query(
//                    // MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                    MediaStore.Images.Media.getContentUri(
//                            MediaStore.VOLUME_EXTERNAL_PRIMARY),
//                    projection,
//                    selection,
//                    selectionArgs,
//                    null
//                )) {
//                    // Cache column indices.
//                    int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
//                    int nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
//
//                    while (cursor.moveToNext()) {
//                        // Get values of columns for a given image.
//                        long id = cursor.getLong(idColumn);
//                        String name = cursor.getString(nameColumn);
//                        Log.d(TAG, "mediaStore image name: " + name);
//
//                        contentUri = ContentUris.withAppendedId(
//                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
//
////                        try {
////                            photo = mContext.getApplicationContext().getContentResolver().loadThumbnail(
////                                    contentUri, new Size(640, 480), null);
////                        } catch (IOException e) {
////                            e.printStackTrace();
////                        }
//                    }
//                }
//            } else {
                File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
                File imageSrc = new File(storageDir, currentImage.getPath() + "/" + currentImage.getName());
                Log.d(TAG, "bindTo: imageSrc: " + imageSrc);

                if (imageSrc.exists()) {
                    // BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    // photo = BitmapFactory.decodeFile(imageSrc.getAbsolutePath(), bmOptions);
                    contentUri = Uri.fromFile(imageSrc);
                }
//            }
//
//            if (photo != null) {
//                mImageView.setImageBitmap(photo);
//            }

            if (contentUri != null) {
                mImageView.setTag(contentUri);

                Glide.with(mContext)
                        .load(contentUri)
                        .centerCrop()
                        .override(500, 500)
                        .into(mImageView);
            }
        }

        @Override
        public void onClick(View v) {
            Uri imageUri = (Uri) mImageView.getTag();

            Intent fullScreenImageIntent = new Intent(mContext, FullScreenImageActivity.class);
            fullScreenImageIntent.setData(imageUri);
            mContext.startActivity(fullScreenImageIntent);
        }
    }
}
