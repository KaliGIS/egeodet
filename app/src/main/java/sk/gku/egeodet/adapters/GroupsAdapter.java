package sk.gku.egeodet.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import sk.gku.egeodet.models.Group;

import java.util.ArrayList;
import java.util.List;

public class GroupsAdapter extends ArrayAdapter<Group> {
    private static final String TAG = "GroupsAdapter";
    private List<Group> mGroups = new ArrayList<>();
    private Context mContext;

    public GroupsAdapter(Context context) {
        super(context, android.R.layout.simple_spinner_dropdown_item);
        mContext = context;
    }

    public void setGroups(List<Group> groups) {
        mGroups = groups;
    }

    @Override
    public int getCount() {
        return mGroups.size();
    }

    @Override
    public Group getItem(int position) {
        return mGroups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView itemTV = (TextView) super.getView(position, convertView, parent);
        Group currGroup = mGroups.get(position);
        itemTV.setText(currGroup.getName());
        return itemTV;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView itemTV = (TextView) super.getView(position, convertView, parent);
        Group currGroup = mGroups.get(position);
        itemTV.setText(currGroup.getName());
        return itemTV;
    }
}
