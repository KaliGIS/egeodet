package sk.gku.egeodet.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.GeodeticPointSelectionActivity;
import sk.gku.egeodet.models.Mission;

import java.util.List;

public class MissionListAdapter extends RecyclerView.Adapter<MissionListAdapter.ViewHolder> {
    private static final String TAG = "MissionListAdapter";
    private List<Mission> mMissions;
    private Context mContext;


    public MissionListAdapter(Context context, List<Mission> missions) {
        this.mMissions = missions;
        this.mContext = context;
    }

    public void setMissions(List<Mission> missions) {
        Log.d(TAG, "onChanged: Setting missions");
        for (int i = 0; i < missions.size(); i++) {
            Log.d(TAG, missions.get(i).getName());
        }
        mMissions = missions;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MissionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.recyclerview_item_mission, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MissionListAdapter.ViewHolder holder, int position) {
        // Get current mission.
        Mission currentMission = mMissions.get(position);

        // Populate the textviews with data.
        holder.bindTo(currentMission);
    }

    @Override
    public int getItemCount() {
        return mMissions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Member Variables for the TextViews
        private TextView mNameText;

        /**
         * Constructor for the ViewHolder, used in onCreateViewHolder().
         *
         * @param itemView The rootview of the recyclerview_item_mission.xml layout file.
         */
        ViewHolder(View itemView) {
            super(itemView);

            // Initialize the views.
            mNameText = itemView.findViewById(R.id.missionName);

            itemView.setOnClickListener(this);
        }

        void bindTo(Mission currentMission) {
            // Populate the textview with data.
            mNameText.setText(currentMission.getName() + ": " + currentMission.getYear());
        }

        @Override
        public void onClick(View v) {
            Mission currentMission = mMissions.get(getAdapterPosition());

            Intent geodeticPointIntent = new Intent(mContext, GeodeticPointSelectionActivity.class);
            geodeticPointIntent.putExtra("missionId", currentMission.getId());
            geodeticPointIntent.putExtra("missionName", currentMission.getName());
            geodeticPointIntent.putExtra("missionYear", currentMission.getYear());
            mContext.startActivity(geodeticPointIntent);
        }
    }
}
