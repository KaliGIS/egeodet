package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.TaskDao;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class TaskRepository {
    public static final String TAG = "TaskRepository";
    private TaskDao mTaskDao;
    private LiveData<List<Task>> mTasks;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;
    private DbSyncAPI mDbSyncAPI;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    public TaskRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mTaskDao = db.taskDao();
        mTasks = mTaskDao.getTasks();
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
        mDbSyncAPI = new DbSyncAPI(application);
    }

    public LiveData<List<Task>> getTasks() {
        return mTasks;
    }

    public void insert(Task task) {
        TaskRepository.InsertAsyncTask asyncTask = new TaskRepository.InsertAsyncTask(mTaskDao);
        asyncTask.delegate = this;
        asyncTask.execute(task);
    }

    private static class InsertAsyncTask extends AsyncTask<Task, Void, HashMap<String, Integer>> {
        private TaskDao asyncTaskDao;
        private TaskRepository delegate = null;

        InsertAsyncTask(TaskDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected HashMap<String, Integer> doInBackground(final Task... params) {
            HashMap<String, Integer> modelIdData = new HashMap<>();
            long id = asyncTaskDao.insert(params[0]);
            modelIdData.put(Constants.TASK_MODEL, (int) id);

            return modelIdData;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            for (String modelName : modelIdData.keySet()) {
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }
        }
    }

    public AsyncTask insertChunk(List<Task> tasks) {
        TaskRepository.InsertChunkAsyncTask task = new TaskRepository.InsertChunkAsyncTask(mTaskDao);
        task.execute(tasks.toArray());
        return task;
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Void> {
        private static final String TAG = "InsertAsyncTask";
        private TaskDao mDao;

        InsertChunkAsyncTask(TaskDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... tasks) {
            int count = tasks.length;

            for (int i = 0; i < count; i++) {
                Task currTask = (Task) tasks[i];
                long id = mDao.insert(currTask);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currTask);
                }

                Log.d(TAG, "doInBackground: task inserted, name: " + currTask.getName());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    public void downloadTasks(Date lastModifiedDate, final TaskRepository.SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncAPI.getTasks(1, 100, null, lastModifiedDate, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> tasksChunk, int totalCount) {
                processedCount[0] += tasksChunk.size();
                insertChunk((List<Task>) tasksChunk);
            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> geodeticPointTasksChunk, List<?> geodeticPointsChunk, List<?> missionsChunk, List<?> tasksChunk, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: Tasks downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }
        });
    }

    public void deleteAll() {
        TaskRepository.DeleteAllAsyncTask task = new TaskRepository.DeleteAllAsyncTask(mTaskDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        TaskDao mDao;

        DeleteAllAsyncTask(TaskDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
