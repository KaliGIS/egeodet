package sk.gku.egeodet.repositories;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.lifecycle.LiveData;
import androidx.room.Ignore;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseDao;
import sk.gku.egeodet.interfaces.GeodeticPointDao;
import sk.gku.egeodet.interfaces.GeodeticPointMissionDao;
import sk.gku.egeodet.interfaces.ImageDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.ToSynchronizeEntityDao;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class ImageRepository extends BaseRepository {
    private static final String TAG = "ImageRepository";
    private ImageDao mImageDao;
    private GeodeticPointDao mGeodeticPointDao;
    private GeodeticPointMissionDao mGeodeticPointMissionDao;
    private MissionDao mMissionDao;
    private ToSynchronizeEntityDao mToSynchronizeEntityDao;
    private LiveData<List<Image>> mImages;
    private LiveData<List<Image>> mImageSearchResults;
    private Integer mMissionId;
    private ProgressDialog mProgressDialog;
    private Context mContext;
    private DbSyncAPI mDbSyncApi;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    private RepositoryCallBack mRepositoryCallback;

    public interface RepositoryCallBack
    {
        void after(Boolean shouldProcess);
    }

    public ImageRepository(Context context, int missionId, int geodeticPointId) {
        mContext = context;
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(context);
        mImageDao = db.imageDao();
        mGeodeticPointDao = db.geodeticPointDao();
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mToSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mMissionId = missionId;
        mImages = mImageDao.getImages();
        mImageSearchResults = mImageDao.getImagesByMissionIdAndGeodeticPointId(missionId, geodeticPointId);
        mRepositoryCallback = null;
        mDbSyncApi = null;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(context);
    }

    @Ignore
    public ImageRepository(Context context, int missionId, RepositoryCallBack repositoryCallBack) {
        mContext = context;
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(context);
        mImageDao = db.imageDao();
        mGeodeticPointDao = db.geodeticPointDao();
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mToSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mMissionId = missionId;
        mImages = mImageDao.getImages();
        mRepositoryCallback = repositoryCallBack;
        mDbSyncApi = null;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(context);
    }

    @Ignore
    public ImageRepository(Application application) {
        mContext = application.getApplicationContext();
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(mContext);
        mImageDao = db.imageDao();
        mGeodeticPointDao = db.geodeticPointDao();
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mToSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mMissionId = null;
        mImages = mImageDao.getImages();
        mImageSearchResults = null;
        mRepositoryCallback = null;
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(mContext);
    }

    public LiveData<List<Image>> getImages() {
        return mImages;
    }

    public LiveData<List<Image>> getImagesByMissionIdAndGeodeticPointId() { return mImageSearchResults; }

    public void insert(Image image) {
        new ImageRepository.InsertAsyncTask(mImageDao).execute(image);
    }

    private static class InsertAsyncTask extends AsyncTask<Image, Void, Integer> {
        private ImageDao mDao;
        private ImageRepository delegate = null;

        InsertAsyncTask(ImageDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(final Image... params) {
            long imageId = mDao.insert(params[0]);
            return (int) imageId;
        }

        @Override
        protected void onPostExecute(Integer imageId) {
            if (imageId > -1) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.IMAGE_MODEL, imageId);
            }
        }
    }

    public void insertIfNotExist(Image image) {
        ImageRepository.InsertIfNotExistAsyncTask task = new ImageRepository.InsertIfNotExistAsyncTask(mImageDao, mGeodeticPointMissionDao, mMissionDao);
        task.delegate = this;
        task.execute(image);
    }

    private class InsertIfNotExistAsyncTask extends AsyncTask<Image, Void, HashMap<String, Integer>> {
        private ImageDao mImageDao;
        private GeodeticPointMissionDao mGeodeticPointMissionDao;
        private MissionDao mMissionDao;
        private ImageRepository delegate = null;

        InsertIfNotExistAsyncTask(ImageDao imageDao, GeodeticPointMissionDao geodeticPointMissionDao, MissionDao missionDao) {
            mImageDao = imageDao;
            mGeodeticPointMissionDao = geodeticPointMissionDao;
            mMissionDao = missionDao;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            boolean isInserted = false;

            for (String modelName : modelIdData.keySet()) {
                if (modelName.equals(Constants.IMAGE_MODEL)) {
                    isInserted = true;
                }
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }

            if (mRepositoryCallback != null) {
                mRepositoryCallback.after(isInserted);
            }
        }

        @Override
        protected HashMap<String, Integer> doInBackground(final Image... params) {
            Image image = params[0];
            Integer imageCount = null;
            HashMap<String, Integer> modelIdData = new HashMap<>();

            if (image != null) {
                imageCount = mImageDao.getCountByAttributes(image.getName(), image.getPath(), image.getGeodeticPointId(), image.getMissionId());
            }

            Log.d(TAG, "doInBackground: existing image count is: " + imageCount);
            if (imageCount != null && imageCount == 0) {
                mGeodeticPointMissionDao.updateStatus(image.getGeodeticPointId(), image.getMissionId(), 1);
                mMissionDao.updateModified(image.getMissionId());
                Integer[] geodeticPointMissionIds = mGeodeticPointMissionDao.getGeodeticPointMissionIdsByGeodeticPointIdAndMissionId(image.getGeodeticPointId(), image.getMissionId());
                if (geodeticPointMissionIds.length > 0) {
                    modelIdData.put(Constants.GEODETIC_POINT_MISSION_MODEL, geodeticPointMissionIds[0]);
                }

                long newId = mImageDao.insert(image);
                if (newId > -1) {
                    modelIdData.put(Constants.IMAGE_MODEL, (int) newId);
                }
            }

            return modelIdData;
        }
    }

    public void insertChunk(List<Image> images) {
        ImageRepository.InsertChunkAsyncTask task = new ImageRepository.InsertChunkAsyncTask(mImageDao);
        task.execute(images.toArray(new Image[images.size()]));
    }

    private class InsertChunkAsyncTask extends AsyncTask<Image, Void, Void> {
        ImageDao mDao;

        InsertChunkAsyncTask(ImageDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Image... images) {
            int count = images.length;

            for (int i = 0; i < count; i++) {
                Image currImage = (Image) images[i];
                if (mDao.geodeticPointExists(currImage.getGeodeticPointId())) {
                    long id = mDao.insert(currImage);
                    if (id == -1) {
                        Integer updatedCount = mDao.update(currImage);
                    }
                } else {
                    Log.w(TAG, "doInBackground: Geodetic point with id " + currImage.getGeodeticPointId() + " does not exist, but image connected to that geodetiPointId has tried to be inserted");
                }
            }

            return null;
        }
    }

    public void deleteImage(Image image) {
        ImageRepository.DeleteImageAsyncTask task = new ImageRepository.DeleteImageAsyncTask(mImageDao, mGeodeticPointMissionDao, mMissionDao, mToSynchronizeEntityDao);
        task.delegate = this;
        task.execute(image);
    }

    private static class DeleteImageAsyncTask extends AsyncTask<Image, Void, HashMap<String, Integer>> {
        private ImageDao mImageDao;
        private GeodeticPointMissionDao mGeodeticPointMissionDao;
        private MissionDao mMissionDao;
        private ToSynchronizeEntityDao mToSynchronizeEntityDao;
        private ImageRepository delegate = null;

        DeleteImageAsyncTask(ImageDao imageDao, GeodeticPointMissionDao geodeticPointMissionDao, MissionDao missionDao,
                             ToSynchronizeEntityDao toSynchronizeEntityDao) {
            mImageDao = imageDao;
            mGeodeticPointMissionDao = geodeticPointMissionDao;
            mMissionDao = missionDao;
            mToSynchronizeEntityDao = toSynchronizeEntityDao;
        }

        @Override
        protected HashMap<String, Integer> doInBackground(Image... images) {
            Image image = images[0];
            HashMap<String, Integer> modelIdData = new HashMap<>();

            if (image != null) {
                mImageDao.delete(image);
                mGeodeticPointMissionDao.updateStatus(image.getGeodeticPointId(), image.getMissionId(), 1);
                mMissionDao.updateModified(image.getMissionId());
                Integer[] geodeticPointMissionIds =
                        mGeodeticPointMissionDao.getGeodeticPointMissionIdsByGeodeticPointIdAndMissionId(image.getGeodeticPointId(), image.getMissionId());
                if (geodeticPointMissionIds.length > 0) {
                    modelIdData.put(Constants.GEODETIC_POINT_MISSION_MODEL, geodeticPointMissionIds[0]);
                }
                modelIdData.put(Constants.IMAGE_MODEL, image.getId());

                ToSynchronizeEntity toSynchronizeEntity = mToSynchronizeEntityDao.getToSynchronizeEntityByModelNameAndEntityId(Constants.IMAGE_MODEL,image.getId());
                if (toSynchronizeEntity != null) {
                    mToSynchronizeEntityDao.delete(toSynchronizeEntity);
                }
            }

            return modelIdData;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            for (String modelName : modelIdData.keySet()) {
                if (modelName.equals(Constants.GEODETIC_POINT_MISSION_MODEL) && delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
                if (modelName.equals(Constants.IMAGE_MODEL) && delegate != null) {
                    delegate.mToSynchronizeEntityRepository.deleteByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }
        }
    }

    public void deleteByIds(Integer... ids) {
        ImageRepository.DeleteByIdsAsyncTask task = new ImageRepository.DeleteByIdsAsyncTask(mImageDao);
        try {
            task.execute(ids).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private ImageDao mAsyncTaskDao;

        DeleteByIdsAsyncTask(ImageDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mAsyncTaskDao.deleteByIds(ids);
            return null;
        }
    }

    public void checkIfShouldAddNewPhotosToDb(int mMissionId, String mMissionName, int mMissionYear) {
        new ImageRepository.CheckIfShouldAddNewPhotosToDbAsyncTask(mImageDao, mMissionId, mMissionName, mMissionYear).execute();
    }

    private class CheckIfShouldAddNewPhotosToDbAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private int mMissionId;
        private String mMissionName;
        private int mMissionYear;
        private ImageDao mDao;

        public CheckIfShouldAddNewPhotosToDbAsyncTask(ImageDao imageDao, int missionId, String missionName, int missionYear) {
            mDao = imageDao;
            mMissionId = missionId;
            mMissionName = missionName;
            mMissionYear = missionYear;
        }

        @Override
        protected void onPostExecute(Boolean shouldNotify) {
            super.onPostExecute(shouldNotify);
            mRepositoryCallback.after(shouldNotify);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            File currPath = new File(getExternalStoragePublicDirectory("/eGeodetImages"), mMissionName + "_" + mMissionYear);
            List<File> photoSrcs = new ArrayList<>();
            boolean shouldAdd = false;

            photoSrcs.addAll(getRecursivelyAllPhotoSourcesInPath(currPath));

            Log.d(TAG, "shouldAddNewPhotosToDb: CurrPath: " + currPath.getAbsolutePath());
            for (File photoSrc : photoSrcs) {
                Log.d(TAG, "shouldAddNewPhotosToDb: PhotoName: " + photoSrc.getName());
                String[] photoPathParts = photoSrc.getAbsolutePath().split("eGeodetImages/");
                String photoRelSrcFS = photoPathParts[photoPathParts.length - 1];
                String[] photoRelPathParts = photoRelSrcFS.split("/" + photoSrc.getName());
                String photoRelPathFS = photoRelPathParts[0];
                Log.d(TAG, "doInBackground: photo rel path FS: " + photoRelPathFS);
                String geodeticPointLabelRaw = photoSrc.toString().split(currPath.getAbsolutePath())[1].split(photoSrc.getName())[0];
                String geodeticPointLabel = geodeticPointLabelRaw.substring(1, geodeticPointLabelRaw.length() - 1);
                Log.d(TAG, "doInBackground: finding geodetic point by label: " + geodeticPointLabel);
                GeodeticPoint[] geodeticPointsRes = mGeodeticPointDao.getGeodeticPointsByLabel(geodeticPointLabel);

                Integer count = mDao.getCountByPathAndName(photoRelPathFS, photoSrc.getName());
                if (count.equals(0) && geodeticPointsRes.length > 0) {
                    Log.d(TAG, "doInBackground: photo does not exist in database: " + photoRelSrcFS);
                    shouldAdd = true;
                }
            }

            return shouldAdd;
        }
    }

    /**
     * Searches recursively for photo files in the given path.
     * The subdir "raw" is not taken into account.
     *
     * @param path
     * @return
     *
     * @author martin.kalioda
     */
    public static List<File> getRecursivelyAllPhotoSourcesInPath(File path) {
        List<File> photoSrcs = new ArrayList<>();
        List<File> currContent = new ArrayList<>();

        if (path.exists()) {
            File[] currContentArr = path.listFiles();
            currContent.addAll(Arrays.asList(currContentArr));
        } else {
            return photoSrcs;
        }

        for (File item : currContent) {
            if (item.isDirectory() && !item.getName().equals("raw")) {
                Log.d(TAG, "getRecursivelyAllPhotoSourcesInPath: item is directory: " + item);
                photoSrcs.addAll(getRecursivelyAllPhotoSourcesInPath(item));
            } else if (item.isFile()) {
                Log.d(TAG, "getRecursivelyAllPhotoSourcesInPath: item is file: " + item);
                Log.d(TAG, "getRecursivelyAllPhotoSourcesInPath: MIME type of file: " + getMimeType(item.getAbsolutePath()));
                if (getMimeType(item.getAbsolutePath()).equals("image/jpeg")) {
                    photoSrcs.add(item);
                }
            }
        }

        return photoSrcs;
    }

    public static String getMimeType(String src) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(src);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public void findNewImagesAndAddThemToDatabase(String missionName, int missionYear) {
        new FindNewImagesAndAddThemToDatabaseAsyncTask(mGeodeticPointDao, mMissionId, missionName, missionYear).execute();
    }

    private class FindNewImagesAndAddThemToDatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
        private GeodeticPointDao mGeodeticPointDao;
        private String mMissionName;
        private int mMissionYear;
        private int mMissionId;

        public FindNewImagesAndAddThemToDatabaseAsyncTask(GeodeticPointDao geodeticPointDao, int missionId, String missionName, int missionYear) {
            mGeodeticPointDao = geodeticPointDao;
            mMissionName = missionName;
            mMissionYear = missionYear;
            mMissionId = missionId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = ProgressDialog.show(mContext,"Fotografie sa aktualizujú!", "Prosím čakajte...",false,false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // TODO implement for Android 10 and more
            File imagesRootPath = getExternalStoragePublicDirectory("/eGeodetImages");
            File currPath = new File(imagesRootPath, mMissionName + "_" + mMissionYear);
            List<File> photoSrcs = getRecursivelyAllPhotoSourcesInPath(currPath);

            for (File photoSrc : photoSrcs) {
                Log.d(TAG, "doInBackground: photoSrc: " + photoSrc.getAbsolutePath());
                String imageName = photoSrc.getName();

                String imageRelSrc = photoSrc.toString().split(imagesRootPath.getAbsolutePath())[1];
                String imageRelPathRaw = imageRelSrc.split(imageName)[0];
                String imageRelPath = imageRelPathRaw.substring(1, imageRelPathRaw.length() - 1);

                String geodeticPointLabelRaw = photoSrc.toString().split(currPath.getAbsolutePath())[1]
                        .split(imageName)[0];
                String geodeticPointLabel = geodeticPointLabelRaw.substring(1, geodeticPointLabelRaw.length() - 1);
                GeodeticPoint[] geodeticPointsRes = mGeodeticPointDao.getGeodeticPointsByLabel(geodeticPointLabel);

                if (geodeticPointsRes.length > 0) {
                    GeodeticPoint geodeticPoint = mGeodeticPointDao.getGeodeticPointsByLabel(geodeticPointLabel)[0];

                    Image image = new Image(imageName, imageRelPath, geodeticPoint.getId(), mMissionId);
                    Log.d(TAG, "doInBackground: image to insert: ");
                    Log.d(TAG, "doInBackground: name: " + image.getName());
                    Log.d(TAG, "doInBackground: path: " + image.getPath());
                    Log.d(TAG, "doInBackground: geodetic point id: " + image.getGeodeticPointId());
                    Log.d(TAG, "doInBackground: mission id: " + image.getMissionId());

                    insertIfNotExist(image);
                } else {
                    Log.w(TAG, "doInBackground: WARNING: No records found for the geodetic point label " + geodeticPointLabel);
                }
            }

            return null;
        }
    }

    public void getAndSynchronizeById(Integer id, DbSyncAPI.SyncCallback callback) {
        Log.d(TAG, "getAndSynchronizeById: image id: " + id);
        ImageRepository.QueryAsyncTask task = new ImageRepository.QueryAsyncTask(mImageDao);
        List<Image> images;

        try {
            images = task.execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        }

        Log.d(TAG, "getAndSynchronizeById: images to sync size: " + images.size());
        if (images.size() > 0) {
            Image image = images.get(0);
            File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
            String imageRelSrc = image.getPath() + "/" + image.getName();
            File imageFile = new File(storageDir, imageRelSrc);
            mDbSyncApi.syncImageData(image, imageFile, callback);
        }
    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, List<Image>> {
        private ImageDao asyncTaskDao;

        QueryAsyncTask(ImageDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<Image> doInBackground(final Integer... ids) {
            return asyncTaskDao.getImagesByIds(ids);
        }
    }

    public List<Image> getImagesByGeoPntId(Integer geodeticPointId) {
        ImageRepository.GetImagesByGeoPntIdAsyncTask task =
                new ImageRepository.GetImagesByGeoPntIdAsyncTask(mImageDao);
        List<Image> images = new ArrayList<>();

        try {
            images = task.execute(geodeticPointId).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return images;
    }

    private class GetImagesByGeoPntIdAsyncTask extends AsyncTask<Integer, Void, List<Image>> {
        private ImageDao mAsyncTaskDao;

        public GetImagesByGeoPntIdAsyncTask(ImageDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<Image> doInBackground(Integer... integers) {
            int geodeticPointId = integers[0];
            List<Image> images;

            images = mAsyncTaskDao.getImagesByGeoPntId(geodeticPointId);

            return images;
        }
    }

    public void synchronize(Image image, DbSyncAPI.SyncCallback callback) {
        File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
        String imageRelSrc = image.getPath() + "/" + image.getName();
        File imageFile = new File(storageDir, imageRelSrc);
        mDbSyncApi.syncImageData(image, imageFile, callback);
    }

    public List<Image> getByIds(List<Integer> ids) {
        ImageRepository.QueryAsyncTask task = new ImageRepository.QueryAsyncTask(mImageDao);
        List<Image> images = new ArrayList<>();
        Integer[] idsArr = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            idsArr[i] = ids.get(i);
        }

        try {
            images = task.execute(idsArr).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return images;
    }

    private class GetImagesByIdsAsyncTask extends AsyncTask<Integer, Void, List<Image>> {
        private ImageDao mAsyncTaskDao;

        public GetImagesByIdsAsyncTask(ImageDao dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected List<Image> doInBackground(Integer... integers) {
            return mAsyncTaskDao.getImagesByIds(integers);
        }
    }

//    public void deleteById(Integer... ids) {
//        ImageRepository.DeleteByIdsAsyncTask task = new ImageRepository.DeleteByIdsAsyncTask(mImageDao);
//        task.execute(ids);
//    }
//
//    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
//        private ImageDao mAsyncTaskDao;
//
//        DeleteByIdsAsyncTask(ImageDao dao) {
//            mAsyncTaskDao = dao;
//        }
//
//        @Override
//        protected Void doInBackground(Integer... ids) {
//            mAsyncTaskDao.deleteByIds(ids);
//            return null;
//        }
//    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask((BaseDao) mImageDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void batchChangeIds(List<ToSynchronizeEntity> toSynchronizeEntities) {
        List<Integer> entityIds = new ArrayList<>();
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            entityIds.add(toSynchronizeEntity.getEntityId());
        }
        List<Image> cachedEntities = getByIds(entityIds);
        deleteByIds(entityIds.toArray(new Integer[entityIds.size()]));
        for (Image entity : cachedEntities) {
            for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
                if (toSynchronizeEntity.getEntityId() == entity.getId()) {
                    Log.d(TAG, "batchChangeIds: origId: " + entity.getId() + ", newId: " + toSynchronizeEntity.getMasterId());
                    entity.setId(toSynchronizeEntity.getMasterId());
                    break;
                }
            }
        }
        insertChunk(cachedEntities);
    }

    public void absIds() {
        new AbsIdsAsyncTask(mImageDao).execute();
    }

    public void deleteAll() {
        ImageRepository.DeleteAllAsyncTask task = new ImageRepository.DeleteAllAsyncTask(mImageDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        ImageDao mDao;

        DeleteAllAsyncTask(ImageDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }

    public void downloadImages(Date lastModifiedDate, List<Integer> missionIds, final GeodeticPointTaskRepository.SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncApi.getImages(1, 100, lastModifiedDate, missionIds, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> imagesChunk, int totalCount) {
                processedCount[0] += imagesChunk.size();
                insertChunk((List<Image>) imagesChunk);
            }

            @Override
            public void onDataChunk(List<?> imagesChunk, List<?> geodeticPointsChunk, List<?> missionsChunk, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: images downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }
        });
    }
}
