package sk.gku.egeodet.repositories;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.SynchronizationDao;
import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class SynchronizationRepository {
    private static final String TAG = "SynchronizationRepository";
    private SynchronizationDao mSynchronizationDao;
    private LiveData<List<Synchronization>> mSynchronizations;
    private MutableLiveData<List<Synchronization>> mSearchResults;
    private Context mContext;
    private Boolean synchronizing = false;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    public SynchronizationRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mSynchronizationDao = db.synchronizationDao();
        mContext = application.getApplicationContext();
    }

    public MutableLiveData<List<Synchronization>> getSearchResults() {
        return mSearchResults;
    }

    private void asyncFinished(List<Synchronization> results) {
        mSearchResults.setValue(results);
    }

    public Synchronization getSynchronizationBySyncKey(String syncKey) throws ExecutionException, InterruptedException {
        SynchronizationRepository.GetSynchronizationBySyncKey task =
                new SynchronizationRepository.GetSynchronizationBySyncKey(mSynchronizationDao);
        Synchronization[] synchronizations = task.execute(syncKey).get();

        if (synchronizations.length > 0) {
            return synchronizations[0];
        } else {
            return null;
        }
    }

    private class GetSynchronizationBySyncKey extends AsyncTask<String, Void, Synchronization[]> {
        private SynchronizationDao asyncTaskDao;

        GetSynchronizationBySyncKey(SynchronizationDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Synchronization[] doInBackground(final String... syncKeys) {
            return asyncTaskDao.getSynchronizationBySyncKey(syncKeys[0]);
        }

        @Override
        protected void onPostExecute(Synchronization[] result) {

        }
    }

    public void upsertSynchronizationEntity(Synchronization synchronization) {
        UpsertSynchronizationEntity task = new UpsertSynchronizationEntity(mSynchronizationDao);
        task.execute(synchronization);
    }

    private class UpsertSynchronizationEntity extends AsyncTask<Synchronization, Void, Void> {
        private SynchronizationDao asyncTaskDao;

        UpsertSynchronizationEntity(SynchronizationDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Synchronization... synchronizations) {
            Log.d("UPSERT_SYNC", synchronizations[0].toString());
            long id = asyncTaskDao.insert(synchronizations[0]);
            if (id == -1) {
                Integer updatedCount = asyncTaskDao.update(synchronizations[0]);
            }

            return null;
        }
    }

    public void insertSynchronizationEntity(Synchronization synchronization) {
        InsertSynchronizationEntity task = new InsertSynchronizationEntity(mSynchronizationDao);
        task.execute(synchronization);
    }

    private class InsertSynchronizationEntity extends AsyncTask<Synchronization, Void, Long> {
        private SynchronizationDao asyncTaskDao;

        InsertSynchronizationEntity(SynchronizationDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final Synchronization... synchronizations) {
            long id = asyncTaskDao.insert(synchronizations[0]);
            if (id == -1) {
                Integer updatedCount = asyncTaskDao.update(synchronizations[0]);
            }
            return id;
        }

        @Override
        protected void onPostExecute(Long result) {

        }
    }

    public void syncSynchronization(final Synchronization synchronization, final SyncCallback callback) {
        Log.d("SynchronizationRepo", "syncSynchronization: To synchronize: " + synchronization);
        Log.d("SynchronizationRepo", "syncSynchronization: start_date: " + synchronization.getStartDate());
        DbSyncAPI dbSyncAPI = new DbSyncAPI(mContext);
        dbSyncAPI.syncSynchronizationData(synchronization, new DbSyncAPI.SyncCallback() {
            @Override
            public void onDone(Object syncedItem, Integer origId) {
                Synchronization syncedSynchronization = (Synchronization) syncedItem;
                upsertSynchronizationEntity(syncedSynchronization);
                if (synchronization.getId() != syncedSynchronization.getId()) {
                    deleteSynchronization(synchronization);
                }
                Log.d("SynchronizationRepo", "syncedSynchronization " + syncedSynchronization);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(String errorStr, String verbose, Integer origId) {
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }
        });
    }

    public void deleteSynchronization(Synchronization synchronization) {
        SynchronizationRepository.DeleteSynchronizationAsyncTask task =
                new SynchronizationRepository.DeleteSynchronizationAsyncTask(mSynchronizationDao);
        task.execute(synchronization);
    }

    private class DeleteSynchronizationAsyncTask extends AsyncTask<Synchronization, Void, Void> {
        SynchronizationDao mAsyncTaskDao;

        DeleteSynchronizationAsyncTask(SynchronizationDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Synchronization... synchronizations) {
            mAsyncTaskDao.delete(synchronizations[0]);
            return null;
        }
    }
}

