package sk.gku.egeodet.repositories;

import android.os.AsyncTask;
import android.util.Log;

import sk.gku.egeodet.interfaces.BaseDao;

public class BaseRepository {
    protected static String TAG = "BaseRepo";

    protected class ChangeIdAsyncTask <T extends BaseDao> extends AsyncTask<Integer, Void, Void> {
        private T mAsyncTaskDao;

        ChangeIdAsyncTask(T dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            Log.d(TAG, "doInBackground: Count of ids: " + ids.length);
            Integer origId = (Integer) ids[0];
            Integer newId = (Integer) ids[1];

            mAsyncTaskDao.updateId(origId, newId);

            return null;
        }
    }

    protected class AbsIdsAsyncTask <T extends BaseDao> extends AsyncTask<Object, Void, Void> {
        private T mAsyncTaskDao;

        AbsIdsAsyncTask(T dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Object... voids) {
            mAsyncTaskDao.absIds();
            return null;
        }
    }
}
