package sk.gku.egeodet.repositories;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.NoteDao;
import sk.gku.egeodet.models.JoinNoteUser;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class NoteRepository extends BaseRepository {
    private static final String TAG = "NoteRepository";
    private MutableLiveData<List<Note>> mNoteSearchResults = new MutableLiveData<>();
    private NoteDao mNoteDao;
    private MissionDao mMissionDao;
    private LiveData<List<Note>> mNotes;
    private LiveData<List<JoinNoteUser>> mJoinNoteUserResults;
    private DbSyncAPI mDbSyncApi;
    private Application mApplication;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    public NoteRepository(Application application, int missionId, int geodeticPointId) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mNoteDao = db.noteDao();
        mMissionDao = db.missionDao();
        mNotes = mNoteDao.getNotes();
        mJoinNoteUserResults = mNoteDao.getNotesWithUsersByMissionIdAndGeodeticPointId(missionId, geodeticPointId);
        mDbSyncApi = null;
        mApplication = application;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
    }

    public NoteRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mNoteDao = db.noteDao();
        mMissionDao = db.missionDao();
        mNotes = mNoteDao.getNotes();
        mJoinNoteUserResults = null;
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
        mApplication = application;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
    }

    public LiveData<List<Note>> getNotes() {
        return mNotes;
    }

    public MutableLiveData<List<Note>> getNoteSearchResults() {
        return mNoteSearchResults;
    }

    public LiveData<List<JoinNoteUser>> getNotesWithUsersByMissionIdAndGeoPntId() {
        return mJoinNoteUserResults;
    }

    private void asyncNoteFinished(List<Note> results) {
        mNoteSearchResults.setValue(results);
    }

    public void getNotesByGeodeticPointIdAndMissionId(Integer geodeticPointId, Integer missionId) {
        NoteRepository.QueryAsyncTask task = new NoteRepository.QueryAsyncTask(mNoteDao);
        task.delegate = this;
        task.execute(geodeticPointId, missionId);
    }

    private static class QueryAsyncTask extends AsyncTask<Integer, Void, List<Note>> {
        private NoteDao asyncTaskDao;
        private NoteRepository delegate = null;

        QueryAsyncTask(NoteDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<Note> doInBackground(final Integer... params) {
            return asyncTaskDao.getNotesByGeodeticPointIdAndMissionId(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(List<Note> result) {
            delegate.asyncNoteFinished(result);
        }
    }

    public void insertNote(Note note) {
        NoteRepository.InsertAsyncTask task = new NoteRepository.InsertAsyncTask(mNoteDao, mMissionDao);
        task.delegate = this;
        task.execute(note);
    }

    private static class InsertAsyncTask extends AsyncTask<Object, Void, List<HashMap<String, Integer>>> {
        private NoteDao mAsyncTaskDao;
        private MissionDao mMissionDao;
        private NoteRepository delegate = null;

        InsertAsyncTask(NoteDao dao, MissionDao missionDao) {
            mAsyncTaskDao = dao;
            mMissionDao = missionDao;
        }

        @Override
        protected List<HashMap<String, Integer>> doInBackground(final Object... notes) {
            int count = notes.length;
            List<HashMap<String, Integer>> modelIdData = new ArrayList<>();

            for (int i = 0; i < count; i++) {
                Note currNote = (Note) notes[i];
                Log.d(TAG, "doInBackground: note to insert: " + currNote.toString());
                try {
                    Integer id = (int) mAsyncTaskDao.insert(currNote);
                    if (id == -1) {
                        Integer updatedCount = mAsyncTaskDao.update(currNote);
                        id = currNote.getId();
                    }
                    mMissionDao.updateModified(currNote.getMissionId());
                    HashMap<String, Integer> modelIdMap = new HashMap<>();
                    modelIdMap.put(Constants.NOTE_MODEL, id);
                    modelIdData.add(modelIdMap);

                    Log.d(TAG, "doInBackground: note inserted, id: " + currNote.getId());
                } catch (SQLiteConstraintException e) {
                    Log.w(TAG, "doInBackground: Foreign key exception raised. Note with id " + currNote.getId() + " was not inserted or updated");
                }
            }

            return modelIdData;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, Integer>> modelIdData) {
            for (HashMap<String, Integer> modelIdMap : modelIdData) {
                for (String modelName : modelIdMap.keySet()) {
                    if (delegate != null) {
                        delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdMap.get(modelName));
                    }
                }
            }
        }
    }

    public void updateNote(Note note) {
        NoteRepository.UpdateNoteAsyncTask task = new NoteRepository.UpdateNoteAsyncTask(mNoteDao, mMissionDao);
        task.delegate = this;
        task.execute(note);
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Note, Void, HashMap<String, Integer>> {
        private NoteDao mAsyncTaskDao;
        private MissionDao mMissionDao;
        private NoteRepository delegate = null;

        UpdateNoteAsyncTask(NoteDao dao, MissionDao missionDao) {
            mAsyncTaskDao = dao;
            mMissionDao = missionDao;
        }

        @Override
        protected HashMap<String, Integer> doInBackground(Note... notes) {
            Note note = notes[0];
            mAsyncTaskDao.update(note);
            mMissionDao.updateModified(note.getMissionId());
            HashMap<String, Integer> modelIdData = new HashMap<>();
            modelIdData.put(Constants.NOTE_MODEL, note.getId());

            return modelIdData;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            for (String modelName : modelIdData.keySet()) {
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }
        }
    }

    public void deleteNote(Note note) {
        NoteRepository.DeleteNoteAsyncTask task = new NoteRepository.DeleteNoteAsyncTask(mNoteDao, mMissionDao);
        task.delegate = this;
        task.execute(note);
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Note, Void, HashMap<String, Integer>> {
        private NoteDao mAsyncTaskDao;
        private MissionDao mMissionDao;
        private NoteRepository delegate = null;

        DeleteNoteAsyncTask(NoteDao dao, MissionDao missionDao) {
            mAsyncTaskDao = dao;
            mMissionDao = missionDao;
        }

        @Override
        protected HashMap<String, Integer> doInBackground(Note... notes) {
            Note note = notes[0];
            mAsyncTaskDao.delete(note);
            mMissionDao.updateModified(note.getMissionId());
            HashMap<String, Integer> modelIdData = new HashMap<>();
            modelIdData.put(Constants.NOTE_MODEL, notes[0].getId());

            return modelIdData;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            for (String modelName : modelIdData.keySet()) {
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.deleteByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }
        }
    }

    public void getAndSynchronizeById(Integer id, DbSyncAPI.SyncCallback callback) {
        NoteRepository.QueryByIdsAsyncTask task = new NoteRepository.QueryByIdsAsyncTask(mNoteDao);
        List<Note> notes;

        try {
            notes = task.execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        }

        if (notes.size() > 0) {
            Note note = notes.get(0);
            mDbSyncApi.syncNoteData(note, callback);
        }
    }

    private class QueryByIdsAsyncTask extends AsyncTask<Integer, Void, List<Note>> {
        private NoteDao asyncTaskDao;

        QueryByIdsAsyncTask(NoteDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<Note> doInBackground(final Integer... ids) {
            return asyncTaskDao.getNotesByIds(ids);
        }
    }

    public void downloadNotes(Date lastModifiedDate, Integer userId, boolean alsoAdminNotes, final NoteRepository.SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncApi.getNotes(1, 100, lastModifiedDate, userId, alsoAdminNotes, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> notesChunk, int totalCount) {
                List<Note> allNotes = (List<Note>) notesChunk;
                List<Note> filteredNotes = filterNotesByExistingUserMissionGeodeticPoint(allNotes);
                processedCount[0] += filteredNotes.size();
                insertChunk(filteredNotes);
            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: Users downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }

        });
    }

    private List<Note> filterNotesByExistingUserMissionGeodeticPoint(List<Note> allNotes) {
        UserRepository userRepository = new UserRepository(mApplication);
        List<Integer> userIds = userRepository.getIds();
        GeodeticPointRepository geodeticPointRepository = new GeodeticPointRepository(mApplication);
        List<Integer> geodeticPointIds = geodeticPointRepository.getIds();
        MissionRepository missionRepository = new MissionRepository(mApplication);
        List<Integer> missionIds = missionRepository.getIds();

        List<Note> filteredNotes = new ArrayList<>();
        for (Note note : allNotes) {
            if (
                userIds.contains(note.getUserId()) &&
                geodeticPointIds.contains(note.getGeodeticPointId()) &&
                missionIds.contains(note.getMissionId())
            ) {
                filteredNotes.add(note);
            }
        }
        return filteredNotes;
    }

    public void insertChunk(List<Note> notes) {
        NoteRepository.InsertAsyncTask task = new NoteRepository.InsertAsyncTask(mNoteDao, mMissionDao);
        try {
            task.execute(notes.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<Note> getNotesByGeoPntId(Integer geodeticPointId) {
        NoteRepository.GetNotesByGeoPntIdAsyncTask task =
                new NoteRepository.GetNotesByGeoPntIdAsyncTask(mNoteDao);
        List<Note> notes = new ArrayList<>();

        try {
            notes = task.execute(geodeticPointId).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return notes;
    }

    private class GetNotesByGeoPntIdAsyncTask extends AsyncTask<Integer, Void, List<Note>> {
        private NoteDao mAsyncTaskDao;

        public GetNotesByGeoPntIdAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<Note> doInBackground(Integer... integers) {
            int geodeticPointId = integers[0];
            List<Note> notes;

            notes = mAsyncTaskDao.getNotesByGeoPntId(geodeticPointId);

            return notes;
        }
    }

    public void synchronize(Note note, DbSyncAPI.SyncCallback callback) {
        mDbSyncApi.syncNoteData(note, callback);
    }

    public List<Note> getByIds(List<Integer> ids) {
        NoteRepository.GetNotesByIdsAsyncTask task = new NoteRepository.GetNotesByIdsAsyncTask(mNoteDao);
        List<Note> notes = new ArrayList<>();
        Integer[] idsArr = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            idsArr[i] = ids.get(i);
        }

        try {
            notes = task.execute(idsArr).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return notes;
    }

    private class GetNotesByIdsAsyncTask extends AsyncTask<Integer, Void, List<Note>> {
        private NoteDao mAsyncTaskDao;

        public GetNotesByIdsAsyncTask(NoteDao dao) {
            this.mAsyncTaskDao = dao;
        }

        @Override
        protected List<Note> doInBackground(Integer... integers) {
            return mAsyncTaskDao.getNotesByIds(integers);
        }
    }

    public void deleteByIds(Integer... ids) {
        NoteRepository.DeleteByIdsAsyncTask task = new NoteRepository.DeleteByIdsAsyncTask(mNoteDao);
        try {
            task.execute(ids).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private NoteDao mAsyncTaskDao;

        DeleteByIdsAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mAsyncTaskDao.deleteByIds(ids);
            return null;
        }
    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask((BaseDao) mNoteDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void batchChangeIds(List<ToSynchronizeEntity> toSynchronizeEntities) {
        List<Integer> entityIds = new ArrayList<>();
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            entityIds.add(toSynchronizeEntity.getEntityId());
        }
        List<Note> cachedEntities = getByIds(entityIds);
        deleteByIds(entityIds.toArray(new Integer[entityIds.size()]));
        for (Note entity : cachedEntities) {
            for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
                if (toSynchronizeEntity.getEntityId() == entity.getId()) {
                    Log.d(TAG, "batchChangeIds: origId: " + entity.getId() + ", newId: " + toSynchronizeEntity.getMasterId());
                    entity.setId(toSynchronizeEntity.getMasterId());
                    break;
                }
            }
        }
        insertChunk(cachedEntities);
    }

    public void absIds() {
        new AbsIdsAsyncTask(mNoteDao).execute();
    }

    public void deleteAll() {
        NoteRepository.DeleteAllAsyncTask task = new NoteRepository.DeleteAllAsyncTask(mNoteDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        NoteDao mDao;

        DeleteAllAsyncTask(NoteDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
