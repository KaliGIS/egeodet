package sk.gku.egeodet.repositories;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.sqlite.db.SimpleSQLiteQuery;

import sk.gku.egeodet.R;
import sk.gku.egeodet.activities.MainActivity;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.RawDao;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.AlarmReveiver;

import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

public class RawRepository {
    public static final String TAG = "RawRepository";
    private RawDao mRawDao;
    private eGeodetRoomDatabase mDb;
    private Activity mActivity;
    private NotificationManager mNotifyManager;
    private Application mApplication;

    public RawRepository(Application application, Activity activity) {
        mApplication = application;
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mActivity = activity;
        mRawDao = db.rawDao();
        mDb = db;

        createNotificationChannel();
    }

    public void commitSqlStatements(List<String> sqlStatements) {
        RawRepository.RawQueryAsyncTask task = new RawRepository.RawQueryAsyncTask(mDb, mRawDao, mActivity);
        task.delegate = this;
        task.execute(sqlStatements);
    }

    public void restartApplication() {
        Log.d(TAG, "restartApplication: MainActivity");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            NotificationCompat.Builder notifyBuilder = getNotificationBuilder();
            mNotifyManager.notify(Constants.RESTART_APPLICATION_ACTIVITY_ID, notifyBuilder.build());
        } else {
            Intent restartIntent = new Intent(mActivity.getApplicationContext(), AlarmReveiver.class);
            restartIntent.setAction(Constants.RESTART_APPLICATION_INTENT);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    mActivity,
                    Constants.RESTART_APPLICATION_ACTIVITY_ID,
                    restartIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager mgr = (AlarmManager) mActivity.getSystemService(Context.ALARM_SERVICE);
            mgr.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000, pendingIntent);
        }

        System.exit(0);
    }

    public void createNotificationChannel() {
        mNotifyManager = (NotificationManager) mApplication.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    Constants.PRIMARY_NOTIFICATION_CHANNEL_ID,
                    "eGeodet Notifikácia",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notifikácia z aplikácie eGeodet");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }

    private NotificationCompat.Builder getNotificationBuilder() {
        Intent notificationIntent = new Intent(mApplication, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent notificationPendingIntent = PendingIntent.getBroadcast(
                mApplication,
                Constants.RESTART_APPLICATION_ACTIVITY_ID,
                notificationIntent,
                PendingIntent.FLAG_NO_CREATE
        );

        String message = "Databáza bola aktualizovaná. Reštartujte aplikáciu.";
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(mApplication, Constants.PRIMARY_NOTIFICATION_CHANNEL_ID)
                .setContentTitle(mApplication.getString(R.string.egeodet_notification))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        return notifyBuilder;
    }

    private static class RawQueryAsyncTask extends AsyncTask<List<String>, Void, Void> {
        private RawRepository delegate = null;
        private eGeodetRoomDatabase mDb;
        private RawDao asyncTaskDao;
        private Context mContext;
        private ProgressDialog mProgressDialog;

        RawQueryAsyncTask(eGeodetRoomDatabase db, RawDao dao, Context context) {
            mDb = db;
            asyncTaskDao = dao;
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Show progress dialog
            mProgressDialog = ProgressDialog.show(mContext,"Databáza sa aktualizuje", "Prosím čakajte...",false,false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //Dismiss progress dialog
            mProgressDialog.dismiss();

            delegate.restartApplication();
        }

        @Override
        protected Void doInBackground(final List<String>... params) {
            List<String> sqlStatements = params[0];
            for (String sql : sqlStatements) {
                Log.d(TAG, "updateDatabaseFromFile: SQL statement: " + sql);
                try {
                    asyncTaskDao.execSQL(new SimpleSQLiteQuery(sql));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            return null;
        }
    }
}
