package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.configs.GeodeticPointTypeTaskType;
import sk.gku.egeodet.interfaces.BaseDao;
import sk.gku.egeodet.interfaces.GeodeticPointDao;
import sk.gku.egeodet.interfaces.GeodeticPointMissionDao;
import sk.gku.egeodet.interfaces.GeodeticPointTaskDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.TaskDao;
import sk.gku.egeodet.interfaces.UserDao;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class GeodeticPointRepository extends BaseRepository {
    private static final String TAG = "GeodeticPointRepository";
    private MutableLiveData<List<GeodeticPoint>> mSearchResults = new MutableLiveData<>();
    private GeodeticPointDao mGeodeticPointDao;
    private GeodeticPointTaskDao mGeodeticPointTaskDao;
    private TaskDao mTaskDao;
    private GeodeticPointMissionDao mGeodeticPointMissionDao;
    private MissionDao mMissionDao;
    private LiveData<List<GeodeticPoint>> mGeodeticPoints;
    private DbSyncAPI mDbSyncApi;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    public GeodeticPointRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointDao = db.geodeticPointDao();
        mGeodeticPointTaskDao = db.geodeticPointTaskDao();
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mTaskDao = db.taskDao();
        mMissionDao = db.missionDao();
        mGeodeticPoints = mGeodeticPointDao.getGeodeticPoints();
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
    }

    public LiveData<List<GeodeticPoint>> getGeodeticPoints() {
        return mGeodeticPoints;
    }

    public MutableLiveData<List<GeodeticPoint>> getSearchResults() {
        return mSearchResults;
    }

    private void asyncFinished(List<GeodeticPoint> results) {
        mSearchResults.setValue(results);
    }

    public List<Integer> getIds() {
        GeodeticPointRepository.GetGeodeticPointsAsyncTask task = new GeodeticPointRepository.GetGeodeticPointsAsyncTask(mGeodeticPointDao);
        List<GeodeticPoint> geodeticPoints = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();

        try {
            geodeticPoints = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        for (GeodeticPoint geodeticPoint : geodeticPoints) {
            ids.add(geodeticPoint.getId());
        }
        return ids;
    }

    private class GetGeodeticPointsAsyncTask extends AsyncTask<Void, Void, List<GeodeticPoint>> {
        private GeodeticPointDao mDao;

        GetGeodeticPointsAsyncTask(GeodeticPointDao dao) {
            mDao = dao;
        }

        @Override
        protected List<GeodeticPoint> doInBackground(Void... voids) {
            List<GeodeticPoint> geodeticPoints = mDao.getGeodeticPointList();
            return geodeticPoints;
        }
    }

    public List<GeodeticPoint> getByIds(List<Integer> ids) {
        QueryAsyncTask task = new QueryAsyncTask(mGeodeticPointDao);
        List<GeodeticPoint> geodeticPoints = new ArrayList<>();

        try {
            geodeticPoints = task.execute(ids.toArray(new Integer[ids.size()])).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return geodeticPoints;
    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, List<GeodeticPoint>> {
        private GeodeticPointDao asyncTaskDao;
        private GeodeticPointRepository delegate = null;

        QueryAsyncTask(GeodeticPointDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<GeodeticPoint> doInBackground(final Integer... ids) {
            return asyncTaskDao.getGeodeticPointsByIds(ids);
        }

        @Override
        protected void onPostExecute(List<GeodeticPoint> result) {
            if (delegate != null) {
                delegate.asyncFinished(result);
            }

//            if (mRepositoryCallback != null) {
//                GeodeticPoint geodeticPoint = new GeodeticPoint("Android2", "AGP");
//                mRepositoryCallback.processGeodeticPoint(geodeticPoint);
//            }
        }
    }

    public void insertAlsoIntoAssociatedTables(GeodeticPoint geodeticPoint, Bundle dataToInsert) {
        GeodeticPointRepository.InsertAsyncTask task =  new GeodeticPointRepository.InsertAsyncTask(mGeodeticPointDao);
        task.delegate = this;
        task.execute(geodeticPoint, dataToInsert);
    }

    /**
     * When geodetic point comes from slave (room) database
     */
    private static class InsertAsyncTask extends AsyncTask<Object, Void, Bundle> {
        private static final String TAG = "InsertAsyncTask";
        private GeodeticPointDao mGeodeticPointDao;
        private GeodeticPointRepository delegate = null;

        InsertAsyncTask(GeodeticPointDao geodeticPointDao) {
            mGeodeticPointDao = geodeticPointDao;
        }

        @Override
        protected Bundle doInBackground(final Object... params) {
            GeodeticPoint geodeticPoint = (GeodeticPoint) params[0];
            Bundle dataToInsert = (Bundle) params[1];

            long geodeticPointId;
            GeodeticPoint[] existingGeodeticPoints = mGeodeticPointDao.getGeodeticPointsByLabel(geodeticPoint.getLabel());
            if (existingGeodeticPoints.length == 0) {
                geodeticPointId = mGeodeticPointDao.insert(geodeticPoint);
                Log.d(TAG, "doInBackground: geodetic point inserted, new gpid is: " + geodeticPointId);
            } else {
                geodeticPointId = existingGeodeticPoints[0].getId();
                Log.d(TAG, "doInBackground: geodetic point exists, it's id is: " + geodeticPointId);
            }

            dataToInsert.putInt("newGeodeticPointId", (int) geodeticPointId);
            dataToInsert.putString("newGeodeticPointType", geodeticPoint.getType());
            return dataToInsert;
        }

        @Override
        protected void onPostExecute(Bundle dataToInsert) {
            delegate.insertIntoAssociatedTables(dataToInsert);
            Integer geodeticPointId = dataToInsert.getInt("newGeodeticPointId", -1);
            if (geodeticPointId > -1) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_MODEL, geodeticPointId);
            }
        }
    }

    public void insertIntoAssociatedTables(Bundle dataToInsert) {
        GeodeticPointRepository.InsertIntoAssociatedTablesAsyncTask task = new GeodeticPointRepository.InsertIntoAssociatedTablesAsyncTask(mGeodeticPointMissionDao, mGeodeticPointTaskDao, mTaskDao, mMissionDao);
        task.delegate = this;
        task.execute(dataToInsert);
    }

    private static class InsertIntoAssociatedTablesAsyncTask extends AsyncTask<Bundle, Void, List<HashMap<String, Integer>>> {
        private static final String TAG = "InsAssocTabsAsyncTask";
        private GeodeticPointMissionDao mGeodeticPointMissionDao;
        private GeodeticPointTaskDao mGeodeticPointTaskDao;
        private TaskDao mTaskDao;
        private MissionDao mMissionDao;
        private GeodeticPointRepository delegate = null;

        InsertIntoAssociatedTablesAsyncTask(GeodeticPointMissionDao geodeticPointMissionDao,
                                            GeodeticPointTaskDao geodeticPointTaskDao,
                                            TaskDao taskDao, MissionDao missionDao) {
            mGeodeticPointMissionDao = geodeticPointMissionDao;
            mGeodeticPointTaskDao = geodeticPointTaskDao;
            mTaskDao = taskDao;
            mMissionDao = missionDao;
        }

        @Override
        protected List<HashMap<String, Integer>> doInBackground(final Bundle... params) {
            Bundle dataToInsert = params[0];
            List<HashMap<String, Integer>> relatedModelIds = new ArrayList<>();
            int missionId = dataToInsert.getInt("missionId", -1);
            int geodeticPointId = dataToInsert.getInt("newGeodeticPointId", -1);
            String geodeticPointType = dataToInsert.getString("newGeodeticPointType", "");
            Integer userId = dataToInsert.getInt("userId", -1);
            int groupId = dataToInsert.getInt("groupId", -1);
            int status = 0;

            List<String> taskTypes = new ArrayList<>();
            for (GeodeticPointTypeTaskType geodeticPointTypeTaskType : Constants.GEODETIC_POINT_TYPE_TASK_TYPES) {
                if (geodeticPointTypeTaskType.getGeodeticPointType().equals(geodeticPointType)) {
                    Log.d(TAG, "doInBackground: task type: " + geodeticPointTypeTaskType.getTaskType() + ", point type: " + geodeticPointType);
                    taskTypes.add(geodeticPointTypeTaskType.getTaskType());
                }
            }
            List<Task> suitableTasks = mTaskDao.getTasksByTypes(taskTypes);
            List<Integer> taskIds = new ArrayList<>();
            for (Task task : suitableTasks) {
                Log.d(TAG, "doInBackground: suitable task: " + task.getName());
                taskIds.add(task.getId());
            }

            GeodeticPointMission[] existingGeodeticPointMissions = mGeodeticPointMissionDao.getGeodeticPointMissionsByGeoPntIdAndMissionId(geodeticPointId, missionId);
            if (existingGeodeticPointMissions.length == 0) {
                Log.d(TAG, "doInBackground: Inserting geodeticPointMission, missionId: " + missionId + ", geodeticPointId: " + geodeticPointId);
                GeodeticPointMission geodeticPointMission = new GeodeticPointMission(missionId, geodeticPointId, userId, groupId, status);
                long geodeticPointMissionId = mGeodeticPointMissionDao.insert(geodeticPointMission);
                Map<String, Integer> modelIdMap = new HashMap<>();
                modelIdMap.put(Constants.GEODETIC_POINT_MISSION_MODEL, (int) geodeticPointMissionId);
                relatedModelIds.add((HashMap<String, Integer>) modelIdMap);
                Log.d(TAG, "doInBackground: new geodetic point mission inserted.");
            } else {
                long geodeticPointMissionId = existingGeodeticPointMissions[0].getId();
                Log.d(TAG, "doInBackground: geodetic point mission exists, it's id is: " + geodeticPointMissionId);
            }

            List<GeodeticPointTask> existingGeodeticPointTasks = mGeodeticPointTaskDao.getGeodeticPointTasksByGeoPntIdAndMissionIdAndTaskIds(geodeticPointId, missionId, taskIds);
            ArrayList<Integer> existingTaskIds = new ArrayList<>();
            for (GeodeticPointTask currentGeoPntTask : existingGeodeticPointTasks) {
                existingTaskIds.add(currentGeoPntTask.getTaskId());
            }

            for (Task task : suitableTasks) {
                if (!existingTaskIds.contains(task.getId())) {
                    boolean isMandatory;
                    List<String> mandatoryTasks = Arrays.asList(Constants.MANDATORY_TASKS);

                    if (mandatoryTasks.contains(task.getName())) {
                        isMandatory = true;
                    } else {
                        isMandatory = false;
                    }

                    Log.d(TAG, "doInBackground: Inserting geodeticPointTask, taskId: " + task.getId() + ", geodeticPointId: " + geodeticPointId + ", isMandatory: " + isMandatory);
                    GeodeticPointTask geodeticPointTask = new GeodeticPointTask(missionId, geodeticPointId, task.getId(), null, groupId, isMandatory, false);
                    long geodeticPointTaskId = mGeodeticPointTaskDao.insert(geodeticPointTask);
                    Map<String, Integer> modelIdMap = new HashMap<>();
                    modelIdMap.put(Constants.GEODETIC_POINT_TASK_MODEL, (int) geodeticPointTaskId);
                    relatedModelIds.add((HashMap<String, Integer>) modelIdMap);
                    Log.d(TAG, "doInBackground: new geodetic point task inserted.");
                } else {
                    Log.d(TAG, "doInBackground: geodetic point task exists for geodetic point id: " + geodeticPointId + ", task id: " + task.getId());
                }
            }

            if (missionId > 0) {
                mMissionDao.updateModified(missionId);
            }

            return relatedModelIds;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, Integer>> relatedModelIds) {
            for (HashMap<String, Integer> modelIdMap : relatedModelIds) {
                for (String modelName : modelIdMap.keySet()) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdMap.get(modelName));
                }
            }
        }
    }

    public void updateLastMaintenance(int geodeticPointId) {
        GeodeticPointRepository.UpdateLastMaintenanceAsyncTask task = new GeodeticPointRepository.UpdateLastMaintenanceAsyncTask(mGeodeticPointDao);
        task.delegate = this;
        task.execute(geodeticPointId);
    }

    private static class UpdateLastMaintenanceAsyncTask extends AsyncTask<Integer, Void, Integer> {
        private GeodeticPointDao mDao;
        GeodeticPointRepository delegate = null;

        UpdateLastMaintenanceAsyncTask(GeodeticPointDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(Integer... integers) {
            Integer geodeticPointId = integers[0];

            if (geodeticPointId != null) {
                mDao.updateLastMaintenance(geodeticPointId);
            }

            return geodeticPointId;
        }

        @Override
        protected void onPostExecute(Integer geodeticPointId) {
            if (geodeticPointId != null) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_MODEL, geodeticPointId);
            }
        }
    }

    /**
     * When geodetic point comes from master database
     */
    public class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Void> {
        private static final String TAG = "InsertOnlyAsyncTask";
        private GeodeticPointDao mDao;

        InsertChunkAsyncTask(GeodeticPointDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... geodeticPoints) {
            int count = geodeticPoints.length;

            for (int i = 0; i < count; i++) {
                GeodeticPoint currGeodeticPoint = (GeodeticPoint) geodeticPoints[i];
                long id = mDao.insert(currGeodeticPoint);
                Log.d(TAG, "doInBackground: Geodetic point id after insert: " + id);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currGeodeticPoint);
                    Log.d(TAG, "doInBackground: Mission updatedCount: " + updatedCount);
                }

                Log.d(TAG, "doInBackground: geodetic point inserted, name: " + currGeodeticPoint.getLabel());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "onPostExecute: Geodetic point insertChunk finished");
            return;
        }
    }

    public void getAndSynchronizeById(Integer id, DbSyncAPI.SyncCallback callback) {
        QueryAsyncTask task = new QueryAsyncTask(mGeodeticPointDao);
        task.delegate = this;
        List<GeodeticPoint> geodeticPoints;

        try {
            geodeticPoints = task.execute(id).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        } catch (InterruptedException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        }

        if (geodeticPoints.size() > 0) {
            GeodeticPoint geodeticPoint = geodeticPoints.get(0);
            mDbSyncApi.syncGeodeticPointData(geodeticPoint, callback);
        }
    }

    public AsyncTask insertChunk(List<GeodeticPoint> geodeticPoints) {
        InsertChunkAsyncTask task = new InsertChunkAsyncTask(mGeodeticPointDao);
        task.execute(geodeticPoints.toArray());
        return task;
    }

    public void deleteByIds(Integer... ids) {
        GeodeticPointRepository.DeleteByIdsAsyncTask task = new GeodeticPointRepository.DeleteByIdsAsyncTask(mGeodeticPointDao);
        try {
            task.execute(ids).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private GeodeticPointDao mAsyncTaskDao;

        DeleteByIdsAsyncTask(GeodeticPointDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mAsyncTaskDao.deleteByIds(ids);
            return null;
        }
    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask((BaseDao) mGeodeticPointDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void batchChangeIds(List<ToSynchronizeEntity> toSynchronizeEntities) {
        List<Integer> entityIds = new ArrayList<>();
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            entityIds.add(toSynchronizeEntity.getEntityId());
        }
        List<GeodeticPoint> cachedEntities = getByIds(entityIds);
        setIdsToNegativeValues(entityIds);
        for (GeodeticPoint entity : cachedEntities) {
            for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
                if (toSynchronizeEntity.getEntityId() == entity.getId()) {
                    Log.d(TAG, "batchChangeIds: origId: " + entity.getId() + ", newId: " + toSynchronizeEntity.getMasterId());
                    entity.setId(toSynchronizeEntity.getMasterId());
                    break;
                }
            }
        }
        insertChunk(cachedEntities);
    }

    private void setIdsToNegativeValues(List<Integer> entityIds) {
        GeodeticPointRepository.SetIdsToNegativeValuseAsyncTask task = new GeodeticPointRepository.SetIdsToNegativeValuseAsyncTask(mGeodeticPointDao);
        try {
            task.execute(entityIds.toArray(new Integer[entityIds.size()])).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class SetIdsToNegativeValuseAsyncTask extends AsyncTask <Integer, Void, Void> {
        GeodeticPointDao mDao;

        SetIdsToNegativeValuseAsyncTask(GeodeticPointDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mDao.setNegativeIds(ids);

            return null;
        }
    }

    public void absIds() {
        try {
            new AbsIdsAsyncTask(mGeodeticPointDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        GeodeticPointRepository.DeleteAllAsyncTask task = new GeodeticPointRepository.DeleteAllAsyncTask(mGeodeticPointDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        GeodeticPointDao mDao;

        DeleteAllAsyncTask(GeodeticPointDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
