package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Ignore;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.GeodeticPointMissionDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.ToSynchronizeEntityDao;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointMissionGeoPnt;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GeodeticPointMissionRepository extends BaseRepository {
    private static final String TAG = "GeoPointMissionRepo";
    private GeodeticPointMissionDao mGeodeticPointMissionDao;
    private MissionDao mMissionDao;
    private LiveData<List<GeodeticPointMission>> mGeodeticPointMissions;
    private MutableLiveData<List<GeodeticPointMission>> mGeodeticPointMissionSearchResults = new MutableLiveData<>();
    private LiveData<List<JoinGeodeticPointMissionGeoPnt>> mJoinGeodeticPointMissionGeoPntSearchResults;
    private LiveData<List<ToSynchronizeEntity>> mToSynchronizeEntities;
    private Application mApplication;
    private DbSyncAPI mDbSyncApi;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    private interface WaitForAsyncTasksCallback {
        void then();
    }

    public GeodeticPointMissionRepository(Application application, int missionId, List<Integer> groupIds) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mGeodeticPointMissions = mGeodeticPointMissionDao.getGeodeticPointMissions();
        Log.d(TAG, "GeodeticPointMissionRepository: groupIds size: " + groupIds.size());
        if (groupIds.size() > 0) {
            mJoinGeodeticPointMissionGeoPntSearchResults = mGeodeticPointMissionDao.getGeodeticPointMissionsWithGeoPntsByMissionIdAndGroupIds(missionId, groupIds.toArray(new Integer[groupIds.size()]));
        } else {
            mJoinGeodeticPointMissionGeoPntSearchResults = mGeodeticPointMissionDao.getGeodeticPointMissionsWithGeoPntsByMissionId(missionId);
        }
        mApplication = application;
        mDbSyncApi = null;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(mApplication);
        ToSynchronizeEntityDao toSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mToSynchronizeEntities = toSynchronizeEntityDao.getNotSynchronizedToSynchronizeEntities();
    }

    public GeodeticPointMissionRepository(Application application, int missionId) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mGeodeticPointMissions = mGeodeticPointMissionDao.getGeodeticPointMissions();
        mJoinGeodeticPointMissionGeoPntSearchResults = mGeodeticPointMissionDao.getGeodeticPointMissionsWithGeoPntsByMissionId(missionId);
        mApplication = application;
        mDbSyncApi = null;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(mApplication);
        ToSynchronizeEntityDao toSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mToSynchronizeEntities = toSynchronizeEntityDao.getNotSynchronizedToSynchronizeEntities();
    }

    @Ignore
    public GeodeticPointMissionRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointMissionDao = db.geodeticPointMissionDao();
        mMissionDao = db.missionDao();
        mGeodeticPointMissions = mGeodeticPointMissionDao.getGeodeticPointMissions();
        mJoinGeodeticPointMissionGeoPntSearchResults = null;
        mApplication = application;
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(mApplication);
    }

    public LiveData<List<GeodeticPointMission>> getGeodeticPointMissions() {
        return mGeodeticPointMissions;
    }

    public LiveData<List<JoinGeodeticPointMissionGeoPnt>> getGeodeticPointMissionsWithGeoPntsByMissionId() {
        return mJoinGeodeticPointMissionGeoPntSearchResults;
    }

    public LiveData<List<ToSynchronizeEntity>> getToSynchronizeEntities() {
        return mToSynchronizeEntities;
    }

    public void updateGeodeticPointStatus(int geodeticPointId, int missionId, int geodeticPointStatus) {
        Integer[] updateData = {geodeticPointId, missionId, geodeticPointStatus};
        GeodeticPointMissionRepository.UpdateGeodeticPointStatusAsyncTask task = new UpdateGeodeticPointStatusAsyncTask(mGeodeticPointMissionDao, mMissionDao);
        task.delegate = this;
        task.execute(updateData);
    }

    private static class UpdateGeodeticPointStatusAsyncTask extends AsyncTask<Integer, Void, Integer[]> {
        private static final String TAG = "UpdateGPStatusAsyncTask";
        private GeodeticPointMissionDao mGeodeticPointMissionDao;
        private MissionDao mMissionDao;
        private GeodeticPointMissionRepository delegate = null;;

        UpdateGeodeticPointStatusAsyncTask(GeodeticPointMissionDao geodeticPointMissionDao, MissionDao missionDao) {
            mGeodeticPointMissionDao = geodeticPointMissionDao;
            mMissionDao = missionDao;
        }

        @Override
        protected Integer[] doInBackground(final Integer... params) {
            Integer missionId = params[1];
            mGeodeticPointMissionDao.updateStatus(params[0], missionId, params[2]);
            Integer[] geodeticPointMissionIds = mGeodeticPointMissionDao.getGeodeticPointMissionIdsByGeodeticPointIdAndMissionId(params[0], params[1]);
            mMissionDao.updateModified(missionId);
            return geodeticPointMissionIds;
        }

        @Override
        protected void onPostExecute(Integer[] geodeticPointMissionIds) {
            for (Integer geodeticPointMissionId : geodeticPointMissionIds) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_MISSION_MODEL, geodeticPointMissionId);
            }
        }
    }

    public void downloadGeodeticPointMissions(Date lastModifiedDate, Integer userId, List<Integer> groupIds, final SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncApi.getGeodeticPointMissions(1, 100, lastModifiedDate, userId, groupIds, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> geodeticPointMissionsChunk, int totalCount) {

            }

            @Override
            public void onDataChunk(final List<?> geodeticPointMissionsChunk, List<?> geodeticPointsChunk, List<?> missionsChunk, int totalCount) {
                processedCount[0] += geodeticPointMissionsChunk.size();
                List<AsyncTask> asyncTasks = new ArrayList<>();

                GeodeticPointRepository geodeticPointRepository = new GeodeticPointRepository(mApplication);
                List<GeodeticPoint> allGeodeticPoints = (List<GeodeticPoint>) geodeticPointsChunk;
                List<GeodeticPoint> filteredGeodeticPoints = new ArrayList<>(new HashSet<>(allGeodeticPoints));
                asyncTasks.add(geodeticPointRepository.insertChunk(filteredGeodeticPoints));

                MissionRepository missionRepository = new MissionRepository(mApplication);
                List<Mission> allMissions = (List<Mission>) missionsChunk;
                List<Mission> filteredMissions = new ArrayList<>(new HashSet<>(allMissions));
                asyncTasks.add(missionRepository.insertChunk(filteredMissions));

                waitForAsyncTasks(asyncTasks, new GeodeticPointMissionRepository.WaitForAsyncTasksCallback() {
                    @Override
                    public void then() {
                        insertChunk((List<GeodeticPointMission>) geodeticPointMissionsChunk);
                    }
                });
            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: Geodetic point missions downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }
        });
    }

    public void waitForAsyncTasks(List<AsyncTask> asyncTasks, GeodeticPointMissionRepository.WaitForAsyncTasksCallback callback) {
        for (AsyncTask asyncTask : asyncTasks) {
            try {
                asyncTask.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
//        boolean asyncTasksAreFinished = false;
//        while (!asyncTasksAreFinished) {
//            asyncTasksAreFinished = true;
//            int i = 1;
//            for (AsyncTask asyncTask : asyncTasks) {
//                asyncTasksAreFinished = asyncTasksAreFinished && (asyncTask.getStatus() == AsyncTask.Status.FINISHED);
//                Log.d(TAG, "waitForAsyncTasks: asyncTask " + i + " status: " + asyncTask.getStatus());
//                i++;
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        Log.d(TAG, "waitForAsyncTasks: geodeticPointMission related data inserted");
        callback.then();
    }

    public void insert(GeodeticPointMission geodeticPointMission) {
        GeodeticPointMissionRepository.InsertChunkAsyncTask task = new GeodeticPointMissionRepository.InsertChunkAsyncTask(mGeodeticPointMissionDao);
        task.execute(geodeticPointMission);
    }

    public void insertChunk(List<GeodeticPointMission> geodeticPointMissions) {
        Log.d(TAG, "insertChunk: Insert geodetic point missions chunk size " + geodeticPointMissions.size());
        GeodeticPointMissionRepository.InsertChunkAsyncTask task = new GeodeticPointMissionRepository.InsertChunkAsyncTask(mGeodeticPointMissionDao);
        try {
            task.execute(geodeticPointMissions.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "insertChunk: All chunks inserted");
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Void> {
        private static final String TAG = "InsertChunkAsyncTask";
        private GeodeticPointMissionDao mDao;

        InsertChunkAsyncTask(GeodeticPointMissionDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... geodeticPointMissions) {
            int count = geodeticPointMissions.length;

            for (int i = 0; i < count; i++) {
                GeodeticPointMission currGeodeticPointMission = (GeodeticPointMission) geodeticPointMissions[i];
                long id = mDao.insert(currGeodeticPointMission);
                Log.d(TAG, "doInBackground: Geodetic point mission incoming id: " + currGeodeticPointMission.getId());
                Log.d(TAG, "doInBackground: Geodetic point mission id after insert: " + id);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currGeodeticPointMission);
                    Log.d(TAG, "doInBackground: Geodetic point missions updated count: " + updatedCount);
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    public void synchronize(GeodeticPointMission geodeticPointMission, DbSyncAPI.SyncCallback callback) {
        mDbSyncApi.syncGeodeticPointMissionData(geodeticPointMission, callback);
    }

    public void getAndSynchronizeById(Integer id, DbSyncAPI.SyncCallback callback) {
        GeodeticPointMissionRepository.QueryAsyncTask task = new GeodeticPointMissionRepository.QueryAsyncTask(mGeodeticPointMissionDao);
        List<GeodeticPointMission> geodeticPointMissions;

        try {
            geodeticPointMissions = task.execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        }

        Log.d(TAG, "getAndSynchronizeById: geodeticPointMission size: " + geodeticPointMissions.size());
        if (geodeticPointMissions.size() > 0) {
            GeodeticPointMission geodeticPointMission = geodeticPointMissions.get(0);
            Log.d(TAG, "getAndSynchronizeById: geodeticPointMission: " + geodeticPointMission.toString());
            mDbSyncApi.syncGeodeticPointMissionData(geodeticPointMission, callback);
        }
    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, List<GeodeticPointMission>> {
        private GeodeticPointMissionDao asyncTaskDao;

        QueryAsyncTask(GeodeticPointMissionDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<GeodeticPointMission> doInBackground(Integer... ids) {
            return asyncTaskDao.getGeodeticPointMissionsByIds(ids);
        }
    }

    public List<GeodeticPointMission> getGeodeticPointMissionsByGeoPntId(Integer geodeticPointId) {
        GeodeticPointMissionRepository.GetGeodeticPointMissionsByGeoPntIdAsyncTask task =
                new GeodeticPointMissionRepository.GetGeodeticPointMissionsByGeoPntIdAsyncTask(mGeodeticPointMissionDao);
        List<GeodeticPointMission> geodeticPointMissions = new ArrayList<>();

        try {
            geodeticPointMissions = task.execute(geodeticPointId).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return geodeticPointMissions;
    }

    private class GetGeodeticPointMissionsByGeoPntIdAsyncTask extends AsyncTask<Integer, Void, List<GeodeticPointMission>> {
        private GeodeticPointMissionDao mAsyncTaskDao;

        public GetGeodeticPointMissionsByGeoPntIdAsyncTask(GeodeticPointMissionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<GeodeticPointMission> doInBackground(Integer... integers) {
            int geodeticPointId = integers[0];
            List<GeodeticPointMission> geodeticPointMissions;

            geodeticPointMissions = mAsyncTaskDao.getGeodeticPointMissionsByGeoPntId(geodeticPointId);

            return geodeticPointMissions;
        }
    }

    public List<GeodeticPointMission> getByIds(List<Integer> ids) {
        GeodeticPointMissionRepository.QueryAsyncTask task = new GeodeticPointMissionRepository.QueryAsyncTask(mGeodeticPointMissionDao);
        List<GeodeticPointMission> geodeticPointMissions = new ArrayList<>();
        Integer[] idsArr = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            idsArr[i] = ids.get(i);
        }

        try {
            geodeticPointMissions = task.execute(idsArr).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return geodeticPointMissions;
    }

    public void deleteByIds(Integer... ids) {
        GeodeticPointMissionRepository.DeleteByIdsAsyncTask task = new GeodeticPointMissionRepository.DeleteByIdsAsyncTask(mGeodeticPointMissionDao);
        try {
            task.execute(ids).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private GeodeticPointMissionDao mAsyncTaskDao;

        DeleteByIdsAsyncTask(GeodeticPointMissionDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mAsyncTaskDao.deleteByIds(ids);
            return null;
        }
    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask(mGeodeticPointMissionDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void batchChangeIds(List<ToSynchronizeEntity> toSynchronizeEntities) {
        List<Integer> entityIds = new ArrayList<>();
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            entityIds.add(toSynchronizeEntity.getEntityId());
        }
        List<GeodeticPointMission> cachedEntities = getByIds(entityIds);
        deleteByIds(entityIds.toArray(new Integer[entityIds.size()]));
        for (GeodeticPointMission entity : cachedEntities) {
            for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
                if (toSynchronizeEntity.getEntityId() == entity.getId()) {
                    Log.d(TAG, "batchChangeIds: origId: " + entity.getId() + ", newId: " + toSynchronizeEntity.getMasterId());
                    entity.setId(toSynchronizeEntity.getMasterId());
                    break;
                }
            }
        }
        insertChunk(cachedEntities);
    }

    public void absIds() {
        new AbsIdsAsyncTask(mGeodeticPointMissionDao).execute();
    }

    public void deleteAll() {
        GeodeticPointMissionRepository.DeleteAllAsyncTask task = new GeodeticPointMissionRepository.DeleteAllAsyncTask(mGeodeticPointMissionDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        GeodeticPointMissionDao mDao;

        DeleteAllAsyncTask(GeodeticPointMissionDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }

    public void updatePointOwner(Integer geodeticPointId, Integer missionId, Integer loggedInUserId) {
        GeodeticPointMissionRepository.UpdatePointOwnerAsyncTask task = new GeodeticPointMissionRepository.UpdatePointOwnerAsyncTask(mGeodeticPointMissionDao);
        task.execute(geodeticPointId, missionId, loggedInUserId);
    }

    private class UpdatePointOwnerAsyncTask extends AsyncTask<Integer, Void, Void> {
        GeodeticPointMissionDao mDao;

        UpdatePointOwnerAsyncTask(GeodeticPointMissionDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            Integer geodeticPointId = integers[0];
            Integer missionId = integers[1];
            Integer userId = integers[2];

            if (geodeticPointId > 0 && missionId > 0 && userId > 0) {
                mDao.updateUserId(geodeticPointId, missionId, userId);
            } else {
                Log.w(TAG, "doInBackground: geodeticPointMissionId or userId has illegal value, geodeticPointId: " + geodeticPointId + ", missionId: " + missionId + ", userId: " + userId);
            }

            return null;
        }
    }
}
