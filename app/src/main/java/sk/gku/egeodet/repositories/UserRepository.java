package sk.gku.egeodet.repositories;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.sqlite.db.SimpleSQLiteQuery;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.UserDao;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserRepository extends BaseRepository {
    private static final String TAG = "UserRepository";
    private final UserDao mUserDao;
    private LiveData<List<User>> mUsers;
    private final DbSyncAPI mDbSyncApi;
    private final Context mContext;
    private final ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    public UserRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mUserDao = db.userDao();
        mUsers = mUserDao.getUsers();
        mContext = application.getApplicationContext();
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
    }

    public LiveData<List<User>> getUsers() {
        return mUsers;
        // return mUserDao.getUsers();
    }

    public List<Integer> getIds() {
        UserRepository.GetUsersAsyncTask task = new UserRepository.GetUsersAsyncTask(mUserDao);
        List<User> users = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();

        try {
            users = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        for (User user : users) {
            ids.add(user.getId());
        }
        return ids;
    }

    private class GetUsersAsyncTask extends AsyncTask<Void, Void, List<User>> {
        private final UserDao mDao;

        GetUsersAsyncTask(UserDao dao) {
            mDao = dao;
        }

        @Override
        protected List<User> doInBackground(Void... voids) {
            List<User> users = mDao.getUserList();
            return users;
        }
    }

    public void insert(User user) {
         UserRepository.InsertAsyncTask task = new UserRepository.InsertAsyncTask(mUserDao);
         task.delegate = this;
         task.execute(user);
    }

    private static class InsertAsyncTask extends AsyncTask<User, Void, HashMap<String, Integer>> {
        private final UserDao mAsyncTaskDao;
        private UserRepository delegate = null;

        InsertAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected HashMap<String, Integer> doInBackground(final User... params) {
            HashMap<String, Integer> modelIdData = new HashMap<>();
            mAsyncTaskDao.insert(params[0]);
            modelIdData.put(Constants.USER_MODEL, params[0].getId());

            return modelIdData;
        }

        @Override
        protected void onPostExecute(HashMap<String, Integer> modelIdData) {
            for (String modelName : modelIdData.keySet()) {
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(modelName, modelIdData.get(modelName));
                }
            }
        }
    }

    public void execSQL(SimpleSQLiteQuery query) {
        UserRepository.RawQueryAsyncTask task = new UserRepository.RawQueryAsyncTask(mUserDao);
        task.delegate = this;
        task.execute(query);
    }

    private static class RawQueryAsyncTask extends AsyncTask<SimpleSQLiteQuery, Void, LiveData<List<User>> > {
        private final UserDao asyncTaskDao;
        private UserRepository delegate = null;

        RawQueryAsyncTask(UserDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected void onPostExecute(LiveData<List<User>> result) {
            super.onPostExecute(result);
            delegate.loadUsers(result);
            Log.d(TAG, "onPostExecute: mUsers: " + delegate.mUsers);
        }

        @Override
        protected LiveData<List<User>>  doInBackground(final SimpleSQLiteQuery... params) {
            asyncTaskDao.execSQL(params[0]);
            return asyncTaskDao.getUsers();
        }
    }

    private void loadUsers(LiveData<List<User>> result) {
        mUsers = result;
        List<User> users = result.getValue();
        if (users != null) {
            Log.d(TAG, "loadUsers: users.size: " + users.size());
        }
    }

    public void downloadUsers(final SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        DbSyncAPI dbSyncAPI = new DbSyncAPI(mContext);

        dbSyncAPI.getUsers(1, 100, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> usersChunk, int totalCount) {
                new InsertChunkAsyncTask(mUserDao).execute(usersChunk.toArray());
            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: Users downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }

        });
    }

    public void insertChunk(List<User> users) {
        UserRepository.InsertChunkAsyncTask task = new UserRepository.InsertChunkAsyncTask(mUserDao);
        try {
            task.execute(users.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Integer> {
        private static final String TAG = "InsertAsyncTask";
        private final UserDao mDao;
        private final UserRepository delegate = null;

        InsertChunkAsyncTask(UserDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... users) {
            int count = users.length;
            long id = -1;

            for (int i = 0; i < count; i++) {
                User currUser = (User) users[i];
                id = mDao.insert(currUser);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currUser);
                }

                Log.d(TAG, "doInBackground: user inserted, name: " + currUser.getName());
            }

            return (int) id;
        }

        @Override
        protected void onPostExecute(Integer userId) {

        }
    }

    public void updateUser(User user) {
        new InsertChunkAsyncTask(mUserDao).execute(user);
        mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.USER_MODEL, user.getId());
    }

    public void synchronize(User user, DbSyncAPI.SyncCallback callback) {
        mDbSyncApi.syncUserData(user, callback);
    }

    public List<User> getByIds(List<Integer> ids) {
        UserRepository.QueryAsyncTask task = new UserRepository.QueryAsyncTask(mUserDao);
        List<User> users = new ArrayList<>();
        Integer[] idsArr = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            idsArr[i] = ids.get(i);
        }

        try {
            users = task.execute(idsArr).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return users;
    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, List<User>> {
        private final UserDao asyncTaskDao;

        QueryAsyncTask(UserDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<User> doInBackground(Integer... ids) {
            return asyncTaskDao.getUsersByIds(ids);
        }
    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask((BaseDao) mUserDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void absIds() {
        try {
            new AbsIdsAsyncTask(mUserDao).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        UserRepository.DeleteAllAsyncTask task = new UserRepository.DeleteAllAsyncTask(mUserDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        UserDao mDao;

        DeleteAllAsyncTask(UserDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
