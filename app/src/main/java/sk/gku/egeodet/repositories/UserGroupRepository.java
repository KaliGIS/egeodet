package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.UserGroupDao;
import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserGroupRepository {
    public static final String TAG = "UserGroupRepository";
    private UserGroupDao mUserGroupDao;
    private LiveData<List<UserGroup>> mUserGroups;
    private Application mApplication;
    private DbSyncAPI mDbSyncApi;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    public UserGroupRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mUserGroupDao = db.userGroupDao();
        mUserGroups = mUserGroupDao.getUserGroups();
        mApplication = application;
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
    }

    public LiveData<List<UserGroup>> getUserGroups() {
        return mUserGroups;
    }

    public void downloadUserGroups(Date lastModifiedDate, Integer userId, List<Integer> groupIds, final UserGroupRepository.SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncApi.getUserGroups(1, 100, lastModifiedDate, userId, groupIds,  new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> items1, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> userGroupsChunk, List<?> usersChunk, List<?> groupsChunk, int totalCount) {
                processedCount[0] += userGroupsChunk.size();

                UserRepository userRepository = new UserRepository(mApplication);
                List<User> allUsers = (List<User>) usersChunk;
                Log.d(TAG, "onDataChunk: All users chunk size: " + allUsers.size());
                List<User> filteredUsers = new ArrayList<>(new HashSet<>(allUsers));
                Log.d(TAG, "onDataChunk: filtered users chunk size: " + filteredUsers.size());
                userRepository.insertChunk(filteredUsers);

                GroupRepository groupRepository = new GroupRepository(mApplication);
                List<Group> allGroups = (List<Group>) groupsChunk;
                List<Group> filteredGroups = new ArrayList<>(new HashSet<>(allGroups));
                groupRepository.insertChunk(filteredGroups);

                insertChunk((List<UserGroup>) userGroupsChunk);
            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount) {

            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: UserGroups downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }

        });
    }

    public void insertChunk(List<UserGroup> userGroups) {
        Log.d(TAG, "insertChunk: Insert userGroups chunk size " + userGroups.size());
        UserGroupRepository.InsertChunkAsyncTask task = new UserGroupRepository.InsertChunkAsyncTask(mUserGroupDao);
        try {
            task.execute(userGroups.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "insertChunk: All chunks inserted");
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Integer> {
        private static final String TAG = "InsertAsyncTask";
        private UserGroupDao mDao;
        private UserGroupRepository delegate = null;

        InsertChunkAsyncTask(UserGroupDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... userGroups) {
            int count = userGroups.length;
            long id = -1;

            for (int i = 0; i < count; i++) {
                UserGroup currUserGroup = (UserGroup) userGroups[i];
                id = mDao.insert(currUserGroup);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currUserGroup);
                }
            }

            return (int) id;
        }

        @Override
        protected void onPostExecute(Integer userId) {

        }
    }

    public void deleteAll() {
        UserGroupRepository.DeleteAllAsyncTask task = new UserGroupRepository.DeleteAllAsyncTask(mUserGroupDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        UserGroupDao mDao;

        DeleteAllAsyncTask(UserGroupDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }

    public List<UserGroup> getUserGroupsOfUser(Integer userId) {
        UserGroupRepository.GetUserGroupsOfUserAsyncTask task = new UserGroupRepository.GetUserGroupsOfUserAsyncTask(mUserGroupDao);
        List<UserGroup> userGroups = new ArrayList<>();

        try {
            userGroups = task.execute(userId).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return userGroups;
    }

    private class GetUserGroupsOfUserAsyncTask extends AsyncTask<Integer, Void, List<UserGroup>> {
        private UserGroupDao mDao;

        GetUserGroupsOfUserAsyncTask(UserGroupDao dao) {
            mDao = dao;
        }

        @Override
        protected List<UserGroup> doInBackground(Integer... integers) {
            if (integers.length == 0) {
                return new ArrayList<>();
            }

            Integer userId = integers[0];
            List<UserGroup> userGroups = mDao.getUserGroupsByUserId(userId);
            return userGroups;
        }
    }
}
