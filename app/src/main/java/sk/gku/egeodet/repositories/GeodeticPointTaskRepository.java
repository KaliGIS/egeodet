package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import androidx.lifecycle.LiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseDao;
import sk.gku.egeodet.interfaces.GeodeticPointTaskDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.models.TaskType;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutionException;

public class GeodeticPointTaskRepository extends BaseRepository {
    private static final String TAG = "GeoPointTaskRepository";
    private GeodeticPointTaskDao mGeodeticPointTaskDao;
    private MissionDao mMissionDao;
    private LiveData<List<JoinGeodeticPointTaskTask>> mJoinGeodeticPointTaskTaskSearchResults;
    private LiveData<List<GeodeticPointTask>> mGeodeticPointTasks;
    private LiveData<List<TaskType>> mDistinctTaskTypeSearchResults;
    private Application mApplication;
    private DbSyncAPI mDbSyncApi;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;

    public interface SyncCallback {
        void onStatusChange(Integer status);
    }

    private interface WaitForAsyncTasksCallback {
        void then();
    }

    public GeodeticPointTaskRepository(Application application, int missionId, int geodeticPointId, String taskType) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointTaskDao = db.geodeticPointTaskDao();
        mMissionDao = db.missionDao();
        mGeodeticPointTasks = mGeodeticPointTaskDao.getGeodeticPointTasks();
        String[] taskTypes = {taskType.toLowerCase(), Constants.GEODETIC_EXAMINATION_TASK_NAME.toLowerCase()};
        mJoinGeodeticPointTaskTaskSearchResults = mGeodeticPointTaskDao.getGPTasksWithTasksByMissionIdAndGPIdAndTaskType(missionId, geodeticPointId, taskTypes);
        mDistinctTaskTypeSearchResults = mGeodeticPointTaskDao.getDistinctTaskTypesByMissionIdAndGeoPntId(missionId, geodeticPointId);
        mApplication = null;
        mDbSyncApi = null;
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
    }

    public GeodeticPointTaskRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGeodeticPointTaskDao = db.geodeticPointTaskDao();
        mMissionDao = db.missionDao();
        mGeodeticPointTasks = mGeodeticPointTaskDao.getGeodeticPointTasks();
        mApplication = application;
        mDbSyncApi = new DbSyncAPI(application.getApplicationContext());
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
    }

    public LiveData<List<GeodeticPointTask>> getGeodeticPointTasks() {
        return mGeodeticPointTasks;
    }

    public LiveData<List<JoinGeodeticPointTaskTask>> getGeoPntTasksWithTasksByMissionIdAndGeoPntIdAndTaskType() {
        return mJoinGeodeticPointTaskTaskSearchResults;
    }

    public LiveData<List<TaskType>> getDistinctTaskTypesByMissionIdAndGeoPntId() {
        return mDistinctTaskTypeSearchResults;
    }

    public void insertIfDoesNotExist(GeodeticPointTask geodeticPointTask) {
        GeodeticPointTaskRepository.InsertIfDoesNotExistAsyncTask task = new GeodeticPointTaskRepository.InsertIfDoesNotExistAsyncTask(mGeodeticPointTaskDao);
        task.delegate = this;
        task.execute(geodeticPointTask);
    }

    private static class InsertIfDoesNotExistAsyncTask extends AsyncTask<GeodeticPointTask, Void, Integer> {
        private GeodeticPointTaskDao mDao;
        private GeodeticPointTaskRepository delegate = null;

        InsertIfDoesNotExistAsyncTask(GeodeticPointTaskDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(final GeodeticPointTask... params) {
            GeodeticPointTask geodeticPointTask = params[0];
            Integer recordCount = mDao.getCountOfRecords(geodeticPointTask.getMissionId(), geodeticPointTask.getGeodeticPointId(), geodeticPointTask.getTaskId());
            Integer geodeticPointTaskId = null;

            if (recordCount == 0) {
                geodeticPointTaskId = (int) mDao.insert(geodeticPointTask);
            } else {
                Log.d(TAG, "insertIfDoesNotExist: There is more than 0 geodeticPointTask records: " + recordCount.toString());
            }

            return geodeticPointTaskId;
        }

        @Override
        protected void onPostExecute(Integer geodeticPointTaskId) {
            if (geodeticPointTaskId > -1) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_TASK_MODEL, geodeticPointTaskId);
            }
        }
    }

    public void insert(GeodeticPointTask geodeticPointTask) {
        GeodeticPointTaskRepository.InsertAsyncTask task = new GeodeticPointTaskRepository.InsertAsyncTask(mGeodeticPointTaskDao, mMissionDao);
        task.delegate = this;
        task.execute(geodeticPointTask);
    }

    private static class InsertAsyncTask extends AsyncTask<GeodeticPointTask, Void, Integer> {
        private GeodeticPointTaskDao mDao;
        private MissionDao mMissionDao;
        private GeodeticPointTaskRepository delegate = null;

        InsertAsyncTask(GeodeticPointTaskDao dao, MissionDao missionDao) {
            mDao = dao;
            mMissionDao = missionDao;
        }

        @Override
        protected Integer doInBackground(final GeodeticPointTask... params) {
            GeodeticPointTask geodeticPointTask = params[0];
            long id = mDao.insert(geodeticPointTask);
            if (id == -1) {
                Integer updatedCount = mDao.update(geodeticPointTask);
            }
            mMissionDao.updateModified(geodeticPointTask.getMissionId());

            return (int) id;
        }

        @Override
        protected void onPostExecute(Integer geodeticPointTaskId) {
            if (geodeticPointTaskId > -1) {
                delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_TASK_MODEL, geodeticPointTaskId);
            }
        }
    }

    public void updateIsDone(int geodeticPointTaskId, boolean isChecked, int userId) {
        Log.d(TAG, "updateIsDone: geodeticPointTaskId: " + geodeticPointTaskId + ", isChecked: " + isChecked);
        Bundle updateData = new Bundle();
        updateData.putInt("geodeticPointTaskId", geodeticPointTaskId);
        updateData.putBoolean("isChecked", isChecked);

        GeodeticPointTaskRepository.UpdateIsDoneAsyncTask task = new GeodeticPointTaskRepository.UpdateIsDoneAsyncTask(mGeodeticPointTaskDao, mMissionDao, userId);
        task.delegate = this;
        task.execute(updateData);
    }

    private static class UpdateIsDoneAsyncTask extends AsyncTask<Object, Void, Integer> {
        private GeodeticPointTaskDao mDao;
        private MissionDao mMissionDao;
        private int mUserId;
        private GeodeticPointTaskRepository delegate = null;

        UpdateIsDoneAsyncTask(GeodeticPointTaskDao geodeticPointTaskDao, MissionDao missionDao, int userId) {
            mDao = geodeticPointTaskDao;
            mMissionDao = missionDao;
            mUserId = userId;
        }

        @Override
        protected Integer doInBackground(final Object... params) {
            Bundle updateData = (Bundle) params[0];
            int geodeticPointTaskId = updateData.getInt("geodeticPointTaskId", -1);
            boolean isChecked = updateData.getBoolean("isChecked");
            Log.d(TAG, "doInBackground: geodeticPointTaskId: " + geodeticPointTaskId + ", isChecked: " + isChecked);
            Date doneDate = isChecked ? new Date() : null;
            List<GeodeticPointTask> gpTasks = mDao.getGeodeticPointTasksByIds(geodeticPointTaskId);
            if (gpTasks.size() > 0) {
                GeodeticPointTask gpTask = gpTasks.get(0);
                gpTask.setIsDone(isChecked);
                gpTask.setDoneDate(doneDate);
                Log.d(TAG, "doInBackground: current gpTask id: " + gpTask.getId());
                if (isChecked) {
                    gpTask.setUserId(mUserId);
                    setUndoneAndUserIdToAssociatedTasks(gpTask, mUserId);
                } else {
                    gpTask.setUserId(null);
                    setUndoneAndUserIdToAssociatedTasks(gpTask, null);
                }
                mDao.update(gpTask);
                Log.d(TAG, "doInBackground: geodeticPointTaskId after update: " + geodeticPointTaskId);
                mMissionDao.updateModified(gpTask.getMissionId());
            }

            return geodeticPointTaskId;
        }

        protected void setUndoneAndUserIdToAssociatedTasks(GeodeticPointTask gpTask, Integer userId) {
            List<JoinGeodeticPointTaskTask> groupedGPTasks = mDao.getGPTasksWithTasksByMissionIdAndGPId(gpTask.getMissionId(), gpTask.getGeodeticPointId());
            String currTaskName = getCurrentTaskName(gpTask.getId(), groupedGPTasks);
            List<String> taskNamesGroup = getCurrentTaskNamesGroup(currTaskName);
            for (JoinGeodeticPointTaskTask assocGPTask : groupedGPTasks) {
                if (taskNamesGroup.contains(assocGPTask.getTaskName().toLowerCase()) && !assocGPTask.getTaskName().equals(currTaskName)) {
                    // int id, int missionId, int geodeticPointId, int taskId, Integer userId, Integer groupId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified
                    GeodeticPointTask currGPTask = new GeodeticPointTask(
                            assocGPTask.getId(),
                            assocGPTask.getMissionId(),
                            assocGPTask.getGeodeticPointId(),
                            assocGPTask.getTaskId(),
                            userId,
                            assocGPTask.getGroupId(),
                            assocGPTask.getIsMandatory(),
                            false,
                            null,
                            assocGPTask.getCreated(),
                            assocGPTask.getModified()
                    );
                    Log.d(TAG, "setUndoneToAssociatedTasks: current Task Name is done: " + currTaskName + ", this gpTask id is set to undone: " + currGPTask.getId());
                    mDao.update(currGPTask);
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_TASK_MODEL, currGPTask.getId());
                }
            }
        }

        protected String getCurrentTaskName(Integer gpTaskId, List<JoinGeodeticPointTaskTask> groupedGPTasks) {
            for (JoinGeodeticPointTaskTask currGPTask: groupedGPTasks) {
                if (currGPTask.getId() == gpTaskId) {
                    return currGPTask.getTaskName();
                }
            }
            return null;
        }

        protected List<String> getCurrentTaskNamesGroup(String currTaskName) {
            currTaskName = currTaskName.toLowerCase();
            for (int i = 0; i < Constants.TASK_NAMES_GROUPS.length; i++) {
                List<String> taskNamesGroup = Arrays.asList(Constants.TASK_NAMES_GROUPS[i]);
                ListIterator<String> iterator = taskNamesGroup.listIterator();
                while (iterator.hasNext()) {
                    iterator.set(iterator.next().toLowerCase());
                }
                for (String item: taskNamesGroup) {
                    Log.d(TAG, "getCurrentTaskNamesGroup: lowercased task name: " + item);
                }
                if (taskNamesGroup.contains(currTaskName)) {
                    return taskNamesGroup;
                }
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(Integer geodeticPointTaskId) {
            if (geodeticPointTaskId > -1) {
                if (delegate != null) {
                    delegate.mToSynchronizeEntityRepository.upsertByModelNameAndEntityId(Constants.GEODETIC_POINT_TASK_MODEL, geodeticPointTaskId);
                }
            }
        }
    }

    public void deleteGeodeticPointTask(int geodeticPointTaskId) {
        GeodeticPointTaskRepository.DeleteGeodeticPointTaskAsyncTask task = new GeodeticPointTaskRepository.DeleteGeodeticPointTaskAsyncTask(mGeodeticPointTaskDao);
        task.delegate = this;
        task.execute(geodeticPointTaskId);
    }

    private static class DeleteGeodeticPointTaskAsyncTask extends AsyncTask<Integer, Void, Integer> {
        private GeodeticPointTaskDao mDao;
        private GeodeticPointTaskRepository delegate = null;

        DeleteGeodeticPointTaskAsyncTask(GeodeticPointTaskDao dao) {
            mDao = dao;
        }


        @Override
        protected Integer doInBackground(Integer... integers) {
            Integer geodeticPointTaskId = integers[0];

            Log.d(TAG, "doInBackground: Deleting geodetic point task with id: " + geodeticPointTaskId);
            mDao.delete(geodeticPointTaskId);

            return geodeticPointTaskId;
        }

        @Override
        protected void onPostExecute(Integer geodeticPointTaskId) {
            if (geodeticPointTaskId > -1) {
                delegate.mToSynchronizeEntityRepository.deleteByModelNameAndEntityId(Constants.GEODETIC_POINT_TASK_MODEL, geodeticPointTaskId);
            }
        }
    }

    public void getAndSynchronizeById(Integer id, DbSyncAPI.SyncCallback callback) {
        GeodeticPointTaskRepository.QueryAsyncTask task = new GeodeticPointTaskRepository.QueryAsyncTask(mGeodeticPointTaskDao);
        List<GeodeticPointTask> geodeticPointTasks;

        try {
            geodeticPointTasks = task.execute(id).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), id);
            return;
        }

        if (geodeticPointTasks.size() > 0) {
            GeodeticPointTask geodeticPointTask = geodeticPointTasks.get(0);
            mDbSyncApi.syncGeodeticPointTaskData(geodeticPointTask, callback);
        }
    }

    private class QueryAsyncTask extends AsyncTask<Integer, Void, List<GeodeticPointTask>> {
        private GeodeticPointTaskDao asyncTaskDao;

        QueryAsyncTask(GeodeticPointTaskDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<GeodeticPointTask> doInBackground(final Integer... ids) {
            return asyncTaskDao.getGeodeticPointTasksByIds(ids);
        }
    }

    public void downloadGeodeticPointTasks(Date lastModifiedDate, Integer userId, List<Integer> groupIds, final GeodeticPointTaskRepository.SyncCallback callback) {
        callback.onStatusChange(Constants.SYNC_STATUS_DOWNLOADING);
        final int[] processedCount = {0};

        mDbSyncApi.getGeodeticPointTasks(1, 500, lastModifiedDate, userId, groupIds, new DbSyncAPI.RequestCallback() {
            @Override
            public void onDataChunk(List<?> geodeticPointTasksChunk, int totalCount) {

            }

            @Override
            public void onDataChunk(List<?> items1, List<?> items2, List<?> items3, int totalCount) {

            }

            @Override
            public void onDataChunk(final List<?> geodeticPointTasksChunk, List<?> geodeticPointsChunk, List<?> missionsChunk, List<?> tasksChunk, int totalCount) {
                processedCount[0] += geodeticPointTasksChunk.size();
                List<AsyncTask> asyncTasks = new ArrayList<>();

                GeodeticPointRepository geodeticPointRepository = new GeodeticPointRepository(mApplication);
                List<GeodeticPoint> allGeodeticPoints = (List<GeodeticPoint>) geodeticPointsChunk;
                List<GeodeticPoint> filteredGeodeticPoints = new ArrayList<>(new HashSet<>(allGeodeticPoints));
                asyncTasks.add(geodeticPointRepository.insertChunk(filteredGeodeticPoints));
                Log.d(TAG, "onDataChunk: Async task emmited for geodetic points");

                MissionRepository missionRepository = new MissionRepository(mApplication);
                List<Mission> allMissions = (List<Mission>) missionsChunk;
                List<Mission> filteredMissions = new ArrayList<>(new HashSet<>(allMissions));
                Log.d(TAG, "onDataChunk: All incoming missions count: " + allMissions.size() + ", filtered missions count: " + filteredMissions.size());
                asyncTasks.add(missionRepository.insertChunk(filteredMissions));
                Log.d(TAG, "onDataChunk: Async task emmited for missions");

                TaskRepository taskRepository = new TaskRepository(mApplication);
                List<Task> allTasks = (List<Task>) tasksChunk;
                List<Task> filteredTasks = new ArrayList<>(new HashSet<>(allTasks));
                asyncTasks.add(taskRepository.insertChunk(filteredTasks));
                Log.d(TAG, "onDataChunk: Async task emmited for tasks");

                waitForAsyncTasks(asyncTasks, new WaitForAsyncTasksCallback() {
                    @Override
                    public void then() {
                        Log.d(TAG, "then: Tasks, missions and geodetic points async tasks done");
                        insertChunk((List<GeodeticPointTask>) geodeticPointTasksChunk);
                    }
                });
            }

            @Override
            public void onDone() {
                Log.d(TAG, "onDone: Geodetic point tasks downloaded and saved");
                callback.onStatusChange(Constants.SYNC_STATUS_DONE);
            }

            @Override
            public void onError(Integer status, String errorText) {
                Log.e(TAG, "onError: status code: " + status + ", error text: " + errorText);
                callback.onStatusChange(Constants.SYNC_STATUS_DONE_WITH_ERROR);
            }
        });
    }

    public void waitForAsyncTasks(List<AsyncTask> asyncTasks, WaitForAsyncTasksCallback callback) {
        for (AsyncTask asyncTask : asyncTasks) {
            try {
                asyncTask.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
//        boolean asyncTasksAreFinished = false;
//        while (!asyncTasksAreFinished) {
//            asyncTasksAreFinished = true;
//            int i = 1;
//            for (AsyncTask asyncTask : asyncTasks) {
//                asyncTasksAreFinished = asyncTasksAreFinished && (asyncTask.getStatus() == AsyncTask.Status.FINISHED);
//                Log.d(TAG, "waitForAsyncTasks: asyncTask " + i + " status: " + asyncTask.getStatus());
//                i++;
//            }
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
        callback.then();
    }

    public void insertChunk(List<GeodeticPointTask> geodeticPointTasks) {
        GeodeticPointTaskRepository.InsertChunkAsyncTask task = new GeodeticPointTaskRepository.InsertChunkAsyncTask(mGeodeticPointTaskDao);
        try {
            task.execute(geodeticPointTasks.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Void> {
        private static final String TAG = "InsertAsyncTask";
        private GeodeticPointTaskDao mDao;

        InsertChunkAsyncTask(GeodeticPointTaskDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... geodeticPointTasks) {
            int count = geodeticPointTasks.length;

            for (int i = 0; i < count; i++) {
                GeodeticPointTask currGeodeticPointTask = (GeodeticPointTask) geodeticPointTasks[i];
                long id = mDao.insert(currGeodeticPointTask);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currGeodeticPointTask);
                }

                Log.d(TAG, "doInBackground: geodeticPointTask inserted, id: " + currGeodeticPointTask.getId());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    public List<GeodeticPointTask> getGeodeticPointTasksByGeoPntId(Integer geodeticPointId) {
        GeodeticPointTaskRepository.GetGeodeticPointTasksByGeoPntIdAsyncTask task =
                new GeodeticPointTaskRepository.GetGeodeticPointTasksByGeoPntIdAsyncTask(mGeodeticPointTaskDao);
        List<GeodeticPointTask> geodeticPointTasks = new ArrayList<>();

        try {
            geodeticPointTasks = task.execute(geodeticPointId).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return geodeticPointTasks;
    }

    private class GetGeodeticPointTasksByGeoPntIdAsyncTask extends AsyncTask<Integer, Void, List<GeodeticPointTask>> {
        private GeodeticPointTaskDao mAsyncTaskDao;

        public GetGeodeticPointTasksByGeoPntIdAsyncTask(GeodeticPointTaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<GeodeticPointTask> doInBackground(Integer... integers) {
            int geodeticPointId = integers[0];
            List<GeodeticPointTask> geodeticPointTasks;

            geodeticPointTasks = mAsyncTaskDao.getGeodeticPointTasksByGeoPntId(geodeticPointId);

            return geodeticPointTasks;
        }
    }

    public void synchronize(GeodeticPointTask geodeticPointTask, DbSyncAPI.SyncCallback callback) {
        mDbSyncApi.syncGeodeticPointTaskData(geodeticPointTask, callback);
    }

    public List<GeodeticPointTask> getByIds(List<Integer> ids) {
        GeodeticPointTaskRepository.QueryAsyncTask task = new GeodeticPointTaskRepository.QueryAsyncTask(mGeodeticPointTaskDao);
        List<GeodeticPointTask> geodeticPointTasks = new ArrayList<>();
        Integer[] idsArr = new Integer[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            idsArr[i] = ids.get(i);
        }

        try {
            geodeticPointTasks = task.execute(idsArr).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return geodeticPointTasks;
    }

    public void deleteByIds(Integer... ids) {
        GeodeticPointTaskRepository.DeleteByIdsAsyncTask task = new GeodeticPointTaskRepository.DeleteByIdsAsyncTask(mGeodeticPointTaskDao);
        try {
            task.execute(ids).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteByIdsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private GeodeticPointTaskDao mAsyncTaskDao;

        DeleteByIdsAsyncTask(GeodeticPointTaskDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... ids) {
            mAsyncTaskDao.deleteByIds(ids);
            return null;
        }
    }

    public void changeId(Integer origId, Integer newId) {
        ChangeIdAsyncTask task = new ChangeIdAsyncTask((BaseDao) mGeodeticPointTaskDao);
        if (!origId.equals(Math.abs(newId))) {
            try {
                Log.d(TAG, "changeId: Changing ID from " + origId + " to " + newId);
                task.execute(new Integer[]{origId, newId}).get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void batchChangeIds(List<ToSynchronizeEntity> toSynchronizeEntities) {
        List<Integer> entityIds = new ArrayList<>();
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            entityIds.add(toSynchronizeEntity.getEntityId());
        }
        List<GeodeticPointTask> cachedEntities = getByIds(entityIds);
        deleteByIds(entityIds.toArray(new Integer[entityIds.size()]));
        for (GeodeticPointTask entity : cachedEntities) {
            for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
                if (toSynchronizeEntity.getEntityId() == entity.getId()) {
                    Log.d(TAG, "batchChangeIds: origId: " + entity.getId() + ", newId: " + toSynchronizeEntity.getMasterId());
                    entity.setId(toSynchronizeEntity.getMasterId());
                    break;
                }
            }
        }
        insertChunk(cachedEntities);
    }

    public void absIds() {
        new AbsIdsAsyncTask(mGeodeticPointTaskDao).execute();
    }

    public void deleteAll() {
        GeodeticPointTaskRepository.DeleteAllAsyncTask task = new GeodeticPointTaskRepository.DeleteAllAsyncTask(mGeodeticPointTaskDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        GeodeticPointTaskDao mDao;

        DeleteAllAsyncTask(GeodeticPointTaskDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
