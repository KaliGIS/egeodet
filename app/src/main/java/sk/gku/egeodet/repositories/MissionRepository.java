package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Ignore;

import sk.gku.egeodet.interfaces.ImageDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.UserDao;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.eGeodetRoomDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MissionRepository {
    private static final String TAG = "MissionRepository";
    private MissionDao mMissionDao;
    private LiveData<List<Mission>> mMissions;
    private Application mApplication;

    public MissionRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mMissionDao = db.missionDao();
        mMissions = mMissionDao.getMissions();
        mApplication = application;
    }

    public LiveData<List<Mission>> getMissions() {
        return mMissions;
    }

    public List<Integer> getIds() {
        MissionRepository.GetMissionsAsyncTask task = new MissionRepository.GetMissionsAsyncTask(mMissionDao);
        List<Mission> missions = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();

        try {
            missions = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        for (Mission mission : missions) {
            ids.add(mission.getId());
        }
        return ids;
    }

    private class GetMissionsAsyncTask extends AsyncTask<Void, Void, List<Mission>> {
        private MissionDao mDao;

        GetMissionsAsyncTask(MissionDao dao) {
            mDao = dao;
        }

        @Override
        protected List<Mission> doInBackground(Void... voids) {
            List<Mission> missions = mDao.getMissionList();
            return missions;
        }
    }

    public AsyncTask insertChunk(List<Mission> missions) {
        MissionRepository.InsertChunkAsyncTask task = new MissionRepository.InsertChunkAsyncTask(mMissionDao);
        task.execute(missions.toArray());
        return task;
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Void> {
        private static final String TAG = "InsertAsyncTask";
        private MissionDao mDao;

        InsertChunkAsyncTask(MissionDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(final Object... missions) {
            int count = missions.length;

            for (int i = 0; i < count; i++) {
                Mission currMission = (Mission) missions[i];
                long id = mDao.insert(currMission);
                Log.d(TAG, "doInBackground: Mission id after insert: " + id);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currMission);
                    Log.d(TAG, "doInBackground: Mission updatedCount: " + updatedCount);
                }

                Log.d(TAG, "doInBackground: mission inserted, name: " + currMission.getName());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }
    }

    public void deleteAll() {
        MissionRepository.DeleteAllAsyncTask task = new MissionRepository.DeleteAllAsyncTask(mMissionDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        MissionDao mDao;

        DeleteAllAsyncTask(MissionDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }

    public List<Mission> getMissionsByGroupIds(HashSet<Integer> groupIds) {
        MissionRepository.GetMissionsAsyncTask task = new MissionRepository.GetMissionsAsyncTask(mMissionDao);
        List<Mission> missions = new ArrayList<>();
        List<Mission> filteredMissions = new ArrayList<>();

        try {
            missions = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        if (groupIds.size() == 0) { // For admin list all missions
            return missions;
        }

        for (Mission mission : missions) {
            if (groupIds.contains(mission.getGroupId())) {
                filteredMissions.add(mission);
            }
        }

        return filteredMissions;
    }
}
