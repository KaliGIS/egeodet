package sk.gku.egeodet.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import sk.gku.egeodet.interfaces.GroupDao;
import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.models.eGeodetRoomDatabase;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class GroupRepository {
    public static final String TAG = "GroupRepository";
    private GroupDao mGroupDao;
    private Application mApplication;
    private LiveData<List<Group>> mGroups;

    public GroupRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mGroupDao = db.groupDao();
        mGroups = mGroupDao.getGroups();
        mApplication = application;
    }

    public LiveData<List<Group>> getGroups() {
        return mGroups;
    }

    public void insertChunk(List<Group> groups) {
        GroupRepository.InsertChunkAsyncTask task = new GroupRepository.InsertChunkAsyncTask(mGroupDao);
        try {
            task.execute(groups.toArray()).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class InsertChunkAsyncTask extends AsyncTask<Object, Integer, Integer> {
        private static final String TAG = "InsertAsyncTask";
        private GroupDao mDao;
        private GroupRepository delegate = null;

        InsertChunkAsyncTask(GroupDao dao) {
            mDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... groups) {
            int count = groups.length;
            long id = -1;

            for (int i = 0; i < count; i++) {
                Group currGroup = (Group) groups[i];
                id = mDao.insert(currGroup);
                if (id == -1) {
                    Integer updatedCount = mDao.update(currGroup);
                }
            }

            return (int) id;
        }

        @Override
        protected void onPostExecute(Integer userId) {

        }
    }
}
