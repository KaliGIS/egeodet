package sk.gku.egeodet.repositories;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.ArrayMap;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Ignore;

import sk.gku.egeodet.interfaces.GeodeticPointTaskDao;
import sk.gku.egeodet.interfaces.ToSynchronizeEntityDao;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.eGeodetRoomDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class ToSynchronizeEntityRepository {
    private static final String TAG = "ToSyncEntityRepository";
    private ToSynchronizeEntityDao mToSynchronizeEntityDao;
    private Context mContext;
    private LiveData<List<ToSynchronizeEntity>> mToSynchronizeEntities;


    public ToSynchronizeEntityRepository(Application application) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(application);
        mToSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mContext = application.getApplicationContext();
        mToSynchronizeEntities = mToSynchronizeEntityDao.getNotSynchronizedToSynchronizeEntities();
    }

    @Ignore
    public ToSynchronizeEntityRepository(Context context) {
        eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(context);
        mToSynchronizeEntityDao = db.toSynchronizeEntityDao();
        mContext = context;
        mToSynchronizeEntities = mToSynchronizeEntityDao.getNotSynchronizedToSynchronizeEntities();
    }

    public LiveData<List<ToSynchronizeEntity>> getToSynchronizeEntities() {
        return mToSynchronizeEntities;
    }

    public Integer getCountOfNotSynchronizedToSynchronizeEntities() {
        ToSynchronizeEntityRepository.GetCountOfNotSynchronizedToSynchronizeEntitiesAsyncTask task =
                new ToSynchronizeEntityRepository.GetCountOfNotSynchronizedToSynchronizeEntitiesAsyncTask(mToSynchronizeEntityDao);
        Integer count = 0;

        try {
            count = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return count;
    }

    private class GetCountOfNotSynchronizedToSynchronizeEntitiesAsyncTask extends AsyncTask<Void, Void, Integer> {
        ToSynchronizeEntityDao mAsyncTaskDao;

        GetCountOfNotSynchronizedToSynchronizeEntitiesAsyncTask(ToSynchronizeEntityDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            Integer count = mAsyncTaskDao.getCountOfNotSynchronizedToSynchronizeEntities();
            return count;
        }
    }

    public List<ToSynchronizeEntity> getSynchronizedToSynchronizeEntities() {
        ToSynchronizeEntityRepository.GetSynchronizedToSynchronizeEntitiesAsyncTask task =
                new ToSynchronizeEntityRepository.GetSynchronizedToSynchronizeEntitiesAsyncTask(mToSynchronizeEntityDao);
        List<ToSynchronizeEntity> toSynchronizeEntities = new ArrayList<>();

        try {
            toSynchronizeEntities = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return toSynchronizeEntities;
    }

    private class GetSynchronizedToSynchronizeEntitiesAsyncTask extends AsyncTask<Void, Void, List<ToSynchronizeEntity>> {
        ToSynchronizeEntityDao mAsyncTaskDao;

        GetSynchronizedToSynchronizeEntitiesAsyncTask(ToSynchronizeEntityDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<ToSynchronizeEntity> doInBackground(Void... voids) {
            List<ToSynchronizeEntity> toSynchronizeEntities = new ArrayList<>();
            toSynchronizeEntities = mAsyncTaskDao.getSynchronizedToSynchronizeEntities();
            Log.d(TAG, "doInBackground GetSynchronizedToSynchronizeEntitiesAsyncTask: toSynchronizeEntities size: " + toSynchronizeEntities.size());
            return toSynchronizeEntities;
        }
    }

    public ToSynchronizeEntity getToSynchronizeEntityByModelAndId(String modelName, Integer entityId) {
        ToSynchronizeEntityRepository.GetToSynchronizeEntityByModelAndIdAsyncTask task =
                new ToSynchronizeEntityRepository.GetToSynchronizeEntityByModelAndIdAsyncTask(mToSynchronizeEntityDao);
        Map<String, Integer> params = new ArrayMap<>();
        params.put(modelName, entityId);

        ToSynchronizeEntity toSynchronizeEntity = null;
        try {
            toSynchronizeEntity = task.execute(params).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        return toSynchronizeEntity;
    }

    private class GetToSynchronizeEntityByModelAndIdAsyncTask extends AsyncTask<Map<String, Integer>, Void, ToSynchronizeEntity> {
        private ToSynchronizeEntityDao mAsyncTaskDao;

        GetToSynchronizeEntityByModelAndIdAsyncTask(ToSynchronizeEntityDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected ToSynchronizeEntity doInBackground(Map<String, Integer>... data) {
            String modelName = "";
            for (String key : data[0].keySet()) {
                modelName = key;
                break;
            }
            if (modelName.equals("")) {
                return null;
            }
            Integer entityId = data[0].get(modelName);

            ToSynchronizeEntity toSynchronizeEntity = mAsyncTaskDao.getToSynchronizeEntityByModelNameAndEntityId(modelName, entityId);

            return toSynchronizeEntity;
        }
    }

    public List<ToSynchronizeEntity> getToSynchronizeEntitiesRefByModelName(String modelName) {
        GetToSynchronizeEntitiesAsyncTask task = new GetToSynchronizeEntitiesAsyncTask(mToSynchronizeEntityDao);
        List<ToSynchronizeEntity> toSynchronizeEntities = new ArrayList<>();
        try {
            toSynchronizeEntities = (List<ToSynchronizeEntity>) task.execute(modelName).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return toSynchronizeEntities;
    }

    private class GetToSynchronizeEntitiesAsyncTask extends AsyncTask<String, Void, List<ToSynchronizeEntity>> {
        private ToSynchronizeEntityDao asyncTaskDao;

        GetToSynchronizeEntitiesAsyncTask(ToSynchronizeEntityDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<ToSynchronizeEntity> doInBackground(String... modelNames) {
            Log.d(TAG, "doInBackground: modelNames" + modelNames);
            List<ToSynchronizeEntity> toSynchronizeEntities = asyncTaskDao.getToSynchronizeEntitiesByModelName(modelNames[0]);
            return toSynchronizeEntities;
        }
    }

    public List<ToSynchronizeEntity> getNotSynchronizedEntities(String modelName) {
        GetRemainingToSynchronizeEntitiesAsyncTask task = new GetRemainingToSynchronizeEntitiesAsyncTask(mToSynchronizeEntityDao);
        List<ToSynchronizeEntity> toSynchronizeEntities = new ArrayList<>();
        try {
            toSynchronizeEntities = (List<ToSynchronizeEntity>) task.execute(modelName).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return toSynchronizeEntities;
    }

    private class GetRemainingToSynchronizeEntitiesAsyncTask extends AsyncTask<String, Void, List<ToSynchronizeEntity>> {
        private ToSynchronizeEntityDao asyncTaskDao;

        GetRemainingToSynchronizeEntitiesAsyncTask(ToSynchronizeEntityDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected List<ToSynchronizeEntity> doInBackground(String... modelNames) {
            List<ToSynchronizeEntity> toSynchronizeEntities;
            if (modelNames[0] == null) {
                toSynchronizeEntities = asyncTaskDao.getRemainingToSynchronizeEntities();
            } else {
                toSynchronizeEntities = asyncTaskDao.getRemainingToSynchronizeEntitiesByModelName(modelNames[0]);
            }

            return toSynchronizeEntities;
        }
    }

    public void delete(ToSynchronizeEntity toSynchronizeEntity) {
        DeleteToSynchronizeEntityAsyncTask task = new DeleteToSynchronizeEntityAsyncTask(mToSynchronizeEntityDao);
        task.execute(toSynchronizeEntity);
    }

    private class DeleteToSynchronizeEntityAsyncTask extends AsyncTask<ToSynchronizeEntity, Void, Void> {
        private ToSynchronizeEntityDao asyncTaskDao;

        DeleteToSynchronizeEntityAsyncTask(ToSynchronizeEntityDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(ToSynchronizeEntity... toSynchronizeEntities) {
            asyncTaskDao.delete(toSynchronizeEntities[0]);
            return null;
        }
    }

    public void update(ToSynchronizeEntity toSynchronizeEntity) {
        UpdateToSynchronizeEntityAsyncTask task = new UpdateToSynchronizeEntityAsyncTask(mToSynchronizeEntityDao);
        task.execute(toSynchronizeEntity);
    }

    private class UpdateToSynchronizeEntityAsyncTask extends AsyncTask<ToSynchronizeEntity, Void, Void> {
        private ToSynchronizeEntityDao asyncTaskDao;

        UpdateToSynchronizeEntityAsyncTask(ToSynchronizeEntityDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(ToSynchronizeEntity... toSynchronizeEntities) {
            asyncTaskDao.update(toSynchronizeEntities[0]);
            return null;
        }
    }

    public void updateSynchronizationErrors(Integer value) {
        UpdateToSynchronizeEntityErrorsAsyncTask task = new UpdateToSynchronizeEntityErrorsAsyncTask(mToSynchronizeEntityDao);
        try {
            task.execute(value).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class UpdateToSynchronizeEntityErrorsAsyncTask extends AsyncTask<Integer, Void, Void> {
        private ToSynchronizeEntityDao asyncTaskDao;

        UpdateToSynchronizeEntityErrorsAsyncTask(ToSynchronizeEntityDao dao) {
            asyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Integer... values) {
            asyncTaskDao.updateSynchronizationErrors(values[0]);
            return null;
        }
    }

    public List<Integer> getEntityIdsByModelName(String modelName) {
        List<Integer> entityIds = new ArrayList<>();
        ToSynchronizeEntityRepository.GetEntityIdsByModelNameAsyncTask task =
                new ToSynchronizeEntityRepository.GetEntityIdsByModelNameAsyncTask(mToSynchronizeEntityDao);

        try {
            entityIds = task.execute(modelName).get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return entityIds;
    }

    private class GetEntityIdsByModelNameAsyncTask extends AsyncTask<String, Void, List<Integer>> {
        ToSynchronizeEntityDao mAsyncTaskDao;

        GetEntityIdsByModelNameAsyncTask(ToSynchronizeEntityDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<Integer> doInBackground(String... modelNames) {
            List<Integer> entityIds = new ArrayList<>();
            if (modelNames[0] != null) {
                entityIds = mAsyncTaskDao.getEntityIdsByModelName(modelNames[0]);
            }

            return entityIds;
        }
    }

    public void upsertByModelNameAndEntityId(String modelName, Integer entityId) {
        Bundle params = new Bundle();
        params.putString("modelName", modelName);
        params.putInt("entityId", entityId);

        ToSynchronizeEntityRepository.UpsertByModelNameAndEntityIdAsyncTask task =
                new ToSynchronizeEntityRepository.UpsertByModelNameAndEntityIdAsyncTask(mToSynchronizeEntityDao);
        task.execute(params);
    }

    private class UpsertByModelNameAndEntityIdAsyncTask extends AsyncTask<Object, Void, Void> {
        ToSynchronizeEntityDao mToSynchronizeEntityDao;

        UpsertByModelNameAndEntityIdAsyncTask(ToSynchronizeEntityDao dao) {
            mToSynchronizeEntityDao = dao;
        }

        @Override
        protected Void doInBackground(Object... bundle) {
            Bundle params = (Bundle) bundle[0];
            String modelName = params.getString("modelName", "");
            Integer entityId = params.getInt("entityId", -1);
            ToSynchronizeEntity toSynchronizeEntity = mToSynchronizeEntityDao.getToSynchronizeEntityByModelNameAndEntityId(modelName, entityId);

            if (toSynchronizeEntity == null) {
                // Insert new
                ToSynchronizeEntity newToSynchronizeEntity = new ToSynchronizeEntity(modelName, entityId, 0);
                mToSynchronizeEntityDao.insert(newToSynchronizeEntity);
            } else {
                // update
                toSynchronizeEntity.setModified(new Date());
                toSynchronizeEntity.setMasterId(0);
                mToSynchronizeEntityDao.update(toSynchronizeEntity);
            }

            return null;
        }
    }

    public void deleteByModelNameAndEntityId(String modelName, Integer entityId) {
        Bundle params = new Bundle();
        params.putString("modelName", modelName);
        params.putInt("entityId", entityId);
        new ToSynchronizeEntityRepository.DeleteByModelNameAndEntityIdAsyncTask(mToSynchronizeEntityDao).execute(params);
    }

    private class DeleteByModelNameAndEntityIdAsyncTask extends AsyncTask<Object, Void, Void> {
        private ToSynchronizeEntityDao mAsyncTaskDao;

        DeleteByModelNameAndEntityIdAsyncTask(ToSynchronizeEntityDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Object... paramsBundle) {
            Bundle modelIdBundle = (Bundle) paramsBundle[0];
            String modelName = modelIdBundle.getString("modelName", "");
            Integer entityId = modelIdBundle.getInt("entityId", -1);
            Log.d(TAG, "doInBackground: deleting toSynchronizeEntity of model: " + modelName + " and entityId: " + entityId);

            if (!modelName.equals("") && entityId > -1) {
                ToSynchronizeEntity toSynchronizeEntity = mAsyncTaskDao.getToSynchronizeEntityByModelNameAndEntityId(modelName, entityId);
                if (toSynchronizeEntity != null) {
                    mAsyncTaskDao.delete(toSynchronizeEntity);
                }
            }

            return null;
        }
    }

    public void insert(ToSynchronizeEntity toSynchronizeEntity) {
        ToSynchronizeEntityRepository.InsertAsyncTask task = new ToSynchronizeEntityRepository.InsertAsyncTask(mToSynchronizeEntityDao);
        task.execute(toSynchronizeEntity);
    }

    private class InsertAsyncTask extends AsyncTask<Object, Void, Void> {
        private ToSynchronizeEntityDao mDao;

        InsertAsyncTask(ToSynchronizeEntityDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Object... objects) {
            ToSynchronizeEntity toSynchronizeEntity = (ToSynchronizeEntity) objects[0];
            mDao.insert(toSynchronizeEntity);
            return null;
        }
    }

    public List<ToSynchronizeEntity> getToChangeIdToSynchronizeEntities() {
        ToSynchronizeEntityRepository.GetToChangeIdToSynchronizeEntitiesAsyncTask task =
                new ToSynchronizeEntityRepository.GetToChangeIdToSynchronizeEntitiesAsyncTask(mToSynchronizeEntityDao);
        List<ToSynchronizeEntity> toSynchronizeEntities = new ArrayList<>();

        try {
            toSynchronizeEntities = task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        return toSynchronizeEntities;
    }

    private class GetToChangeIdToSynchronizeEntitiesAsyncTask extends AsyncTask<Void, Void, List<ToSynchronizeEntity>> {
        private ToSynchronizeEntityDao mDao;

        GetToChangeIdToSynchronizeEntitiesAsyncTask(ToSynchronizeEntityDao dao) {
            mDao = dao;
        }

        @Override
        protected List<ToSynchronizeEntity> doInBackground(Void... voids) {
            List<ToSynchronizeEntity> toSynchronizeEntities = mDao.getToChangeIdToSynchronizeEntities();
            return toSynchronizeEntities;
        }
    }

    public void deleteAll() {
        ToSynchronizeEntityRepository.DeleteAllAsyncTask task = new ToSynchronizeEntityRepository.DeleteAllAsyncTask(mToSynchronizeEntityDao);
        try {
            task.execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        ToSynchronizeEntityDao mDao;

        DeleteAllAsyncTask(ToSynchronizeEntityDao dao) {
            mDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDao.deleteAll();

            return null;
        }
    }
}
