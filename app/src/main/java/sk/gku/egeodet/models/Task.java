package sk.gku.egeodet.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;
import java.util.Objects;

@Entity(tableName = "tasks")
public class Task {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "type")
    private String mType;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Task(int id, String name, String type, Date created, Date modified) {
        this.mId = id;
        this.mName = name;
        this.mType = type;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Task(String name, String type, Date created, Date modified) {
        this.mName = name;
        this.mType = type;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Task(String name, String type) {
        this.mName = name;
        this.mType = type;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    @Ignore
    public Task(int id, String name, String type) {
        this.mId = id;
        this.mName = name;
        this.mType = type;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getType() {
        return this.mType;
    }

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return mId == task.mId &&
                Objects.equals(mName, task.mName) &&
                Objects.equals(mType, task.mType) &&
                Objects.equals(mCreated, task.mCreated) &&
                Objects.equals(mModified, task.mModified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId, mName, mType, mCreated, mModified);
    }
}
