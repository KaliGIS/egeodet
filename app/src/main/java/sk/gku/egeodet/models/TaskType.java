package sk.gku.egeodet.models;

public class TaskType {
    private String mTaskType;

    public String getTaskType() {
        return mTaskType;
    }
    public void setTaskType(String taskType) {
        this.mTaskType = taskType;
    }
}
