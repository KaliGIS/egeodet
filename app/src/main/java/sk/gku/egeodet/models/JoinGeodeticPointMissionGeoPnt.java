package sk.gku.egeodet.models;

import androidx.room.TypeConverters;

import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;

public class JoinGeodeticPointMissionGeoPnt {
    private int mId;
    private int mMissionId;
    private int mGeodeticPointId;
    private Integer mUserId;
    private int mStatus;
    private String mGeodeticPointLabel;
    private String mGeodeticPointType;

    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public int getId() {
        return this.mId;
    }
    public void setId(int id) {
        this.mId = id;
    }

    public int getMissionId() { return this.mMissionId; };
    public void setMissionId(int missionId) { this.mMissionId = missionId; };

    public int getGeodeticPointId() { return this.mGeodeticPointId; };
    public void setGeodeticPointId(int geodeticPointId) { this.mGeodeticPointId = geodeticPointId; };

    public Integer getUserId() { return this.mUserId; };
    public void setUserId(Integer userId) { this.mUserId = userId; };

    public int getStatus() { return this.mStatus; };
    public void setStatus(int status) { this.mStatus = status; };

    public String getGeodeticPointLabel() { return this.mGeodeticPointLabel; };
    public void setGeodeticPointLabel(String geodeticPointLabel) { this.mGeodeticPointLabel = geodeticPointLabel; };

    public String getGeodeticPointType() { return this.mGeodeticPointType; };
    public void setGeodeticPointType(String geodeticPointType) { this.mGeodeticPointType = geodeticPointType; };

    public Date getCreated() {
        return this.mCreated;
    }
    public void setCreated(Date created) { this.mCreated = created; };

    public Date getModified() {
        return this.mModified;
    }
    public void setModified(Date modified) { this.mModified = modified; };
}
