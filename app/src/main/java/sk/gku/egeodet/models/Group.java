package sk.gku.egeodet.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;

@Entity(tableName = "groups")
public class Group implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Group(int id, String name, Date created, Date modified) {
        this.mId = id;
        this.mName = name;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Override
    public int getId() {
        return this.mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public Date getCreated() {
        return mCreated;
    }

    public void setCreated(Date created) {
        this.mCreated = created;
    }

    public Date getModified() {
        return mModified;
    }

    public void setModified(Date modified) {
        this.mModified = modified;
    }
}
