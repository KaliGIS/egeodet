package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(
        tableName = "images",
        foreignKeys = {
                @ForeignKey(entity = GeodeticPoint.class,
                        parentColumns = "id",
                        childColumns = "geodetic_point_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Mission.class,
                        parentColumns = "id",
                        childColumns = "mission_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
        },
        indices = {
                @Index(value = "mission_id"),
                @Index(value = "geodetic_point_id"),
        }
)
public class Image implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "path")
    private String mPath;

    @ColumnInfo(name = "geodetic_point_id")
    private int mGeodeticPointId;

    @ColumnInfo(name = "mission_id")
    private int mMissionId;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Image(int id, String name, String path, int geodeticPointId, int missionId, Date created, Date modified) {
        this.mId = id;
        this.mName = name;
        this.mPath = path;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Image(String name, String path, int geodeticPointId, int missionId, Date created, Date modified) {
        this.mName = name;
        this.mPath = path;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Image(String name, String path, int geodeticPointId, int missionId) {
        this.mName = name;
        this.mPath = path;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getPath() {
        return this.mPath;
    }

    public int getGeodeticPointId() { return this.mGeodeticPointId; };

    public int getMissionId() { return this.mMissionId; };

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setGeodeticPointId(int geodeticPointId) {
        this.mGeodeticPointId = geodeticPointId;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;

        // int id, String name, String path, int geodeticPointId, int missionId, Date created, Date modified
        Map<String, String> imageMap = new ArrayMap<>();
        imageMap.put("id", String.valueOf(getId()));
        imageMap.put("name", getName());
        imageMap.put("path", getPath());
        imageMap.put("geodeticPointId", String.valueOf(getGeodeticPointId()));
        imageMap.put("missionId", String.valueOf(getMissionId()));
        imageMap.put("created", created);
        imageMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(imageMap);
    }
}
