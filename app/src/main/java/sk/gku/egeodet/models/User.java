package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(tableName = "users")
public class User implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "role")
    private String mRole;

    @ColumnInfo(name = "email")
    private String mEmail;

    @ColumnInfo(name = "password")
    private String mPassword;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public User(String name, String role, String email, String password, Date created, Date modified) {
        this.mName = name;
        this.mRole = role;
        this.mEmail = email;
        this.mPassword = password;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public User(Integer id, String name, String role, String email, String password, Date created, Date modified) {
        this.mId = id;
        this.mName = name;
        this.mRole = role;
        this.mEmail = email;
        this.mPassword = password;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public User(String name, String role, String email) {
        this.mName = name;
        this.mRole = role;
        this.mEmail = email;
        this.mCreated = new Date(System.currentTimeMillis());
        this.mModified = new Date(System.currentTimeMillis());
    }

    @Ignore
    public User(String name, String role) {
        this.mName = name;
        this.mRole = role;
        this.mEmail = "";
        this.mCreated = new Date(System.currentTimeMillis());
        this.mModified = new Date(System.currentTimeMillis());
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public String getRole() {
        return this.mRole;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;

        // Integer id, String name, String role, String email, String password, Date created, Date modified
        Map<String, String> userMap = new ArrayMap<>();
        userMap.put("id", String.valueOf(getId()));
        userMap.put("name", getName());
        userMap.put("role", getRole());
        userMap.put("email", getEmail());
        userMap.put("password", getPassword());
        userMap.put("created", created);
        userMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(userMap);
    }
}
