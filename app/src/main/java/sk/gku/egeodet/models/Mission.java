package sk.gku.egeodet.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;
import java.util.Objects;

@Entity(
    tableName = "missions",
    foreignKeys = {
        @ForeignKey(entity = Group.class,
                parentColumns = "id",
                childColumns = "group_id",
                onDelete = ForeignKey.SET_NULL,
                onUpdate = ForeignKey.CASCADE),
    },
    indices = {
            @Index(value = "group_id"),
    }
)
public class Mission {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "year")
    private int mYear;

    @ColumnInfo(name = "group_id")
    private Integer mGroupId;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Mission(int id, String name, int year, Integer groupId, Date created, Date modified) {
        this.mId = id;
        this.mName = name;
        this.mYear = year;
        this.mGroupId = groupId;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Mission(String name, int year, Date created, Date modified) {
        this.mName = name;
        this.mYear = year;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Mission(String name, int year) {
        this.mName = name;
        this.mYear = year;
        this.mCreated = new Date(System.currentTimeMillis());;
        this.mModified = new Date(System.currentTimeMillis());;
    }

    public int getId() {
        return this.mId;
    }

    public String getName() {
        return this.mName;
    }

    public int getYear() {
        return this.mYear;
    }

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public Integer getGroupId() {
        return mGroupId;
    }

    public void setGroupId(Integer groupId) {
        this.mGroupId = groupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mission mission = (Mission) o;
        return mId == mission.mId &&
                mYear == mission.mYear &&
                Objects.equals(mName, mission.mName) &&
                Objects.equals(mGroupId, mission.mGroupId) &&
                Objects.equals(mCreated, mission.mCreated) &&
                Objects.equals(mModified, mission.mModified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId, mName, mYear, mGroupId, mCreated, mModified);
    }
}
