package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(
        tableName = "notes",
        foreignKeys = {
                @ForeignKey(entity = Mission.class,
                        parentColumns = "id",
                        childColumns = "mission_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = GeodeticPoint.class,
                        parentColumns = "id",
                        childColumns = "geodetic_point_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = User.class,
                        parentColumns = "id",
                        childColumns = "user_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
        },
        indices = {
                @Index(value = "mission_id"),
                @Index(value = "geodetic_point_id"),
                @Index(value = "user_id"),
        }
)
public class Note implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "note")
    private String mNote;

    @ColumnInfo(name = "user_id")
    private int mUserId;

    @ColumnInfo(name = "geodetic_point_id")
    private int mGeodeticPointId;

    @ColumnInfo(name = "mission_id")
    private int mMissionId;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Note(int id, String note, int userId,  int geodeticPointId, int missionId, Date created, Date modified) {
        this.mId = id;
        this.mNote = note;
        this.mUserId = userId;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Note(String note, int userId,  int geodeticPointId, int missionId, Date created, Date modified) {
        this.mNote = note;
        this.mUserId = userId;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public Note(String note, int userId,  int geodeticPointId, int missionId) {
        this.mNote = note;
        this.mUserId = userId;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    @Ignore
    public Note(int id, String note, int userId,  int geodeticPointId, int missionId) {
        this.mId = id;
        this.mNote = note;
        this.mUserId = userId;
        this.mGeodeticPointId = geodeticPointId;
        this.mMissionId = missionId;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public String getNote() {
        return this.mNote;
    }

    public int getUserId() { return this.mUserId; };

    public int getGeodeticPointId() { return this.mGeodeticPointId; };

    public int getMissionId() { return this.mMissionId; };

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setGeodeticPointId(int geodeticPointId) {
        this.mGeodeticPointId = geodeticPointId;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;

        // int id, String note, int userId,  int geodeticPointId, int missionId, Date created, Date modified
        Map<String, String> noteMap = new ArrayMap<>();
        noteMap.put("id", String.valueOf(getId()));
        noteMap.put("note", getNote());
        noteMap.put("userId", String.valueOf(getUserId()));
        noteMap.put("geodeticPointId", String.valueOf(getGeodeticPointId()));
        noteMap.put("missionId", String.valueOf(getMissionId()));
        noteMap.put("created", created);
        noteMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(noteMap);
    }
}
