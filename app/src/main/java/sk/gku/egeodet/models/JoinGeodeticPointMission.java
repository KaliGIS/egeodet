package sk.gku.egeodet.models;

public class JoinGeodeticPointMission {
    private String mGeodeticPointLabel;
    private String mMissionName;
    private int mMissionYear;

    public String getGeodeticPointLabel() { return this.mGeodeticPointLabel; };
    public void setGeodeticPointLabel(String geodeticPointLabel) { this.mGeodeticPointLabel = geodeticPointLabel; };

    public String getMissionName() { return this.mMissionName; };
    public void setMissionName(String missionName) { this.mMissionName = missionName; };

    public int getMissionYear() { return this.mMissionYear; };
    public void setMissionYear(int missionYear) { this.mMissionYear = missionYear; };
}
