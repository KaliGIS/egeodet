package sk.gku.egeodet.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;

@Entity(tableName = "to_synchronize_entities")
public class ToSynchronizeEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "model_name")
    private String mModelName;

    @ColumnInfo(name = "entity_id")
    private Integer mEntityId;

    @ColumnInfo(name = "master_id")
    private Integer mMasterId;

    @ColumnInfo(name = "synchronization_error")
    private Integer mSynchronizationError;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    @NonNull
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public ToSynchronizeEntity(int id, String modelName, Integer entityId, Integer masterId, Integer synchronizationError, @NonNull Date created, Date modified) {
        this.mId = id;
        this.mModelName = modelName;
        this.mEntityId = entityId;
        this.mMasterId = masterId;
        this.mSynchronizationError = synchronizationError;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public ToSynchronizeEntity(String modelName, Integer entityId, Integer synchronizationError) {
        this.mModelName = modelName;
        this.mEntityId = entityId;
        this.mMasterId = 0;
        this.mSynchronizationError = synchronizationError;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    @Ignore
    public ToSynchronizeEntity(String modelName, Integer origId, Integer newId, Integer synchronizationError) {
        this.mModelName = modelName;
        this.mEntityId = origId;
        this.mMasterId = newId;
        this.mSynchronizationError = synchronizationError;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return mId;
    }

    public String getModelName() {
        return mModelName;
    }

    public Integer getEntityId() {
        return mEntityId;
    }

    public Integer getMasterId() {
        return mMasterId;
    }

    public Integer getSynchronizationError() {
        return mSynchronizationError;
    }

    @NonNull
    public Date getCreated() {
        return mCreated;
    }

    public Date getModified() {
        return mModified;
    }

    public void setSynchronizationError(Integer synchronizationError) {
        this.mSynchronizationError = synchronizationError;
    }

    public void setEntityId(Integer entityId) {
        this.mEntityId = entityId;
    }

    public void setMasterId(Integer masterId) {
        this.mMasterId = masterId;
    }

    public void setCreated(@NonNull Date created) {
        this.mCreated = created;
    }

    public void setModified(Date modified) {
        this.mModified = modified;
    }
}
