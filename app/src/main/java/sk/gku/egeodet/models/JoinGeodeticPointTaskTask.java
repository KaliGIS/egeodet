package sk.gku.egeodet.models;

import androidx.room.Embedded;

import java.util.Date;

public class JoinGeodeticPointTaskTask extends GeodeticPointTask {
    private String mTaskName;
    private String mTaskType;
    private String mUserName;
    private String mGeodeticPointLabel;
    @Embedded(prefix = "gpt") public GeodeticPointTask geodeticPointTask;

    public JoinGeodeticPointTaskTask(int id, int missionId, int geodeticPointId, int taskId, Integer userId, Integer groupId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified, String taskName, String taskType, String userName, String geodeticPointLabel) {
        super(id, missionId, geodeticPointId, taskId, userId, groupId, isMandatory, isDone, doneDate, created, modified);
        this.mTaskName = taskName;
        this.mTaskType = taskType;
        this.mUserName = userName;
        this.mGeodeticPointLabel = geodeticPointLabel;
    }

    public String getTaskName() {return this.mTaskName; };
    public void setTaskName(String taskName) { this.mTaskName = taskName; };

    public String getTaskType() {return this.mTaskType; };
    public void setTaskType(String taskType) { this.mTaskType = taskType; };

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getGeodeticPointLabel() {
        return mGeodeticPointLabel;
    }

    public void setGeodeticPointLabel(String geodeticPointLabel) {
        this.mGeodeticPointLabel = geodeticPointLabel;
    }
}
