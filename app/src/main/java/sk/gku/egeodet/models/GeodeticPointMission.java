package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(
        tableName = "geodetic_point_missions",
        foreignKeys = {
                @ForeignKey(entity = Mission.class,
                        parentColumns = "id",
                        childColumns = "mission_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = GeodeticPoint.class,
                        parentColumns = "id",
                        childColumns = "geodetic_point_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE),
                @ForeignKey(entity = Group.class,
                        parentColumns = "id",
                        childColumns = "group_id",
                        onDelete = ForeignKey.SET_NULL,
                        onUpdate = ForeignKey.CASCADE),
        },
        indices = {
                @Index(value = "mission_id"),
                @Index(value = "geodetic_point_id"),
                @Index(value = "group_id"),
        }
)
public class GeodeticPointMission implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "mission_id")
    private int mMissionId;

    @ColumnInfo(name = "geodetic_point_id")
    private int mGeodeticPointId;

    @ColumnInfo(name = "user_id")
    private Integer mUserId;

    @ColumnInfo(name = "group_id")
    private Integer mGroupId;

    @ColumnInfo(name = "status")
    private Integer mStatus;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public GeodeticPointMission(int id, int missionId, int geodeticPointId, Integer userId, Integer groupId, Integer status, Date created, Date modified) {
        this.mId = id;
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mUserId = userId;
        this.mGroupId = groupId;
        this.mStatus = status;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPointMission(int missionId, int geodeticPointId, Integer userId, Integer status, Date created, Date modified) {
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mUserId = userId;
        this.mStatus = status;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPointMission(int missionId, int geodeticPointId, Integer userId, Integer groupId, Integer status) {
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mUserId = userId;
        this.mGroupId = groupId;
        this.mStatus = status;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    @Ignore
    public GeodeticPointMission(int id) { // fake geodeticPointMission
        this.mId = id;
        this.mMissionId = -1;
        this.mGeodeticPointId = -1;
        this.mUserId = -1;
        this.mStatus = -1;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public int getMissionId() { return this.mMissionId; };

    public int getGeodeticPointId() { return this.mGeodeticPointId; };

    public Integer getUserId() { return this.mUserId; };

    public Integer getStatus() { return this.mStatus; };

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setGeodeticPointId(int geodeticPointId) {
        this.mGeodeticPointId = geodeticPointId;
    }

    public Integer getGroupId() {
        return mGroupId;
    }

    public void setGroupId(Integer groupId) {
        this.mGroupId = groupId;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;
        String groupId = getGroupId() != null ? getGroupId().toString() : null;
        String userId = getUserId() != null ? getUserId().toString() : null;

        // int id, int missionId, int geodeticPointId, Integer userId, Integer status, Date created, Date modified
        Map<String, String> geodeticPointMissionMap = new ArrayMap<>();
        geodeticPointMissionMap.put("id", String.valueOf(getId()));
        geodeticPointMissionMap.put("missionId", String.valueOf(getMissionId()));
        geodeticPointMissionMap.put("geodeticPointId", String.valueOf(getGeodeticPointId()));
        geodeticPointMissionMap.put("userId", userId);
        geodeticPointMissionMap.put("groupId", groupId);
        geodeticPointMissionMap.put("status", getStatus().toString());
        geodeticPointMissionMap.put("created", created);
        geodeticPointMissionMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(geodeticPointMissionMap);
    }
}
