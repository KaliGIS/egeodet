package sk.gku.egeodet.models;

import androidx.room.TypeConverters;

import sk.gku.egeodet.utils.DatetimeTypeConverters;

import java.util.Date;

public class JoinNoteUser {
    private Integer mNoteId;
    private String mNote;
    private Integer mUserId;
    private String mUserName;
    private String mUserRole;

    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public Integer getNoteId() { return this.mNoteId; };
    public void setNoteId(Integer noteId) { this.mNoteId = noteId; };

    public String getNote() {
        return mNote;
    }
    public void setNote(String note) {
        this.mNote = note;
    }

    public Integer getUserId() { return this.mUserId; };
    public void setUserId(Integer userId) { this.mUserId = userId; };

    public String getUserName() {
        return mUserName;
    }
    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getUserRole() {
        return mUserRole;
    }
    public void setUserRole(String userRole) {
        this.mUserRole = userRole;
    }

    public Date getCreated() {
        return this.mCreated;
    }
    public void setCreated(Date created) { this.mCreated = created; };

    public Date getModified() {
        return this.mModified;
    }
    public void setModified(Date modified) { this.mModified = modified; };
}
