package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(tableName = "synchronizations")
public class Synchronization {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "sync_key")
    private String mSyncKey;

    @ColumnInfo(name = "user_id")
    private Integer mUserId;

    @ColumnInfo(name = "start_date")
    @TypeConverters({DatetimeTypeConverters.class})
    @NonNull
    private Date mStartDate;

    @ColumnInfo(name = "end_date")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mEndDate;

    @ColumnInfo(name = "status")
    private Integer mStatus;

    public Synchronization(int id, String syncKey, int userId, Date startDate, Date endDate, int status) {
        this.mId = id;
        this.mSyncKey = syncKey;
        this.mUserId = userId;
        this.mStartDate = startDate;
        this.mEndDate = endDate;
        this.mStatus = status;
    }

    @Ignore
    public Synchronization(String syncKey, int userId, Date startDate, Date endDate, int status) {
        this.mSyncKey = syncKey;
        this.mUserId = userId;
        this.mStartDate = startDate;
        this.mEndDate = endDate;
        this.mStatus = status;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public void setSyncKey(String mSyncKey) {
        this.mSyncKey = mSyncKey;
    }

    public void setUserId(int mUserId) {
        this.mUserId = mUserId;
    }

    public void setStartDate(Date mStartDate) {
        this.mStartDate = mStartDate;
    }

    public void setEndDate(Date mEndDate) {
        this.mEndDate = mEndDate;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public int getId() {
        return mId;
    }

    public String getSyncKey() {
        return mSyncKey;
    }

    public Integer getUserId() {
        return mUserId;
    }

    public Date getStartDate() {
        return mStartDate;
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public Integer getStatus() {
        return mStatus;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String startDate = getStartDate() != null ? df.format(getStartDate()) : null;
        String endDate = getEndDate() != null ? df.format(getEndDate()) : null;

        Map<String, String> synchronizationMap = new ArrayMap<>();
        synchronizationMap.put("id", String.valueOf(getId()));
        synchronizationMap.put("syncKey", getSyncKey());
        synchronizationMap.put("userId", String.valueOf(getUserId()));
        synchronizationMap.put("startDate", startDate);
        synchronizationMap.put("endDate", endDate);
        synchronizationMap.put("status", String.valueOf(getStatus()));

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(synchronizationMap);
    }
}
