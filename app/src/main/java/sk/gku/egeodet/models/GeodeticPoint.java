package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

@Entity(tableName = "geodetic_points")
public class GeodeticPoint {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "label")
    private String mLabel;

    @ColumnInfo(name = "type")
    private String mType;

    @ColumnInfo(name = "last_maintenance")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mLastMaintenance;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public GeodeticPoint(int id, String label, String type, Date lastMaintenance, Date created, Date modified) {
        this.mId = id;
        this.mLabel = label;
        this.mType = type;
        this.mLastMaintenance = lastMaintenance;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPoint(String label, String type, Date lastMaintenance, Date created, Date modified) {
        this.mLabel = label;
        this.mType = type;
        this.mLastMaintenance = lastMaintenance;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPoint(String label, String type) {
        this.mLabel = label;
        this.mType = type;
        this.mLastMaintenance = null;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public String getLabel() {
        return this.mLabel;
    }

    public String getType() {
        return this.mType;
    }

    public Date getLastMaintenance() {
        return this.mLastMaintenance;
    }

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;
        String lastMaintenance = getLastMaintenance() != null ? df.format(getLastMaintenance()) : null;

        // int id, String label, String type, Date lastMaintenance, Date created, Date modified
        Map<String, String> geodeticPointMap = new ArrayMap<>();
        geodeticPointMap.put("id", String.valueOf(getId()));
        geodeticPointMap.put("label", getLabel());
        geodeticPointMap.put("type", getType());
        geodeticPointMap.put("lastMaintenance", lastMaintenance);
        geodeticPointMap.put("created", created);
        geodeticPointMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(geodeticPointMap);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeodeticPoint that = (GeodeticPoint) o;
        return mId == that.mId &&
                Objects.equals(mLabel, that.mLabel) &&
                Objects.equals(mType, that.mType) &&
                Objects.equals(mLastMaintenance, that.mLastMaintenance) &&
                Objects.equals(mCreated, that.mCreated) &&
                Objects.equals(mModified, that.mModified);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mId, mLabel, mType, mLastMaintenance, mCreated, mModified);
    }
}
