package sk.gku.egeodet.models;

import android.util.ArrayMap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.utils.DatetimeTypeConverters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Entity(
    tableName = "geodetic_point_tasks",
    foreignKeys = {
            @ForeignKey(entity = Mission.class,
                    parentColumns = "id",
                    childColumns = "mission_id",
                    onDelete = ForeignKey.CASCADE,
                    onUpdate = ForeignKey.CASCADE),
            @ForeignKey(entity = GeodeticPoint.class,
                    parentColumns = "id",
                    childColumns = "geodetic_point_id",
                    onDelete = ForeignKey.CASCADE,
                    onUpdate = ForeignKey.CASCADE),
            @ForeignKey(entity = Task.class,
                    parentColumns = "id",
                    childColumns = "task_id",
                    onDelete = ForeignKey.CASCADE,
                    onUpdate = ForeignKey.CASCADE),
            @ForeignKey(entity = Group.class,
                    parentColumns = "id",
                    childColumns = "group_id",
                    onDelete = ForeignKey.SET_NULL,
                    onUpdate = ForeignKey.CASCADE),
            @ForeignKey(entity = User.class,
                    parentColumns = "id",
                    childColumns = "user_id",
                    onDelete = ForeignKey.SET_NULL,
                    onUpdate = ForeignKey.CASCADE),
    },
    indices = {
            @Index(value = "mission_id"),
            @Index(value = "geodetic_point_id"),
            @Index(value = "task_id"),
            @Index(value = "group_id"),
            @Index(value = "user_id"),
    }
)
public class GeodeticPointTask implements BaseModel {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "mission_id")
    private int mMissionId;

    @ColumnInfo(name = "geodetic_point_id")
    private int mGeodeticPointId;

    @ColumnInfo(name = "task_id")
    private int mTaskId;

    @ColumnInfo(name = "user_id")
    private Integer mUserId;

    @ColumnInfo(name = "group_id")
    private Integer mGroupId;

    @ColumnInfo(name = "is_mandatory")
    private boolean mIsMandatory;

    @ColumnInfo(name = "is_done")
    private boolean mIsDone;

    @ColumnInfo(name = "done_date")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mDoneDate;

    @ColumnInfo(name = "created")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mCreated;

    @ColumnInfo(name = "modified")
    @TypeConverters({DatetimeTypeConverters.class})
    private Date mModified;

    public GeodeticPointTask(int id, int missionId, int geodeticPointId, int taskId, Integer userId, Integer groupId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified) {
        this.mId = id;
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mTaskId = taskId;
        this.mUserId = userId;
        this.mGroupId = groupId;
        this.mIsMandatory = isMandatory;
        this.mIsDone = isDone;
        this.mDoneDate = doneDate;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPointTask(int missionId, int geodeticPointId, int taskId, Integer userId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified) {
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mTaskId = taskId;
        this.mUserId = userId;
        this.mIsMandatory = isMandatory;
        this.mIsDone = isDone;
        this.mDoneDate = doneDate;
        this.mCreated = created;
        this.mModified = modified;
    }

    @Ignore
    public GeodeticPointTask(int missionId, int geodeticPointId, int taskId, Integer userId, Integer groupId, boolean isMandatory, boolean isDone) {
        this.mMissionId = missionId;
        this.mGeodeticPointId = geodeticPointId;
        this.mTaskId = taskId;
        this.mUserId = userId;
        this.mGroupId = groupId;
        this.mIsMandatory = isMandatory;
        this.mIsDone = isDone;
        this.mDoneDate = null;
        this.mCreated = new Date();
        this.mModified = new Date();
    }

    public int getId() {
        return this.mId;
    }

    public int getMissionId() { return this.mMissionId; };

    public int getGeodeticPointId() { return this.mGeodeticPointId; };

    public int getTaskId() { return this.mTaskId; };

    public Integer getUserId() { return this.mUserId; };

    public void setUserId(Integer userId) {
        this.mUserId = userId;
    }

    public boolean getIsMandatory() { return this.mIsMandatory; };

    public boolean getIsDone() { return this.mIsDone; };

    public Date getDoneDate() {return this.mDoneDate; };

    public Date getCreated() {
        return this.mCreated;
    }

    public Date getModified() {
        return this.mModified;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setGeodeticPointId(int geodeticPointId) {
        this.mGeodeticPointId = geodeticPointId;
    }

    public Integer getGroupId() {
        return mGroupId;
    }

    public void setGroupId(Integer groupId) {
        this.mGroupId = groupId;
    }

    public void setIsDone(boolean isDone) {
        this.mIsDone = isDone;
    }

    public void setDoneDate(Date doneDate) {
        this.mDoneDate = doneDate;
    }

    @NonNull
    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
        String created = getCreated() != null ? df.format(getCreated()) : null;
        String modified = getModified() != null ? df.format(getModified()) : null;
        String doneDate = getDoneDate() != null ? df.format(getDoneDate()) : null;
        String userId = getUserId() != null ? getUserId().toString() : null;
        String groupId = getGroupId() != null ? getGroupId().toString() : null;

        // int id, int missionId, int geodeticPointId, int taskId, Integer userId, boolean isMandatory, boolean isDone, Date doneDate, Date created, Date modified
        Map<String, String> geodeticPointTaskMap = new ArrayMap<>();
        geodeticPointTaskMap.put("id", String.valueOf(getId()));
        geodeticPointTaskMap.put("missionId", String.valueOf(getMissionId()));
        geodeticPointTaskMap.put("geodeticPointId", String.valueOf(getGeodeticPointId()));
        geodeticPointTaskMap.put("taskId", String.valueOf(getTaskId()));
        geodeticPointTaskMap.put("userId", userId);
        geodeticPointTaskMap.put("groupId", groupId);
        geodeticPointTaskMap.put("isMandatory", String.valueOf(getIsMandatory()));
        geodeticPointTaskMap.put("isDone", String.valueOf(getIsDone()));
        geodeticPointTaskMap.put("doneDate", doneDate);
        geodeticPointTaskMap.put("created", created);
        geodeticPointTaskMap.put("modified", modified);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(geodeticPointTaskMap);
    }
}
