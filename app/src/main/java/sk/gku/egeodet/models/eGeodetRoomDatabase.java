package sk.gku.egeodet.models;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import sk.gku.egeodet.interfaces.GeodeticPointDao;
import sk.gku.egeodet.interfaces.GeodeticPointMissionDao;
import sk.gku.egeodet.interfaces.GeodeticPointTaskDao;
import sk.gku.egeodet.interfaces.GroupDao;
import sk.gku.egeodet.interfaces.ImageDao;
import sk.gku.egeodet.interfaces.MissionDao;
import sk.gku.egeodet.interfaces.NoteDao;
import sk.gku.egeodet.interfaces.RawDao;
import sk.gku.egeodet.interfaces.SynchronizationDao;
import sk.gku.egeodet.interfaces.TaskDao;
import sk.gku.egeodet.interfaces.ToSynchronizeEntityDao;
import sk.gku.egeodet.interfaces.UserDao;
import sk.gku.egeodet.interfaces.UserGroupDao;
import sk.gku.egeodet.utils.DatetimeTypeConverters;

@Database(entities = {User.class, GeodeticPoint.class, GeodeticPointTask.class,
    Image.class, Mission.class, Note.class, Task.class, GeodeticPointMission.class,
    Synchronization.class, ToSynchronizeEntity.class, Group.class, UserGroup.class}, version = 9)
@TypeConverters(DatetimeTypeConverters.class)
public abstract class eGeodetRoomDatabase extends RoomDatabase {
    public static final String DBNAME = "eGeodet_database.db";
    public static Context mContext;

    public abstract UserDao userDao();
    public abstract UserGroupDao userGroupDao();
    public abstract GroupDao groupDao();
    public abstract MissionDao missionDao();
    public abstract GeodeticPointDao geodeticPointDao();
    public abstract GeodeticPointTaskDao geodeticPointTaskDao();
    public abstract NoteDao noteDao();
    public abstract TaskDao taskDao();
    public abstract GeodeticPointMissionDao geodeticPointMissionDao();
    public abstract RawDao rawDao();
    public abstract ImageDao imageDao();
    public abstract SynchronizationDao synchronizationDao();
    public abstract ToSynchronizeEntityDao toSynchronizeEntityDao();

    private static eGeodetRoomDatabase INSTANCE;

    public static eGeodetRoomDatabase getDatabase(final Context context) {
        mContext = context;

        if (INSTANCE == null) {
            synchronized (eGeodetRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            eGeodetRoomDatabase.class,
                            DBNAME
                    )
                    // .fallbackToDestructiveMigration()
                    .addCallback(sRoomDatabaseCallback)
                    .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5, MIGRATION_5_6, MIGRATION_6_7, MIGRATION_7_8, MIGRATION_8_9)
                    .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
        };
    };

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `synchronizations` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sync_key` TEXT, `user_id` INTEGER, `start_date` TEXT NOT NULL, `end_date` TEXT, `status` INTEGER)");
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `to_synchronize_entities` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `model_name` TEXT, `entity_id` INTEGER, `synchronization_error` INTEGER, `created` TEXT NOT NULL, `modified` TEXT)");
        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `to_synchronize_entities` ADD COLUMN `master_id` INTEGER");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `users` ADD COLUMN `password` TEXT NOT NULL DEFAULT ''");
        }
    };

    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `groups` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT, `created` TEXT, `modified` TEXT)");
            database.execSQL("CREATE TABLE `user_groups` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `user_id` INTEGER NOT NULL, `group_id` INTEGER NOT NULL, `created` TEXT, `modified` TEXT, FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE, FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE CASCADE)");
            database.execSQL("ALTER TABLE `geodetic_point_tasks` ADD COLUMN `group_id` INTEGER REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL");
            database.execSQL("ALTER TABLE `geodetic_point_missions` ADD COLUMN `group_id` INTEGER REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL");
        }
    };

    static final Migration MIGRATION_6_7 = new Migration(6, 7) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `missions` ADD COLUMN `group_id` INTEGER REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL");
        }
    };

    static final Migration MIGRATION_7_8 = new Migration(7, 8) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `geodetic_point_tasks_temp` (" +
                    "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "`mission_id` INTEGER NOT NULL, " +
                    "`geodetic_point_id` INTEGER NOT NULL," +
                    "`task_id` INTEGER NOT NULL," +
                    "`user_id` INTEGER," +
                    "`group_id` INTEGER," +
                    "`is_mandatory` INTEGER NOT NULL," +
                    "`is_done` INTEGER NOT NULL," +
                    "`done_date` TEXT," +
                    "`created` TEXT, " +
                    "`modified` TEXT, " +
                    "FOREIGN KEY (`mission_id`) REFERENCES `missions` (`id`) ON UPDATE CASCADE ON DELETE CASCADE," +
                    "FOREIGN KEY (`geodetic_point_id`) REFERENCES `geodetic_points` (`id`) ON UPDATE CASCADE ON DELETE CASCADE," +
                    "FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON UPDATE CASCADE ON DELETE CASCADE," +
                    "FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE SET NULL," +
                    "FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE SET NULL)");
            database.execSQL("INSERT INTO `geodetic_point_tasks_temp` (`id`, `mission_id`, `geodetic_point_id`, `task_id`, `user_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`, `created`, `modified`)" +
                    "SELECT `id`, `mission_id`, `geodetic_point_id`, `task_id`, `user_id`, `group_id`, `is_mandatory`, `is_done`, `done_date`, `created`, `modified` " +
                    "FROM `geodetic_point_tasks`");
            database.execSQL("DROP TABLE `geodetic_point_tasks`");
            database.execSQL("ALTER TABLE `geodetic_point_tasks_temp` RENAME TO `geodetic_point_tasks`");
        }
    };

    static final Migration MIGRATION_8_9 = new Migration(8, 9) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE INDEX `index_geodetic_point_tasks_mission_id` ON `geodetic_point_tasks`(`mission_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_tasks_geodetic_point_id` ON `geodetic_point_tasks`(`geodetic_point_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_tasks_task_id` ON `geodetic_point_tasks`(`task_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_tasks_group_id` ON `geodetic_point_tasks`(`group_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_tasks_user_id` ON `geodetic_point_tasks`(`user_id`)");

            database.execSQL("CREATE INDEX `index_geodetic_point_missions_mission_id` ON `geodetic_point_missions`(`mission_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_missions_geodetic_point_id` ON `geodetic_point_missions`(`geodetic_point_id`)");
            database.execSQL("CREATE INDEX `index_geodetic_point_missions_group_id` ON `geodetic_point_missions`(`group_id`)");

            database.execSQL("CREATE INDEX `index_images_mission_id` ON `images`(`mission_id`)");
            database.execSQL("CREATE INDEX `index_images_geodetic_point_id` ON `images`(`geodetic_point_id`)");

            database.execSQL("CREATE INDEX `index_missions_group_id` ON `missions`(`group_id`)");

            database.execSQL("CREATE INDEX `index_notes_mission_id` ON `notes`(`mission_id`)");
            database.execSQL("CREATE INDEX `index_notes_geodetic_point_id` ON `notes`(`geodetic_point_id`)");
            database.execSQL("CREATE INDEX `index_notes_user_id` ON `notes`(`user_id`)");

            database.execSQL("CREATE INDEX `index_user_groups_group_id` ON `user_groups`(`group_id`)");
            database.execSQL("CREATE INDEX `index_user_groups_user_id` ON `user_groups`(`user_id`)");
        }
    };
}
