package sk.gku.egeodet.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.MissionListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.utils.AESUtils;
import sk.gku.egeodet.utils.InternetCheck;
import sk.gku.egeodet.utils.MissionViewModelFactory;
import sk.gku.egeodet.utils.SkGeodesyMailAPI;
import sk.gku.egeodet.utils.ZIPApi;
import sk.gku.egeodet.viewmodels.MissionViewModel;
import sk.gku.egeodet.viewmodels.UserViewModel;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class MissionSelectionActivity extends AppCompatActivity {
    private static final String TAG = "MissionSelActivity";
    private Context mContext;
    private RecyclerView mRecyclerView;
    private TextView mAppInfo;
    private MissionViewModel mMissionViewModel;
    private List<Mission> mMissions;
    private MissionListAdapter mAdapter;
    private NotificationManager mNotifyManager;
    private String mUserName;
    private String mUserMail;
    private String mUserRole;
    private Integer mUserId;
    private HashSet<Integer> mGroupIds = new HashSet<>();
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission_selection);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mContext = this;

        // set mNofityManager and create notification channel
        createNotificationChannel();

        // Get the userId from an intent
        Intent intent = getIntent();
        mUserId = intent.getIntExtra("userId", -1);
        mUserName = intent.getStringExtra("userName");
        String typedPassword = intent.getStringExtra("userPassword");
        Boolean shouldRememberPassword = intent.getBooleanExtra("shouldRememberPassword", false);
        mUserMail = intent.getStringExtra("userMail");
        mUserRole = intent.getStringExtra("userRole");

        Log.d(TAG, "onCreate: typed password from intent: " + typedPassword);
        String encryptedPassword = "";
        try {
            encryptedPassword = AESUtils.encrypt(typedPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set the logged in user id to shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.clear();
        preferencesEditor.putInt("loggedInUserId", mUserId);
        preferencesEditor.putString("loggedInUserName", mUserName);
        preferencesEditor.putString("loggedInUserMail", mUserMail);
        preferencesEditor.putBoolean("shouldRememberPassword", shouldRememberPassword);
        if (shouldRememberPassword) {
            preferencesEditor.putString("loggedInUserPassword", encryptedPassword);
        }
        preferencesEditor.putString("loggedInUserRole", mUserRole);
        preferencesEditor.apply();

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(mUserName);

        // Initialize the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerViewMissions);

        // Set the Layout Manager.
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

        // Set empty ArrayList to mMissions
        mMissions = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mAdapter = new MissionListAdapter(this, mMissions);
        mRecyclerView.setAdapter(mAdapter);

        // Get the data.
        mMissionViewModel = ViewModelProviders.of(this).get(MissionViewModel.class);;
        mMissionViewModel.getUserGroups().observe(this, new Observer<List<UserGroup>>() {
            @Override
            public void onChanged(List<UserGroup> userGroups) {
                for (UserGroup userGroup : userGroups) {
                    if (userGroup.getUserId() == mUserId) {
                        Log.d(TAG, "onChanged: userGroup, groupId: " + userGroup.getGroupId() + ", userId: " + userGroup.getUserId());
                        mGroupIds.add(userGroup.getGroupId());
                    }
                }
                SharedPreferences.Editor preferencesEditor = mPreferences.edit();
                int i = 0;
                for (Integer groupId : mGroupIds) {
                    Log.d(TAG, "onCreate: setting group id to preferences: " + groupId);
                    preferencesEditor.putInt(String.format("groupId%d", i), groupId);
                    i++;
                }
                preferencesEditor.apply();

                List<Mission> missions;
                if (mUserRole.equals(Constants.USER_ROLE_ADMIN)) {
                    missions = mMissionViewModel.getMissionsByGroupIds(new HashSet<Integer>()); // all group ids
                } else {
                    missions = mMissionViewModel.getMissionsByGroupIds(mGroupIds);
                }
                mMissions.clear();
                mMissions.addAll(missions);
                mAdapter.setMissions(missions);
            }
        });

//        mMissionViewModel.getMissions().observe(this, new Observer<List<Mission>>() {
//            @Override
//            public void onChanged(@Nullable final List<Mission> missions) {
//                List<Mission> filteredMissions = new ArrayList<>();
//                for (Mission mission : missions) {
//                    Log.d(TAG, "onChanged: current mission from db: " + mission.getName());
//                    if (mGroupIds.contains(mission.getGroupId())) {
//                        filteredMissions.add(mission);
//                    }
//                }
//                mMissions.clear();
//                mMissions.addAll(filteredMissions);
//                mAdapter.setMissions(filteredMissions);
//            }
//        });

        checkIfShouldSynchronizeData();
    }

    /**
     * This method is called when the options menu is created.
     *
     * @param menu
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mission_selection_menu, menu);
        return true;
    }

    /**
     * This method is called when the options item is selected.
     *
     * @param item
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.send_logfiles) {
            InternetCheck ic = new InternetCheck(getApplicationContext());
            if (ic.isConnected()) {
                sendAndDeleteLogfiles();
            } else {
                noInternetConnectionAlert();
            }

            return true;
        }

        if (id == R.id.change_password_activity) {
            Intent changePasswordIntent = new Intent(mContext, ChangePasswordActivity.class);
            mContext.startActivity(changePasswordIntent);

            return true;
        }

        if (id == R.id.synchronization_activity) {
            Intent synchronizationIntent = new Intent(mContext, SynchronizationActivity.class);
            mContext.startActivity(synchronizationIntent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Opens alert dialog with information about no internet connection
     *
     * @author martin.kalivoda
     */
    public void noInternetConnectionAlert() {
        new AlertDialog.Builder(MissionSelectionActivity.this)
                .setTitle(R.string.warning)
                .setMessage(R.string.no_internet_dialog_text)
                .setPositiveButton(R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Sends mail with all log files found to administrator and afterwards removes them.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void sendAndDeleteLogfiles() {
        String mail = BuildConfig.NOREPLY_EGEODET_EMAIL;
        String subject = mUserName + ", logfiles, eGeodet";
        String model = eGeodetApplication.getModel();
        PackageInfo info = eGeodetApplication.getPackageInfo(this);

        String message = "Dátum a čas: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()) + "<br/>" +
                "Verzia systému Android: " + Build.VERSION.SDK_INT + "<br/>" +
                "Zariadenie: " + model + "<br/>" +
                "Verzia aplikácie: " + (info == null ? "(null)" : info.versionCode);

        String path = getExternalFilesDir(null) + "/" + "log" + "/";
        File logPath = new File(path);

        final ArrayList<String> logFileSrcs = new ArrayList<>();
        if (logPath.exists()) {
            File[] logFiles = logPath.listFiles();

            for (File logFile : logFiles) {
                if (logFile.getName().endsWith(".log")) {
                    logFileSrcs.add(logFile.getAbsolutePath());
                }
            }
        }

        if (logFileSrcs.size() > 0) {
            String[] logFileSrcsArr = logFileSrcs.toArray(new String[logFileSrcs.size()]);
            final String zipFileSrc = getFilesDir().getAbsolutePath() + "/eGeodet_logs_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".zip";
            try {
                ZIPApi.zip(logFileSrcsArr, zipFileSrc);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final File zipFile = new File(zipFileSrc);
            if (!zipFile.exists()) {
                throw new RuntimeException("ZIP file does not exist");
            }

            String attachmentSrc = zipFileSrc;

            new SkGeodesyMailAPI(this, mail, subject, message, attachmentSrc, false,
                    new SkGeodesyMailAPI.Consumer() {
                        @Override
                        public void afterSend(Boolean isSent) {
                            Log.d("afterSend", "afterSend: After email was sent");
                            if (isSent) {
                                if (zipFile.exists()) {
                                    Log.d("afterSend", "afterSend: zipFile exists -> deleting");
                                    zipFile.delete();
                                }

                                for(String logFileSrc : logFileSrcs) {
                                    File logFile = new File(logFileSrc);
                                    if (logFile.exists()) {
                                        Log.d("afterSend", "afterSend: logFile " + logFileSrc + " exists -> deleting");
                                        logFile.delete();
                                    }
                                }
                            }
                        }
                    }
            );
        } else {
            Toast.makeText(this, R.string.no_log_files_found, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Checks if a synchronization is needed. The last synchronization has to be done in current week.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void checkIfShouldSynchronizeData() {
        Integer lastUploadWeek = mMissionViewModel.getLastUploadWeek(mUserId);
        Integer currentWeek = mMissionViewModel.getCurrentWeek();
        Integer toSynchronizeCount = mMissionViewModel.getToSynchronizeCount();
        
        Log.d(TAG, "checkIfShouldSynchronizeData: toSynchronizeCount: " + toSynchronizeCount);
        Log.d(TAG, "checkIfShouldSynchronizeData: currentWeek: " + currentWeek);
        Log.d(TAG, "checkIfShouldSynchronizeData: lastUploadWeek: " + lastUploadWeek);
        if ((toSynchronizeCount > 0) && (lastUploadWeek < currentWeek) && (lastUploadWeek > -1)) {
            Log.d(TAG, "checkIfShouldSynchronizeData: Releasing a notification");
            notifyToSynchronize(toSynchronizeCount);
        }
    }

    /**
     * Releases the notification about a need of synchronization.
     *
     * @author martin.kalivoda@gmail.com
     */
    private void notifyToSynchronize(Integer toSynchronizeCount) {
        String notifyText = "Nemáte zosynchronizované dáta z predchádzajúceho týždňa. Zosynchronizujte hneď, ako to bude možné.";
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(this, Constants.PRIMARY_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_upload)
                .setContentTitle("Zosynchronizujte si dáta!")
                .setContentText(notifyText)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(notifyText));

        mNotifyManager.notify(Constants.SYNCHRONIZATION_NOTIFICATION_ID, notifyBuilder.build());
    }

    /**
     * Sets notification manager and creates a notification channel in the devices with Android version 7.1.
     *
     * @uathor martin.kalivoda@gmail.com
     */
    public void createNotificationChannel() {
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    Constants.PRIMARY_NOTIFICATION_CHANNEL_ID,
                    "eGeodet Notifikácia",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notifikácia z aplikácie eGeodet");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }
}
