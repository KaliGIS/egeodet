package sk.gku.egeodet.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.UserListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.interfaces.GeodeticPointMissionDao;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointMission;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.models.eGeodetRoomDatabase;
import sk.gku.egeodet.repositories.RawRepository;
import sk.gku.egeodet.repositories.UserGroupRepository;
import sk.gku.egeodet.utils.AESUtils;
import sk.gku.egeodet.utils.DbSyncAPI;
import sk.gku.egeodet.utils.InternetCheck;
import sk.gku.egeodet.viewmodels.UserViewModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class MainActivity extends AppCompatActivity {
    private UserViewModel mUserViewModel;
    private User mSelectedUser;
    private List<UserGroup> mUserGroups = new ArrayList<>();
    private final String TAG = "MainActivity";
    private Context mContext;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";
    private Integer mLastLoggedUserId;
    private String mLastLoggedUserPassword = "";
    private RelativeLayout mProgressBarLayout;
    private ProgressBar mProgressBar;
    private CheckBox mRememberPwdCheckBox;
    private EditText mPasswordEditText;

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setSubtitle("v. " + BuildConfig.VERSION_NAME); // Set subtitle of activity to current app version
        setSupportActionBar(toolbar);
        mContext = this;
        mProgressBarLayout = findViewById(R.id.layoutProgressBar);

        // Request permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUEST_READ_STORAGE);
        }

        // Get the last logged in user from the shared preferences if exists
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        if (mPreferences.contains("loggedInUserId")) {
            mLastLoggedUserId = mPreferences.getInt("loggedInUserId", -1);
        }

        Boolean rememberPassword = mPreferences.getBoolean("shouldRememberPassword", false);
        mRememberPwdCheckBox = findViewById(R.id.rememberPasswordCheckBox);
        mRememberPwdCheckBox.setChecked(rememberPassword);

        AutoCompleteTextView actv = findViewById(R.id.autoCompleteUsers);
        mPasswordEditText = findViewById(R.id.passwordEditText);

        List<User> users = new ArrayList<User>();
        final UserListAdapter adapter = new UserListAdapter(this, R.layout.autocomplete_dropdown, users);

        mUserViewModel = ViewModelProviders.of(this).get(UserViewModel.class);
        mUserViewModel.getUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> users) {
                Log.d(TAG, "onChanged: users data changed");

                for (User user : users) {
                    if (mLastLoggedUserId != null && user.getId() == mLastLoggedUserId) { // Set the last logged user
                        mSelectedUser = user;
                        AutoCompleteTextView actv = findViewById(R.id.autoCompleteUsers);
                        actv.setText(mSelectedUser.getName());
                        mPasswordEditText.setText(mLastLoggedUserPassword);
                    }
                }

                adapter.setUsers(users);
            }
        });

        mUserViewModel.getUserGroups().observe(this, new Observer<List<UserGroup>>() {
            @Override
            public void onChanged(List<UserGroup> userGroups) {
                for(UserGroup userGroup: userGroups) {
                    Log.d(TAG, "onChanged: userGroup, groupId: " + userGroup.getGroupId() + ", userId: " + userGroup.getUserId());
                }
                mUserGroups.addAll(userGroups);
            }
        });

        actv.setAdapter(adapter);
        actv.setOnItemClickListener(onItemClickListener);

        Button button = findViewById(R.id.button_enter_user);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedUser != null) {
                    String typedPassword = mPasswordEditText.getText().toString();
                    MessageDigest digest = null;
                    try {
                        digest = MessageDigest.getInstance("SHA-256");
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                    digest.update(typedPassword.getBytes(StandardCharsets.UTF_8));
                    byte[] passwordHash = digest.digest();
                    StringBuilder passwordHashHex = new StringBuilder(passwordHash.length * 2);
                    for (byte b : passwordHash)
                        passwordHashHex.append(String.format("%02x", b & 0xFF));

                    Log.d(TAG, "onClick: selected user: " + mSelectedUser.getName());
                    Log.d(TAG, "onClick: passwordHash: " + passwordHashHex.toString());
                    Log.d(TAG, "onClick: password from DB: " + mSelectedUser.getPassword());
                    if (mSelectedUser.getPassword().contentEquals(passwordHashHex)) {
                        Intent missionSelectionIntent = new Intent(mContext, MissionSelectionActivity.class);
                        missionSelectionIntent.putExtra("userId", mSelectedUser.getId());
                        missionSelectionIntent.putExtra("userName", mSelectedUser.getName());
                        missionSelectionIntent.putExtra("userPassword", typedPassword);
                        missionSelectionIntent.putExtra("shouldRememberPassword", mRememberPwdCheckBox.isChecked());
                        missionSelectionIntent.putExtra("userMail", mSelectedUser.getEmail());
                        missionSelectionIntent.putExtra("userRole", mSelectedUser.getRole());
                        ArrayList<Integer> groupIds = getGroupIdsOfUser(mSelectedUser.getId());;
                        missionSelectionIntent.putIntegerArrayListExtra("groupIds", groupIds);
                        startActivity(missionSelectionIntent);
                    } else {
                        mPasswordEditText.setText("");
                        mPasswordEditText.setError("Zadali ste nesprávne heslo");
                    }
                } else {
                    Toast.makeText(mContext, "Najskôr vyberte používateľa", Toast.LENGTH_SHORT).show();
                }
            }
        });

        String currentDbPath = getDatabasePath("eGeodet_database").getAbsolutePath();
        Log.d(TAG, "onCreate: currentDbPath: " + currentDbPath);

        String currentFilesPath = getFilesDir().getAbsolutePath();
        Log.d(TAG, "onCreate: currentFilesPath: " + currentFilesPath);

        File[] currentExternalMediaPaths = getExternalMediaDirs();
        for (File externalMediaPath : currentExternalMediaPaths) {
            Log.d(TAG, "onCreate: externalMediaPath: " + externalMediaPath.getAbsolutePath());
        }

        String currentExternalFilePath = getExternalFilesDir(null).getAbsolutePath();
        Log.d(TAG, "onCreate: currentExternalFilePath: " + currentExternalFilePath);

        String currentDataDirectory = Environment.getDataDirectory().getAbsolutePath();
        Log.d(TAG, "onCreate: currentDataDirecotory: " + currentDataDirectory);

        String currentExternalStoragePublicDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        Log.d(TAG, "onCreate: externalStoragePublicDirectory: " + currentExternalStoragePublicDirectory);

        createFolderStructure(); // creates folder structure for geodetic point images

//        Button syncTestBtn = findViewById(R.id.synchronizationTest);
//        syncTestBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: Sync initialized");
//                // int id, int missionId, int geodeticPointId, Integer userId, Integer groupId, Integer status, Date created, Date modified
//                GeodeticPointMission entity = new GeodeticPointMission(111111, 30, 1045, 24, 18, 2, null, null);
//                // GeodeticPointTask entity = new GeodeticPointTask(18561, 30, 1045, 101, 24, 18, false, true, null, null, null);
//                DbSyncAPI dbSyncAPI = new DbSyncAPI(mContext);
//                // dbSyncAPI.syncGeodeticPointTaskData(entity, new DbSyncAPI.SyncCallback() {
//                dbSyncAPI.syncGeodeticPointMissionData(entity, new DbSyncAPI.SyncCallback() {
//                    @Override
//                    public void onDone(Object syncedItem, Integer origId) {
//                        Log.d(TAG, "onDone: OK");
//                    }
//
//                    @Override
//                    public void onError(String errorStr, String errorText, Integer origId) {
//                        Log.e(TAG, "onError: errorStr " + errorStr + ", verbose: " + errorText + ", origId: " + origId);
//                    }
//                });
//            }
//        });
    }

    private ArrayList<Integer> getGroupIdsOfUser(int userId) {
        HashSet<Integer> groupIds = new HashSet<>();
        for (UserGroup userGroup : mUserGroups) {
            if (userGroup.getUserId() == userId) {
                groupIds.add(userGroup.getGroupId());
            }
        }
        for(Integer groupId : groupIds) {
            Log.d(TAG, "getGroupIdsOfUser: current user's group id:" + groupId);
        }
        return new ArrayList<>(groupIds);
    }

    private AdapterView.OnItemClickListener onItemClickListener =
        new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedUser = (User) adapterView.getItemAtPosition(i);
                AutoCompleteTextView actv = findViewById(R.id.autoCompleteUsers);
                actv.setText(mSelectedUser.getName());
            }
        };

    /**
     * This method is called when the permission request is resulted.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUEST_READ_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT);
            } else {
                Toast.makeText(mContext, "Permission not granted", Toast.LENGTH_SHORT);
                finish();
            }
        }
    }

    /**
     * This method is called when the file picker activity is resulted.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch(requestCode) {
                case Constants.REQUEST_CODE_IMPLICIT_FILE_PICKER:
                    if (data.getData() != null) {
                        Uri sqlDataUri = data.getData();
                        File sqlDataFile = new File(sqlDataUri.getPath());

                        try {
                            updateDatabaseFromFile(data.getData());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPasswordEditText.setText("");
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLastLoggedUserPassword = "";
        if (mPreferences.contains("loggedInUserPassword")) {
            try {
                mLastLoggedUserPassword = AESUtils.decrypt(mPreferences.getString("loggedInUserPassword", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mPasswordEditText.setText(mLastLoggedUserPassword);
    }

    /**
     * Updates the SQLite database with the raw SQL statements.
     *
     * @param sqlFileUri
     * @throws IOException
     * @author martin.kalivoda@gmail.com
     */
    private void updateDatabaseFromFile(Uri sqlFileUri) throws IOException {
        RawRepository rawRepository = new RawRepository(getApplication(), this);

        try (
            InputStream inputStream = getContentResolver().openInputStream(sqlFileUri);
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(inputStream)))
        ) {
            List<String> sqlStatements = new ArrayList<>();
            String sqlStatement;
            while ((sqlStatement = reader.readLine()) != null) {
                if (
                    sqlStatement.toLowerCase().contains("insert") ||
                    sqlStatement.toLowerCase().contains("update") ||
                    sqlStatement.toLowerCase().contains("delete")
                ) {
                    sqlStatements.add(sqlStatement);
                }
            }
            rawRepository.commitSqlStatements(sqlStatements);
        }
    }

    /**
     * This method is called when the options menu is created.
     *
     * @param menu
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    /**
     * This method is called when the options item is selected.
     *
     * @param item
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        if (id == R.id.add_data) {
//            Intent pickFileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//            pickFileIntent.addCategory(Intent.CATEGORY_OPENABLE);
//            // pickFileIntent.setType("application/x-sql");
//            pickFileIntent.setType("*/*");
//            startActivityForResult(Intent.createChooser(pickFileIntent, mContext.getString(R.string.choose_file)), Constants.REQUEST_CODE_IMPLICIT_FILE_PICKER);
//            return true;
//        }

        if (id == R.id.download_users) {
            InternetCheck ic = new InternetCheck(getApplicationContext());
            if (ic.isConnected()) {
                mProgressBarLayout.setVisibility(View.VISIBLE);
                TextView progressText = mProgressBarLayout.findViewById(R.id.progressBarText);
                progressText.setText("Sťahujem záznamy modelu " + Constants.USER_GROUP_MODEL + "\n");
                mUserViewModel.downloadUserGroups(new UserGroupRepository.SyncCallback() {
                    @Override
                    public void onStatusChange(Integer status) {
                        Log.d(TAG, "onStatusChange: " + status);
                        if (status.equals(Constants.SYNC_STATUS_DOWNLOADING)) {
                            mProgressBarLayout.setVisibility(View.VISIBLE);
                            TextView progressText = mProgressBarLayout.findViewById(R.id.progressBarText);
                            progressText.setText("Sťahujem záznamy modelu " + Constants.USER_GROUP_MODEL + "\n");
                        } else {
                            mProgressBarLayout.setVisibility(View.GONE);
                            TextView progressText = mProgressBarLayout.findViewById(R.id.progressBarText);
                            progressText.setText("");
                        }
                    }
                });
            } else {
                noInternetConnectionAlert();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates an empty point eGeodetImages folder structure by the actual geodetic point missions data.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void createFolderStructure() {
        new CreateFolderStructureAsyncTask(mContext).execute();
    }

    private static class CreateFolderStructureAsyncTask extends AsyncTask<Void, Void, Void> {
        public static final String TAG = "FolderStrctAsncTsk";
        private GeodeticPointMissionDao mDao;

        CreateFolderStructureAsyncTask(Context context) {
            eGeodetRoomDatabase db = eGeodetRoomDatabase.getDatabase(context);
            mDao = db.geodeticPointMissionDao();
        }

        @Override
        protected Void doInBackground(final Void... voids) {
            List<JoinGeodeticPointMission> joinGeodeticPointMissions = mDao.getGeodeticPointMissionsJoin();

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                // TODO implement for Android 10 and more
//            } else {
                for (JoinGeodeticPointMission geodeticPointMission : joinGeodeticPointMissions) {
                    String missionName = geodeticPointMission.getMissionName();
                    int missionYear = geodeticPointMission.getMissionYear();
                    String geodeticPointLabel = geodeticPointMission.getGeodeticPointLabel();

                    String currImageRelPath = missionName + "_" + missionYear + "/" + geodeticPointLabel;
                    File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
                    File geoPntImagePath = new File(storageDir, currImageRelPath);

                    Log.d(TAG, "doInBackground: Creating folder: " + geoPntImagePath.getAbsolutePath());
                    geoPntImagePath.mkdirs();
                }
//            }

            return null;
        }
    }

    /**
     * Opens alert dialog with information about no internet connection
     *
     * @author martin.kalivoda
     */
    public void noInternetConnectionAlert() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.warning)
                .setMessage(R.string.no_internet_dialog_text)
                .setPositiveButton(R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
