package sk.gku.egeodet.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sk.gku.egeodet.R;

public class NoteEditActivity extends AppCompatActivity {
    public static final String NOTE_TEXT = "com.example.android.eGeodet.NOTE_TEXT";
    public static final String NOTE_ID = "com.example.android.eGeodet.NOTE_ID";
    private TextView mAppInfo;
    private EditText mEditTextNote;
    private Integer mNoteId;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mNoteId = -1;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_edit);

        // Get data from shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        String userName = mPreferences.getString("loggedInUserName", "");
        String missionName = mPreferences.getString("missionName", "");
        int missionYear = mPreferences.getInt("missionYear", -1);
        String geodeticPointLabel = mPreferences.getString("geodeticPointLabel", "");

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + missionName + ":" + missionYear + " > " + geodeticPointLabel);

        mEditTextNote = findViewById(R.id.edittext_note);

        final Bundle extras = getIntent().getExtras();

        if (extras != null) {
            if (extras.containsKey("noteText") && extras.containsKey("noteId") ) {
                String noteText = extras.getString("noteText", "");
                mNoteId = extras.getInt("noteId", -1);

                if (!noteText.isEmpty()) {
                    mEditTextNote.setText(noteText);
                    mEditTextNote.setSelection(noteText.length());
                    mEditTextNote.requestFocus();
                }
            }
        }

        final Button buttonSave = findViewById(R.id.button_save_note);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditTextNote.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String noteText = mEditTextNote.getText().toString();
                    replyIntent.putExtra(NOTE_TEXT, noteText);
                    replyIntent.putExtra(NOTE_ID, mNoteId);
                    setResult(RESULT_OK, replyIntent);
                }

                finish();
            }
        });
    }

    /**
     * This method is called when the back button is pressed.
     *
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent replyIntent = new Intent();
        setResult(RESULT_CANCELED, replyIntent);
    }
}
