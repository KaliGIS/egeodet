package sk.gku.egeodet.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Spinner;
import android.widget.TextView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.GeodeticExaminationListAdapter;
import sk.gku.egeodet.adapters.TaskListArrayAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.repositories.GeodeticPointMissionRepository;
import sk.gku.egeodet.utils.GeodeticPointTaskViewModelFactory;
import sk.gku.egeodet.viewmodels.GeodeticPointTaskViewModel;
import sk.gku.egeodet.viewmodels.TaskViewModel;

import java.util.ArrayList;
import java.util.List;

public class GeodeticExaminationActivity extends AppCompatActivity {
    private static final String TAG = "GeoExaminationActivity";
    private RecyclerView mRecyclerView;
    private TextView mAppInfo;
    private Spinner mSpinner;
    private TaskViewModel mTaskViewModel;
    private GeodeticPointTaskViewModel mGeodeticPointTaskViewModel;
    private SharedPreferences mPreferences;
    private TaskListArrayAdapter mTaskListArrayAdapter;
    private GeodeticExaminationListAdapter mGeoExaminationListAdapter;
    private List<Task> mTasks;
    private List<JoinGeodeticPointTaskTask> mGeodeticPointTasksData;
    private Integer mUserId;
    private Integer mMissionId;
    private Integer mGeodeticPointId;
    private Integer mExaminationGroupId;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geodetic_examination);

        // Register local broadcast manager to observe for messages from TaskListArrayAdapter
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver,
                new IntentFilter(Constants.SELECTED_TASK_INTENT)
        );

        Intent intent = getIntent();
        mExaminationGroupId = intent.getIntExtra("examinationGroupId", -1);

        // Get values from shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mMissionId = mPreferences.getInt("missionId", -1);
        mGeodeticPointId = mPreferences.getInt("geodeticPointId", -1);
        mUserId = mPreferences.getInt("loggedInUserId", -1);

        String userName = mPreferences.getString("loggedInUserName", "");
        String missionName = mPreferences.getString("missionName", "");
        int missionYear = mPreferences.getInt("missionYear", -1);
        String geodeticPointLabel = mPreferences.getString("geodeticPointLabel", "");
        String taskType = mPreferences.getString("taskType", "");
        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + missionName + ": " + missionYear + " > " + geodeticPointLabel + " > " + taskType);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setSubtitle(geodeticPointLabel + " > " + taskType); // Set subtitle of activity to current geodetic point label and task type
        setSupportActionBar(toolbar);

        // Geodetic examination spinner part
        mTasks = new ArrayList<>();

        mTaskListArrayAdapter = new TaskListArrayAdapter(
                this,
                R.id.spinner_geodetic_examination,
                mTasks
        );

        mSpinner = findViewById(R.id.spinner_geodetic_examination);
        mSpinner.setAdapter(mTaskListArrayAdapter);

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        mTaskViewModel.getTasks().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable final List<Task> tasks) {
                ArrayList<Task> filteredTasks = new ArrayList<>();

                if (tasks.size() > 0) {
                    for (Task task : tasks) {
                        if (task.getType().toLowerCase().equals(Constants.GEODETIC_EXAMINATION_TASK_NAME)) {
                            filteredTasks.add(task);
                        }
                    }
                }

                Task fakeTask = new Task(getString(R.string.assign_tasks), null);
                filteredTasks.add(0, fakeTask);
                mTaskListArrayAdapter.setTasks(filteredTasks);
            }
        });

        // GeodeticPointTaskTask recycler view part
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        // Initialize the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerViewGeodeticExaminations);

        // Set the Layout Manager.
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

        // Set empty ArrayList to mGeodeticPointTasksData
        mGeodeticPointTasksData = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mGeoExaminationListAdapter = new GeodeticExaminationListAdapter(this, mGeodeticPointTasksData);
        mRecyclerView.setAdapter(mGeoExaminationListAdapter);

        // Get the tasks data.
        mGeodeticPointTaskViewModel = ViewModelProviders.of(this, new GeodeticPointTaskViewModelFactory(getApplication(), mMissionId, mGeodeticPointId, Constants.GEODETIC_EXAMINATION_TASK_NAME)).get(GeodeticPointTaskViewModel.class);
        mGeodeticPointTaskViewModel.getGPTasksByMissionIdAndGeodeticPointIdAndTaskType().observe(GeodeticExaminationActivity.this, new Observer<List<JoinGeodeticPointTaskTask>>() {
            @Override
            public void onChanged(@Nullable final List<JoinGeodeticPointTaskTask> tasks) {
                Log.d(TAG, "onChanged: task size " + tasks.size());

                if (tasks.size() > 0) {
                    mGeoExaminationListAdapter.setGeodeticPointTasks(tasks);
                }
            }
        });

        // Recycler View Item touch helper part
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                JoinGeodeticPointTaskTask geodeticPointTaskTask = mGeoExaminationListAdapter.getGeodeticPointTaskAtPosition(position);
                Integer geodeticPointTaskId = geodeticPointTaskTask.getId();

                Log.d(TAG, "onSwiped: Deleting geodetic point task id: " + geodeticPointTaskId);
                mGeodeticPointTaskViewModel.deleteGeodeticPointTask(geodeticPointTaskId);
            }

            @Override
            public int getSwipeDirs(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                JoinGeodeticPointTaskTask currJoinedGPTask = mGeoExaminationListAdapter.getGeodeticPointTaskAtPosition(position);

                // Allow swiping only on tasks if the logged in user is their owner
                if (currJoinedGPTask.getUserId() == mUserId) {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                } else {
                    return 0;
                }
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    /**
     * This method is call when the activity is destroyed.
     *
     * @author martin.kalivoda@gmil.com
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        /**
         * This method is called when the broadcast is received.
         *
         * @param context
         * @param intent
         * @author martin.kalivoda@gmail.com
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: Broadcast received");
            // Get extra data included in the Intent
            int taskId = intent.getIntExtra("taskId", -1);
            String taskName = intent.getStringExtra("taskName");
            String taskType = intent.getStringExtra("taskType");
            Integer groupId = mExaminationGroupId;

            GeodeticPointTask geodeticPointTask = new GeodeticPointTask(mMissionId, mGeodeticPointId, taskId, mUserId, groupId, false, true);

            Log.d(TAG, "onReceive: inserting geodetic point task: " + taskName + ", " + taskType);
            mGeodeticPointTaskViewModel.insert(geodeticPointTask);

            // Automatically change the geodetic point status to in progress (1)
            Log.d(TAG, "onReceive: Setting geodetic point status to 1");
            GeodeticPointMissionRepository geodeticPointMissionRepository = new GeodeticPointMissionRepository(getApplication(), mMissionId);
            geodeticPointMissionRepository.updateGeodeticPointStatus(mGeodeticPointId, mMissionId, 1);
            geodeticPointMissionRepository.updatePointOwner(mGeodeticPointId, mMissionId, mUserId);

            SharedPreferences.Editor preferencesEditor = mPreferences.edit();
            preferencesEditor.putInt("geodeticPointStatus", 1);
            preferencesEditor.apply();
        }
    };
}
