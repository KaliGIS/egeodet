package sk.gku.egeodet.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.utils.SkGeodesyMailAPI;
import sk.gku.egeodet.utils.ZIPApi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static sk.gku.egeodet.eGeodetApplication.getPackageInfo;

public class SendLogActivity extends AppCompatActivity {
    public static final String TAG = "SendLogActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_log);

        TextView crashedTextView = findViewById(R.id.crashed_textview);

        String stackTrace = getIntent().getStringExtra("stackTrace");
        Log.d(TAG, "onCreate: incoming stackTrace: " + stackTrace);
        String errorText = getString(R.string.egeodet_crashed_with_error, stackTrace);
        crashedTextView.setText(errorText);

        String logFileSrc = extractLogcatToFile();
        sendMailWithLogFile(logFileSrc, stackTrace);

        Log.d("eGeodetApplication", "handleUncaughtException: Email sent");
    }

    private String extractLogcatToFile()
    {

        PackageInfo info = getPackageInfo(this);
        String model = eGeodetApplication.getModel();

        // Make file name - file must be saved to external storage or it wont be readable by
        // the email app.
        String path = getExternalFilesDir(null) + "/" + "log" + "/";
        File filePath = new File(path);
        filePath.mkdir();
        String logFileSrc = path + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".log";
        Log.d("eGeodetApplication", "extractLogcatToFile: logFileSrc is: " + logFileSrc);

        // Extract to file.
        File file = new File(logFileSrc);
        InputStreamReader reader = null;
        FileWriter writer = null;
        try
        {
            // For Android 4.0 and earlier, you will get all app's log output, so filter it to
            // mostly limit it to your app's output.  In later versions, the filtering isn't needed.
            String cmd = (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) ?
                    "logcat -d -v time MyApp:v dalvikvm:v System.err:v *:s" :
                    "logcat -d -v time";

            // get input stream
            Log.d("eGeodetApplication", "extractLogcatToFile: Getting input stream");
            Process process = Runtime.getRuntime().exec(cmd);
            reader = new InputStreamReader(process.getInputStream());

            // write output stream
            writer = new FileWriter(file);
            Log.d("eGeodetApplication", "extractLogcatToFile: Writing output stream: " + writer);
            writer.write("Android version: " +  Build.VERSION.SDK_INT + "\n");
            writer.write("Device: " + model + "\n");
            writer.write("App version: " + (info == null ? "(null)" : info.versionCode) + "\n");

            Log.d("eGeodetApplication", "verions, device and app version written");

            char[] buffer = new char[10000];
            do
            {
                int n = reader.read(buffer, 0, buffer.length);
                if (n == -1)
                    break;
                writer.write(buffer, 0, n);
            } while (true);

            Log.d("eGeodetApplication", "extractLogcatToFile: Closing reader and writer");
            reader.close();
            writer.close();
        }
        catch (IOException e)
        {
            Log.d("eGeodetApplication", "extractLogcatToFile: In IOException");
            Log.e("eGeodetApplication", "ERROR: ", e);
            if (writer != null)
                try {
                    writer.close();
                } catch (IOException e1) {
                }
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e1) {
                }

            // You might want to write a failure message to the log here.
            return null;
        }

        return logFileSrc;
    }

    public void sendMailWithLogFile(final String logFileSrc, String stackTrace) {
        String mail = BuildConfig.NOREPLY_EGEODET_EMAIL;
        String subject = "Aplikácia eGeodet padla!";

        String model = eGeodetApplication.getModel();
        PackageInfo info = getPackageInfo(this);

        String message = "Dátum a čas: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()) + "<br/>" +
                "Verzia systému Android: " + Build.VERSION.SDK_INT + "<br/>" +
                "Zariadenie: " + model + "<br/>" +
                "Verzia aplikácie: " + (info == null ? "(null)" : info.versionCode) + "<br/><br/>" +
                "Chyba: <br/>" + stackTrace;

        String[] logFileSrcArr = {logFileSrc};
        final String zipFileSrc = getFilesDir().getAbsolutePath() + "/eGeodet_logs_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".zip";

        try {
            ZIPApi.zip(logFileSrcArr, zipFileSrc);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        final File zipFile = new File(zipFileSrc);
        if (!zipFile.exists()) {
            throw new RuntimeException("ZIP file does not exist");
        }

        String attachmentSrc = zipFileSrc;

        Log.d("eGeodetApplication", "sendMailWithLogFile: Instantiation of SkGeodesyMailAPI and executing");
        new SkGeodesyMailAPI(this, mail, subject, message, attachmentSrc, true,
                new SkGeodesyMailAPI.Consumer() {
                    @Override
                    public void afterSend(Boolean isSent) {
                        Log.d("afterSend", "afterSend: After email was sent");
                        if (isSent) {
                            File logFile = new File(logFileSrc);
                            if (logFile.exists()) {
                                logFile.delete();
                            }
                            if (zipFile.exists()) {
                                Log.d("afterSend", "afterSend: zipFile exists -> deleting");
                                zipFile.delete();
                            }
                        }
                    }
                }
        );
    }
}
