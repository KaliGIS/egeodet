package sk.gku.egeodet.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.utils.InternetCheck;
import sk.gku.egeodet.utils.SkGeodesyMailAPI;
import sk.gku.egeodet.utils.SynchronizationComponent;
import sk.gku.egeodet.utils.ZIPApi;
import sk.gku.egeodet.viewmodels.SynchronizationViewModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class SynchronizationActivity extends AppCompatActivity {
    public static final String TAG = "SyncActivity";
    private SynchronizationViewModel mSynchronizationViewModel;
    private SynchronizationComponent mSynchronizationComponent;
    private SyncListener mSyncListener;
    private TextView mAppInfo;
    private TextView mConnectionTypeWarnTxt;
    private TextView mRecordsToUpdateTxt;
    private Button mUploadBtn;
    private Button mDownloadBtn;
    private List<ToSynchronizeEntity> mToSynchronizeEntities;
    private Integer mLoggedInUserId;
    private String mUserName;
    private String mUserRole;
    private String mUserMail;
    private List<Integer> mGroupIds = new ArrayList<>();
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";
    private HashMap<String, Integer> mSyncStatus = new HashMap<>();
    private InternetCheck mInternetCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_synchronization);
        Toolbar toolbar = findViewById(R.id.synchronizationToolbar);
        setSupportActionBar(toolbar);

        // Get data from sharedPreferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mLoggedInUserId = mPreferences.getInt("loggedInUserId", -1);
        mUserName = mPreferences.getString("loggedInUserName", "");
        mUserRole = mPreferences.getString("loggedInUserRole", "");
        mUserMail = mPreferences.getString("loggedInUserMail", null);
        // getGroupIdsFromSharedPreferences();

        mRecordsToUpdateTxt = findViewById(R.id.records_to_update_txt);
        mUploadBtn = findViewById(R.id.uploadToDbButton);
        mDownloadBtn = findViewById(R.id.downloadFromDbButton);

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(mUserName);

        mToSynchronizeEntities = new ArrayList<>();

        mSynchronizationComponent = new SynchronizationComponent(getApplication(), mLoggedInUserId);
        mSyncListener = new SyncListener();

        mSynchronizationComponent.addPropertyChangeListener(mSyncListener);

        // Get the toSynchronizeEntities data
        mSynchronizationViewModel = ViewModelProviders.of(this).get(SynchronizationViewModel.class);

        mSynchronizationViewModel.getToSynchronizeEntities().observe(this, new Observer<List<ToSynchronizeEntity>>() {
            @Override
            public void onChanged(List<ToSynchronizeEntity> toSynchronizeEntities) {
                mToSynchronizeEntities = toSynchronizeEntities;
                Integer count = toSynchronizeEntities.size();
                if (count > 0) {
                    mRecordsToUpdateTxt.setText("Počet zmien, ktoré je potrebné nahrať na server: " + count);
                    mRecordsToUpdateTxt.setTextColor(getResources().getColor(R.color.colorMandatory));
                    mUploadBtn.setEnabled(true);
                } else {
                    mRecordsToUpdateTxt.setText("Nie je potrebné nahrať žiadne zmeny na server");
                    mRecordsToUpdateTxt.setTextColor(getResources().getColor(R.color.GeodeticPointStatusDone));
                    mUploadBtn.setEnabled(false);
                }
            }
        });

        mGroupIds = mSynchronizationViewModel.getGroupIdsOfUser(mLoggedInUserId);

        mUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleConnectionTypeWarning();
                Toast.makeText(
                        getApplicationContext(),
                        "Upload sa spúšťa",
                        Toast.LENGTH_LONG
                ).show();

                final InternetCheck ic = new InternetCheck(getApplicationContext());
                if (ic.isConnected()) {
                    // synchronize synchronization
                    final Synchronization syncEntity = mSynchronizationComponent.getOrCreateSynchronizationEntity(Constants.SynchronizationDirection.UPLOAD);
                    mSynchronizationComponent.synchronizeSynchronization(syncEntity, null);

                    mSynchronizationComponent.synchronizeNotSynchronizedEntities(new SynchronizationComponent.BatchSyncCallback() {
                        @Override
                        public void then(boolean isOk) {
                            syncEntity.setEndDate(new Date());
                            if (isOk) {
                                syncEntity.setStatus(Constants.SYNC_STATUS_DONE);
                            } else {
                                syncEntity.setStatus(Constants.SYNC_STATUS_DONE_WITH_ERROR);
                            }
                            // mSynchronizationComponent.changeNegativeIdsToPositive(); // If there are updated negative ids (preventing conflicts)
                            mSynchronizationComponent.synchronizeSynchronization(syncEntity, new SynchronizationComponent.BatchDownloadCallback() {
                                @Override
                                public void then(String modelName, Integer status) {
                                    showDialogAfterSynchronization();
                                }
                            });
                        }
                    });
                } else {
                    noInternetConnectionAlert();
                }
            }
        });

        mDownloadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleConnectionTypeWarning();
                InternetCheck ic = new InternetCheck(getApplicationContext());

                if (ic.isConnected()) {
                    Integer count = mToSynchronizeEntities.size();
                    if (count > 0) {
                        new AlertDialog.Builder(SynchronizationActivity.this)
                                .setTitle(R.string.warning)
                                .setMessage("Stiahnutie dát zo servera spôsobí stratu nezosynchronizovaných dát v zariadení. Neozaj chcete pokračovať?")
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        launchDownload();
                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        launchDownload();
                    }
                } else {
                    noInternetConnectionAlert();
                }

            }
        });

        mConnectionTypeWarnTxt = findViewById(R.id.connection_type_warning_txt);
        mInternetCheck = new InternetCheck(getApplicationContext());
        handleConnectionTypeWarning();
    }

    @Override
    protected void onResume() {
        super.onResume();
        handleConnectionTypeWarning();
    }

    private void handleConnectionTypeWarning() {
        if (mInternetCheck.isConnected() && mInternetCheck.isMetered()) {
            String connectionWarning = "Vaše pripojenie na internet je platené. Fotografie sa nezosynchronizujú. Ak chcete zosynchronizovať fotografie, pripojte sa na internet pomocou WiFi";
            mConnectionTypeWarnTxt.setText(connectionWarning);
            mConnectionTypeWarnTxt.setVisibility(View.VISIBLE);
        } else {
            String connectionWarning = "";
            mConnectionTypeWarnTxt.setText(connectionWarning);
            mConnectionTypeWarnTxt.setVisibility(View.GONE);
        }
    }

    public void launchDownload() {
        // launch synchronization
        Toast.makeText(
                getApplicationContext(),
                "Download sa spúšťa",
                Toast.LENGTH_LONG
        ).show();

        final HashMap<String, Boolean> modelsDownloaded = new HashMap<>();
        final Synchronization syncEntity = mSynchronizationComponent.getOrCreateSynchronizationEntity(Constants.SynchronizationDirection.DOWNLOAD);
        final Date lastSynchronizationDate = mSynchronizationComponent.getLastSynchronizationDate(syncEntity);
        final Integer currentUserId = (mUserRole.toLowerCase().equals(Constants.USER_ROLE_ADMIN)) ? null : mLoggedInUserId; // if admin, download all data

        final SynchronizationComponent.BatchDownloadCallback callback = new SynchronizationComponent.BatchDownloadCallback() {
            @Override
            public void then(String modelName, Integer status) {
                boolean allModelsDownloaded = true;
                final Integer[] finalStatus = {0};

                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    modelsDownloaded.put(modelName, true);
                }

                for (String model : modelsDownloaded.keySet()) {
                    Log.d(TAG, "then: model downloaded " + model + ": " + modelsDownloaded.get(model));
                    allModelsDownloaded = allModelsDownloaded  && modelsDownloaded.get(model);
                    finalStatus[0] = finalStatus[0] > status ? finalStatus[0] : status;
                }

                if (allModelsDownloaded) {
                    Log.d(TAG, "then: all models downloaded without notes");
                    // Notes must be downloaded last
                    modelsDownloaded.put(Constants.NOTE_MODEL, false);
                    mSynchronizationComponent.downloadNotes(null, null, true, new SynchronizationComponent.BatchDownloadCallback() {
                        @Override
                        public void then(String modelName, Integer status) {
                            if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                                modelsDownloaded.put(modelName, true);
                                Log.d(TAG, "then: All models downloaded");
                                finalStatus[0] = finalStatus[0] > status ? finalStatus[0] : status;

                                // delete already synchronized entities which have different master id
                                mSynchronizationComponent.deleteNotUpdatedSynchronizedEntities();

                                // Set lastSynchronizationDate, fake date. Changed 19/01/2021 -> master DB overwrites this
                                Date endDate = null;
                                try {
                                    endDate = new SimpleDateFormat(Constants.DATE_TIME_FORMAT).parse(Constants.EPOCH_START_DATE_TIME);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                syncEntity.setEndDate(endDate);
                                syncEntity.setStartDate(null); // This does not overwrite start date in master

                                if (finalStatus[0].equals(Constants.SYNC_STATUS_DONE)) {
                                    syncEntity.setStatus(0);
                                } else {
                                    syncEntity.setStatus(2);
                                }
                                mSynchronizationComponent.synchronizeSynchronization(syncEntity, new SynchronizationComponent.BatchDownloadCallback() {
                                    @Override
                                    public void then(String modelName, Integer status) {
                                        showDialogAfterSynchronization();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        };

        RelativeLayout progressBarLayout = findViewById(R.id.layoutProgressBar);
        TextView progressText = progressBarLayout.findViewById(R.id.progressBarText);
        progressBarLayout.setVisibility(View.VISIBLE);
        progressText.setText("Vymazávam dáta z lokálnej databázy");
        mSynchronizationComponent.removeDataFromDatabaseSync();
        progressBarLayout.setVisibility(View.GONE);
        progressText.setText("");
        mSynchronizationComponent.synchronizeSynchronization(syncEntity, null);

        modelsDownloaded.put(Constants.USER_GROUP_MODEL, false);
        mSynchronizationComponent.downloadUserGroups(new SynchronizationComponent.BatchDownloadCallback() {
            @Override
            public void then(String modelName, Integer status) {
                Log.d(TAG, "then: UserGroups downloaded");
                List<Integer> groupIds = mSynchronizationViewModel.getGroupIdsOfUser(mLoggedInUserId);
                // setGroupIdsToSharedPreferences(groupIds);
                mGroupIds.clear();
                mGroupIds.addAll(groupIds);
                // getGroupIdsFromSharedPreferences();
                final List<Integer> currentGroupIds = (mUserRole.toLowerCase().equals(Constants.USER_ROLE_ADMIN)) ? new ArrayList<Integer>() : mGroupIds; // if admin, download all data
                for (Integer groupId : currentGroupIds) {
                    Log.d(TAG, "launchDownload: userRole: " + mUserRole + ", current group id: " + groupId);
                }
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    modelsDownloaded.put(modelName, true);
                }

                modelsDownloaded.put(Constants.TASK_MODEL, false);
                mSynchronizationComponent.downloadTasks(null, new SynchronizationComponent.BatchDownloadCallback() {
                    @Override
                    public void then(String modelName, Integer status) {
                        Log.d(TAG, "then: Tasks downloaded");
                        if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                            modelsDownloaded.put(modelName, true);
                        }

                        modelsDownloaded.put(Constants.GEODETIC_POINT_MISSION_MODEL, false);
                        mSynchronizationComponent.downloadGeodeticPointMissions(null, null, currentGroupIds, new SynchronizationComponent.BatchDownloadCallback() {
                            @Override
                            public void then(String modelName, Integer status) {
                                Log.d(TAG, "then: GeodeticPointMissions downloaded");
                                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                                    modelsDownloaded.put(modelName, true);
                                }

                                modelsDownloaded.put(Constants.GEODETIC_POINT_TASK_MODEL, false);
                                mSynchronizationComponent.downloadGeodeticPointTasks(null, null, currentGroupIds, new SynchronizationComponent.BatchDownloadCallback() {
                                    @Override
                                    public void then(String modelName, Integer status) {
                                        Log.d(TAG, "then: GeodeticPointTasks downloaded");
                                        if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                                            modelsDownloaded.put(modelName, true);
                                        }

                                        List<Integer> currentMissionIds = mSynchronizationViewModel.getDownloadedMissionIds();
                                        for (Integer missionId : currentMissionIds) {
                                            Log.d(TAG, "then: currentMissionId: " + missionId);
                                        }
                                        modelsDownloaded.put(Constants.IMAGE_MODEL, false);
                                        mSynchronizationComponent.downloadImages(null, currentMissionIds, callback);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    private class SyncListener implements PropertyChangeListener {
        /**
         * Control the progress bar screen
         * @param evt
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            Map<String, Integer> currentSyncInfo = (Map<String, Integer>) evt.getNewValue();
            String modelName;
            Integer currStatus;
            boolean isSyncing = false;
            String progressStr = "";
            RelativeLayout progressBarLayout = findViewById(R.id.layoutProgressBar);
            TextView progressText = progressBarLayout.findViewById(R.id.progressBarText);
            Log.d(TAG, "propertyChange: root");

            for (String key : currentSyncInfo.keySet()) {
                modelName = key;
                currStatus = currentSyncInfo.get(modelName);
                mSyncStatus.put(modelName, currStatus);
                Log.d(TAG, "propertyChange: modelName: " + modelName);
            }

            for (String currModel : mSyncStatus.keySet()) {
                if (mSyncStatus.get(currModel).equals(Constants.SYNC_STATUS_DOWNLOADING)) {
                    isSyncing = true;
                    progressStr += "Sťahujem záznamy modelu " + currModel + "\n";
                }
                if (mSyncStatus.get(currModel).equals(Constants.SYNC_STATUS_UPLOADING)) {
                    isSyncing = true;
                    progressStr += "Nahrávam záznamy modelu " + currModel + "\n";
                }
                Log.d("MissionSelActivity", "propertyChange: syncStatus is: " + currModel + " - " + mSyncStatus.get(currModel));
            }

            if (isSyncing) {
                progressBarLayout.setVisibility(View.VISIBLE);
                progressText.setText(progressStr);
            } else {
                progressBarLayout.setVisibility(View.GONE);
                progressText.setText("");
            }
        }
    }

    public void showDialogAfterSynchronization() {
        StringBuilder errorMsg = new StringBuilder();
        String syncConflicts = getSynchronizationConflictsFromSharedPreferences();

        for (String currModel : mSyncStatus.keySet()) {
            if (mSyncStatus.get(currModel).equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                errorMsg.append(currModel).append("\n");
            }
        }

        if (errorMsg.length() == 0) { // Synchronization completed without error
            new AlertDialog.Builder(SynchronizationActivity.this)
                    .setTitle(R.string.ok)
                    .setMessage("Synchronizácia úspešne ukončená")
                    .setPositiveButton(R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        } else { // Synchronization completed with error
            new AlertDialog.Builder(SynchronizationActivity.this)
                    .setTitle(R.string.error)
                    .setMessage("Synchronizácia skončila s chybou pri entitách" + "\n\n" + errorMsg + "\nAkciu zopakujte. Ak chyba pretrváva, obráťte sa na správcu.")
                    .setPositiveButton(R.string.ok, null)
                    .setIcon(R.drawable.ic_error)
                    .show();
        }

        if (syncConflicts.length() > 0) {
            new AlertDialog.Builder(SynchronizationActivity.this)
                    .setTitle("Konflikty používateľov na bodoch a úlohách")
                    .setMessage(syncConflicts)
                    .setPositiveButton(R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    private String getSynchronizationConflictsFromSharedPreferences() {
        StringBuilder synchronizationConflicts = new StringBuilder();
        Set<String> conflictSet = mPreferences.getStringSet(Constants.SHARED_PREF_CONFLICTS_KEY, new HashSet<String>());
        for(String conflict : conflictSet) {
            synchronizationConflicts.append(conflict + "\n");
        }

        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putStringSet(Constants.SHARED_PREF_CONFLICTS_KEY, new HashSet<String>()); // remove all conflict from shared preferences
        preferencesEditor.apply();

        return synchronizationConflicts.toString();
    }

    /**
     * Opens alert dialog with information about no internet connection
     *
     * @author martin.kalivoda
     */
    public void noInternetConnectionAlert() {
        new AlertDialog.Builder(SynchronizationActivity.this)
                .setTitle(R.string.warning)
                .setMessage(R.string.no_internet_dialog_text)
                .setPositiveButton(R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

//    /**
//     * Gets group ids from shared preferences and fills the mGroupIds property. When the user is admin,
//     * method fills no group id to mGroupIds property.
//     */
//    private void getGroupIdsFromSharedPreferences() {
//        String key = "groupId0";
//        int i = 0;
//        while (mPreferences.contains(key)) {
//            Log.d(TAG, "getGroupIdsFromSharedPreferences: Adding group id: " + mPreferences.getInt(key, -1));
//            mGroupIds.add(mPreferences.getInt(key, -1));
//            i++;
//            key = String.format("groupId%d", i);
//        }
//    }

//    /**
//     * Removes old group ids and sets new ones to shared preferences
//     */
//    private void setGroupIdsToSharedPreferences(List<Integer> groupIds) {
//        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
//        String key = "groupId0";
//        int i = 0;
//        while (mPreferences.contains(key)) {
//            preferencesEditor.remove(key);
//            i++;
//            key = String.format("groupId%d", i);
//        }
//
//        i = 0;
//        for (Integer groupId : groupIds) {
//            Log.d(TAG, "onCreate: setting group id to preferences: " + groupId);
//            preferencesEditor.putInt(String.format("groupId%d", i), groupId);
//            i++;
//        }
//        preferencesEditor.apply();
//    }

    /**
     * This method is called when the options menu is created.
     *
     * @param menu
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.synchronization_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.synchronize_database) {
            InternetCheck ic = new InternetCheck(getApplicationContext());
            if (ic.isConnected()) {
                sendMailWithDatabase();
            } else {
                new AlertDialog.Builder(SynchronizationActivity.this)
                        .setTitle(R.string.warning)
                        .setMessage(R.string.no_internet_dialog_text)
                        .setPositiveButton(R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Sends mail with the SQLite database files to the administrator and user.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void sendMailWithDatabase() {
        List<String> mails = new ArrayList<>();
        mails.add(BuildConfig.RECIPIENT_EMAIL);
        if (mUserMail != null && mUserMail.contains("@")) {
            mails.add(mUserMail);
        }
        for (String mail : mails) {
            Log.d("MisSelActivity", "sendMailWithDatabase: recipient is: " + mail);
        }
        String subject = mUserName + ", eGeodet";

        String model = eGeodetApplication.getModel();
        PackageInfo info = eGeodetApplication.getPackageInfo(this);

        String message = "Používateľ <strong>" + mUserName + "</strong> vám odoslal dáta z aplikácie eGeodet.<br/><br/>" +
                "Dátum a čas: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()) + "<br/>" +
                "Verzia systému Android: " + Build.VERSION.SDK_INT + "<br/>" +
                "Zariadenie: " + model + "<br/>" +
                "Verzia aplikácie: " + (info == null ? "(null)" : info.versionCode);

        String[] databaseFileSrcs = {
                getDatabasePath("eGeodet_database").getAbsolutePath() + ".db",
                getDatabasePath("eGeodet_database").getAbsolutePath() + ".db-shm",
                getDatabasePath("eGeodet_database").getAbsolutePath() + ".db-wal"
        };

        final String zipFileSrc = getFilesDir().getAbsolutePath() + "/eGeodet_database_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) +".zip";
        try {
            ZIPApi.zip(databaseFileSrcs, zipFileSrc);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final File zipFile = new File(zipFileSrc);
        if (!zipFile.exists()) {
            throw new RuntimeException("ZIP file does not exist");
        }

        String attachmentSrc = zipFileSrc;

        new SkGeodesyMailAPI(this, mails, subject, message, attachmentSrc, false,
                new SkGeodesyMailAPI.Consumer() {
                    @Override
                    public void afterSend(Boolean isSent) {
                        Log.d("afterSend", "afterSend: After email was sent");
                        if (isSent) {
                            if (zipFile.exists()) {
                                Log.d("afterSend", "afterSend: zipFile " + zipFileSrc + " exists -> deleting");
                                zipFile.delete();
                            }
                        }
                    }
                }
        );
    }
}