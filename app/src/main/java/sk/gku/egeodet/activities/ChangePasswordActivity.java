package sk.gku.egeodet.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;


import sk.gku.egeodet.R;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.utils.InternetCheck;
import sk.gku.egeodet.viewmodels.ChangePasswordViewModel;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class ChangePasswordActivity extends AppCompatActivity {
    public static final String TAG = "SyncActivity";
    private ChangePasswordViewModel mChangePasswordViewModel;
    private User mCurrUser;
    private TextView mAppInfo;
    private EditText mOldPassEditText;
    private EditText mNewPassEditText;
    private EditText mRepeatPassEditText;
    private Button mChangePasswordBtn;
    private Integer mLoggedInUserId;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        // Get data from sharedPreferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mLoggedInUserId = mPreferences.getInt("loggedInUserId", -1);
        String userName = mPreferences.getString("loggedInUserName", "");

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName);

        mOldPassEditText = findViewById(R.id.oldPasswordEditText);
        mNewPassEditText = findViewById(R.id.newPasswordEditText);
        mRepeatPassEditText = findViewById(R.id.repeatPasswordEditText);

        // Get user data.
        mChangePasswordViewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel.class);
        mChangePasswordViewModel.getUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> users) {
                for (User user : users) {
                    if (user.getId() == mLoggedInUserId) {
                        mCurrUser = user;
                    }
                }
            }
        });

        mChangePasswordBtn = findViewById(R.id.changePasswordBtn);
        mChangePasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View changePassBtn) {
                String currPass = mCurrUser.getPassword();
                String currCheckPass = hashPassword(mOldPassEditText.getText().toString());
                final String newPass = hashPassword(mNewPassEditText.getText().toString());
                String newCheckPass = hashPassword(mRepeatPassEditText.getText().toString());
                Log.d(TAG, "onClick: currPass: " + currPass + ", currCheckPass: " + currCheckPass);

                boolean currCheckOk = validatePasswords(currPass, currCheckPass);
                boolean newCheckOk = validatePasswords(newPass, newCheckPass);

                if (!currCheckOk) {
                    mOldPassEditText.setError("Nezadali ste správne aktuálne heslo");
                    return;
                }
                if (!newCheckOk) {
                    mRepeatPassEditText.setError("Opakované heslo nie je totožné s novým heslom");
                    return;
                }

                InternetCheck ic = new InternetCheck(getApplicationContext());
                if (ic.isConnected()) {
                    changePasswordOfCurrentUser(newPass);
                } else {
                    noInternetConnectionAlert();
                }
            }
        });
    }

    public void changePasswordOfCurrentUser(String newPass) {
        mCurrUser.setPassword(newPass);
        mChangePasswordViewModel.updateUser(mCurrUser);

        final RelativeLayout progressBarLayout = findViewById(R.id.layoutProgressBar);
        final TextView progressText = progressBarLayout.findViewById(R.id.progressBarText);
        progressBarLayout.setVisibility(View.VISIBLE);
        progressText.setText("Prebieha synchronizácia nového hesla");
        mChangePasswordViewModel.synchronizeUser(mCurrUser, new ChangePasswordViewModel.SyncCallback() {
            @Override
            public void then(boolean isOk) {
                progressBarLayout.setVisibility(View.GONE);
                progressText.setText("");
                if (isOk) { // Synchronization completed without error
                    new AlertDialog.Builder(ChangePasswordActivity.this)
                            .setTitle(R.string.ok)
                            .setMessage("Synchronizácia nového hesla úspešne ukončená")
                            .setPositiveButton(R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                } else { // Synchronization completed with error
                    new AlertDialog.Builder(ChangePasswordActivity.this)
                            .setTitle(R.string.error)
                            .setMessage("Synchronizácia nového hesla skončila s chybou.\nAkciu zopakujte spôsobom, ako keby ste synchronizovali iné dáta. Ak chyba pretrváva, obráťte sa na správcu.")
                            .setPositiveButton(R.string.ok, null)
                            .setIcon(R.drawable.ic_error)
                            .show();
                }
            }
        });
    }

    public boolean validatePasswords(String pass1, String pass2) {
        if (pass1.equals(pass2)) {
            return true;
        }
        return false;
    }

    /*
     * Opens alert dialog with information about no internet connection
     *
     * @author martin.kalivoda
     */
    public void noInternetConnectionAlert() {
        new AlertDialog.Builder(ChangePasswordActivity.this)
                .setTitle(R.string.warning)
                .setMessage(R.string.no_internet_dialog_text)
                .setPositiveButton(R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private String hashPassword(String pass) {
        MessageDigest digest = null;
                        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (
        NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.update(pass.getBytes(StandardCharsets.UTF_8));
        byte[] passwordHash = digest.digest();
        StringBuilder passwordHashHex = new StringBuilder(passwordHash.length * 2);
        for (byte b : passwordHash)
            passwordHashHex.append(String.format("%02x", b & 0xFF));
        return passwordHashHex.toString();
    }
}