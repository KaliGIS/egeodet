package sk.gku.egeodet.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.GeodeticPointListAdapter;
import sk.gku.egeodet.adapters.GeodeticPointStatusListAdapter;
import sk.gku.egeodet.adapters.GeodeticPointTypeListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.JoinGeodeticPointMissionGeoPnt;
import sk.gku.egeodet.repositories.ImageRepository;
import sk.gku.egeodet.utils.GeodeticPointMissionViewModelFactory;
import sk.gku.egeodet.viewmodels.GeodeticPointMissionViewModel;
import sk.gku.egeodet.viewmodels.GeodeticPointViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class GeodeticPointSelectionActivity extends AppCompatActivity implements GeodeticPointListAdapter.AdapterCallBack {
    private static final String TAG = "GeoPntSelectionActivity";
    private RecyclerView mRecyclerView;
    private SearchView mSearchView;
    private TextView mAppInfo;
    private CheckBox mOnlyMyCheckbox;
    private Toolbar mToolbar;
    private GeodeticPointViewModel mGeodeticPointViewModel;
    private GeodeticPointMissionViewModel mGeodeticPointMissionViewModel;
    private SharedPreferences mPreferences;
    private GeodeticPointStatusListAdapter mGPStatusListAdapter;
    private GeodeticPointTypeListAdapter mGPTypeListAdapter;
    private GeodeticPointListAdapter mAdapter;
    private NotificationManager mNotifyManager;
    private NotificationReceiver mReceiver = new NotificationReceiver();
    private List<JoinGeodeticPointMissionGeoPnt> mGeodeticPointMissionsData;
    private List<JoinGeodeticPointMissionGeoPnt> mGeodeticPointMissionsDataFiltered = new ArrayList<>();
    private List<Integer> mGeodeticPointIds;
    private Integer mUserId;
    private String mUserRole;
    private List<Integer> mGroupIds = new ArrayList<>();
    private int mMissionId;
    private String mMissionName;
    private int mMissionYear;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    private Boolean mIsOnlyMyPnts = false;
    private List<Integer> mSelectedGeoPntStatuses = new ArrayList<>();
    private List<String> mSelectedGeoPntTypes = new ArrayList<>();

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);
        mGeodeticPointIds = new ArrayList<>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geodetic_point_selection);
        mToolbar = findViewById(R.id.toolbar);
        mToolbar.setSubtitle(getString(R.string.points_count) + ": 0"); // Set subtitle of activity to current geodetic points count
        setSupportActionBar(mToolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newGeodeticPointIntent = new Intent(GeodeticPointSelectionActivity.this, NewGeodeticPointActivity.class);
                startActivityForResult(newGeodeticPointIntent, Constants.REQUEST_CODE_NEW_GEODETIC_POINT_ACTIVITY);
            }
        });

        // Register local broadcast manager to observe for messages from GeodeticPointStatusListAdapter
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.GEODETIC_POINT_STATUS_INTENT);
        intentFilter.addAction(Constants.GEODETIC_POINT_TYPE_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver,
                intentFilter
        );

        // Setup notifications
        createNotificationChannel();

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.INSERT_PHOTOS_TO_DB_INTENT);
        registerReceiver(mReceiver, filter);

        // Get the missionId from an intent
        Intent intent = getIntent();
        mMissionId = intent.getIntExtra("missionId", -1);
        mMissionName = intent.getStringExtra("missionName");
        mMissionYear = intent.getIntExtra("missionYear", -1);

        // Set the mission id to shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt("missionId", mMissionId);
        preferencesEditor.putString("missionName", mMissionName);
        preferencesEditor.putInt("missionYear", mMissionYear);
        preferencesEditor.apply();

        String userName = mPreferences.getString("loggedInUserName", "");
        mUserId = mPreferences.getInt("loggedInUserId", -1);
        mUserRole = mPreferences.getString("loggedInUserRole", "");
        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + mMissionName + ": " + mMissionYear);
        if (!mUserRole.equals(Constants.USER_ROLE_ADMIN)) {
            getGroupIdsFromSharedPreferences();
        } else {
            Log.d(TAG, "onCreate: Group ids stays empty. Because of the user is admin");
        }

        // Initialize the SearchView
        mSearchView = findViewById(R.id.searchViewGeoPnts);
        mSearchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        // Initialize the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerViewGeodeticPoints);

        // Set the Layout Manager.
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

        // Set empty ArrayList to mGeodeticPointMissionsData
        mGeodeticPointMissionsData = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mAdapter = new GeodeticPointListAdapter(this, mGeodeticPointMissionsData, this);
        mRecyclerView.setAdapter(mAdapter);

        // Get the geodetic point missions data
        mGeodeticPointMissionViewModel = ViewModelProviders.of(this, new GeodeticPointMissionViewModelFactory(getApplication(), mMissionId, mGroupIds)).get(GeodeticPointMissionViewModel.class);
        mGeodeticPointViewModel = ViewModelProviders.of(this).get(GeodeticPointViewModel.class);

        mGeodeticPointMissionViewModel.getGeodeticPointMissionsWithGeoPntsByMissionId().observe(this, new Observer<List<JoinGeodeticPointMissionGeoPnt>>() {
            @Override
            public void onChanged(List<JoinGeodeticPointMissionGeoPnt> geodeticPointMissions) {
                if (geodeticPointMissions.size() > 0) {
                    mGeodeticPointMissionsData = geodeticPointMissions;
                    CharSequence prefix = mSearchView.getQuery();

                    mAdapter.setGeodeticPointMissionsGeoPnt(geodeticPointMissions, prefix);
                }
            }
        });

        mOnlyMyCheckbox = findViewById(R.id.checkbox_my_geodetic_points);
        mOnlyMyCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mIsOnlyMyPnts = isChecked;
                filterGeodeticPoints();
            }
        });

        // Set geodetic point status list adapter
        mGPStatusListAdapter = new GeodeticPointStatusListAdapter(
            this,
            R.layout.spinner_item_geodetic_point_attribute
        );
        Spinner statusSpinner = findViewById(R.id.spinner_geodetic_point_status);
        statusSpinner.setAdapter(mGPStatusListAdapter);

        // Set geodetic point type list adapter
        mGPTypeListAdapter = new GeodeticPointTypeListAdapter(
                this,
                R.layout.spinner_item_geodetic_point_attribute
        );
        Spinner typeSpinner = findViewById(R.id.spinner_geodetic_point_type);
        typeSpinner.setAdapter(mGPTypeListAdapter);

        checkIfShouldAddNewPhotosToDb();
    }

    /**
     * Gets group ids from shared preferences and fills the mGroupIds property. When the user is admin,
     * method fills no group id to mGroupIds property.
     */
    private void getGroupIdsFromSharedPreferences() {
        String key = "groupId0";
        int i = 0;
        while (mPreferences.contains(key)) {
            Log.d(TAG, "getGroupIdsFromSharedPreferences: Adding group id: " + mPreferences.getInt(key, -1));
            mGroupIds.add(mPreferences.getInt(key, -1));
            i++;
            key = String.format("groupId%d", i);
        }
    }

    /**
     * This method is called when the activity is destroyed.
     *
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        /**
         * This method is called when the broadcast is received.
         *
         * @param context
         * @param intent
         * @author martin.kalivoda@gmail.com
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: Broadcast received");

            if (intent.getAction().equals(Constants.GEODETIC_POINT_STATUS_INTENT)) {
                Log.d(TAG, "onReceive: intent action: " + Constants.GEODETIC_POINT_STATUS_INTENT);
                int statusId = intent.getIntExtra("statusId", -1);
                Boolean isChecked = intent.getBooleanExtra("isChecked", false);

                int idx = mSelectedGeoPntStatuses.indexOf(statusId);

                if (isChecked) {
                    if (idx == -1) mSelectedGeoPntStatuses.add(statusId);
                } else {
                    if (idx > -1) mSelectedGeoPntStatuses.remove(idx);
                }
            } else if (intent.getAction().equals(Constants.GEODETIC_POINT_TYPE_INTENT)) {
                Log.d(TAG, "onReceive: intent action: " + Constants.GEODETIC_POINT_TYPE_INTENT);
                String type = intent.getStringExtra("type");
                Boolean isChecked = intent.getBooleanExtra("isChecked", false);

                int idx = mSelectedGeoPntTypes.indexOf(type);

                if (isChecked) {
                    if (idx == -1) mSelectedGeoPntTypes.add(type);
                } else {
                    if (idx > -1) mSelectedGeoPntTypes.remove(idx);
                }
            }

            filterGeodeticPoints();
        }
    };

    /**
     * Filters geodetic points by the incoming filter parameters.
     *
     * @author martin.kalivoda@gmail.com
     */
    private void filterGeodeticPoints() {
        Boolean shouldAdd;
        mGeodeticPointMissionsDataFiltered.clear();

        for (JoinGeodeticPointMissionGeoPnt geodeticPointMission : mGeodeticPointMissionsData) {
            shouldAdd = true;

            if (mSelectedGeoPntStatuses.size() > 0) {
                if (!mSelectedGeoPntStatuses.contains(geodeticPointMission.getStatus())) {
                    shouldAdd = false;
                }
            }

            if (mSelectedGeoPntTypes.size() > 0) {
                if (!mSelectedGeoPntTypes.contains(geodeticPointMission.getGeodeticPointType())) {
                    shouldAdd = false;
                }
            }

            Integer currUserId = (geodeticPointMission.getUserId() != null) ? geodeticPointMission.getUserId() : -1;
            if (mIsOnlyMyPnts && !currUserId.equals(mUserId)) {
                shouldAdd = false;
            }

            Log.d(TAG, "filterGeodeticPoints: final should Add: " + shouldAdd);
            if (shouldAdd) {
                mGeodeticPointMissionsDataFiltered.add(geodeticPointMission);
            }
        }

        mGPStatusListAdapter.setSelectedStatuses(mSelectedGeoPntStatuses);
        mGPTypeListAdapter.setSelectedTypes(mSelectedGeoPntTypes);
        CharSequence prefix = mSearchView.getQuery();
        mAdapter.setGeodeticPointMissionsGeoPnt(mGeodeticPointMissionsDataFiltered, prefix);
    }

    /**
     * Called when the NewGeodeticPointActivity is resulted.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ResultCode: " + resultCode);

        if (requestCode == Constants.REQUEST_CODE_NEW_GEODETIC_POINT_ACTIVITY && resultCode == Constants.RESULT_CODE_CREATE) {
            Bundle extras = data.getExtras();
            String geodeticPointLabel = extras.getString(NewGeodeticPointActivity.EXTRA_REPLY_GP_LABEL, "");
            String geodeticPointType = extras.getString(NewGeodeticPointActivity.EXTRA_REPLY_GP_TYPE, "");
            Integer groupId = extras.getInt(NewGeodeticPointActivity.EXTRA_REPLY_GROUP_ID, -1);

            if (!geodeticPointLabel.isEmpty()) {
                int missionId = mPreferences.getInt("missionId", -1);

                Bundle dataToInsert = new Bundle();
                dataToInsert.putInt("missionId", missionId);
                dataToInsert.putInt("groupId", groupId);
                dataToInsert.putInt("userId", mUserId);

                GeodeticPoint geodeticPoint = new GeodeticPoint(geodeticPointLabel, geodeticPointType);
                Log.d(TAG, "onActivityResult: Geodetic point to set: " + geodeticPoint.toString());
                mGeodeticPointViewModel.insertAlsoIntoAssociatedTables(geodeticPoint, dataToInsert);

                createFolderForPoint(geodeticPointLabel);
            }
        }
    }

    /**
     * Creates a folder for point photos in the external storage.
     *
     * @param geodeticPointLabel
     * @author martin.kalivoda@gmail.com
     */
    private void createFolderForPoint(String geodeticPointLabel) {
        // TODO implement for Android 10 and more
        String currImageRelPath = mMissionName + "_" + mMissionYear + "/" + geodeticPointLabel;
        File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
        File geoPntImagePath = new File(storageDir, currImageRelPath);

        Log.d(TAG, "createFolderForPoint: Creating folder: " + geoPntImagePath.getAbsolutePath());
        geoPntImagePath.mkdirs();
    }

    /**
     * This method is called when the points are filtered and their count is changed.
     *
     * @param pointsCount
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public void onChangePointsCount(int pointsCount) {
        mToolbar.setSubtitle(getString(R.string.points_count) + ": " + pointsCount); // Set subtitle of activity to current geodetic points count
        setSupportActionBar(mToolbar);
    }

    /**
     * Checks the count of photo files in the point folder and the count of photos of current point
     * in the database and notifies the user if should update data.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void checkIfShouldAddNewPhotosToDb() {
        ImageRepository imageRepository = new ImageRepository(this, mMissionId, new ImageRepository.RepositoryCallBack() {
            @Override
            public void after(Boolean should) {
                Log.d(TAG, "shouldSendNotification: should: " + should);
                if (should) {
                    notifyToInsertImagesToDb();
                }
            }
        });

        imageRepository.checkIfShouldAddNewPhotosToDb(mMissionId, mMissionName, mMissionYear);
    }

    /**
     * Releases the notification to insert images found to the database.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void notifyToInsertImagesToDb() {
        Intent updateIntent = new Intent(Constants.INSERT_PHOTOS_TO_DB_INTENT);
        PendingIntent updatePendingIntent = PendingIntent.getBroadcast(
                this,
                Constants.INSERT_PHOTOS_TO_DB_NOTIFICATION_ID,
                updateIntent,
                PendingIntent.FLAG_ONE_SHOT
        );

        NotificationCompat.Builder notifyBuilder = getNotificationBuilder();
        notifyBuilder.addAction(R.drawable.ic_update, "Vložiť", updatePendingIntent);

        mNotifyManager.notify(Constants.INSERT_PHOTOS_TO_DB_NOTIFICATION_ID, notifyBuilder.build());
    }

    /**
     * Creates a notification channel in the devices with Android version 7.1.
     *
     * @uathor martin.kalivoda@gmail.com
     */
    public void createNotificationChannel() {
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    Constants.PRIMARY_NOTIFICATION_CHANNEL_ID,
                    "eGeodet Notifikácia",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notifikácia z aplikácie eGeodet");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }

    /**
     * Returns a notification builder to build a notification about possible update of images.
     *
     * @return
     * @author martin.kalivoda@gmail.com
     */
    private NotificationCompat.Builder getNotificationBuilder() {
        Intent notificationIntent = new Intent();
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                this,
                Constants.INSERT_PHOTOS_TO_DB_NOTIFICATION_ID,
                notificationIntent,
                PendingIntent.FLAG_NO_CREATE
        );

        String message = "Boli nájdené nové fotografie. Prajete si ich vložiť do databázy?";
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(this, Constants.PRIMARY_NOTIFICATION_CHANNEL_ID)
                .setContentTitle(getString(R.string.egeodet_notification))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentText(message)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        return notifyBuilder;
    }

    /**
     * Notification receiver class
     *
     * @author martin.kalivoda@gmail.com
     */
    private class NotificationReceiver extends BroadcastReceiver {

        public NotificationReceiver() {

        }

        /**
         * This method is called when the notification broadcast is received.
         *
         * @param context
         * @param intent
         * @uathor martin.kalivoda@gmail.com
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();
            Log.d(TAG, "onReceive: intentAction is " + intentAction);

            if (intentAction.equals(Constants.INSERT_PHOTOS_TO_DB_INTENT)) {
                findNewImagesAndAddThemToDatabase();
            }
        }
    }

    /**
     * Finds new images in the point folder and adds them to the database.
     *
     * @author martin.kalivoda@gmail.com
     */
    private void findNewImagesAndAddThemToDatabase() {
        ImageRepository imageRepository = new ImageRepository(this, mMissionId, null);
        imageRepository.findNewImagesAndAddThemToDatabase(mMissionName, mMissionYear);
    }
}
