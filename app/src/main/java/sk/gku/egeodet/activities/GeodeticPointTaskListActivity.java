package sk.gku.egeodet.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import sk.gku.egeodet.adapters.GeodeticPointTaskListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.repositories.GeodeticPointMissionRepository;
import sk.gku.egeodet.utils.GeodeticPointTaskViewModelFactory;
import sk.gku.egeodet.viewmodels.GeodeticPointTaskViewModel;
import sk.gku.egeodet.viewmodels.TaskViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import sk.gku.egeodet.R;

import java.util.List;

public class GeodeticPointTaskListActivity extends AppCompatActivity {
    private static final String TAG = "GeoPntTaskListActivity";
    private Context mContext;
    private RecyclerView mRecyclerView;
    private TextView mAppInfo;
    private GeodeticPointTaskViewModel mGeodeticPointTaskViewModel;
    private TaskViewModel mTaskViewModel;
    private GeodeticPointTaskListAdapter mAdapter;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";
    private String mTaskType;
    private int mMissionId;
    private int mGeodeticPointId;
    private int mUserId;
    private int mGeodeticPointStatus;

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geodetic_point_task_list);
        Toolbar toolbar = findViewById(R.id.toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent taskIntent = new Intent(GeodeticPointTaskListActivity.this, NewGeodeticPointTaskActivity.class);
                startActivityForResult(taskIntent, Constants.REQUEST_CODE_NEW_GEODETIC_POINT_TASK_ACTIVITY);
            }
        });

        mContext = this;

        // Register local broadcast manager to observe for messages from GeodeticPointTaskListAdapter
        LocalBroadcastManager.getInstance(this).registerReceiver(
                mMessageReceiver,
                new IntentFilter(Constants.GEODETIC_POINT_TASK_INTENT)
        );

        // Get the taskType from an intent
        Intent intent = getIntent();
        mTaskType = intent.getStringExtra("taskType");

        // Set the taskType to shared preferences and get ids
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putString("taskType", mTaskType);
        preferencesEditor.apply();

        mMissionId = mPreferences.getInt("missionId", -1);
        mGeodeticPointId = mPreferences.getInt("geodeticPointId", -1);
        mUserId = mPreferences.getInt("loggedInUserId", -1);
        mGeodeticPointStatus = mPreferences.getInt("geodeticPointStatus", -1);

        String userName = mPreferences.getString("loggedInUserName", "");
        String missionName = mPreferences.getString("missionName", "");
        int missionYear = mPreferences.getInt("missionYear", -1);
        String geodeticPointLabel = mPreferences.getString("geodeticPointLabel", "");
        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + missionName + ": " + missionYear + " > " + geodeticPointLabel + " > " + mTaskType);

        toolbar.setSubtitle(geodeticPointLabel + " > " + mTaskType); // Set subtitle of activity to current geodetic point label and task type
        setSupportActionBar(toolbar);

        Log.d(TAG, "onCreate: Before geodetic point tasks part");
        // Geodetic point tasks part
        final int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        // Initialize the RecyclerView.
        mRecyclerView = findViewById(R.id.recyclerViewGeodeticPointTasks);

        // Get the tasks data.
        mGeodeticPointTaskViewModel = ViewModelProviders.of(this, new GeodeticPointTaskViewModelFactory(getApplication(), mMissionId, mGeodeticPointId, mTaskType)).get(GeodeticPointTaskViewModel.class);
        Log.d(TAG, "onCreate: getting geodetic point tasks, missionId: " + mMissionId + ", mGeodeticPointId: " + mGeodeticPointId + ", taskType: " + mTaskType);
        mGeodeticPointTaskViewModel.getGPTasksByMissionIdAndGeodeticPointIdAndTaskType().observe(GeodeticPointTaskListActivity.this, new Observer<List<JoinGeodeticPointTaskTask>>() {
            @Override
            public void onChanged(@Nullable final List<JoinGeodeticPointTaskTask> tasks) {
                Log.d(TAG, "onChanged: task size " + tasks.size());

                // Initialize the adapter and set it to the RecyclerView.
                mAdapter = new GeodeticPointTaskListAdapter(mContext, tasks, mGeodeticPointStatus, mUserId);
                mRecyclerView.setAdapter(mAdapter);

                // Set the Layout Manager.
                mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, gridColumnCount));
            }
        });
    }

    /**
     * This method is called when the activity is destroyed.
     *
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        /**
         * This method is called when the broadcast is received.
         *
         * @param context
         * @param intent
         * @author martin.kalivoda@gmail.com
         */
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive: Broadcast received");
            // Get extra data included in the Intent
            int geodeticPointTaskId = intent.getIntExtra("geodeticPointTaskId", -1);
            Boolean isChecked = intent.getBooleanExtra("isChecked", false);

            if (geodeticPointTaskId != -1) {
                mGeodeticPointTaskViewModel.updateIsDone(geodeticPointTaskId, isChecked, mUserId);

                // Automatically change the geodetic point status to in progress (1)
                Log.d(TAG, "onReceive: Setting geodetic point status to 1");
                GeodeticPointMissionRepository geodeticPointMissionRepository = new GeodeticPointMissionRepository(getApplication(), mMissionId);
                geodeticPointMissionRepository.updateGeodeticPointStatus(mGeodeticPointId, mMissionId, 1);
                geodeticPointMissionRepository.updatePointOwner(mGeodeticPointId, mMissionId, mUserId);

                SharedPreferences.Editor preferencesEditor = mPreferences.edit();
                preferencesEditor.putInt("geodeticPointStatus", 1);
                preferencesEditor.apply();
            }
        }
    };

    /**
     * Called when the NewGeodeticPointTaskActivity is resulted. This method is inactive in the version 1.0.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ResultCode: " + resultCode);

        if (requestCode == Constants.REQUEST_CODE_NEW_GEODETIC_POINT_TASK_ACTIVITY && resultCode == Constants.RESULT_CODE_CREATE) {
            String taskName = data.getStringExtra(NewGeodeticPointTaskActivity.EXTRA_REPLY);

            if (!taskName.isEmpty()) {
                Task task = new Task(taskName, mTaskType);
                mTaskViewModel.insert(task);
            }
        } else if(requestCode == Constants.REQUEST_CODE_NEW_GEODETIC_POINT_TASK_ACTIVITY && resultCode == Constants.RESULT_CODE_ADD) {
            Integer taskId = data.getIntExtra(NewGeodeticPointTaskActivity.EXTRA_REPLY, -1);
            Integer groupId = null; // todo: select group id

            GeodeticPointTask geodeticPointTask = new GeodeticPointTask(mMissionId, mGeodeticPointId, taskId, mUserId, groupId, false, false);
            mGeodeticPointTaskViewModel.insertIfDoesNotExist(geodeticPointTask);
        }
    }
}
