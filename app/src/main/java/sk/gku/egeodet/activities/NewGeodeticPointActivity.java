package sk.gku.egeodet.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import sk.gku.egeodet.R;

import sk.gku.egeodet.adapters.GroupsAdapter;
import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.viewmodels.NewGeodeticPointViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewGeodeticPointActivity extends AppCompatActivity {
    private static final String TAG = "NewGeoPntActivity";
    public static final String EXTRA_REPLY_GP_LABEL = "com.example.android.eGeodet.REPLY_GP_LABEL";
    public static final String EXTRA_REPLY_GP_TYPE = "com.example.android.eGeodet.REPLY_GP_TYPE";
    public static final String EXTRA_REPLY_GROUP_ID = "com.example.android.eGeodet.EXTRA_REPLY_GROUP_ID";
    public static final int RESULT_CODE_CREATE = 2;
    private NewGeodeticPointViewModel mNewGeodeticPointViewModel;
    private GroupsAdapter mGroupsAdapter;
    private TextView mAppInfo;
    private Spinner mGeodeticPointTypeSpinner;
    private Spinner mGroupNameSpinner;
    private EditText mEditTextGPLabel;
    private SharedPreferences mPreferences;
    private List<Integer> mGroupIds = new ArrayList<>();
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_geodetic_point);

        // Get views
        mGeodeticPointTypeSpinner = findViewById(R.id.spinner_geodetic_point_type);
        mGroupNameSpinner = findViewById(R.id.spinner_group_name);
        mEditTextGPLabel = findViewById(R.id.edittext_geodetic_point_label);

        // Get values from shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        String userName = mPreferences.getString("loggedInUserName", "");
        String missionName = mPreferences.getString("missionName", "");
        int missionYear = mPreferences.getInt("missionYear", -1);
        getGroupIdsFromSharedPreferences();

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + missionName + ": " + missionYear);

        // Initialize the geodeticPointTypeAdapter and set it to the Spinner.
        ArrayAdapter<CharSequence> geodeticPointTypeAdapter = ArrayAdapter.createFromResource(this,
                R.array.geodetic_point_types, android.R.layout.simple_spinner_item);
        geodeticPointTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (mGeodeticPointTypeSpinner != null) {
            mGeodeticPointTypeSpinner.setAdapter(geodeticPointTypeAdapter);
        }

        // Initialize the adapter and set it to the RecyclerView.
        mGroupsAdapter = new GroupsAdapter(this);

        // Get the groups data
        mNewGeodeticPointViewModel = ViewModelProviders.of(this).get(NewGeodeticPointViewModel.class);
        mNewGeodeticPointViewModel.getGroups().observe(this, new Observer<List<Group>>() {
            @Override
            public void onChanged(List<Group> groups) {
                List<Group> selectedGroups = new ArrayList<>();
                for (Group group : groups) {
                    if (mGroupIds.contains(group.getId())) {
                        selectedGroups.add(group);
                    }
                }
                if (selectedGroups.size() > 0) {
                    mGroupsAdapter.setGroups(selectedGroups);
                    mGroupNameSpinner.setAdapter(mGroupsAdapter); // this will set list of values to spinner
                }
            }
        });

        final Button buttonCreate = findViewById(R.id.button_create_geodetic_point);
        buttonCreate.setEnabled(false);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditTextGPLabel.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String geodeticPointLabel = mEditTextGPLabel.getText().toString();
                    String geodeticPointType = mGeodeticPointTypeSpinner.getSelectedItem().toString();
                    Group selectedGroup = (Group) mGroupNameSpinner.getSelectedItem();
                    Integer groupId = selectedGroup.getId();
                    Log.d(TAG, "onClick: Selected group id: " + groupId);

                    replyIntent.putExtra(EXTRA_REPLY_GP_LABEL, geodeticPointLabel);
                    replyIntent.putExtra(EXTRA_REPLY_GP_TYPE, geodeticPointType);
                    replyIntent.putExtra(EXTRA_REPLY_GROUP_ID, groupId);
                    setResult(RESULT_CODE_CREATE, replyIntent);
                }
                finish();
            }
        });

        mEditTextGPLabel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String currText = mEditTextGPLabel.getText().toString();
                Pattern alphanumeric = Pattern.compile("^[a-zA-Z0-9/.-]+$");
                Matcher matcher = alphanumeric.matcher(currText);

                if (currText.length() == 0) {
                    mEditTextGPLabel.setError("Toto pole nesmie byť prázdne");
                    buttonCreate.setEnabled(false);
                } else if (!matcher.matches()) {
                    mEditTextGPLabel.setError("Názov bodu smie obsahovať iba alfanumerické znaky, bodku '.' a znak lomené '/'");
                    buttonCreate.setEnabled(false);
                } else {
                    buttonCreate.setEnabled(true);
                }
            }
        });
    }

    /**
     * Gets group ids from shared preferences and fills the mGroupIds property. When the user is admin,
     * method fills no group id to mGroupIds property.
     */
    private void getGroupIdsFromSharedPreferences() {
        String key = "groupId0";
        int i = 0;
        while (mPreferences.contains(key)) {
            Log.d(TAG, "getGroupIdsFromSharedPreferences: Adding group id: " + mPreferences.getInt(key, -1));
            mGroupIds.add(mPreferences.getInt(key, -1));
            i++;
            key = String.format("groupId%d", i);
        }
    }
}
