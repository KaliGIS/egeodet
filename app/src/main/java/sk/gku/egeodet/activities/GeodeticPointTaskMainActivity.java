package sk.gku.egeodet.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.ImageListAdapter;
import sk.gku.egeodet.adapters.TaskTypeListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.models.JoinNoteUser;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.models.TaskType;
import sk.gku.egeodet.repositories.GeodeticPointMissionRepository;
import sk.gku.egeodet.repositories.GeodeticPointRepository;
import sk.gku.egeodet.repositories.ImageRepository;
import sk.gku.egeodet.utils.GeodeticPointMissionViewModelFactory;
import sk.gku.egeodet.utils.GeodeticPointTaskViewModelFactory;
import sk.gku.egeodet.utils.ImageViewModelFactory;
import sk.gku.egeodet.utils.InternetCheck;
import sk.gku.egeodet.utils.NoteViewModelFactory;
import sk.gku.egeodet.utils.SkGeodesyMailAPI;
import sk.gku.egeodet.utils.ZIPApi;
import sk.gku.egeodet.viewmodels.GeodeticPointMissionViewModel;
import sk.gku.egeodet.viewmodels.GeodeticPointTaskViewModel;
import sk.gku.egeodet.viewmodels.ImageViewModel;
import sk.gku.egeodet.viewmodels.NoteViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class GeodeticPointTaskMainActivity extends AppCompatActivity {
    private static final String TAG = "GeoPntTaskMainActivity";
    private TextView mTextView;
    private Button mButtonNotes;
    private TextView mAppInfo;
    private TextView mStatus;
    private MenuItem mChangeStatusItem;
    private RecyclerView mRecyclerViewTaskType;
    private RecyclerView mRecyclerViewImage;
    private ImageView mImageView;
    private String mCurrentPhotoPath;
    private Uri mCurrentPhotoUri;
    private Context mContext;
    private GeodeticPointTaskViewModel mGeodeticPointTaskViewModel;
    private NoteViewModel mNoteViewModel;
    private GeodeticPointMissionViewModel mGeodeticPointMissionViewModel;
    private ImageViewModel mImageViewModel;
    private SharedPreferences mPreferences;
    private TaskTypeListAdapter mTaskTypeAdapter;
    private ImageListAdapter mImageAdapter;
    ItemTouchHelper.SimpleCallback mSimpleItemTouchCallback;
    private List<TaskType> mTaskTypesData;
    private List<Note> mNotesData;
    private List<Image> mImagesData;
    private int mGridColumnCount;
    private int mGeodeticPointStatus;
    private int mGeodeticPointMissionId;
    private int mGeodeticPointId;
    private int mMissionId;
    private String mUserName;
    private Integer mLoggedInUserId;
    private String mMissionName;
    private int mMissionYear;
    private String mGeodeticPointLabel;
    private String mGeodeticPointType;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geodetic_point_task_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        mContext = this;
        mGridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        // Request permissions
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.PERMISSION_REQUEST_WRITE_STORAGE);
        }

        // Get the geodetic point info from an intent
        Intent intent = getIntent();
        mGeodeticPointId = intent.getIntExtra("geodeticPointId", -1);
        mGeodeticPointLabel = intent.getStringExtra("geodeticPointLabel");
        mGeodeticPointType = intent.getStringExtra("geodeticPointType");
        mGeodeticPointMissionId = intent.getIntExtra("geodeticPointMissionId", -1);

        toolbar.setSubtitle(mGeodeticPointLabel); // Set subtitle of activity to current geodetic point label
        setSupportActionBar(toolbar);

        // Set the geodetic point id to shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt("geodeticPointId", mGeodeticPointId);
        preferencesEditor.putString("geodeticPointLabel", mGeodeticPointLabel);
        preferencesEditor.apply();

        mMissionId = mPreferences.getInt("missionId", -1);
        mGeodeticPointStatus = mPreferences.getInt("geodeticPointStatus", -1);
        Log.d(TAG, "onCreate: Current geodetic point status: " + mGeodeticPointStatus);
        mUserName = mPreferences.getString("loggedInUserName", "");
        mLoggedInUserId = mPreferences.getInt("loggedInUserId", -1);
        mMissionName = mPreferences.getString("missionName", "");
        mMissionYear = mPreferences.getInt("missionYear", -1);

        setAppInfoAndStatus();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                // Ensure that there's a camera activity to handle the intent
                if (photoIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    Uri photoUri = createImageFile();

                    Log.d(TAG, "onClick: photoFile is: " + photoUri);
                    // Continue only if the File was successfully created
                    if (photoUri != null) {
                        photoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                        startActivityForResult(photoIntent, Constants.REQUEST_CODE_TAKE_PHOTO);
                    }
                }
            }
        });
        if (mGeodeticPointStatus == 2) { // make a fab visible by point status
            fab.setVisibility(View.INVISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }

        mTextView = findViewById(R.id.adminNote);

        mNoteViewModel = ViewModelProviders.of(this, new NoteViewModelFactory(getApplication(), mMissionId, mGeodeticPointId)).get(NoteViewModel.class);
        mNoteViewModel.getNotesWithUsersByMissionIdAndGeoPntId().observe(this, new Observer<List<JoinNoteUser>>() {
            @Override
            public void onChanged(List<JoinNoteUser> notes) {
                boolean shouldBeVisible = false;

                if (notes.size() > 0) {
                    for (JoinNoteUser note : notes) {
                        Log.d(TAG, "onChanged: noteUser " + note.getUserName() + ", " + note.getUserRole());
                        if (note.getUserRole().equals("admin")) {
                            Log.d(TAG, "onChanged: note " + note.getNote());
                            shouldBeVisible = true;
                            mTextView.setText(note.getNote());
                            break;
                        }
                    }
                }

                if (shouldBeVisible) {
                    mTextView.setVisibility(View.VISIBLE);
                } else {
                    mTextView.setVisibility(View.GONE);
                }
            }
        });

        mGeodeticPointMissionViewModel = ViewModelProviders.of(this, new GeodeticPointMissionViewModelFactory(getApplication(), mMissionId, new ArrayList<Integer>())).get(GeodeticPointMissionViewModel.class);

        // Task types part
        // Initialize the RecyclerView.
        mRecyclerViewTaskType = findViewById(R.id.recyclerViewTaskTypes);

        // Set the Layout Manager.
        mRecyclerViewTaskType.setLayoutManager(new GridLayoutManager(this, mGridColumnCount));

        // Set empty ArrayList to mGeodeticPointTasksData
        mTaskTypesData = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mTaskTypeAdapter = new TaskTypeListAdapter(this, mTaskTypesData);
        mRecyclerViewTaskType.setAdapter(mTaskTypeAdapter);

        // Get the tasks data.
        mGeodeticPointTaskViewModel = ViewModelProviders.of(this, new GeodeticPointTaskViewModelFactory(getApplication(), mMissionId, mGeodeticPointId, "")).get(GeodeticPointTaskViewModel.class);
        mGeodeticPointTaskViewModel.getDistinctTaskTypesByMissionIdAndGeoPntId().observe(GeodeticPointTaskMainActivity.this, new Observer<List<TaskType>>() {
            @Override
            public void onChanged(@Nullable final List<TaskType> taskTypes) {
                Log.d(TAG, "onChanged: taskTypes size " + taskTypes.size());
                mTaskTypeAdapter.setTaskTypes(taskTypes);
            }
        });

        // Note button part
        mButtonNotes = findViewById(R.id.button_notes);
        mButtonNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noteIntent = new Intent(GeodeticPointTaskMainActivity.this, NoteActivity.class);
                mContext.startActivity(noteIntent);
            }
        });

        // Images part
        // Initialize the RecyclerView.
        mRecyclerViewImage = findViewById(R.id.recyclerViewImages);

        // Set the Layout Manager.
        mRecyclerViewImage.setLayoutManager(new GridLayoutManager(this, mGridColumnCount * 2));

        // Set empty ArrayList to mImagesData
        mImagesData = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mImageAdapter = new ImageListAdapter(this, mImagesData);
        mRecyclerViewImage.setAdapter(mImageAdapter);

        // Get the images data.
        mImageViewModel = ViewModelProviders.of(this, new ImageViewModelFactory(getApplication(), mMissionId, mGeodeticPointId)).get(ImageViewModel.class);
        mImageViewModel.getImagesByMissionIdAndGeodeticPointId().observe(GeodeticPointTaskMainActivity.this, new Observer<List<Image>>() {
            @Override
            public void onChanged(@Nullable final List<Image> images) {
                Log.d(TAG, "onChanged: images size " + images.size());
                mImageAdapter.setImages(images);
            }
        });

        // Image item touch helper
        int swipeDirs;
        if (mGeodeticPointStatus == 2) {
            swipeDirs = 0;
        } else {
            swipeDirs = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        }
        mSimpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0,
                swipeDirs) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int swipeDir) {
                new AlertDialog.Builder(viewHolder.itemView.getContext())
                        .setMessage(R.string.question_remove_selected_item)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                int position = viewHolder.getAdapterPosition();
                                Image image = mImageAdapter.getImageAtPosition(position);

                                if (image != null) {
                                    Integer imageId = image.getId();
                                    Log.d(TAG, "onSwiped: Deleting image with id: " + imageId);
                                    mImageViewModel.delete(image);
                                    deleteFromFileSystem(image);

                                    mGeodeticPointStatus = 1;
                                    updateStatusInSharedPreferences(mGeodeticPointStatus);
                                    setAppInfoAndStatus();
                                    setChangeStatusItem();
                                }
                            }})
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog,
                                    // so we will refresh the adapter to prevent hiding the item from UI
                                    mImageAdapter.notifyItemChanged(viewHolder.getAdapterPosition());
                                }
                            })
                        .create()
                        .show();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(mSimpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerViewImage);

        findNewImagesAndAddThemToDatabase();
    }

    /**
     * Deletes the image from the file system.
     *
     * @param image
     * @author martin.kalivoda@gmail.com
     */
    private void deleteFromFileSystem(Image image) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//            // TODO implement for Android 10 and more
//            String currImageRelPath = "Pictures/eGeodetImages/" + mMissionName + "_" + mMissionYear + "/" + mGeodeticPointLabel + "/";
//            String[] projection = new String[] {
//                    MediaStore.Images.Media._ID,
//                    MediaStore.Images.Media.DISPLAY_NAME,
//            };
//            String selection = MediaStore.Images.Media.RELATIVE_PATH + " LIKE ? AND " +
//                    MediaStore.Images.Media.DISPLAY_NAME + " LIKE ?";
//            String[] selectionArgs = new String[] {
//                    "%" + currImageRelPath + "%",
//                    "%" + image.getName() + "%"
//            };
//
//            try (Cursor cursor = getContentResolver().query(
////                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                    MediaStore.Images.Media.getContentUri(
//                            MediaStore.VOLUME_EXTERNAL_PRIMARY),
//                    projection,
//                    selection,
//                    selectionArgs,
//                    null
//            )) {
//                // Cache column indices.
//                int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
//                int nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
//
//                while (cursor.moveToNext()) {
//                    // Get values of columns for a given image.
//                    long id = cursor.getLong(idColumn);
//                    String imageName = cursor.getString(nameColumn);
//                    Log.d(TAG, "mediaStore image name: " + imageName + ", relPath: " + currImageRelPath +
//                            ", geoPointId: " + mGeodeticPointId + ", missionId: " + mMissionId);
//                    Log.d(TAG, "deleteFromFileSystem: image name from database: " + image.getName());
//
//                    Uri contentUri = ContentUris.withAppendedId(
//                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
//
//                    ContentResolver resolver = getContentResolver();
//                    int removeCount = -1;
//
//                    try {
//                        removeCount = resolver.delete(contentUri, null, null);
//                    } catch (SecurityException securityException) {
//                        RecoverableSecurityException recoverableSecurityException;
//                        if (securityException instanceof RecoverableSecurityException) {
//                            recoverableSecurityException =
//                                    (RecoverableSecurityException) securityException;
//                        } else {
//                            throw new RuntimeException(
//                                    securityException.getMessage(), securityException);
//                        }
//
//                        IntentSender intentSender = recoverableSecurityException.getUserAction()
//                                .getActionIntent().getIntentSender();
//                        try {
//                            startIntentSenderForResult(intentSender, Constants.REQUEST_CODE_RECOVERABLE_SECURITY_EXCEPTION,
//                                    null, 0, 0, 0, null);
//                        } catch (IntentSender.SendIntentException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//
//                    Log.d(TAG, "deleteFromFileSystem: count of removed images: " + removeCount);
//                    if (removeCount > 0) {
//                        Log.d(TAG, "deleteFromFileSystem: image deleted: " + imageName);
//                    } else {
//                        Log.d(TAG, "deleteFromFileSystem: image could not delete");
//                    }
//                }
//            }
//        } else {
            File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
            File imagePath = new File(storageDir, mMissionName + "_" + mMissionYear + "/" + mGeodeticPointLabel);
            File imageSrc = new File(imagePath, image.getName());

            if (imageSrc.exists()) {
                boolean deleted = imageSrc.delete();
                if (deleted) {
                    Log.d(TAG, "deleteFromFileSystem: image deleted: " + image.getName());
                } else {
                    Log.d(TAG, "deleteFromFileSystem: image could not delete");
                }
            }
//        }
    }

    /**
     * Finds new images of current point and adds them to the database.
     *
     * @author martin.kalivoda@gmail.com
     */
    private void findNewImagesAndAddThemToDatabase() {
        String imageName = "";
        String currImageRelPath;

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            // TODO implement for Android 10 and more
//            currImageRelPath = "Pictures/eGeodetImages/" + mMissionName + "_" + mMissionYear + "/" + mGeodeticPointLabel + "/";
//            String[] projection = new String[] {
//                    MediaStore.Images.Media._ID,
//                    MediaStore.Images.Media.DISPLAY_NAME,
//            };
//            String selection = MediaStore.Images.Media.RELATIVE_PATH + " LIKE ? ";
//            String[] selectionArgs = new String[] {
//                    "%" + currImageRelPath + "%"
//            };
//
//            Log.d(TAG, "findNewImagesAndAddThemToDatabase: currentImageRelPath: " + currImageRelPath);
//            Log.d(TAG, "mediaStore before query, VOLUME_EXTERNAL_PRIMARY URI: " + MediaStore.Images.Media.getContentUri(
//                    MediaStore.VOLUME_EXTERNAL_PRIMARY));
//            Log.d(TAG, "findNewImagesAndAddThemToDatabase: MediaStore.Images.Media.EXTERNAL_CONTENT_URI: " + MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//
//            try (Cursor cursor = getContentResolver().query(
//                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
////                    MediaStore.Images.Media.getContentUri(
////                            MediaStore.VOLUME_EXTERNAL_PRIMARY),
//                    projection,
//                    selection,
//                    selectionArgs,
//                    null
//            )) {
//                // Cache column indices.
//                int idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
//                int nameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
//
//                Log.d(TAG, "findNewImagesAndAddThemToDatabase: Before cursor.moveToNext");
//                while (cursor.moveToNext()) {
//                    // Get values of columns for a given image.
//                    long id = cursor.getLong(idColumn);
//                    imageName = cursor.getString(nameColumn);
//                    Log.d(TAG, "mediaStore image name: " + imageName + ", relPath: " + currImageRelPath +
//                            ", geoPointId: " + mGeodeticPointId + ", missionId: " + mMissionId);
//
////                    if (imageName != "") {
////                        Image image = new Image(imageName, currImageRelPath, mGeodeticPointId, mMissionId);
////                        mImageViewModel.insertIfNotExist(image);
////                    }
//                }
//            }
//        } else {
            currImageRelPath = mMissionName + "_" + mMissionYear + "/" + mGeodeticPointLabel;
            File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
            File imagePath = new File(storageDir, currImageRelPath);

            FileFilter imgFilter = new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    if (pathname.isFile() && pathname.getName().endsWith(".jpg")) {
                        return true;
                    }
                    return false;
                }
            };

            if (imagePath.exists()) {
                File[] images = imagePath.listFiles(imgFilter);

                for (File imageSrc : images) {
                    Log.d(TAG, "findNewImagesAndAddThemToDatabase: image: " + imageSrc.toString());
                    int lastSlashPosition = imageSrc.toString().lastIndexOf('/');
                    imageName = imageSrc.toString().substring(lastSlashPosition + 1);
                    Log.d(TAG, "mediaStore image name: " + imageName + ", relPath: " + "eGeodetImages/" + currImageRelPath + "/" +
                            ", geoPointId: " + mGeodeticPointId + ", missionId: " + mMissionId);

                    if (imageName != "") {
                        Image image = new Image(imageName, currImageRelPath, mGeodeticPointId, mMissionId);
                        ImageRepository imageRepository = new ImageRepository(getApplication(), mMissionId, new ImageRepository.RepositoryCallBack() {
                            @Override
                            public void after(Boolean isInserted) {
                                if (isInserted) {
                                    mGeodeticPointStatus = 1;
                                    updateStatusInSharedPreferences(mGeodeticPointStatus);
                                    setAppInfoAndStatus();
                                    setChangeStatusItem();
                                }
                            }
                        });
                        imageRepository.insertIfNotExist(image);
                    }
                }
            } else {
                Log.d(TAG, "findNewImagesAndAddThemToDatabase: imagePath does not exist: " + imagePath);
            }
//        }
    }

    /**
     * Sets the UI views with current information (user name, mission name, mission year,
     * geodetic point label, geodetic point status).
     *
     * @author martin.kalivoda@gmail.com
     */
    private void setAppInfoAndStatus() {
        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(mUserName + " > " + mMissionName + ":" + mMissionYear + " > " + mGeodeticPointLabel);

        mStatus = findViewById(R.id.textview_geodetic_point_attribute);
        setStatusTextView(mGeodeticPointStatus);
    }

    /**
     * Changes an image of menu item view.
     *
     * @author martin.kalivoda@gmail.com
     */
    private void setChangeStatusItem() {
        if (mChangeStatusItem != null) {
            switch (mGeodeticPointStatus) {
                case 0:
                case 2:
                    mChangeStatusItem.setIcon(R.drawable.ic_edit);
                    break;
                case 1:
                    mChangeStatusItem.setIcon(R.drawable.ic_done);
                    break;
            }
        }
    }

    /**
     * This method is called when the permission request is resulted.
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constants.PERMISSION_REQUEST_WRITE_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED ) {
                Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT);
            } else {
                Toast.makeText(mContext, "Permission not granted", Toast.LENGTH_SHORT);
                finish();
            }
        }
    }

    /**
     * This method is called when the camera activity is resulted.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCurrentPhotoPath = mPreferences.getString("currentPhotoPath", null);
        Log.d(TAG, "onActivityResult: mCurrentPhotoPath after get from preferences: " + mCurrentPhotoPath);

        if (mCurrentPhotoPath == null) { // Prevent application crash
            return;
        }

        if (requestCode == Constants.REQUEST_CODE_TAKE_PHOTO && resultCode == RESULT_OK) {
            int geoPntId = mPreferences.getInt("geodeticPointId", -1);
            int missionId = mPreferences.getInt("missionId", -1);
            String photoPath = "";
            String photoName = "";

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
//                // TODO implement for Android 10 and more
//                Log.d(TAG, "onActivityResult: VERSION IS Q");
//                if (getContentResolver() != null) {
//                    ContentValues newImageDetails = new ContentValues();
//                    newImageDetails.put(MediaStore.Images.Media.IS_PENDING, 0);
//                    getContentResolver().update(mCurrentPhotoUri, newImageDetails, null, null);
//                    Log.d(TAG, "onActivityResult: image updated. Before cursor");
//                    Cursor cursor = getContentResolver().query(mCurrentPhotoUri, null, null, null, null);
//                    if (cursor != null) {
//                        Log.d(TAG, "onActivityResult: cursor is not null");
//                        cursor.moveToFirst();
//                        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.RELATIVE_PATH);
//                        photoPath = cursor.getString(idx);
//                        Log.d(TAG, "onActivityResult: photo path Android 10: " + photoPath);
//                        idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME);
//                        photoName = cursor.getString(idx);
//                        Log.d(TAG, "onActivityResult: photo name Android 10: " + photoName);
//                        cursor.close();
//                    } else {
//                        Log.d(TAG, "onActivityResult: cursor is null :(");
//                    }
//                } else {
//                    Log.d(TAG, "onActivityResult: getContentResolver() returned null");
//                }
//            } else {
                File currentPhoto = new File(mCurrentPhotoPath);

                // Scale down and compress the photo
                int maxSide = (mGeodeticPointType.equals(Constants.GEODETIC_POINT_TYPE_BORDER))
                        ? Constants.IMAGE_MAX_SIDE_GEODETIC_POINT_TYPE_BORDER
                        : Constants.IMAGE_MAX_SIDE_GEODETIC_POINT_TYPE_OTHER;
                Log.d(TAG, "onActivityResult: geodetic point type: " + mGeodeticPointType + ", maxSide: " + maxSide);
                Bitmap scaledBmp = resizeBitmapFromFile(currentPhoto, maxSide);
                Log.d(TAG, "onActivityResult: Current photo name: " + currentPhoto.getName());

                File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
                String geoPntLabel = mPreferences.getString("geodeticPointLabel", "");
                File imagePath = new File(storageDir, mMissionName + "_" + mMissionYear + "/" + geoPntLabel);
                imagePath.mkdirs();

                File compressedPhoto = new File(imagePath, currentPhoto.getName());
                try {
                    compressedPhoto.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (FileOutputStream out = new FileOutputStream(compressedPhoto)) {
                    scaledBmp.compress(Bitmap.CompressFormat.JPEG, 75, out);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (compressedPhoto.exists()) {
                    photoPath = compressedPhoto.getParentFile().getAbsolutePath();
                    Log.d(TAG, "onActivityResult: compressedPhoto path: " + photoPath);
                    photoName = compressedPhoto.getName();
                    Log.d(TAG, "onActivityResult: compressedPhoto name: " + photoName);
                }

                deleteRawPhoto(currentPhoto);
//            }

            String[] photoAbsPathSplitted = photoPath.split("eGeodetImages/", 2);
            String photoRelPath = photoAbsPathSplitted[1];
            Log.d(TAG, "onActivityResult: currentPhotoName: " + photoName);
            Log.d(TAG, "onActivityResult: currentPhotoParentPath: " + photoPath);
            Log.d(TAG, "onActivityResult: currentPhotoRelParentPath: " + photoRelPath);

            if (!photoName.equals("")) {
                Image image = new Image(photoName, photoRelPath, geoPntId, missionId);
                mImageViewModel.insert(image);

                // Change status to in progress
                if (mGeodeticPointStatus != 1) {
                    mGeodeticPointStatus = 1;
                    setStatusTextView(mGeodeticPointStatus);
                    Log.d(TAG, "onActivityResult: mChangeStatusItem: " + mChangeStatusItem);
                    if (mChangeStatusItem != null) {
                        mChangeStatusItem.setIcon(R.drawable.ic_done);
                    }
                    updateCurrentGeodeticPointMissionStatus(mGeodeticPointStatus);
                    updateStatusInSharedPreferences(mGeodeticPointStatus);
                    updatePointOwner();
                }
            } else {
                Log.e(TAG, "ERROR: New image comes with empty name");
            }
        } else {
            Log.d(TAG, "onActivityResult: RESULT IS NOT OK. Current photo src: " + mCurrentPhotoPath);
            File emptyPhoto = new File(mCurrentPhotoPath);
            if (emptyPhoto.exists()) {
                emptyPhoto.delete();
            }
        }
    }

    private void deleteRawPhoto(File rawPhoto) {
        if (rawPhoto.exists()) {
            rawPhoto.delete();
            File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
            File rawPath = new File(storageDir, mMissionName + "_" + mMissionYear + "/raw");
            if (rawPath.exists() && rawPath.listFiles().length == 0) { // raw path exists and is empty
                rawPath.delete();
            }
        }
    }

    /**
     * Calculates the in sample size bitmap factory parameter.
     *
     * @param options
     * @param maxSide
     * @return
     * @author martin.kalivoda@gmail.com
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int maxSide) {
        // Raw height and width of image
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;

        if (width > maxSide || height > maxSide) {
            final int halfWidth = width / 2;
            final int halfHeight = height / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfWidth / inSampleSize) >= maxSide || (halfHeight / inSampleSize) >= maxSide) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * Resizes the image to passed maximum side pixel value.
     *
     * @param origImage
     * @param maxSide
     * @return
     * @author martin.kalivoda@gmail.com
     */
    public static Bitmap resizeBitmapFromFile(File origImage, int maxSide) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(origImage.getAbsolutePath(), options);

        int inWidth = options.outWidth;
        int inHeight = options.outHeight;
        int inSampleSize = calculateInSampleSize(options, maxSide);

        options.inSampleSize = inSampleSize;

        if (inWidth > inHeight) {
            options.inDensity = inWidth;
        } else {
            options.inDensity = inHeight;
        }

        options.inTargetDensity = maxSide * inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(origImage.getAbsolutePath(), options);
    }

    /**
     * Updates geodetic point status in the shared preferences.
     *
     * @param status
     * @author martin.kalivoda@gmail.com
     */
    private void updateStatusInSharedPreferences(int status) {
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putInt("geodeticPointStatus", status);
        preferencesEditor.apply();
    }

    /**
     * Updates the status of point in the database.
     *
     * @param status
     * @author martin.kalivoda@gmail.com
     */
    private void updateCurrentGeodeticPointMissionStatus(int status) {
        Log.d(TAG, "updateCurrentGeodeticPointMissionStatus: Updating point status to: " + status);
        GeodeticPointMissionRepository geodeticPointMissionRepository = new GeodeticPointMissionRepository(getApplication(), mMissionId);
        geodeticPointMissionRepository.updateGeodeticPointStatus(mGeodeticPointId, mMissionId, status);
    }

    /**
     * Creates an empty image file which is later passed to camera.
     *
     * @return
     * @throws IOException
     * @author martin.kalivoda@gmail.com
     */
    private Uri createImageFile() {
        // Create an image file name
        String geoPntLabel = mPreferences.getString("geodeticPointLabel", "");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp;

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // TODO implement for Android 10 and more
//            Log.d(TAG, "createImageFile: Android version 10 and more");
//            // Add a image item.
//            ContentResolver resolver = getApplicationContext().getContentResolver();
//
//            // Find all audio files on the primary external storage device.
//            // On API <= 28, use VOLUME_EXTERNAL instead.
//            Uri imagesCollection = MediaStore.Images.Media.getContentUri(
//                    MediaStore.VOLUME_EXTERNAL_PRIMARY);
//            // Uri imagesCollection = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//
//            // Publish a new image.
//            ContentValues newImageDetails = new ContentValues();
//            newImageDetails.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/eGeodetImages/" + mMissionName + "_" + mMissionYear + "/" + geoPntLabel + "/");
//            newImageDetails.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
//            newImageDetails.put(MediaStore.Images.Media.DISPLAY_NAME, imageFileName + ".jpg");
//
//            // Keeps a handle to the new image's URI in case we need to modify it
//            // later.
//            Uri newImageUri = resolver.insert(imagesCollection, newImageDetails);
//            Log.d(TAG, "createImageFile: newImageUri: " + newImageUri);
//            mCurrentPhotoUri = newImageUri;
//            return newImageUri;
//        } else {
        Log.d(TAG, "createImageFile: Android version less than 10");
        File storageDir = getExternalStoragePublicDirectory("/eGeodetImages");
        File imagePath = new File(storageDir, mMissionName + "_" + mMissionYear + "/raw");
        imagePath.mkdirs();
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",   /* suffix */
                    imagePath       /* directory */
            );
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = image.getAbsolutePath();
            Uri newImageUri = FileProvider.getUriForFile(mContext,
                    "com.example.android.fileprovider",
                    image);

            Log.d(TAG, "createImageFile: setting mCurrentPhotoPath to preferences: " + mCurrentPhotoPath);
            SharedPreferences.Editor preferencesEditor = mPreferences.edit();
            preferencesEditor.putString("currentPhotoPath", mCurrentPhotoPath);
            preferencesEditor.apply();
            return newImageUri;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
//        }
    }

    /**
     * This method is called when the options menu is created.
     *
     * @param menu
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.geodetic_point_task_main_menu, menu);
        mChangeStatusItem = menu.getItem(0);
        setChangeStatusItem();

        return true;
    }

    /**
     * This method is called when the options item is selected.
     *
     * @param item
     * @return
     * @author martin.kalivoda@gmail.com
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.change_status) {
            int newGeoPntStatus;
            GeodeticPointRepository geodeticPointRepository = new GeodeticPointRepository(getApplication());
            FloatingActionButton fab = findViewById(R.id.fab);

            if (mGeodeticPointStatus == 0 || mGeodeticPointStatus == 2) {
                newGeoPntStatus = 1;
                item.setIcon(R.drawable.ic_done);
                mSimpleItemTouchCallback.setDefaultSwipeDirs(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
                fab.setVisibility(View.VISIBLE);
            } else {
                newGeoPntStatus = 2;
                item.setIcon(R.drawable.ic_edit);
                geodeticPointRepository.updateLastMaintenance(mGeodeticPointId);
                mSimpleItemTouchCallback.setDefaultSwipeDirs(0);
                fab.setVisibility(View.INVISIBLE);
            }

            updateCurrentGeodeticPointMissionStatus(newGeoPntStatus);
            setStatusTextView(newGeoPntStatus);
            updateStatusInSharedPreferences(newGeoPntStatus);
            mGeodeticPointStatus = newGeoPntStatus;
            updatePointOwner();

            return true;
        }

        if (id == R.id.send_photos) {
            InternetCheck ic = new InternetCheck(getApplicationContext());
            if (ic.isConnected()) {
                sendMailWithCurrPntPhotos();
            } else {
                new AlertDialog.Builder(GeodeticPointTaskMainActivity.this)
                        .setTitle(R.string.warning)
                        .setMessage(R.string.no_internet_dialog_text)
                        .setPositiveButton(R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Updates geodetic point missions's user_id
     */
    private void updatePointOwner() {
        mGeodeticPointMissionViewModel.updatePointOwner(mGeodeticPointId, mMissionId, mLoggedInUserId);
    }

    /**
     * Updates the status text UI view with the current geodetic point status.
     *
     * @param geodeticPointStatus
     * @author martin.kalivoda@gmail.com
     */
    public void setStatusTextView(int geodeticPointStatus) {
        switch (geodeticPointStatus) {
            case 0:
                mStatus.setText(Constants.GEODETIC_POINT_STATUSES[1].getName());
                mStatus.setBackgroundColor(getResources().getColor(R.color.GeodeticPointStatusNotStarted));
                break;
            case 1:
                mStatus.setText(Constants.GEODETIC_POINT_STATUSES[2].getName());
                mStatus.setBackgroundColor(getResources().getColor(R.color.GeodeticPointStatusInProgress));
                break;
            case 2:
                mStatus.setText(Constants.GEODETIC_POINT_STATUSES[3].getName());
                mStatus.setBackgroundColor(getResources().getColor(R.color.GeodeticPointStatusDone));
                break;
        }
    }

    /**
     * This method is called when the activity is resumed.
     *
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (mChangeStatusItem != null) {

            mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
            mGeodeticPointStatus = mPreferences.getInt("geodeticPointStatus", -1);
            setAppInfoAndStatus();
            setChangeStatusItem();
        }
    }

    /**
     * Sends an mail with the current point photos to administrator.
     *
     * @author martin.kalivoda@gmail.com
     */
    public void sendMailWithCurrPntPhotos() {
        String mail = BuildConfig.RECIPIENT_EMAIL;
        String subject = mUserName + ", fotografie bodu, eGeodet";

        String model = eGeodetApplication.getModel();
        PackageInfo info = eGeodetApplication.getPackageInfo(this);

        String message = "Používateľ <strong>" + mUserName + "</strong> vám odoslal fotografie bodu " + mGeodeticPointLabel +
                " v misii " + mMissionName + ": " + mMissionYear + " z aplikácie eGeodet.<br/><br/>" +
                "Dátum a čas: " + new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(new Date()) + "<br/>" +
                "Verzia systému Android: " + Build.VERSION.SDK_INT + "<br/>" +
                "Zariadenie: " + model + "<br/>" +
                "Verzia aplikácie: " + (info == null ? "(null)" : info.versionCode);

        List<File> photoFileSrcs;
        File currPntPhotosPath = new File(getExternalStoragePublicDirectory("/eGeodetImages"),
                mMissionName + "_" + mMissionYear + "/" + mGeodeticPointLabel);
        Log.d(TAG, "sendMailWithCurrPntPhotos: currPntPhotosPath: " + currPntPhotosPath.getAbsolutePath());
        photoFileSrcs = ImageRepository.getRecursivelyAllPhotoSourcesInPath(currPntPhotosPath);

        List<String> photoStringSrcs = new ArrayList<>();
        for (File photoFileSrc : photoFileSrcs) {
            Log.d(TAG, "sendMailWithCurrPntPhotos: photoSrc: " + photoFileSrc.getAbsolutePath());
            photoStringSrcs.add(photoFileSrc.getAbsolutePath());
        }

        final String zipFilePhotosSrc = getFilesDir().getAbsolutePath() + "/eGeodetImages_" + mMissionName + "_" + mMissionYear +
                "_" + mGeodeticPointLabel.replace('/', '_') + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) +".zip";
        if (photoStringSrcs.size() > 0) {
            String[] photoSrcsArr = photoStringSrcs.toArray(new String[photoStringSrcs.size()]);
            try {
                ZIPApi.zip(photoSrcsArr, zipFilePhotosSrc);
            } catch (IOException e) {
                e.printStackTrace();
            }

            final File zipFilePhotos = new File(zipFilePhotosSrc);
            if (!zipFilePhotos.exists()) {
                throw new RuntimeException("ZIP file does not exist");
            }

            String attachmentSrc = zipFilePhotosSrc;

            new SkGeodesyMailAPI(this, mail, subject, message, attachmentSrc, false,
                    new SkGeodesyMailAPI.Consumer() {
                        @Override
                        public void afterSend(Boolean isSent) {
                            Log.d("afterSend", "afterSend: After email was sent");
                            if (isSent) {
                                File zipFilePhotos = new File(zipFilePhotosSrc);
                                if (zipFilePhotos.exists()) {
                                    Log.d("afterSend", "afterSend: zipFile " + zipFilePhotosSrc + " exists -> deleting");
                                    zipFilePhotos.delete();
                                }
                            }
                        }
                    }
            );
        } else {
            Toast.makeText(mContext, "Neboli nájdené žiadne fotografie na odoslanie.", Toast.LENGTH_LONG).show();
        }
    }
}
