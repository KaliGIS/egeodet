package sk.gku.egeodet.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import sk.gku.egeodet.R;
import sk.gku.egeodet.adapters.TaskListAdapter;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.viewmodels.TaskViewModel;

import java.util.ArrayList;
import java.util.List;

public class NewGeodeticPointTaskActivity extends AppCompatActivity {
    private final String TAG = "newGPTaskActivity";
    public static final String EXTRA_REPLY = "com.example.android.eGeodet.REPLY";
    public static final int RESULT_CODE_CREATE = 2;
    public static final int RESULT_CODE_ADD = 3;
    private ArrayList<Task> mTasks = new ArrayList<>();
    private TaskViewModel mTaskViewModel;
    private TaskListAdapter mAdapter;
    private Spinner mSpinner;
    private EditText mEditTextCreate;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";
    String mTaskType;

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_geodetic_point_task);

        mEditTextCreate = findViewById(R.id.new_task_value);

        // Get the ids from shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        int missionId = mPreferences.getInt("missionId", -1);
        int geodeticPointId = mPreferences.getInt("geodeticPointId", -1);
        mTaskType = mPreferences.getString("taskType", "");

        mAdapter = new TaskListAdapter(
                this,
                R.layout.spinner_item_task,
                mTasks
        );

        mSpinner = findViewById(R.id.spinner_task);
        mSpinner.setAdapter(mAdapter);

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel.class);
        mTaskViewModel.getTasks().observe(this, new Observer<List<Task>>() {
            @Override
            public void onChanged(@Nullable final List<Task> tasks) {
                mAdapter.setTasks(tasks);
            }
        });

        final Button buttonCreate = findViewById(R.id.button_create_task);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditTextCreate.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String taskName = mEditTextCreate.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, taskName);
                    setResult(RESULT_CODE_CREATE, replyIntent);
                }
            }
        });

        final Button buttonAdd = findViewById(R.id.button_add_task);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Task spinnerValue = (Task) mSpinner.getSelectedItem();
                Log.d(TAG, "onClick: spinner task id: " + spinnerValue.getId());
                Log.d(TAG, "onClick: spinner task name: " + spinnerValue.getName());
                Intent replyIntent = new Intent();

                Integer taskId = spinnerValue.getId();
                replyIntent.putExtra(EXTRA_REPLY, taskId);
                setResult(RESULT_CODE_ADD, replyIntent);
                Log.d(TAG, "onClick: After setResult");
                finish();
            }
        });
    }
}
