package sk.gku.egeodet.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import sk.gku.egeodet.adapters.NoteListAdapter;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.JoinNoteUser;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.utils.NoteViewModelFactory;
import sk.gku.egeodet.viewmodels.NoteViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import sk.gku.egeodet.R;

import java.util.ArrayList;
import java.util.List;

public class NoteActivity extends AppCompatActivity {
    private static final String TAG = "NoteActivity";
    private TextView mAppInfo;
    private RecyclerView mRecyclerViewNote;
    private NoteViewModel mNoteViewModel;
    private SharedPreferences mPreferences;
    private NoteListAdapter mNoteListAdapter;
    private List<JoinNoteUser> mNotesData;
    private int mUserId;
    private int mGeodeticPointId;
    private int mMissionId;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    /**
     * This method is called when the activity is created.
     *
     * @param savedInstanceState
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int gridColumnCount = getResources().getInteger(R.integer.grid_column_count);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent noteIntent = new Intent(NoteActivity.this, NoteEditActivity.class);
                startActivityForResult(noteIntent, Constants.REQUEST_CODE_NEW_NOTE_EDIT_ACTIVITY);
            }
        });

        // Get data from shared preferences
        mPreferences = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mMissionId = mPreferences.getInt("missionId", -1);
        mGeodeticPointId = mPreferences.getInt("geodeticPointId", -1);
        mUserId = mPreferences.getInt("loggedInUserId", -1);
        final String userName = mPreferences.getString("loggedInUserName", "");
        String missionName = mPreferences.getString("missionName", "");
        int missionYear = mPreferences.getInt("missionYear", -1);
        String geodeticPointLabel = mPreferences.getString("geodeticPointLabel", "");

        mAppInfo = findViewById(R.id.textview_app_info);
        mAppInfo.setText(userName + " > " + missionName + ":" + missionYear + " > " + geodeticPointLabel);

        // Initialize the RecyclerView.
        mRecyclerViewNote = findViewById(R.id.recyclerViewNotes);

        // Set the Layout Manager.
        mRecyclerViewNote.setLayoutManager(new GridLayoutManager(this, gridColumnCount));

        // Set empty ArrayList to mNotesData
        mNotesData = new ArrayList<>();

        // Initialize the adapter and set it to the RecyclerView.
        mNoteListAdapter = new NoteListAdapter(this, mNotesData, mUserId);
        mRecyclerViewNote.setAdapter(mNoteListAdapter);

        mNoteViewModel = ViewModelProviders.of(this, new NoteViewModelFactory(getApplication(), mMissionId, mGeodeticPointId)).get(NoteViewModel.class);
        mNoteViewModel.getNotesWithUsersByMissionIdAndGeoPntId().observe(this, new Observer<List<JoinNoteUser>>() {
            @Override
            public void onChanged(List<JoinNoteUser> notes) {
                if (notes.size() > 0) {
                    mNoteListAdapter.setNotes(notes);
                }
            }
        });

        // Touch helper part
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int position = viewHolder.getAdapterPosition();
                JoinNoteUser note = mNoteListAdapter.getNoteAtPosition(position);

                // Allow swiping only on notes of the logged in user
                if (note.getUserId() == mUserId) {
                    return super.getSwipeDirs(recyclerView, viewHolder);
                } else {
                    return 0;
                }
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                JoinNoteUser noteUser = mNoteListAdapter.getNoteAtPosition(position);
                Note note = new Note(noteUser.getNoteId(), noteUser.getNote(), mUserId, mGeodeticPointId, mMissionId);

                mNoteViewModel.deleteNote(note);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerViewNote);
    }

    /**
     * This method is called when the NoteEditActivity is resulted.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @author martin.kalivoda@gmail.com
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            String noteText = data.getStringExtra(NoteEditActivity.NOTE_TEXT);
            int noteId = data.getIntExtra(NoteEditActivity.NOTE_ID, -1);

            if (requestCode == Constants.REQUEST_CODE_NEW_NOTE_EDIT_ACTIVITY) {
                Note note = new Note(noteText, mUserId, mGeodeticPointId, mMissionId);
                mNoteViewModel.insertNote(note);
            } else if (requestCode == Constants.REQUEST_CODE_EDIT_NOTE_EDIT_ACTIVITY) {
                if (noteId != -1) {
                    Note note = new Note(noteId, noteText, mUserId, mGeodeticPointId, mMissionId);
                    mNoteViewModel.updateNote(note);
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.unable_to_update_note,
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.no_note_text_set,
                    Toast.LENGTH_LONG
            ).show();
        }
    }
}
