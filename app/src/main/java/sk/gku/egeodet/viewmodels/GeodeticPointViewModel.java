package sk.gku.egeodet.viewmodels;

import android.app.Application;
import android.os.Bundle;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.repositories.GeodeticPointRepository;

import java.util.List;

public class GeodeticPointViewModel extends AndroidViewModel {
    private GeodeticPointRepository mRepository;
    private LiveData<List<GeodeticPoint>> mGeodeticPoints;
    private MutableLiveData<List<GeodeticPoint>> mSearchResults;

    public GeodeticPointViewModel (Application application) {
        super(application);
        mRepository = new GeodeticPointRepository(application);
        mGeodeticPoints = mRepository.getGeodeticPoints();
        mSearchResults = mRepository.getSearchResults();
    }

    public LiveData<List<GeodeticPoint>> getGeodeticPoints() {
        return mGeodeticPoints;
    }

    public MutableLiveData<List<GeodeticPoint>> getSearchResults() {
        return mSearchResults;
    }

    public void insertAlsoIntoAssociatedTables(GeodeticPoint geodeticPoint, Bundle dataToInsert) {
        mRepository.insertAlsoIntoAssociatedTables(geodeticPoint, dataToInsert);
    }
}
