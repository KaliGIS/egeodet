package sk.gku.egeodet.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.repositories.ToSynchronizeEntityRepository;
import sk.gku.egeodet.repositories.UserRepository;
import sk.gku.egeodet.utils.DbSyncAPI;

import java.util.List;

public class ChangePasswordViewModel extends AndroidViewModel {
    private final static String TAG = "ChangePasswordViewModel";
    private UserRepository mRepository;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;
    private LiveData<List<User>> mUsers;

    public interface SyncCallback {
        void then(boolean isOk);
    }

    public ChangePasswordViewModel(@NonNull Application application) {
        super(application);
        mRepository = new UserRepository(application);
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
        mUsers = mRepository.getUsers();
    }

    public LiveData<List<User>> getUsers() {
        return mUsers;
    }

    public void updateUser(User user) {
        mRepository.updateUser(user);
    }

    public void synchronizeUser(User user, final SyncCallback callback) {
        final ToSynchronizeEntity toSynchronizeEntity =
                mToSynchronizeEntityRepository.getToSynchronizeEntityByModelAndId(Constants.USER_MODEL, user.getId());
        if (toSynchronizeEntity == null) {
            Log.e(TAG, "toSynchronizeEntity is null by model name User and entityId " + user.getId());
        }

        mRepository.synchronize(user, new DbSyncAPI.SyncCallback() {
            @Override
            public void onDone(Object syncedEntity, Integer origId) {
                User userEntity = (User) syncedEntity;
                mRepository.changeId(origId, userEntity.getId());
                if (toSynchronizeEntity != null) {
                    // Update entityid, master id and synchronizationError to 0 in toSynchronizeEntity
                    toSynchronizeEntity.setEntityId(userEntity.getId());
                    toSynchronizeEntity.setMasterId(userEntity.getId());
                    toSynchronizeEntity.setSynchronizationError(0);
                    mToSynchronizeEntityRepository.update(toSynchronizeEntity);
                } else {
                    // Create toSynchronizeEntity record (for change id process)
                    ToSynchronizeEntity newToSynchronizeEntity = new ToSynchronizeEntity(Constants.USER_GROUP_MODEL, origId, userEntity.getId(), 0);
                    mToSynchronizeEntityRepository.insert(newToSynchronizeEntity);
                }

                callback.then(true);
            }

            @Override
            public void onError(String errorStr, String verbose, Integer origId) {
                Log.e(TAG, "onError: " + errorStr + ", " + verbose);
                if (toSynchronizeEntity != null) {
                    // Update ToSynchronizeEntity
                    toSynchronizeEntity.setSynchronizationError(1);
                    mToSynchronizeEntityRepository.update(toSynchronizeEntity);
                } else {
                    // Create toSynchronizeEntity record
                    ToSynchronizeEntity newToSynchronizeEntity = new ToSynchronizeEntity(Constants.USER_GROUP_MODEL, origId, 0, 1);
                    mToSynchronizeEntityRepository.insert(newToSynchronizeEntity);
                }

                callback.then(false);

            }
        });
    }
}
