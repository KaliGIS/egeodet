package sk.gku.egeodet.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.repositories.MissionRepository;
import sk.gku.egeodet.repositories.SynchronizationRepository;
import sk.gku.egeodet.repositories.ToSynchronizeEntityRepository;
import sk.gku.egeodet.repositories.UserGroupRepository;
import sk.gku.egeodet.utils.SynchronizationComponent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MissionViewModel extends AndroidViewModel {
    public static final String TAG = "MissionViewModel";
    private MissionRepository mRepository;
    private SynchronizationRepository mSynchronizationRepository;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;
    private UserGroupRepository mUserGroupRepository;
    private LiveData<List<Mission>> mMissions;
    private LiveData<List<UserGroup>> mUserGroups;

    public MissionViewModel(@NonNull Application application) {
        super(application);
        mRepository = new MissionRepository(application);
        mMissions = mRepository.getMissions();
        mSynchronizationRepository = new SynchronizationRepository(application);
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(application);
        mUserGroupRepository = new UserGroupRepository(application);
        mUserGroups = mUserGroupRepository.getUserGroups();
    }

    public LiveData<List<Mission>> getMissions() {
        return mMissions;
    }

    public Integer getLastUploadWeek(Integer loggedInUserId) {
        SynchronizationComponent synchronizationComponent = new SynchronizationComponent(getApplication(), loggedInUserId);
        String syncKey = synchronizationComponent.getSyncKey(Constants.SynchronizationDirection.UPLOAD.toString());
        Synchronization synchronization = null;
        try {
            synchronization = mSynchronizationRepository.getSynchronizationBySyncKey(syncKey);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "getLastUploadWeek: synchronization: " + synchronization);
        Integer lastUploadWeek = -1;
        if (synchronization != null) {
            Date lastSyncDate;
            try {
                lastSyncDate = new SimpleDateFormat(Constants.DATE_TIME_FORMAT).parse(Constants.EPOCH_START_DATE_TIME);
                lastSyncDate = (synchronization.getEndDate() == null) ? lastSyncDate : synchronization.getEndDate();
                lastUploadWeek = getWeekOfYearFromDate(lastSyncDate);
                Log.d(TAG, "getLastUploadWeek: sync.getEndDate: " + synchronization.getEndDate() + ", lastSyncDate: " + lastSyncDate);
                Log.d(TAG, "getLastUploadWeek: lastUploadWeek: " + lastUploadWeek);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return lastUploadWeek;
    }

    private Integer getWeekOfYearFromDate(Date lastSyncDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(lastSyncDate);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    public Integer getCurrentWeek() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    public Integer getToSynchronizeCount() {
        Integer count = mToSynchronizeEntityRepository.getCountOfNotSynchronizedToSynchronizeEntities();
        return count;
    }

    public LiveData<List<UserGroup>> getUserGroups() {
        return mUserGroups;
    }

    public List<Mission> getMissionsByGroupIds(HashSet<Integer> mGroupIds) {
        return mRepository.getMissionsByGroupIds(mGroupIds);
    }
}
