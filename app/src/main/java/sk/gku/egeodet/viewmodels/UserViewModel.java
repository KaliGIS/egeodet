package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.sqlite.db.SimpleSQLiteQuery;

import sk.gku.egeodet.models.User;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.repositories.UserGroupRepository;
import sk.gku.egeodet.repositories.UserRepository;

import java.util.List;

public class UserViewModel extends AndroidViewModel {
    private UserRepository mRepository;
    private LiveData<List<UserGroup>> mUserGroups;
    private LiveData<List<User>> mUsers;
    private UserGroupRepository mUserGroupRepository;

    public UserViewModel(@NonNull Application application) {
        super(application);
        mRepository = new UserRepository(application);
        mUsers = mRepository.getUsers();
        mUserGroupRepository = new UserGroupRepository(application);
        mUserGroups = mUserGroupRepository.getUserGroups();
    }

    public LiveData<List<User>> getUsers() {
        return mUsers;
    }

    public LiveData<List<UserGroup>> getUserGroups() {
        return mUserGroups;
    }

    public void insert(User user) {
        mRepository.insert(user);
    }

    public void execSQL(SimpleSQLiteQuery query) {
        mRepository.execSQL(query);
    }

    public void downloadUserGroups(UserGroupRepository.SyncCallback syncCallback) {
        mUserGroupRepository.downloadUserGroups(null, null, null, syncCallback);
    }
}
