package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.Group;
import sk.gku.egeodet.repositories.GroupRepository;

import java.util.List;

public class NewGeodeticPointViewModel extends AndroidViewModel {
    private GroupRepository mRepository;
    private LiveData<List<Group>> mGroups;

    public NewGeodeticPointViewModel(@NonNull Application application) {
        super(application);
        mRepository = new GroupRepository(application);
        mGroups = mRepository.getGroups();
    }

    public LiveData<List<Group>> getGroups() {
        return mGroups;
    }
}
