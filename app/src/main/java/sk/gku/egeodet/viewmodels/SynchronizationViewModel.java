package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.UserGroup;
import sk.gku.egeodet.repositories.MissionRepository;
import sk.gku.egeodet.repositories.ToSynchronizeEntityRepository;
import sk.gku.egeodet.repositories.UserGroupRepository;

import java.util.ArrayList;
import java.util.List;

public class SynchronizationViewModel extends AndroidViewModel {
    private ToSynchronizeEntityRepository mRepository;
    private MissionRepository mMissionRepository;
    private UserGroupRepository mUserGroupRepository;
    private LiveData<List<ToSynchronizeEntity>> mToSynchronizeEntities;

    public SynchronizationViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ToSynchronizeEntityRepository(application);
        mMissionRepository = new MissionRepository(application);
        mUserGroupRepository = new UserGroupRepository(application);
        mToSynchronizeEntities = mRepository.getToSynchronizeEntities();
    }

    public LiveData<List<ToSynchronizeEntity>> getToSynchronizeEntities() {
        return mToSynchronizeEntities;
    }

    public List<Integer> getDownloadedMissionIds() {
        return mMissionRepository.getIds();
    }

    public List<Integer> getGroupIdsOfUser(Integer userId) {
        List<Integer> groupIds = new ArrayList<>();
        List<UserGroup> userGroups = mUserGroupRepository.getUserGroupsOfUser(userId);

        for (UserGroup userGroup : userGroups) {
            if (!groupIds.contains(userGroup.getGroupId())) {
                groupIds.add(userGroup.getGroupId());
            }
        }

        return groupIds;
    }
}
