package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.TaskType;
import sk.gku.egeodet.repositories.GeodeticPointTaskRepository;

import java.util.List;

public class GeodeticPointTaskViewModel extends AndroidViewModel {
    private GeodeticPointTaskRepository mRepository;
    private LiveData<List<GeodeticPointTask>> mGeodeticPointTasks;
    private LiveData<List<JoinGeodeticPointTaskTask>> mJoinGeodeticPointTaskTaskSearchResults;
    private LiveData<List<TaskType>> mDistinctTaskTypeSearchResults;

    public GeodeticPointTaskViewModel (Application application, int missionId, int geodeticPointId, String taskType) {
        super(application);
        mRepository = new GeodeticPointTaskRepository(application, missionId, geodeticPointId, taskType);
        mGeodeticPointTasks = mRepository.getGeodeticPointTasks();
        mJoinGeodeticPointTaskTaskSearchResults = mRepository.getGeoPntTasksWithTasksByMissionIdAndGeoPntIdAndTaskType();
        mDistinctTaskTypeSearchResults = mRepository.getDistinctTaskTypesByMissionIdAndGeoPntId();
    }

    public LiveData<List<GeodeticPointTask>> getGeodeticPointTasks() {
        return mGeodeticPointTasks;
    }

    public LiveData<List<JoinGeodeticPointTaskTask>> getGPTasksByMissionIdAndGeodeticPointIdAndTaskType() {
        return mJoinGeodeticPointTaskTaskSearchResults;
    }

    public LiveData<List<TaskType>> getDistinctTaskTypesByMissionIdAndGeoPntId() {
        return mDistinctTaskTypeSearchResults;
    }

    public void insertIfDoesNotExist(GeodeticPointTask geodeticPointTask) {
        mRepository.insertIfDoesNotExist(geodeticPointTask);
    }

    public void insert(GeodeticPointTask geodeticPointTask) {
        mRepository.insert(geodeticPointTask);
    }

    public void updateIsDone(int geodeticPointTaskId, boolean isChecked, int userId) {
        mRepository.updateIsDone(geodeticPointTaskId, isChecked, userId);
    }

    public void deleteGeodeticPointTask(int geodeticPointTaskId) {
        mRepository.deleteGeodeticPointTask(geodeticPointTaskId);
    }
}
