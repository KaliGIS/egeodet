package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.repositories.ImageRepository;

import java.util.List;

public class ImageViewModel extends AndroidViewModel {
    private ImageRepository mRepository;
    private LiveData<List<Image>> mImages;
    private LiveData<List<Image>> mImageSearchResults;

    public ImageViewModel (Application application, int missionId, int geodeticPointId) {
        super(application);
        mRepository = new ImageRepository(application, missionId, geodeticPointId);
        mImages = mRepository.getImages();
        mImageSearchResults = mRepository.getImagesByMissionIdAndGeodeticPointId();
    }

    public LiveData<List<Image>> getImages() {
        return mImages;
    }

    public LiveData<List<Image>> getImagesByMissionIdAndGeodeticPointId() {
        return mImageSearchResults;
    }

    public void insert(Image image) {
        mRepository.insertIfNotExist(image);
    }

    public void insertIfNotExist(Image image) { mRepository.insertIfNotExist(image); }

    public void delete(Image image) { mRepository.deleteImage(image); }
}
