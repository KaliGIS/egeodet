package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.JoinGeodeticPointMissionGeoPnt;
import sk.gku.egeodet.repositories.GeodeticPointMissionRepository;

import java.util.List;

public class GeodeticPointMissionViewModel extends AndroidViewModel {
    private GeodeticPointMissionRepository mRepository;
    private LiveData<List<GeodeticPointMission>> mGeodeticPointMissions;
    private LiveData<List<JoinGeodeticPointMissionGeoPnt>> mJoinGeodeticPointMissionGeoPntSearchResults;

    public GeodeticPointMissionViewModel (Application application, int missionId, List<Integer> groupIds) {
        super(application);
        mRepository = new GeodeticPointMissionRepository(application, missionId, groupIds);
        mGeodeticPointMissions = mRepository.getGeodeticPointMissions();
        mJoinGeodeticPointMissionGeoPntSearchResults = mRepository.getGeodeticPointMissionsWithGeoPntsByMissionId();
    }

    public LiveData<List<GeodeticPointMission>> getGeodeticPointMissions() {
        return mGeodeticPointMissions;
    }

    public LiveData<List<JoinGeodeticPointMissionGeoPnt>> getGeodeticPointMissionsWithGeoPntsByMissionId() {
        return mJoinGeodeticPointMissionGeoPntSearchResults;
    }

    public void updatePointOwner(Integer geodeticPointId, Integer missionId, Integer loggedInUserId) {
        mRepository.updatePointOwner(geodeticPointId, missionId, loggedInUserId);
    }
}
