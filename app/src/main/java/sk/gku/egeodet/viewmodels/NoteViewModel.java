package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.JoinNoteUser;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.repositories.NoteRepository;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {
    private NoteRepository mRepository;
    private LiveData<List<Note>> mNotes;
    private LiveData<List<JoinNoteUser>> mJoinNoteUserSearchResults;

    public NoteViewModel (Application application, int missionId, int geodeticPointId) {
        super(application);
        mRepository = new NoteRepository(application, missionId, geodeticPointId);
        mNotes = mRepository.getNotes();
        mJoinNoteUserSearchResults = mRepository.getNotesWithUsersByMissionIdAndGeoPntId();
    }

    public LiveData<List<Note>> getNotes() {
        return mNotes;
    }

    public LiveData<List<JoinNoteUser>> getNotesWithUsersByMissionIdAndGeoPntId() {
        return mJoinNoteUserSearchResults;
    }

    public void insertNote(Note note) { mRepository.insertNote(note); }

    public void deleteNote(Note note) {
        mRepository.deleteNote(note);
    }

    public void updateNote(Note note) {
        mRepository.updateNote(note);
    }
}
