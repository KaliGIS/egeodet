package sk.gku.egeodet.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.repositories.TaskRepository;

import java.util.List;

public class TaskViewModel extends AndroidViewModel {
    private TaskRepository mRepository;
    private LiveData<List<Task>> mTasks;

    public TaskViewModel(@NonNull Application application) {
        super(application);
        mRepository = new TaskRepository(application);
        mTasks = mRepository.getTasks();
    }

    public LiveData<List<Task>> getTasks() {
        return mTasks;
    }

    public void insert(Task task) {
        mRepository.insert(task);
    }
}
