package sk.gku.egeodet.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.room.Ignore;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class SkGeodesyMailAPI extends AsyncTask<Void,Void,Boolean> {
    //Variables
    public static final String TAG = "SkGeodesyMailAPI";
    private Context mContext;
    private SkGeodesyMailAPI.Consumer mConsumer;

    private String mBoundary;
    private static final String LINE_FEED = "\r\n";
    private HttpURLConnection mHttpConn;
    private String mCharset = "utf-8";
    private OutputStream mOutputStream;
    private PrintWriter mWriter;

    private List<String> mEmails = new ArrayList<>();
    private String mSubject;
    private String mMessage;
    private String mAttachmentSrc;
    private boolean mIsAppCrashed;
    private Throwable mException;

    private ProgressDialog mProgressDialog;

    public interface Consumer { void afterSend(Boolean isSent); }

    public SkGeodesyMailAPI(Context context, String email, String subject, String message, String attachmentSrc) {
        this.mContext = context;
        this.mEmails.add(email);
        this.mSubject = subject;
        this.mMessage = message;
        this.mAttachmentSrc = attachmentSrc;
        this.mIsAppCrashed = false;
    }

    @Ignore
    public SkGeodesyMailAPI(Context context, String email, String subject, String message, String attachmentSrc, boolean isAppCrashed, SkGeodesyMailAPI.Consumer consumer) {
        this.mConsumer = consumer;
        this.mContext = context;
        this.mEmails.add(email);
        this.mSubject = subject;
        this.mMessage = message;
        this.mAttachmentSrc = attachmentSrc;
        this.mIsAppCrashed = isAppCrashed;
        execute();
    }

    @Ignore
    public SkGeodesyMailAPI(Context context, List<String> emails, String subject, String message, String attachmentSrc, boolean isAppCrashed, SkGeodesyMailAPI.Consumer consumer) {
        this.mConsumer = consumer;
        this.mContext = context;
        this.mEmails.addAll(emails);
        this.mSubject = subject;
        this.mMessage = message;
        this.mAttachmentSrc = attachmentSrc;
        this.mIsAppCrashed = isAppCrashed;
        execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!mIsAppCrashed) {
            //Show progress dialog while sending email
            mProgressDialog = ProgressDialog.show(mContext,"Email sa odosiela", "Prosím čakajte...",false,false);
        }
    }

    @Override
    protected void onPostExecute(Boolean isSent) {
        super.onPostExecute(isSent);
        Log.d(TAG, "onPostExecute: EMAIL");

        if (!mIsAppCrashed) {
            //Dismiss progress dialog when message successfully send
            mProgressDialog.dismiss();

            if (isSent) {
                //Show success toast
                Toast.makeText(mContext,"Email odoslaný",Toast.LENGTH_SHORT).show();
            } else {
                new AlertDialog.Builder(mContext)
                        .setTitle(R.string.error)
                        .setMessage("Email sa nepodarilo odoslať." + "\n\n" + mException.getMessage())
                        .setPositiveButton(R.string.ok, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }

        Log.d(TAG, "onPostExecute: Calling consumer.afterSend() method");
        if (mConsumer != null) {
            mConsumer.afterSend(isSent);
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Log.d(TAG, "doInBackground: SEND EMAIL");

        try {
            // creates a unique boundary based on time stamp
            mBoundary = "===" + System.currentTimeMillis() + "===";
            URL url = new URL(BuildConfig.SKGEODESY_MAIL_POST_ENDPOINT);
            mHttpConn = (HttpURLConnection) url.openConnection();
            mHttpConn.setUseCaches(false);
            mHttpConn.setDoOutput(true);    // indicates POST method
            mHttpConn.setDoInput(true);
            mHttpConn.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + mBoundary);
            mOutputStream = mHttpConn.getOutputStream();
            mWriter = new PrintWriter(new OutputStreamWriter(mOutputStream, mCharset),
                    true);

            StringBuilder emails = new StringBuilder();
            String prefix = "";
            for (String mEmail : mEmails) {
                emails.append(prefix).append(mEmail);
                prefix = ";";
            }

            Log.d(TAG, "doInBackground: recipient emails: " + emails.toString());
            addFormField("password", BuildConfig.SKGEODESY_MAIL_PASSWORD);
            addFormField("subject", mSubject);
            addFormField("emails", emails.toString());
            addFormField("message", mMessage);

            if (mAttachmentSrc != null) {
                addFilePart("file", new File(mAttachmentSrc));
            }

            JSONObject resJson = finish();
            
            Log.i(TAG, "SERVER REPLIED:");
            Log.i(TAG, "success: " + resJson.getBoolean("success"));
            Log.i(TAG, "errors: " + resJson.getJSONArray("errors"));

            if (!resJson.getBoolean("success")) {
                JSONObject reason = resJson.getJSONArray("errors").getJSONObject(0);
                throw new RuntimeException(reason.getString("text"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            mException = e;
            return false;
        }

        return true;
    }

    /**
     * Adds a form field to the request
     *
     * @param name  field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        mWriter.append("--" + mBoundary).append(LINE_FEED);
        mWriter.append("Content-Disposition: form-data; name=\"" + name + "\"")
                .append(LINE_FEED);
        mWriter.append("Content-Type: text/plain; charset=" + mCharset).append(
                LINE_FEED);
        mWriter.append(LINE_FEED);
        mWriter.append(value).append(LINE_FEED);
        mWriter.flush();
    }

    /**
     * Adds a upload file section to the request
     *
     * @param fieldName  name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        mWriter.append("--" + mBoundary).append(LINE_FEED);
        mWriter.append(
                "Content-Disposition: form-data; name=\"" + fieldName
                        + "\"; filename=\"" + fileName + "\"")
                .append(LINE_FEED);
        mWriter.append(
                "Content-Type: "
                        + URLConnection.guessContentTypeFromName(fileName))
                .append(LINE_FEED);
        mWriter.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        mWriter.append(LINE_FEED);
        mWriter.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            mOutputStream.write(buffer, 0, bytesRead);
        }
        mOutputStream.flush();
        inputStream.close();
        mWriter.append(LINE_FEED);
        mWriter.flush();
    }

    /**
     * Adds a header field to the request.
     *
     * @param name  - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        mWriter.append(name + ": " + value).append(LINE_FEED);
        mWriter.flush();
    }

    /**
     * Completes the request and receives response from the server.
     *
     * @return JSON object
     * @throws IOException
     */
    public JSONObject finish() throws IOException, JSONException {
        String response;
        JSONObject resJson;
        mWriter.append(LINE_FEED).flush();
        mWriter.append("--" + mBoundary + "--").append(LINE_FEED);
        mWriter.close();

        // checks server's status code first
        int status = mHttpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    mHttpConn.getInputStream()));
            response = reader.readLine();
            resJson = new JSONObject(response);
            reader.close();
            mHttpConn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return resJson;
    }
}
