package sk.gku.egeodet.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import sk.gku.egeodet.viewmodels.GeodeticPointTaskViewModel;

public class GeodeticPointTaskViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private int mMissionId;
    private int mGeodeticPointId;
    private String mTaskType;

    public GeodeticPointTaskViewModelFactory(Application application, int missionId, int geodeticPointId, String taskType) {
        mApplication = application;
        mMissionId = missionId;
        mGeodeticPointId = geodeticPointId;
        mTaskType = taskType;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new GeodeticPointTaskViewModel(mApplication, mMissionId, mGeodeticPointId, mTaskType);
    }
}
