package sk.gku.egeodet.utils;

import android.app.Application;
import android.os.Build;
import android.util.ArrayMap;
import android.util.Log;

import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.interfaces.BaseModel;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.models.ToSynchronizeEntity;
import sk.gku.egeodet.models.User;
import sk.gku.egeodet.repositories.GeodeticPointMissionRepository;
import sk.gku.egeodet.repositories.GeodeticPointRepository;
import sk.gku.egeodet.repositories.GeodeticPointTaskRepository;
import sk.gku.egeodet.repositories.ImageRepository;
import sk.gku.egeodet.repositories.MissionRepository;
import sk.gku.egeodet.repositories.NoteRepository;
import sk.gku.egeodet.repositories.SynchronizationRepository;
import sk.gku.egeodet.repositories.TaskRepository;
import sk.gku.egeodet.repositories.ToSynchronizeEntityRepository;
import sk.gku.egeodet.repositories.UserGroupRepository;
import sk.gku.egeodet.repositories.UserRepository;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class SynchronizationComponent {
    public static final String TAG = "SynchronComponent";
    public int mLoggedUserId;
    private GeodeticPointRepository mGeodeticPointRepository;
    private MissionRepository mMissionRepository;
    private GeodeticPointMissionRepository mGeodeticPointMissionRepository;
    private GeodeticPointTaskRepository mGeodeticPointTaskRepository;
    private NoteRepository mNoteRepository;
    private TaskRepository mTaskRepository;
    private ImageRepository mImageRepository;
    private ToSynchronizeEntityRepository mToSynchronizeEntityRepository;
    private SynchronizationRepository mSynchronizationRepository;
    private UserRepository mUserRepository;
    private UserGroupRepository mUserGroupRepository;
    private Application mApplication;
    private Map<String, Integer> mSyncInfo = new ArrayMap<>();
    private PropertyChangeSupport mPropertyChangeSupport;
    private InternetCheck mInternetCheck;

    public interface BatchSyncCallback {
        void then(boolean isOk);
    }

    public interface BatchDownloadCallback {
        void then(String modelName, Integer status);
    }

    public SynchronizationComponent(Application application, int loggedUserId) {
        mLoggedUserId = loggedUserId;
        mApplication = application;
        mPropertyChangeSupport = new PropertyChangeSupport(this);
        mGeodeticPointRepository = new GeodeticPointRepository(mApplication);
        mMissionRepository = new MissionRepository(mApplication);
        mGeodeticPointMissionRepository = new GeodeticPointMissionRepository(mApplication);
        mGeodeticPointTaskRepository = new GeodeticPointTaskRepository(mApplication);
        mNoteRepository = new NoteRepository(mApplication);
        mTaskRepository = new TaskRepository(mApplication);
        mImageRepository = new ImageRepository(mApplication);
        mUserRepository = new UserRepository(mApplication);
        mUserGroupRepository = new UserGroupRepository(mApplication);
        mToSynchronizeEntityRepository = new ToSynchronizeEntityRepository(mApplication);
        mSynchronizationRepository = new SynchronizationRepository(mApplication);
        mInternetCheck = new InternetCheck(application.getApplicationContext());
    }

    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        mPropertyChangeSupport.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        mPropertyChangeSupport.removePropertyChangeListener(pcl);
    }

    public void setSyncModelInfo(String modelName, Integer status) {
        Log.d(TAG, "setSyncModelInfo: modelName: " + modelName + ", status: " + status);
        Map<String, Integer> currentSyncInfo = new ArrayMap<>();
        currentSyncInfo.put(modelName, status);
        mPropertyChangeSupport.firePropertyChange("mSynchronizingModels", this.mSyncInfo, currentSyncInfo);
        Log.d(TAG, "setSyncModelInfo: mSyncInfo old: " + mSyncInfo.get(modelName));
        this.mSyncInfo.clear();
        this.mSyncInfo.put(modelName, status);
    }

    public String getSyncKey(String direction) {
        String syncKey = direction + "_" + eGeodetApplication.getModel() + "_" + Build.VERSION.SDK_INT + "_" + mLoggedUserId;
        return syncKey;
    }

    public Synchronization getOrCreateSynchronizationEntity(Constants.SynchronizationDirection direction) {
        SynchronizationRepository synchronizationRepository = new SynchronizationRepository(mApplication);
        String syncKey = getSyncKey(direction.toString());
        Synchronization syncEntity = null;
        try {
            syncEntity = synchronizationRepository.getSynchronizationBySyncKey(syncKey);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        // Set startSynchronizationDate, fake date. Changed 19/01/2021 -> master DB overwrites this
        Date startDate = null;
        try {
            startDate = new SimpleDateFormat(Constants.DATE_TIME_FORMAT).parse(Constants.EPOCH_START_DATE_TIME);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (syncEntity != null) { // update synchronization entity
            syncEntity.setStatus(1);
            syncEntity.setStartDate(startDate);
        } else { // create synchronization entity
            syncEntity = new Synchronization(syncKey, mLoggedUserId, startDate, null, 1);
        }
        synchronizationRepository.upsertSynchronizationEntity(syncEntity);
        return syncEntity;
    }

    public void synchronizeSynchronization(Synchronization syncEntity, final BatchDownloadCallback callback) {
        setSyncModelInfo(Constants.SYNCHRONIZATION_MODEL, Constants.SYNC_STATUS_UPLOADING);
        mSynchronizationRepository.syncSynchronization(syncEntity, new SynchronizationRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                Log.d(TAG, "onStatusChange: status " + status);
                setSyncModelInfo(Constants.SYNCHRONIZATION_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    if (callback != null) {
                        callback.then(Constants.SYNCHRONIZATION_MODEL, status);;
                    }
                }
            }
        });
    }

    public Date getLastSynchronizationDate(Synchronization syncEntity) {
        return syncEntity.getEndDate();
    }

    public void synchronizeNotSynchronizedEntities(final BatchSyncCallback callback) {
        final Boolean[] areSynchronizedOk = {true};

        List<Integer> toSynchronizeUsers = mToSynchronizeEntityRepository.getEntityIdsByModelName(
                Constants.USER_MODEL
        );
        List<User> users = mUserRepository.getByIds(toSynchronizeUsers);

        // Synchronize to master
        synchronizeEntitiesByModelName(users, Constants.USER_MODEL, new BatchSyncCallback() {
            @Override
            public void then(boolean areUsersSynchronizedOk) {
                areSynchronizedOk[0] = areSynchronizedOk[0] && areUsersSynchronizedOk;
                List<ToSynchronizeEntity> toSynchronizeGeodeticPointsRef = mToSynchronizeEntityRepository.getToSynchronizeEntitiesRefByModelName(
                        Constants.GEODETIC_POINT_MODEL
                );

                synchronizeGeodeticPointsAndJoinedEntities(toSynchronizeGeodeticPointsRef, new BatchSyncCallback() {
                    @Override
                    public void then(boolean areGeodeticPointsAndJoinedDataSynchronizedOk) {
                        areSynchronizedOk[0] = areSynchronizedOk[0] && areGeodeticPointsAndJoinedDataSynchronizedOk;
                        Log.d(TAG, "then: Geodetic points and joined entities synchronized ok: " + areGeodeticPointsAndJoinedDataSynchronizedOk);
                        Log.d(TAG, "then: areSynchronizedOk: " + areSynchronizedOk[0]);
                        List<Integer> geodeticPointMissionIds = mToSynchronizeEntityRepository.getEntityIdsByModelName(
                                Constants.GEODETIC_POINT_MISSION_MODEL
                        );
                        List<GeodeticPointMission> geodeticPointMissions = mGeodeticPointMissionRepository.getByIds(geodeticPointMissionIds);
                        synchronizeEntitiesByModelName(geodeticPointMissions, Constants.GEODETIC_POINT_MISSION_MODEL, new BatchSyncCallback() {
                            @Override
                            public void then(boolean areGeodeticPointMissionsSynchronizedOk) {
                                areSynchronizedOk[0] = areSynchronizedOk[0] && areGeodeticPointMissionsSynchronizedOk;
                                Log.d(TAG, "then: areSynchronizedOk: " + areSynchronizedOk[0]);
                                List<Integer> geodeticPointTaskIds = mToSynchronizeEntityRepository.getEntityIdsByModelName(
                                        Constants.GEODETIC_POINT_TASK_MODEL
                                );
                                for(Integer id : geodeticPointTaskIds) {
                                    Log.d(TAG, "then: geodeticPointTaskId to sync: " + id);
                                }
                                List<GeodeticPointTask> geodeticPointTasks = mGeodeticPointTaskRepository.getByIds(geodeticPointTaskIds);
                                Log.d(TAG, "then: geodeticPointTasks found by ids size: " + geodeticPointTasks.size());
                                synchronizeEntitiesByModelName(geodeticPointTasks, Constants.GEODETIC_POINT_TASK_MODEL, new BatchSyncCallback() {
                                    @Override
                                    public void then(boolean areGeodeticPointTasksSynchronizedOk) {
                                        areSynchronizedOk[0] = areSynchronizedOk[0] && areGeodeticPointTasksSynchronizedOk;
                                        List<Integer> noteIds = mToSynchronizeEntityRepository.getEntityIdsByModelName(
                                                Constants.NOTE_MODEL
                                        );
                                        List<Note> notes = mNoteRepository.getByIds(noteIds);
                                        synchronizeEntitiesByModelName(notes, Constants.NOTE_MODEL, new BatchSyncCallback() {
                                            @Override
                                            public void then(boolean areNotesSynchronizedOk) {
                                                areSynchronizedOk[0] = areSynchronizedOk[0] && areNotesSynchronizedOk;

                                                if (!mInternetCheck.isMetered()) { // Do not sync images on mobile data connection
                                                    List<Integer> imageIds = mToSynchronizeEntityRepository.getEntityIdsByModelName(
                                                            Constants.IMAGE_MODEL
                                                    );
                                                    List<Image> images = mImageRepository.getByIds(imageIds);
                                                    synchronizeEntitiesByModelName(images, Constants.IMAGE_MODEL, new BatchSyncCallback() {
                                                        @Override
                                                        public void then(boolean areImagesSynchronizedOk) {
                                                            areSynchronizedOk[0] = areSynchronizedOk[0] && areImagesSynchronizedOk;
                                                            Log.d(TAG, "then: after the last entity areSynchronizedOk: " + areSynchronizedOk[0]);
                                                            callback.then(areSynchronizedOk[0]);
                                                        }
                                                    });
                                                } else {
                                                    Log.d(TAG, "then: after the last entity areSynchronizedOk: " + areSynchronizedOk[0]);
                                                    callback.then(areSynchronizedOk[0]);
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

    }

    public void synchronizeGeodeticPointsAndJoinedEntities(List<ToSynchronizeEntity> toSynchronizeGeodeticPointsRef, final BatchSyncCallback callback) {
        final Integer[] remainingGeodeticPoints = {toSynchronizeGeodeticPointsRef.size()};
        final Boolean[] areGeodeticPointsSynchronizedOk = {true};
        if (remainingGeodeticPoints[0].equals(0)) {
            callback.then(areGeodeticPointsSynchronizedOk[0]);
            return;
        }

        for (final ToSynchronizeEntity toSynchronizeGeodeticPointRef: toSynchronizeGeodeticPointsRef) {
            setSyncModelInfo(Constants.GEODETIC_POINT_MODEL, Constants.SYNC_STATUS_UPLOADING);
            final Integer geodeticPointId = toSynchronizeGeodeticPointRef.getEntityId();
            mGeodeticPointRepository.getAndSynchronizeById(geodeticPointId, new DbSyncAPI.SyncCallback() {
                @Override
                public void onDone(Object syncedEntity, Integer origId) {
                    final GeodeticPoint newGeodeticPoint = (GeodeticPoint) syncedEntity;
                    // mGeodeticPointRepository.changeId(origId, -1 * newGeodeticPoint.getId()); // Change to temporary negative value
                    Log.d(TAG, "then: geodetic point is synchronized OK");
                    Log.d(TAG, "then: master geodetic point id is " + newGeodeticPoint.getId());
                    // Update master id and synchronizationError to 0 in toSynchronizeEntity
                    toSynchronizeGeodeticPointRef.setMasterId(newGeodeticPoint.getId());
                    toSynchronizeGeodeticPointRef.setSynchronizationError(0);
                    mToSynchronizeEntityRepository.update(toSynchronizeGeodeticPointRef);

                    // Set remaining geodetic points
                    remainingGeodeticPoints[0] -= 1;
                    Log.d(TAG, "onDone: Remaining geodetic points: " + remainingGeodeticPoints[0]);
                    if (remainingGeodeticPoints[0].equals(0)) {
                        if (areGeodeticPointsSynchronizedOk[0]) {
                            setSyncModelInfo(Constants.GEODETIC_POINT_MODEL, Constants.SYNC_STATUS_DONE);
                        } else {
                            setSyncModelInfo(Constants.GEODETIC_POINT_MODEL, Constants.SYNC_STATUS_DONE_WITH_ERROR);
                        }
                    }

                    synchronizeGeodeticPointJoinedEntities(newGeodeticPoint.getId(), toSynchronizeGeodeticPointRef.getEntityId(), new BatchSyncCallback() {
                        @Override
                        public void then(boolean areJoinedEntitiesSynchronizedOk) {
                            if (remainingGeodeticPoints[0].equals(0)) {
                                callback.then(areGeodeticPointsSynchronizedOk[0] && areJoinedEntitiesSynchronizedOk); // all geodetic points synchronized
                            }
                        }
                    });
                }

                @Override
                public void onError(String errorStr, String verbose, Integer origId) {
                    // Update ToSynchronizeEntity
                    toSynchronizeGeodeticPointRef.setSynchronizationError(1);
                    mToSynchronizeEntityRepository.update(toSynchronizeGeodeticPointRef);
                    Log.d("SyncCallback", "error: " + errorStr + ", " + verbose);
                    areGeodeticPointsSynchronizedOk[0] = false;

                    // Set remaining geodetic points
                    remainingGeodeticPoints[0] -= 1;
                    Log.d(TAG, "onError: Remaining geodetic points: " + remainingGeodeticPoints[0]);
                    if (remainingGeodeticPoints[0].equals(0)) {
                        if (areGeodeticPointsSynchronizedOk[0]) {
                            setSyncModelInfo(Constants.GEODETIC_POINT_MODEL, Constants.SYNC_STATUS_DONE);
                        } else {
                            setSyncModelInfo(Constants.GEODETIC_POINT_MODEL, Constants.SYNC_STATUS_DONE_WITH_ERROR);
                        }

                        callback.then(areGeodeticPointsSynchronizedOk[0]);
                    }
                }
            });
        }
    }

    public void synchronizeGeodeticPointJoinedEntities(Integer newId, Integer oldId, final BatchSyncCallback callback) {
        final Boolean[] areJoinedEntitiesSynchronizedOk = {true};
        final List<GeodeticPointMission> geodeticPointMissions = mGeodeticPointMissionRepository.getGeodeticPointMissionsByGeoPntId(oldId);
        final List<GeodeticPointTask> geodeticPointTasks = mGeodeticPointTaskRepository.getGeodeticPointTasksByGeoPntId(oldId);
        final List<Note> notes = mNoteRepository.getNotesByGeoPntId(oldId);
        final List<Image> images = mImageRepository.getImagesByGeoPntId(oldId);

        for (GeodeticPointMission geodeticPointMission : geodeticPointMissions) {
            geodeticPointMission.setGeodeticPointId(newId); // Synchronize with the new geodeticPointId
        }
        for (GeodeticPointTask geodeticPointTask : geodeticPointTasks) {
            Log.d(TAG, "synchronizeGeodeticPointJoinedEntities: setting new geodetic point id to gpTask:" + newId);
            geodeticPointTask.setGeodeticPointId(newId); // Synchronize with the new geodeticPointId
        }
        for (Note note : notes) {
            note.setGeodeticPointId(newId); // Synchronize with the new geodeticPointId
        }
        for (Image image : images) {
            image.setGeodeticPointId(newId); // Synchronize with the new geodeticPointId
        }
        synchronizeEntitiesByModelName(geodeticPointMissions, Constants.GEODETIC_POINT_MISSION_MODEL, new BatchSyncCallback() {
            @Override
            public void then(boolean areGeodeticPointMissionsSynchronizedOk) {
                areJoinedEntitiesSynchronizedOk[0] = areJoinedEntitiesSynchronizedOk[0] && areGeodeticPointMissionsSynchronizedOk;
                synchronizeEntitiesByModelName(geodeticPointTasks, Constants.GEODETIC_POINT_TASK_MODEL, new BatchSyncCallback() {
                    @Override
                    public void then(boolean areGeodeticPointTasksSynchronizedOk) {
                        areJoinedEntitiesSynchronizedOk[0] = areJoinedEntitiesSynchronizedOk[0] && areGeodeticPointTasksSynchronizedOk;
                        synchronizeEntitiesByModelName(notes, Constants.NOTE_MODEL, new BatchSyncCallback() {
                            @Override
                            public void then(boolean areNotesSynchronizedOk) {
                                areJoinedEntitiesSynchronizedOk[0] = areJoinedEntitiesSynchronizedOk[0] && areNotesSynchronizedOk;
                                synchronizeEntitiesByModelName(images, Constants.IMAGE_MODEL, new BatchSyncCallback() {
                                    @Override
                                    public void then(boolean areImagesSynchronizedOk) {
                                        areJoinedEntitiesSynchronizedOk[0] = areJoinedEntitiesSynchronizedOk[0] && areImagesSynchronizedOk;
                                        callback.then(areJoinedEntitiesSynchronizedOk[0]);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    public <T extends BaseModel> void synchronizeEntitiesByModelName(List<T> entities, final String modelName, final BatchSyncCallback callback) {
        Log.d(TAG, "synchronizeEntitiesByModelName: modelName: " + modelName);
        final Integer[] remainingEntities = {entities.size()};
        final Boolean[] areEntitiesSynchronizedOk = {true};
        Log.d(TAG, "synchronizeEntitiesByModelName: remainingEntities size: " + remainingEntities[0]);
        if (remainingEntities[0].equals(0)) {
            callback.then(areEntitiesSynchronizedOk[0]);
            return;
        }

        for (final T entity : entities) {
            setSyncModelInfo(modelName, Constants.SYNC_STATUS_UPLOADING);
            final ToSynchronizeEntity toSynchronizeEntity =
                    mToSynchronizeEntityRepository.getToSynchronizeEntityByModelAndId(modelName, entity.getId());
            if (toSynchronizeEntity == null) {
                Log.e(TAG, "toSynchronizeEntity is null by model name " + modelName + " and entityId " + entity.getId());
            }

            DbSyncAPI.SyncCallback syncCallback = new DbSyncAPI.SyncCallback() {
                @Override
                public void onDone(Object syncedEntity, Integer origId) {
                    T syncedEntityGeneric = (T) syncedEntity;
                    // changeId(modelName, origId, -1 * syncedEntityGeneric.getId()); // Change to temporary negative value
                    if (toSynchronizeEntity != null) {
                        // Update entity id, master id and synchronizationError to 0 in toSynchronizeEntity
                        toSynchronizeEntity.setMasterId(syncedEntityGeneric.getId());
                        toSynchronizeEntity.setSynchronizationError(0);
                        mToSynchronizeEntityRepository.update(toSynchronizeEntity);
                    } else {
                        // Create toSynchronizeEntity record (for change id process)
                        ToSynchronizeEntity newToSynchronizeEntity = new ToSynchronizeEntity(modelName, origId, syncedEntityGeneric.getId(), 0);
                        mToSynchronizeEntityRepository.insert(newToSynchronizeEntity);
                    }

                    // Set remaining entities
                    remainingEntities[0] -= 1;
                    Log.d(TAG, "onDone model " + modelName + " sync: remaining entities: " + remainingEntities[0]);
                    if (remainingEntities[0].equals(0)) {
                        if (areEntitiesSynchronizedOk[0]) {
                            setSyncModelInfo(modelName, Constants.SYNC_STATUS_DONE);
                        } else {
                            setSyncModelInfo(modelName, Constants.SYNC_STATUS_DONE_WITH_ERROR);
                        }

                        callback.then(areEntitiesSynchronizedOk[0]);
                    }
                }

                @Override
                public void onError(String errorStr, String verbose, Integer origId) {
                    if (toSynchronizeEntity != null) {
                        // Update ToSynchronizeEntity
                        toSynchronizeEntity.setSynchronizationError(1);
                        mToSynchronizeEntityRepository.update(toSynchronizeEntity);
                    } else {
                        // Create toSynchronizeEntity record (for change id task)
                        ToSynchronizeEntity newToSynchronizeEntity = new ToSynchronizeEntity(modelName, origId, 0, 1);
                        mToSynchronizeEntityRepository.insert(newToSynchronizeEntity);
                    }
                    areEntitiesSynchronizedOk[0] = false;
                    Log.e("SyncCallback", "error: " + errorStr + ", " + verbose);

                    // Set remaining entities
                    remainingEntities[0] -= 1;
                    Log.d(TAG, "onError model " + modelName + ", entity id: " + entity.getId() + ", sync: remaining entities: " + remainingEntities[0]);
                    if (remainingEntities[0].equals(0)) {
                        if (areEntitiesSynchronizedOk[0]) {
                            setSyncModelInfo(modelName, Constants.SYNC_STATUS_DONE);
                        } else {
                            setSyncModelInfo(modelName, Constants.SYNC_STATUS_DONE_WITH_ERROR);
                        }

                        callback.then(areEntitiesSynchronizedOk[0]);
                    }
                }
            };

            switch (modelName) {
                case Constants.GEODETIC_POINT_MISSION_MODEL:
                    mGeodeticPointMissionRepository.synchronize((GeodeticPointMission) entity, syncCallback);
                    break;
                case Constants.GEODETIC_POINT_TASK_MODEL:
                    mGeodeticPointTaskRepository.synchronize((GeodeticPointTask) entity, syncCallback);
                    break;
                case Constants.NOTE_MODEL:
                    mNoteRepository.synchronize((Note) entity, syncCallback);
                    break;
                case Constants.IMAGE_MODEL:
                    mImageRepository.synchronize((Image) entity, syncCallback);
                    break;
                case Constants.USER_MODEL:
                    mUserRepository.synchronize((User) entity, syncCallback);
                    break;
            }
        }
    }

    private void changeId(String modelName, Integer origId, int newId) {
        switch (modelName) {
            case Constants.GEODETIC_POINT_MISSION_MODEL:
                mGeodeticPointMissionRepository.changeId(origId, newId);
                break;
            case Constants.GEODETIC_POINT_TASK_MODEL:
                mGeodeticPointTaskRepository.changeId(origId, newId);
                break;
            case Constants.NOTE_MODEL:
                mNoteRepository.changeId(origId, newId);
                break;
            case Constants.IMAGE_MODEL:
                mImageRepository.changeId(origId, newId);
                break;
            case Constants.USER_MODEL:
                mUserRepository.changeId(origId, newId);
                break;
        }
    }

    public void downloadUserGroups(final BatchDownloadCallback callback) {
        Log.d(TAG, "downloadUsers: Downloading userGroups");
        // Download
        mUserGroupRepository.downloadUserGroups(null, null, null, new UserGroupRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.USER_GROUP_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.USER_GROUP_MODEL, status);
                }
            }
        });
    }

    public void downloadNotes(Date lastModifiedDate, Integer userId, boolean alsoAdminNotes, final BatchDownloadCallback callback) {
        mNoteRepository.downloadNotes(lastModifiedDate, userId, alsoAdminNotes, new NoteRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.NOTE_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.NOTE_MODEL, status);
                }
            }
        });
    }

    public void downloadGeodeticPointMissions(Date lastModifiedDate, Integer userId, List<Integer> groupIds, final BatchDownloadCallback callback) {
        Log.d(TAG, "downloadGeodeticPointMissions: for user id:" + userId);
        mGeodeticPointMissionRepository.downloadGeodeticPointMissions(lastModifiedDate, userId, groupIds, new GeodeticPointMissionRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.GEODETIC_POINT_MISSION_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.GEODETIC_POINT_MISSION_MODEL, status);
                }
            }
        });
    }

    public void downloadGeodeticPointTasks(Date lastModifiedDate, Integer userId, List<Integer> groupIds, final BatchDownloadCallback callback) {
        mGeodeticPointTaskRepository.downloadGeodeticPointTasks(lastModifiedDate, userId, groupIds, new GeodeticPointTaskRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.GEODETIC_POINT_TASK_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.GEODETIC_POINT_TASK_MODEL, status);
                }
            }
        });
    }

    public void downloadTasks(Date lastModifiedDate, final BatchDownloadCallback callback) {
        mTaskRepository.downloadTasks(lastModifiedDate, new TaskRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.TASK_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.TASK_MODEL, status);
                }
            }
        });
    }

    public void downloadImages(Date lastModifiedDate, List<Integer> missionIds, final BatchDownloadCallback callback) {
        mImageRepository.downloadImages(lastModifiedDate, missionIds, new GeodeticPointTaskRepository.SyncCallback() {
            @Override
            public void onStatusChange(Integer status) {
                setSyncModelInfo(Constants.IMAGE_MODEL, status);
                if (status.equals(Constants.SYNC_STATUS_DONE) || status.equals(Constants.SYNC_STATUS_DONE_WITH_ERROR)) {
                    callback.then(Constants.IMAGE_MODEL, status);
                }
            }
        });
    }

    public void deleteNotUpdatedSynchronizedEntities() {
        List<ToSynchronizeEntity> toSynchronizeEntities = mToSynchronizeEntityRepository.getSynchronizedToSynchronizeEntities();
        Log.d(TAG, "deleteNotUpdatedSynchronizedEntities: size of toSynchronizeEntities: " + toSynchronizeEntities.size());
        for (ToSynchronizeEntity toSynchronizeEntity : toSynchronizeEntities) {
            Log.d(TAG, "deleteNotUpdatedSynchronizedEntities: Processing toSynchronizeEntity: " + toSynchronizeEntity.toString());
            if (!toSynchronizeEntity.getMasterId().equals(toSynchronizeEntity.getEntityId())) { // Entity was updated with the new id from master
                // Delete entity from database
                switch (toSynchronizeEntity.getModelName()) {
                    case Constants.GEODETIC_POINT_MODEL:
                        mGeodeticPointRepository.deleteByIds(toSynchronizeEntity.getEntityId());
                        break;
                    case Constants.GEODETIC_POINT_MISSION_MODEL:
                        mGeodeticPointMissionRepository.deleteByIds(toSynchronizeEntity.getEntityId());
                        break;
                    case Constants.GEODETIC_POINT_TASK_MODEL:
                        mGeodeticPointTaskRepository.deleteByIds(toSynchronizeEntity.getEntityId());
                        break;
                    case Constants.NOTE_MODEL:
                        mNoteRepository.deleteByIds(toSynchronizeEntity.getEntityId());
                        break;
                }
            }

            // Delete entity from toSynchronizeEntities
            mToSynchronizeEntityRepository.delete(toSynchronizeEntity);
        }
    }

    public void changeNegativeIdsToPositive() {
        Log.d(TAG, "changeNegativeIdsToPositive in geodetic points");
        mGeodeticPointRepository.absIds();
        Log.d(TAG, "changeNegativeIdsToPositive in users");
        mUserRepository.absIds();
        Log.d(TAG, "changeNegativeIdsToPositive in geodetic point tasks");
        mGeodeticPointTaskRepository.absIds();
        Log.d(TAG, "changeNegativeIdsToPositive in geodetic point missions");
        mGeodeticPointMissionRepository.absIds();
        Log.d(TAG, "changeNegativeIdsToPositive in notes");
        mNoteRepository.absIds();
        Log.d(TAG, "changeNegativeIdsToPositive in images");
        mImageRepository.absIds();
    }

    public void removeDataFromDatabaseSync() {
        mGeodeticPointRepository.deleteAll();
        mImageRepository.deleteAll();
        mMissionRepository.deleteAll();
        mNoteRepository.deleteAll();
        mTaskRepository.deleteAll();
        mUserRepository.deleteAll();
        mUserGroupRepository.deleteAll();
        mGeodeticPointMissionRepository.deleteAll();
        mGeodeticPointTaskRepository.deleteAll();
        mToSynchronizeEntityRepository.deleteAll();
    }
}
