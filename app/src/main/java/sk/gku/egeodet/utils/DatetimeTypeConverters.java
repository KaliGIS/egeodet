package sk.gku.egeodet.utils;

import androidx.room.TypeConverter;

import sk.gku.egeodet.configs.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DatetimeTypeConverters {
    static DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
    static DateFormat mariaDbDf = new SimpleDateFormat(Constants.MARIA_DB_DATE_TIME_FORMAT);

    @TypeConverter
    public static Date fromTimestamp(String value) {
        // df.setTimeZone(TimeZone.getTimeZone("UTC"));
        synchronized (DatetimeTypeConverters.class) {
            if (value != null) {
                try {
                    // Log.d("DatetimeTypeConverters", "fromTimestamp: " + value);
                    Date parsed = df.parse(value);
                    // Log.d("DatetimeTypeConverters", "parsed: " + parsed);
                    return parsed;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            } else {
                return null;
            }
        }
    }

    @TypeConverter
    public static String toTimestamp(Date value) {
        // df.setTimeZone(TimeZone.getTimeZone("UTC"));
        synchronized (DatetimeTypeConverters.class) {
            if (value != null) {
                try {
                    // Log.d("DatetimeTypeConverters", "toTimestamp: " + value);
                    String formatted = df.format(value);
                    // Log.d("DatetimeTypeConverters", "formatted: " + formatted);
                    return formatted;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            } else {
                return null;
            }
        }
    }

    public static Date getDateTimeFromMariaDbJson(JSONObject dateTimeObj) throws JSONException {
        if (dateTimeObj.has("date")) {
            String dateTime = dateTimeObj.getString("date");
            try {
                Date parsed = df.parse(dateTime);
                return parsed;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
