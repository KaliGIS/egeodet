package sk.gku.egeodet.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import sk.gku.egeodet.viewmodels.GeodeticPointMissionViewModel;

import java.util.List;

public class GeodeticPointMissionViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private int mMissionId;
    private List<Integer> mGroupIds;

    public GeodeticPointMissionViewModelFactory(Application application, int missionId, List<Integer> groupIds) {
        mApplication = application;
        mMissionId = missionId;
        mGroupIds = groupIds;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new GeodeticPointMissionViewModel(mApplication, mMissionId, mGroupIds);
    }
}
