package sk.gku.egeodet.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class InternetCheck {
    private ConnectivityManager mConnectivityManager;

    private Consumer mConsumer;
    public interface Consumer { void accept(Boolean internet); }

    public InternetCheck(Context context) {
        mConnectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public InternetCheck(Consumer callback) {
        isOnline(callback);
    }

    public void isOnline(Consumer callback) {
        mConsumer = callback;
        new InternetCheckAsyncTask().execute();
    }

    public boolean isConnected() {
        NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();
        boolean isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();
        return isConnected;
    }

    public boolean isMetered() {
        boolean isMetered = mConnectivityManager.isActiveNetworkMetered();
        return isMetered;
    }

    private class InternetCheckAsyncTask extends AsyncTask<Void, Void, Boolean> {
        @Override protected Boolean doInBackground(Void... voids) { try {
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500); // Google DNS
            sock.close();
            return true;
        } catch (IOException e) { return false; } }

        @Override protected void onPostExecute(Boolean internet) { mConsumer.accept(internet); }
    }

}
