package sk.gku.egeodet.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import sk.gku.egeodet.activities.MainActivity;
import sk.gku.egeodet.configs.Constants;

public class AlarmReveiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.RESTART_APPLICATION_INTENT)) {
            Log.i("AlarmReceiver", "onReceive: APLLICATION RESTARTED");
            Intent mainActivityIntent = new Intent(context, MainActivity.class);
            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainActivityIntent);
        }
    }
}
