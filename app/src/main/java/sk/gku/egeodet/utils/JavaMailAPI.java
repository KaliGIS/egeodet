package sk.gku.egeodet.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.room.Ignore;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class JavaMailAPI extends AsyncTask<Void,Void,Boolean>  {

    //Add those line in dependencies
    //implementation files('libs/activation.jar')
    //implementation files('libs/additionnal.jar')
    //implementation files('libs/mail.jar')

    //Need INTERNET permission

    //Variables
    private Context mContext;
    private Session mSession;
    private Consumer mConsumer;

    private List<String> mEmails = new ArrayList<>();
    private String mSubject;
    private String mMessage;
    private String[] mAttachmentSrcs;
    private boolean mIsAppCrashed;
    private Throwable mException;

    private ProgressDialog mProgressDialog;

    public interface Consumer { void afterSend(Boolean isSent); }

    //Constructor
    public JavaMailAPI(Context mContext, String mEmail, String mSubject, String mMessage, String[] mAttachmentSrc) {
        this.mContext = mContext;
        this.mEmails.add(mEmail);
        this.mSubject = mSubject;
        this.mMessage = mMessage;
        this.mAttachmentSrcs = mAttachmentSrc;
        this.mIsAppCrashed = false;
    }

    @Ignore
    public JavaMailAPI(Context mContext, String mEmail, String mSubject, String mMessage, String[] mAttachmentSrc, boolean isAppCrashed, Consumer consumer) {
        this.mConsumer = consumer;
        this.mContext = mContext;
        this.mEmails.add(mEmail);
        this.mSubject = mSubject;
        this.mMessage = mMessage;
        this.mAttachmentSrcs = mAttachmentSrc;
        this.mIsAppCrashed = isAppCrashed;
        execute();
    }

    @Ignore
    public JavaMailAPI(Context mContext, List<String> mEmails, String mSubject, String mMessage, String[] mAttachmentSrc, boolean isAppCrashed, Consumer consumer) {
        this.mConsumer = consumer;
        this.mContext = mContext;
        this.mEmails.addAll(mEmails);
        this.mSubject = mSubject;
        this.mMessage = mMessage;
        this.mAttachmentSrcs = mAttachmentSrc;
        this.mIsAppCrashed = isAppCrashed;
        execute();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (!mIsAppCrashed) {
            //Show progress dialog while sending email
            mProgressDialog = ProgressDialog.show(mContext,"Email sa odosiela", "Prosím čakajte...",false,false);
        }
    }

    @Override
    protected void onPostExecute(Boolean isSent) {
        super.onPostExecute(isSent);
        Log.d("onPostExecute", "onPostExecute: EMAIL");

        if (!mIsAppCrashed) {
            //Dismiss progress dialog when message successfully send
            mProgressDialog.dismiss();

            if (isSent) {
                //Show success toast
                Toast.makeText(mContext,"Email odoslaný",Toast.LENGTH_SHORT).show();
            } else {
                new AlertDialog.Builder(mContext)
                    .setTitle(R.string.error)
                    .setMessage("Email sa nepodarilo odoslať." + "\n\n" + mException.getMessage())
                    .setPositiveButton(R.string.ok, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            }
        }

        Log.d("onPostExecute", "onPostExecute: Calling consumer.afterSend() method");
        if (mConsumer != null) {
            mConsumer.afterSend(isSent);
        }
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        Log.d("doInBackground", "doInBackground: SEND EMAIL");
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
//        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.put("mail.smtp.socketFactory.port", "587");
//        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.port", "587");

        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.quitwait", "false");


        //Creating a new session
        mSession = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(BuildConfig.NOREPLY_EGEODET_EMAIL, BuildConfig.NOREPLY_EGEODET_EMAIL_PASSWORD);
                    }
                });

        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(mSession);

            //Setting sender address
            mm.setFrom(new InternetAddress(BuildConfig.NOREPLY_EGEODET_EMAIL));
            //Adding receiver/-s
            Set<String> uniqueEmails = new HashSet<>(mEmails); // Remove duplicities
            mEmails.clear();
            mEmails.addAll(uniqueEmails);
            for (String mEmail : mEmails) {
                Log.d("JavaMailAPI", "doInBackground: Adding recipient " + mEmail);
                mm.addRecipient(Message.RecipientType.TO, new InternetAddress(mEmail));
            }
            //Adding subject
            mm.setSubject(mSubject);
//            //Adding message
//            mm.setText(mMessage);

            BodyPart messageBodyPart = new MimeBodyPart();

            messageBodyPart.setText(mMessage);

            Multipart multipart = new MimeMultipart();

            multipart.addBodyPart(messageBodyPart);

            // add attachments
            for (String attachmentSrc : mAttachmentSrcs) {
                messageBodyPart = new MimeBodyPart();

                DataSource source = new FileDataSource(attachmentSrc);

                messageBodyPart.setDataHandler(new DataHandler(source));

                int slashPosition = attachmentSrc.lastIndexOf('/');
                String fileName = attachmentSrc.substring(slashPosition + 1);
                messageBodyPart.setFileName(fileName);

                multipart.addBodyPart(messageBodyPart);
            }

            mm.setContent(multipart);

            //Sending email
            Transport.send(mm);

        } catch (MessagingException e) {
            e.printStackTrace();
            mException = e;
            return false;
        }

        return true;
    }
}
