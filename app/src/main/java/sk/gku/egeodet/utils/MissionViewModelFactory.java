package sk.gku.egeodet.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import sk.gku.egeodet.viewmodels.MissionViewModel;

import java.util.List;

public class MissionViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private List<Integer> mGroupIds;

    public MissionViewModelFactory(Application application, List<Integer> groupIds) {
        mApplication = application;
        mGroupIds = groupIds;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new MissionViewModel(mApplication);
    }
}
