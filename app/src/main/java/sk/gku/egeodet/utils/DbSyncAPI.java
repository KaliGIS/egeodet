package sk.gku.egeodet.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import sk.gku.egeodet.BuildConfig;
import sk.gku.egeodet.configs.Constants;
import sk.gku.egeodet.converters.GeodeticPointConverter;
import sk.gku.egeodet.converters.GeodeticPointMissionConverter;
import sk.gku.egeodet.converters.GeodeticPointTaskConverter;
import sk.gku.egeodet.converters.ImageConverter;
import sk.gku.egeodet.converters.MissionConverter;
import sk.gku.egeodet.converters.NoteConverter;
import sk.gku.egeodet.converters.SynchronizationConverter;
import sk.gku.egeodet.converters.TaskConverter;
import sk.gku.egeodet.converters.UserConverter;
import sk.gku.egeodet.converters.UserGroupConverter;
import sk.gku.egeodet.eGeodetApplication;
import sk.gku.egeodet.models.GeodeticPoint;
import sk.gku.egeodet.models.GeodeticPointMission;
import sk.gku.egeodet.models.GeodeticPointTask;
import sk.gku.egeodet.models.Image;
import sk.gku.egeodet.models.JoinGeodeticPointTaskTask;
import sk.gku.egeodet.models.Mission;
import sk.gku.egeodet.models.Note;
import sk.gku.egeodet.models.Synchronization;
import sk.gku.egeodet.models.Task;
import sk.gku.egeodet.models.User;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.crypto.SecretKey;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

public class DbSyncAPI {
    public static final String TAG = "DbSyncAPI";
    private AsyncHttpClient mClient;
    private Context mContext;
    private SharedPreferences mPreferences;
    private String sharedPrefFile =
            "com.example.android.eGeodet";

    public interface RequestCallback
    {
        void onDataChunk(List<?> items, int totalCount);
        void onDataChunk(List<?> items1, List<?> items2, List<?> items3, int totalCount);
        void onDataChunk(List<?> items1, List<?> items2, List<?> items3, List<?> items4, int totalCount);
        void onDone();
        void onError(Integer status, String errorText);
    }

    public interface SyncCallback {
        void onDone(Object syncedItem, Integer origId);
        void onError(String errorStr, String errorText, Integer origId);
    }

    public DbSyncAPI(Context context) {
        this.mClient = new AsyncHttpClient();
        this.mClient.setMaxConnections(100);
        this.mClient.setConnectTimeout(300 * 1000); // 5 minutes
        this.mClient.setResponseTimeout(300 * 1000); // 5 minutes
        this.mClient.setTimeout(300 * 1000); // 5 minutes
        this.mClient.setMaxRetriesAndTimeout(10, 10 * 1000); // 10 sec between retries
        this.mContext = context;
        this.mPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);
    }

    public String getAbsoluteUrl(String endpoint) {
        return BuildConfig.DB_SYNC_BASE_URL + endpoint;
    }

    public static String getSyncKey() {
        String syncKey = eGeodetApplication.getModel() + "_" + Build.VERSION.SDK_INT;

        return syncKey;
    }

    public void syncSynchronizationData(final Synchronization synchronizationEntity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(synchronizationEntity.toString(), ContentType.APPLICATION_JSON);
        Log.d("DbSyncApi", "syncSynchronizationData: stringEntity: " + requestEntity);
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/synchronizations/sync"), requestEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data.has("id")) { // checks if not null
                            SynchronizationConverter getSynchronization = new SynchronizationConverter();
                            Synchronization synchronization = getSynchronization.fromJSONObject(data);
                            callback.onDone(synchronization, synchronizationEntity.getId());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), synchronizationEntity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), synchronizationEntity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, synchronizationEntity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, synchronizationEntity.getId());
            }
        });
    }

    public void syncGeodeticPointData(final GeodeticPoint geodeticPointEntity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(geodeticPointEntity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncGeodeticPointData: to send " + geodeticPointEntity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/geodeticpoints/sync"), requestEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data.has("id")) { // checks if not null
                            GeodeticPointConverter getGeodeticPoint = new GeodeticPointConverter();
                            GeodeticPoint geodeticPoint = getGeodeticPoint.fromJSONObject(data);
                            callback.onDone(geodeticPoint, geodeticPointEntity.getId());
                        } else {
                            throw new IllegalArgumentException("Returned geodetic point JSON has no 'id' property");
                        }
                    } else {
                        throw new IllegalArgumentException("Returned geodetic point JSON has no 'data' property");
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), geodeticPointEntity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), geodeticPointEntity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, geodeticPointEntity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, geodeticPointEntity.getId());
            }
        });
    }

    public void syncGeodeticPointMissionData(final GeodeticPointMission entity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(entity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncGeodeticPointMissionData: to send " + entity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/geodeticpointmissions/sync"), requestEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data.has("id")) { // checks if not null
                            GeodeticPointMissionConverter getGeodeticPointMission = new GeodeticPointMissionConverter();
                            GeodeticPointMission geodeticPointMission = getGeodeticPointMission.fromJSONObject(data);
                            callback.onDone(geodeticPointMission, entity.getId());
                        } else {
                            throw new IllegalArgumentException("Returned geodetic point mission JSON has no 'id' property");
                        }
                    } else {
                        throw new IllegalArgumentException("Returned geodetic point mission JSON has no 'data' property");
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), entity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, entity.getId());
            }
        });
    }

    public void syncGeodeticPointTaskData(final GeodeticPointTask entity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(entity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncGeodeticPointTaskData: to send " + entity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/geodeticpointtasks/sync"), requestEntity, "application/json", new TextHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d(TAG, "onSuccess: geodeticPointTask response: " + responseString);
                    try {
                        JSONObject response = new JSONObject(responseString);
                        Log.d(TAG, "onSuccess: geodeticPointTask response OK");
                        if (response.has("data")) {
                            JSONObject data = response.getJSONObject("data");
                            if (data.has("synchronizedData")) {
                                JSONObject synchronizedData = data.getJSONObject("synchronizedData");
                                if (synchronizedData.has("id")) { // checks if not null
                                    GeodeticPointTaskConverter getGeodeticPointTask = new GeodeticPointTaskConverter();
                                    JoinGeodeticPointTaskTask geodeticPointTask = getGeodeticPointTask.fromJSONObject(synchronizedData);
                                    if (data.has("wasConflict") && data.getBoolean("wasConflict")) {
                                        Log.d(TAG, "onSuccess: wasConflict value: " + data.getBoolean("wasConflict"));
                                        setConflictToSharedPreferences(geodeticPointTask.getGeodeticPointLabel(), geodeticPointTask.getTaskName());
                                    }
                                    callback.onDone(geodeticPointTask, entity.getId());
                                } else {
                                    throw new IllegalArgumentException("Returned geodetic point task JSON has no 'id' property");
                                }
                            } else {
                                throw new IllegalArgumentException("Returned geodetic point task JSON has no 'synchronizedData' property");
                            }
                        } else {
                            throw new IllegalArgumentException("Returned geodetic point task JSON has no 'data' property");
                        }
                    } catch (JSONException | IllegalArgumentException e) {
                        e.printStackTrace();
                        callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), entity.getId());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    throwable.printStackTrace();
                    Log.e(TAG, "onFailure: status code: " + statusCode);
                    callback.onError(throwable.getMessage(), responseString, entity.getId());
                }
        });
    }

    public void syncNoteData(final Note entity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(entity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncNoteData: to send " + entity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/notes/sync"), requestEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");
                        Log.d(TAG, "onSuccess: note data from server: " + data.toString());

                        if (data.has("id")) { // checks if not null
                            NoteConverter getNote = new NoteConverter();
                            Note note = getNote.fromJSONObject(data);
                            callback.onDone(note, entity.getId());
                        } else {
                            throw new IllegalArgumentException("Returned note JSON has no 'id' property");
                        }
                    } else {
                        throw new IllegalArgumentException("Returned note JSON has no 'data' property");
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), entity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, entity.getId());
            }
        });
    }

    public void syncImageData(final Image entity, File imageFile, final SyncCallback callback) {
        Log.d(TAG, "syncImageData: file: " + imageFile + ", entity: " + entity.toString());
        RequestParams requestParams = new RequestParams();
        try {
            requestParams.put("images[]", imageFile);
        } catch (FileNotFoundException e) {
            Log.w(TAG, "syncImageData: Image not found on device: " + imageFile);
            callback.onDone(entity, entity.getId());
            return;
        }
        requestParams.put("id", entity.getId());
        requestParams.put("name", entity.getName());
        requestParams.put("path", entity.getPath());
        requestParams.put("geodeticPointId", entity.getGeodeticPointId());
        requestParams.put("missionId", entity.getMissionId());
        requestParams.put("created", entity.getCreated());
        requestParams.put("modified", entity.getModified());

        StringEntity requestEntity = new StringEntity(entity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncImageData: to send " + entity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/images/sync"), requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data.has("id")) { // checks if not null
                            ImageConverter getImage = new ImageConverter();
                            Image image = getImage.fromJSONObject(data);
                            Log.d(TAG, "onSuccess: Image returned non error status code. Orig ID: " + entity.getId() + ", returned ID: " + image.getId());
                            callback.onDone(image, entity.getId());
                        } else {
                            throw new IllegalArgumentException("Returned image JSON has no 'id' property");
                        }
                    } else {
                        throw new IllegalArgumentException("Returned image JSON has no 'data' property");
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), entity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, entity.getId());
            }
        });
    }

    public void syncUserData(final User entity, final SyncCallback callback) {
        StringEntity requestEntity = new StringEntity(entity.toString(), ContentType.APPLICATION_JSON);
        Log.d(TAG, "syncUserData: to send " + entity.toString());
        mClient.addHeader("Authorization", "Bearer " + getJwt());

        mClient.post(mContext, getAbsoluteUrl("/users/sync"), requestEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    // If the response is JSONObject instead of expected JSONArray
                    if (response.has("data")) {
                        JSONObject data = response.getJSONObject("data");
                        Log.d(TAG, "onSuccess: user data from server: " + data.toString());

                        if (data.has("id")) { // checks if not null
                            UserConverter getUser = new UserConverter();
                            User user = getUser.fromJSONObject(data);
                            callback.onDone(user, entity.getId());
                        } else {
                            throw new IllegalArgumentException("Returned user JSON has no 'id' property");
                        }
                    } else {
                        throw new IllegalArgumentException("Returned user JSON has no 'data' property");
                    }
                } catch (JSONException | IllegalArgumentException e) {
                    e.printStackTrace();
                    callback.onError(e.getMessage(), Arrays.toString(e.getStackTrace()), entity.getId());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                callback.onError(throwable.getMessage(), errorResponse.toString(), entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                callback.onError(throwable.getMessage(), responseString, entity.getId());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                String errorText = jsonArrayToString(errorResponse);
                callback.onError(throwable.getMessage(), errorText, entity.getId());
            }
        });
    }

    public void getUsers(final int fromId, final int limit, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("fromid", fromId);
        params.put("limit", limit);


        mClient.get(getAbsoluteUrl("/users"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray usersArr = data.getJSONArray("users");
                    int totalCount = data.getInt("count");

                    if (usersArr.length() > 0) {
                        UserConverter getUsers = new UserConverter();
                        List<User> usersChunk = getUsers.fromJSONArray(usersArr);
                        requestCallback.onDataChunk(usersChunk, totalCount);
                    }

                    if (data.getInt("nextId") > -1) {
                        System.out.println("Has next");
                        getUsers(data.getInt("nextId"), limit, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getUsers(fromId, limit, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getGeodeticPointMissions(final int fromId, final int limit, final Date lastModifiedDate, final Integer userId, final List<Integer> groupIds, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("lifecyclephase", 1);
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }
        if (userId != null) {
            params.put("userid", userId);
        }
        if (groupIds != null && groupIds.size() > 0) {
            int i = 0;
            for (Integer groupId : groupIds) {
                params.put("groupids[" + i + "]", groupId);
                i++;
                Log.d(TAG, "getGeodeticPointMissions: groupId: " + groupId);
            }
        }

        Log.d(TAG, "getGeodeticPointMissions: request params: " + params.toString());
        mClient.get(getAbsoluteUrl("/geodeticpointmissions"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray geodeticPointMissionsArr = data.getJSONArray("geodeticPointMissions");
                    int totalCount = data.getInt("count");

                    if (geodeticPointMissionsArr.length() > 0) {
                        GeodeticPointMissionConverter getGeodeticPointMissions = new GeodeticPointMissionConverter();
                        Map<String, List> geodeticPointMissionsChunk = getGeodeticPointMissions.fromJSONArray(geodeticPointMissionsArr);

                        requestCallback.onDataChunk(
                                geodeticPointMissionsChunk.get("geodeticPointMissions"),
                                geodeticPointMissionsChunk.get("geodeticPoints"),
                                geodeticPointMissionsChunk.get("missions"),
                                totalCount
                        );
                    }

                    if (data.getInt("nextId") > -1) {
                        getGeodeticPointMissions(data.getInt("nextId"), limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getGeodeticPointMissions(fromId, limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getGeodeticPointTasks(final int fromId, final int limit, final Date lastModifiedDate, final Integer userId, final List<Integer> groupIds, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("lifecyclephase", 1);
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }
        if (userId != null) {
            params.put("userid", userId);
        }
        if (groupIds != null && groupIds.size() > 0) {
            int i = 0;
            for (Integer groupId : groupIds) {
                params.put("groupids[" + i + "]", groupId);
                i++;
                Log.d(TAG, "getGeodeticPointTasks: groupId: " + groupId);
            }
        }
        Log.d(TAG, "getGeodeticPointTasks: userId: " + userId);
        Log.d(TAG, "getGeodeticPointTasks: fromId: " + fromId);
        Log.d(TAG, "getGeodeticPointTasks: limit: " + limit);

        mClient.get(getAbsoluteUrl("/geodeticpointtasks"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    Log.d(TAG, "onSuccess: Geodetic point tasks response String: " + responseString);
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray geodeticPointTasksArr = data.getJSONArray("geodeticPointTasks");
                    int totalCount = data.getInt("count");
                    Log.d(TAG, "onSuccess: incoming geodetic point task data length: " + geodeticPointTasksArr.length());

                    if (geodeticPointTasksArr.length() > 0) {
                        GeodeticPointTaskConverter getGeodeticPointTasks = new GeodeticPointTaskConverter();
                        Map<String, List> geodeticPointTasksChunk = getGeodeticPointTasks.fromJSONArray(geodeticPointTasksArr);

                        requestCallback.onDataChunk(
                                geodeticPointTasksChunk.get("geodeticPointTasks"),
                                geodeticPointTasksChunk.get("geodeticPoints"),
                                geodeticPointTasksChunk.get("missions"),
                                geodeticPointTasksChunk.get("tasks"),
                                totalCount
                        );
                    }

                    if (data.getInt("nextId") > -1) {
                        getGeodeticPointTasks(data.getInt("nextId"), limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getGeodeticPointTasks(fromId, limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getNotes(final int fromId, final int limit, final Date lastModifiedDate, final Integer userId, final boolean alsoAdminNotes, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("lifecyclephase", 1);
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }
        if (userId != null) {
            params.put("userid", userId);
        }

        if (alsoAdminNotes) {
            params.put("alsoadmin", alsoAdminNotes);
        }

        mClient.get(getAbsoluteUrl("/notes"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray notesArr = data.getJSONArray("notes");
                    int totalCount = data.getInt("count");
                    Log.d(TAG, "onSuccess: Incoming notes count: " + notesArr.length());

                    if (notesArr.length() > 0) {
                        NoteConverter getNotes = new NoteConverter();
                        List<Note> notesChunk = getNotes.fromJSONArray(notesArr);

                        requestCallback.onDataChunk(notesChunk, totalCount);
                    }

                    if (data.getInt("nextId") > -1) {
                        getNotes(data.getInt("nextId"), limit, lastModifiedDate, userId, alsoAdminNotes, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getNotes(fromId, limit, lastModifiedDate, userId, alsoAdminNotes, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getTasks(final int fromId, final int limit, final String taskType, final Date lastModifiedDate, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (taskType != null) {
            params.put("type", taskType);
        }

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }

        mClient.get(getAbsoluteUrl("/tasks"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray tasksArr = data.getJSONArray("tasks");
                    int totalCount = data.getInt("count");

                    if (tasksArr.length() > 0) {
                        TaskConverter getTasks = new TaskConverter();
                        List<Task> tasksChunk = getTasks.fromJSONArray(tasksArr);

                        requestCallback.onDataChunk(
                                tasksChunk,
                                totalCount
                        );
                    }

                    if (data.getInt("nextId") > -1) {
                        getTasks(data.getInt("nextId"), limit, taskType, lastModifiedDate, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getTasks(fromId, limit, taskType, lastModifiedDate, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getUserGroups(final int fromId, final int limit, final Date lastModifiedDate, final Integer userId, final List<Integer> groupIds, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }
        if (userId != null) {
            params.put("userid", userId);
        }
        if (groupIds != null && groupIds.size() > 0) {
            int i = 0;
            for (Integer groupId : groupIds) {
                params.put("groupids[" + i + "]", groupId);
                i++;
                Log.d(TAG, "getUserGroups: groupId: " + groupId);
            }
        }

        mClient.get(getAbsoluteUrl("/usergroups"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray userGroupsArr = data.getJSONArray("userGroups");
                    int totalCount = data.getInt("count");

                    if (userGroupsArr.length() > 0) {
                        UserGroupConverter getUserGroups = new UserGroupConverter();
                        Map<String, List> userGroupsChunk = getUserGroups.fromJSONArray(userGroupsArr);

                        requestCallback.onDataChunk(
                                userGroupsChunk.get("userGroups"),
                                userGroupsChunk.get("users"),
                                userGroupsChunk.get("groups"),
                                totalCount
                        );
                    }

                    if (data.getInt("nextId") > -1) {
                        getUserGroups(data.getInt("nextId"), limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getUserGroups(fromId, limit, lastModifiedDate, userId, groupIds, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    public void getImages(final int fromId, final int limit, final Date lastModifiedDate, final List<Integer> missionIds, final RequestCallback requestCallback) {
        mClient.addHeader("Authorization", "Bearer " + getJwt());
        RequestParams params = new RequestParams();
        params.put("lifecyclephase", 1);
        params.put("fromid", fromId);
        params.put("limit", limit);

        if (lastModifiedDate != null) {
            DateFormat df = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
            String lastModifiedStr = df.format(lastModifiedDate);
            params.put("lastmodified", lastModifiedStr);
        }
        if (missionIds != null && missionIds.size() > 0) {
            int i = 0;
            for (Integer missionId : missionIds) {
                params.put("missionids[" + i + "]", missionId);
                i++;
                Log.d(TAG, "getImages: missionId: " + missionId);
            }
        }

        mClient.get(getAbsoluteUrl("/images"), params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    JSONObject response = new JSONObject(responseString);
                    JSONObject data = response.getJSONObject("data");
                    JSONArray imagesArr = data.getJSONArray("images");
                    int totalCount = data.getInt("count");

                    if (imagesArr.length() > 0) {
                        ImageConverter getImages = new ImageConverter();
                        List<Image> imagesChunk = getImages.fromJSONArray(imagesArr);

                        requestCallback.onDataChunk(
                                imagesChunk,
                                totalCount
                        );
                    }

                    if (data.getInt("nextId") > -1) {
                        getImages(data.getInt("nextId"), limit, lastModifiedDate, missionIds, requestCallback);
                    } else {
                        requestCallback.onDone();
                    }
                } catch (JSONException e) {
                    if (statusCode == 200 && Objects.equals(e.getMessage(), "Value <br of type java.lang.String cannot be converted to JSONObject")) { // Server sometimes sends error due to overloading, send request again
                        Log.w(TAG, "Sending request again after exception: " + e.getMessage());
                        getImages(fromId, limit, lastModifiedDate, missionIds, requestCallback);
                    } else {
                        e.printStackTrace();
                        requestCallback.onError(999, e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                requestCallback.onError(statusCode, throwable.getMessage());
            }
        });
    }

    private String getJwt() {
//        Algorithm algorithm = Algorithm.HMAC256(BuildConfig.DB_SYNC_API_SECRET);
//        String token = JWT.create()
//                // .withIssuer("auth0")
//                .sign(algorithm);

        SecretKey key = Keys.hmacShaKeyFor(BuildConfig.DB_SYNC_API_SECRET.getBytes(StandardCharsets.UTF_8));
        String token = Jwts.builder().setPayload("{}").signWith(key).compact();
        return token;
    }

    private void setConflictToSharedPreferences(String geodeticPointLabel, String taskName) {
        // Set the geodetic point label and task name of conflict to shared preferences
        String geodeticPointTaskMix = geodeticPointLabel + " - " + taskName;
        Set<String> conflictGeodeticPointTaskMixes = mPreferences.getStringSet(Constants.SHARED_PREF_CONFLICTS_KEY, new HashSet<String>());
        conflictGeodeticPointTaskMixes.add(geodeticPointTaskMix);
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putStringSet(Constants.SHARED_PREF_CONFLICTS_KEY, conflictGeodeticPointTaskMixes);
        preferencesEditor.apply();
    }

    private String jsonArrayToString(JSONArray errorResponse){
        StringBuilder errorStr = new StringBuilder();
        for (int i = 0; i < errorResponse.length(); i++) {
            try {
                JSONObject json = (JSONObject) errorResponse.get(i);
                errorStr.append(json.toString()).append("\n");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return errorStr.toString();
    }
}
