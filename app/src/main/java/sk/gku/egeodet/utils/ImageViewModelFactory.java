package sk.gku.egeodet.utils;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import sk.gku.egeodet.viewmodels.ImageViewModel;

public class ImageViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private int mMissionId;
    private int mGeodeticPointId;

    public ImageViewModelFactory(Application application, int missionId, int geodeticPointId) {
        mApplication = application;
        mMissionId = missionId;
        mGeodeticPointId = geodeticPointId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new ImageViewModel(mApplication, mMissionId, mGeodeticPointId);
    }
}
